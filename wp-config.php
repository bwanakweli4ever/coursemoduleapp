<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'coursemodule_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ZM(UOg~B^y:@2<A;oPEp4K0q!vUC>|C<j:5IYZDe>o05_1TzN|vm,O9**qltTqm:' );
define( 'SECURE_AUTH_KEY',  'cojU%hS#yv.[D[Xd!pLH0PRE#!>G&: Ib#OUA4{7I2GBf$fJE&C.s_3CtL /h9{r' );
define( 'LOGGED_IN_KEY',    'uuD4jB-8@4LX/@AW-jn{mYV*!t,t$P:a%cp85X_9`52=O+b5_]$C-+L<8~`&X.Pq' );
define( 'NONCE_KEY',        '=[</- Xwx0IK0Y@v>^.R,eV@kNWg#/@a-Xb;%QtjOB}xX,iahj).MyJk2F#e`7E*' );
define( 'AUTH_SALT',        'oA_l?~2xwK8*@sBeSAu|oQ.CSirjB${z,$!.uVck5we=$nhXMi6 8+] !p)s; *z' );
define( 'SECURE_AUTH_SALT', 'gDtL+|U33riR.MGx)/:w|A|A(hMDJ*m<S*o&C!4a@cv5PakpBj4Z#e3GmE$_Bvgl' );
define( 'LOGGED_IN_SALT',   '[L#+s!i<j:;%an?72|h]F--7P(|SikF|rU=2TU65TCdo-uK=o^rcZ=:X|O9=zE8T' );
define( 'NONCE_SALT',       'V]}.C<m&igy(4`:;;2^Hn9sT/PmtLdxVN_(/Ggi7KI<{V/?2Fxxq3Ay5]S.X`j`-' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
