<?php
 
add_theme_support( 'editor-styles' );    //enable custom styleet for WordPress editors
add_editor_style( 'style-editor.css' );  //load the CSS file from your template directory called style-editor.css
add_theme_support( 'align-wide' );       //optional, adds support for full and wide blocks
add_theme_support( 'wp-block-styles' );



//======================================================================
// custom post type Function Start Here
//======================================================================

require get_template_directory() .'/includes/video-custom-post-type.php';

 
 add_action( 'init', 'video_custom_post_type', 0 );

 


//======================================================================
// custom post type Function end
//======================================================================

  // Tell SearchWP to index term names from ACF (Advanced Custom Fields) Taxonomy fields.
add_filter( 'searchwp_custom_fields', function( $meta_value, $meta_key, $the_post ) {
    $acf_taxonomy_field_names = array( 'to_dos' );

    if ( ! in_array( $meta_key, $acf_taxonomy_field_names ) ) {
        return $meta_value;
    }

    // Retrieve the value as per ACF's field settings.
    $acf_field_object = get_field_object( $meta_key, $the_post->ID );

    // We want to index the chosen terms names.
    $meta_value = wp_list_pluck( $acf_field_object['value'], 'name' );

    return $meta_value;
}, 20, 3 );


 //======================================================================
// Theme Menus Function start Here
//======================================================================

function videotheme_menus() {

    $locations = array(
        'primary'  => __( 'Desktop Left side Vertical Menu', 'videomodule' ),
        'secondary' => __( 'Desktop Secondary Menu', 'videomodule' ),  
    );

    register_nav_menus( $locations );
}

add_action( 'init', 'videotheme_menus' );

 //======================================================================
// Theme Menus Function End
//======================================================================



 //======================================================================
 // Theme custom logo Function 
//======================================================================

function videocourse_custom_logo_setup() {
 $defaults = array(
 'height'      => 100,
 'width'       => 400,
 'flex-height' => true,
 'flex-width'  => true,
 'header-text' => array( 'site-title', 'site-description' ),
 );
 add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'videocourse_custom_logo_setup' );

//======================================================================
// Theme custom logo Function  End
//======================================================================


//======================================================================
// CFC Function start
//======================================================================
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
    'key' => 'group_5e97ae3f3e27f',
    'title' => 'Video',
    'fields' => array(
        array(
            'key' => 'field_5e97ae5a532d7',
            'label' => 'Video Title',
            'name' => 'video_title',
            'type' => 'text',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => 'Video Title',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array(
            'key' => 'field_5e97ae87532d8',
            'label' => 'Video Link',
            'name' => 'video_link',
            'type' => 'url',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
        ),
        array(
            'key' => 'field_5e97aed1532d9',
            'label' => 'Short Description',
            'name' => 'short_description',
            'type' => 'textarea',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => 'Brief description about   this particular video',
            'maxlength' => '',
            'rows' => '',
            'new_lines' => '',
        ),
        array(
            'key' => 'field_5e9c7221c0dd3',
            'label' => 'Video Length',
            'name' => 'video_length_',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array(
            'key' => 'field_5e9f67bdac35a',
            'label' => '',
            'name' => 'mark_as_watched',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => 'pending',
            'placeholder' => '',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
        array(
            'key' => 'field_5eac8d7f4065c',
            'label' => 'To Dos',
            'name' => 'to_dos',
            'type' => 'taxonomy',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array(
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'taxonomy' => 'Tasks',
            'field_type' => 'multi_select',
            'allow_null' => 0,
            'add_term' => 1,
            'save_terms' => 0,
            'load_terms' => 0,
            'return_format' => 'id',
            'multiple' => 0,
        ),
    ),
    'location' => array(
        array(
            array(
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'video',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'seamless',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
));

endif;
//======================================================================
// ACF Function  End
//======================================================================



//======================================================================
// TASK ACF Function  Start
//======================================================================
// 
//======================================================================
// TASK ACF Function  End
//======================================================================

//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_Tasks_hierarchical_taxonomy', 0 );
 
//create a custom taxonomy name it Tasks for your posts
 
function create_Tasks_hierarchical_taxonomy() {
 
// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI
 
  $labels = array(
    'name' => _x( 'Tasks', 'taxonomy general name' ),
    'singular_name' => _x( 'Task', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Tasks' ),
    'all_items' => __( 'All Tasks' ),
    'parent_item' => __( 'Parent video' ),
    'parent_item_colon' => __( 'Parent Topic:' ),
    'edit_item' => __( 'Edit Topic' ), 
    'update_item' => __( 'Update Task' ),
    'add_new_item' => __( 'Add New Task' ),
    'new_item_name' => __( 'New Task Name' ),
    'menu_name' => __( 'Tasks' ),
  );    
 
// Now register the taxonomy
 
  register_taxonomy('Tasks',array('video'), 
    array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'task' ),
  ));
 
}





function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/style-login.css' );
    wp_enqueue_script( 'custom-login', get_stylesheet_directory_uri() . '/style-login.js' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

//Login functions

function sp_display_login_form() {

  $args = array(
    'redirect' => esc_url($_SERVER['REQUEST_URI']), 
    'id_username' => 'user',
    'id_password' => 'pass',
    'form_id' => 'sp-loginform',
    'label_username' => __( 'Email Address' ),
    'label_password' => __( 'Password' ),
    //'label_remember' => __( 'Angemeldet bleiben' ),
    //'label_log_in' => __( 'Anmelden' ),
    'remember' => true
  );
  wp_login_form( $args );
  echo '<a title="Click here if you lost your password" class="lost-password" href="' . wp_lostpassword_url() . '">Lost password?</a>';
}


// set new login url to /login
function redirect_login_page() {
  $login_page  = home_url( '/login/' );
  $page_viewed = basename($_SERVER['REQUEST_URI']);
 
  if( $page_viewed == "wp-login.php" && $_SERVER['REQUEST_METHOD'] == 'GET') {
    wp_redirect($login_page);
    exit;
  }
}
add_action('init','redirect_login_page');






// all the following functions help to transfer all functions to the new URL (e.g. if someone entered a wrong password)
function login_failed() {
  $referrer =  strtok($_SERVER['HTTP_REFERER'], '?');  // where did the post submission come from?
  // if there's a valid referrer, and it's not the default log-in screen
  if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
    wp_redirect( $referrer . '?login=failed' );  // let's append some information (login=failed) to the URL for the theme to use
    exit;
  }
}
add_action( 'wp_login_failed', 'login_failed' );





function verify_username_password( $user, $username, $password ) {
  if(isset($_SERVER['HTTP_REFERER'])) {
    $referrer =  strtok($_SERVER['HTTP_REFERER'], '?');  // where did the post submission come from?
  }
  // if there's a valid referrer, and it's not the default log-in screen
  if ( !empty($referrer) && !strstr($referrer,'wp-login') && !strstr($referrer,'wp-admin') ) {
    if( $username == "" || $password == "" ) {
         wp_redirect( $referrer . '?login=empty' );  // let's append some information (login=failed) to the URL for the theme to use
        exit;
    }
  }
}
add_filter( 'authenticate', 'verify_username_password', 1, 3);



add_action('wp_ajax_nopriv_navigate', 'navigate_videos_ajax');
add_action('wp_ajax_navigate', 'navigate_videos_ajax');
function navigate_videos_ajax(){
echo $GET['slag'];
exit();
}


