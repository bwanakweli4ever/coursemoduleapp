<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit; // Silence is golden!
}



?>


<html lang="en" <?php language_attributes(); ?>>
<head>
  <meta <?php bloginfo( 'charset' ); ?>>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="<?php bloginfo( 'description' ); ?>">
  <title> <?php bloginfo( 'name' ); ?></title>
 
  <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css">
    <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/favicon.png" type="image/x-icon">
  <script src="<?php bloginfo('template_url'); ?>/js/jquery-3.2.1.min.js"></script>
  <script src="https://kit.fontawesome.com/e7fbc5e324.js"></script>
</head>

 <body class="has-left-bar has-top-bar" <?php body_class(); ?>>


<!--This is the fixed menu-->

<?php get_template_part( 'template-parts/fix-nav' ); ?>
<!--This is mobile menu-------------------------------------------------------------------------------->

<?php get_template_part( 'template-parts/mobile-nav' ); ?>

<!-------upper header--------------------------------------------------------------------------->

<?php get_template_part( 'template-parts/upper-header' ); ?>

  