<?php


function Task_custom_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Task', 'Post Type General Name', 'Tasks' ),
        'singular_name'       => _x( 'Task', 'Post Type Singular Name', 'Tasks' ),
        'menu_name'           => __( 'Task', 'Tasks' ),
        'parent_item_colon'   => __( 'Parent Course Module', 'Tasks' ),
        'all_items'           => __( 'All Task', 'Tasks' ),
        'view_item'           => __( 'View Task', 'Tasks' ),
        'add_new_item'        => __( 'Add New Task', 'Tasks' ),
        'add_new'             => __( 'Add New', 'Tasks' ),
        'edit_item'           => __( 'Edit Task', 'Tasks' ),
        'update_item'         => __( 'Update Task', 'Tasks' ),
        'search_items'        => __( 'Search Task', 'Tasks' ),
        'not_found'           => __( 'Not Found', 'Tasks' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'Tasks' ),

    );
     
      add_theme_support('post-thumbnails');
add_post_type_support( 'Course', 'thumbnail' ); 
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Task', 'Tasks' ),
        'menu_icon'           => 'dashicons-portfolio',
        'description'         => __( 'Task news and reviews', 'Tasks' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions','custom-fields'),
         'taxonomies'          => array( 'videos' ),
        // You can associate this CPT with a taxonomy or custom taxonomy. 
      
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 4,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        'show_in_rest' => true,
        // This is where we add taxonomies to our CPT
      
 
    );

    
     
     add_filter('pre_get_posts', 'Task_query_post_type');
    function Task_query_post_type($query) {
  if( is_category() ) {
    $post_type = get_query_var('post_type');
    if($post_type)
        $post_type = $post_type;
    else
        $post_type = array('nav_menu_item', 'post', 'Tasks'); 
    $query->set('post_type',$post_type);
    return $query;
    }
}

    // Registering your Custom Post Type
    register_post_type( 'Task', $args );
 
}



