<?php


function video_custom_post_type() {
 
// Set UI labels for Custom Post Type
    $labels = array(
        'name'                => _x( 'Video', 'Post Type General Name', 'videomodule' ),
        'singular_name'       => _x( 'Video', 'Post Type Singular Name', 'videomodule' ),
        'menu_name'           => __( 'Video', 'videomodule' ),
        'parent_item_colon'   => __( 'Parent Course Module', 'videomodule' ),
        'all_items'           => __( 'All Video', 'videomodule' ),
        'view_item'           => __( 'View Video', 'videomodule' ),
        'add_new_item'        => __( 'Add New Video', 'videomodule' ),
        'add_new'             => __( 'Add New', 'videomodule' ),
        'edit_item'           => __( 'Edit Video', 'videomodule' ),
        'update_item'         => __( 'Update Video', 'videomodule' ),
        'search_items'        => __( 'Search Video', 'videomodule' ),
        'not_found'           => __( 'Not Found', 'videomodule' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'videomodule' ),

    );
     
add_theme_support('post-thumbnails');
add_post_type_support( 'Course', 'thumbnail' ); 
// Set other options for Custom Post Type
     
    $args = array(
        'label'               => __( 'Video', 'videomodule' ),
        'menu_icon'           => 'dashicons-portfolio',
        'description'         => __( 'Video news and reviews', 'videomodule' ),
        'labels'              => $labels,
        // Features this CPT supports in Post Editor
        'supports'            => array( 'id','title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'video','custom-fields'),

        // You can associate this CPT with a taxonomy or custom taxonomy. 
       'taxonomies'          => array( 'category' ),
        /* A hierarchical CPT is like Pages and can have
        * Parent and child items. A non-hierarchical CPT
        * is like Posts.
        */ 
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 3,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'show_in_rest' => true,
        // This is where we add taxonomies to our CPT
      
 
    );
     
     add_filter('pre_get_posts', 'query_post_type');
    function query_post_type($query) {
  if( is_category() ) {
    $post_type = get_query_var('post_type');
    if($post_type)
        $post_type = $post_type;
    else
        $post_type = array('nav_menu_item', 'post', 'videomodule'); // don't forget nav_menu_item to allow menus to work!
    $query->set('post_type',$post_type);
    return $query;
    }
}

    // Registering your Custom Post Type
    register_post_type( 'Video', $args );
 
}



