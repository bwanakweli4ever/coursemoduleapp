<?php

    $parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
    require_once( $parse_uri[0] . 'wp-load.php' );
//======================================================================
// Capture video ID AND category
//======================================================================

     $id=$_GET['id'];
     $cat=$_GET['cat'];
     $user_id = wp_get_current_user(); 
//======================================================================
// Capture video ID AND category END
//======================================================================
    


//======================================================================
// Capture video ID AND category
//======================================================================  
      $args = array(
      'tax_query' => array(
      array(
      'taxonomy' => 'category',
      'field' => 'slug',
      'terms' => array($cat)
      ),
      ),
      'post_type' => 'video',
       'orderby' => 'menu_order',
        'order' => 'ASC'
    );

      $video_post = new WP_Query($args);
      if($video_post->have_posts()) {


     ?>

<!-- //======================================================================
// Capture video ID AND category
//====================================================================== -->
        <style>
        .video-wrapper{
        position: relative;
        padding-bottom: 56.25%;

        }
        .video-wrapper iframe{
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        }

        </style>

<div class="video-wrapper">
<iframe src="<?php echo $video_post->posts[$id]->video_link;?>?title=0&byline=0&portrait=0" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<script src="https://player.vimeo.com/api/player.js"></script>

    </div>
   </div>
      <div class="video-bt-section">
      <div class="video-content">
      <div class="video-btn">
      <ul>
      <li>
      <a href="#"><span>Nachstes Video</span><img src="<?php bloginfo('template_url'); ?>/images/icons/arrow.png" /></a>
      </li>
      </ul>
    </div>
      <h1><?php echo  $video_post->posts[$id]->post_name;?> </h1>
      <p> <?php echo  $video_post->posts[$id]->short_description;?> </p>
      <?php
      $videoId=$video_post->posts[$id]->ID;
      $is_video_whatchedby_this_user=get_user_meta($user_id->ID, $videoId,'watched');
      if($is_video_whatchedby_this_user=="watched"){
        $watched="watched";
      }
      else{
        $watched="pending";
      }

      if($watched=="watched"){
      $video_btn_class="btn_watched";

      }
      else{
      $video_btn_class="btn1";

      }
      ?>


  <button class="iswatched <?php echo $video_btn_class;?>" id="iswatched-<?php echo $videoId;?>"><i class="fas fa-check"></i>AIs erledigt markieren </button>
  <div class="footer-btn">
  <label>To-Do</label>
  
    <?php $tasks=$video_post->posts[$id]->to_dos; 
    if ( ! empty( $tasks ) && is_array( $tasks ) ) {
    foreach ($tasks as $task) :
    # code...
     ?>


 <?php
 $check_completion_status=get_user_meta($user_id->ID, $task,'completed');
 // print_r($check_completion_status);
 if( $check_completion_status=="" || $check_completion_status=="pending" ){
   $completion_status="pending";
     $btn_class="btn1-full";
      }
 else{
   $completion_status="completed"; 
   $btn_class="btn-colored";
 }


?>
<?php $showterm_name = $wpdb->get_var("SELECT name FROM $wpdb->terms WHERE term_id=$task");?>
<div class="completion"><button class="status-btn <?php echo $btn_class;?> btn-pressed-<?php echo $task;?>"><i class="fas fa-check"></i><?php echo $showterm_name; ?></button></div>





    <script type="text/javascript">
    $(document).ready(function() {
    $(".btn-pressed-<?php echo $task;?>").click(function(data,status){
    var tr_url="<?php bloginfo('template_url'); ?>/includes/ajax-mark-complete.php";
    var taskval = "<?php echo get_user_meta($user_id->ID, $task,'completed');?>";// video ID to be marked complete/uncomplete
     var tsk_id = "<?php echo $task;?>";

    console.log(taskval);// current status from DB
    $.ajax({
    type: "POST",
    url: tr_url,
    data: {tsk_id:tsk_id,val:taskval,"task_completed":'1'},
    dataType: "JSON",
    success: function(status) {
    console.log(status); // ajax returned status //
    if(status=="completed"){
    $(".btn-pressed-<?php echo $task;?>").addClass("btn-colored");

    }
    else{
    $(".btn-pressed-<?php echo $task;?>").removeClass("btn-colored");
    $("btn-pressed-<?php echo $task;?>").addClass("btn1-full");

    }},
    error: function(err) {

    //error log script 
    }
    });
    });
    });
    </script>
<?php  endforeach;
}
?>






    <script type="text/javascript">
   $(document).ready(function() {
    var r_url="<?php bloginfo('template_url'); ?>/includes/ajax-mark-complete.php";
    var videoId = "<?php echo $videoId;?>";// video ID to be marked complete/uncomplete
    var is_videoW="<?php echo $watched;?>";
    console.log(videoId);
    console.log(is_videoW);
    $(".iswatched").click(function(data,status){
    $.ajax({
    type: "POST",
    url: r_url,
    data: {v_id:videoId,val:is_videoW,"video-watched":'1'},
    dataType: "JSON",
    success: function(status) {
    console.log(status);
    if(status=="watched"){
    $(".iswatched").addClass("btn-colored");

    }
    else{
  $(".iswatched").removeClass("btn-colored");
    $(".iswatched").addClass("btn1");

    }     },
    error: function(err) {

    //error log script 
    }
    });
    });
 });


</script>



</div>

<?php } ?>
