<?php
 
/**
Template Name: Register
 */
 get_header(); ?>



<?php defined( 'ABSPATH' ) || exit; ?>
<?php  do_action( 'woocommerce_before_edit_account_form' ); ?>
<div class="content-wrapper">
      <div class="register-section">
    <form class="woocommerce-EditAccountForm edit-account" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?>>
       <?php do_action( 'woocommerce_edit_account_form_start' ); ?>
        <div class="left-sect">
          <label>Name</label>
          <input type="Text" class="woocommerce-Input woocommerce-Input--text input-text" name="account_first_name" id="account_first_name"  value="<?php echo esc_attr( $user->first_name ); ?>">
          <label>E-Mail</label>
           <input type="email" class="woocommerce-Input woocommerce-Input--email input-text" name="account_email" id="account_email" autocomplete="email" value="<?php echo esc_attr( $user->user_email ); ?>" />
        </div>
        <div class="v-line"></div>
        <div class="right-sect">
          <label>Aktuelles Passwort</label>
          <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_current" id="password_current" placeholder="Enter your current password..." /> 
          <label>Neues Passwort</label>
           <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_1" id="password_1" placeholder="Enter your new password..." /> 
          <label>Passwort wiederholen</label>
          <input type="password" class="woocommerce-Input woocommerce-Input--password input-text" name="password_2" id="password_2" placeholder="Confirm your new password..." /> 
        </div>
        <?php do_action( 'woocommerce_edit_account_form' ); ?>
        
           <button type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Speichern', 'woocommerce' ); ?></button>
        <div class="register-footer">
           <input type="hidden" name="action" value="save_account_details" />
          <h2>Deine Mitgliedschaft ist gebucht bis zum</h2>
          <?php do_action( 'woocommerce_edit_account_form_end' ); ?>
            <span class="st-date">25.04.2020</span>
            <span class="st-text">
              Nach Ablauf der Frist hast du weiterhin sauerhaften Zugriff auf das Videotraining und die
              Deinebibliothek inklusive aller Updates. Um auch der Frist an den Live Calls teillnehmen zu
              konnen und Zugriff auf die Slack Gruppe zu behalten, kannst du deine Mitgliedschaft
              jederzeit verlangern.
            </span>
        </div>
        <?php do_action( 'woocommerce_after_edit_account_form' ); ?>
    </form>
    
  </div>
</div>
</div>
<?php get_footer();