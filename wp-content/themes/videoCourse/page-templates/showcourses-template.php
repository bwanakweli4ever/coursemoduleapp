 <?php 
/**
Template Name: Show Courses
 */
if ( !is_user_logged_in() ) {
    auth_redirect();
} 


 get_header(); ?>

<style>
.training-container{
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin-top: -30px;
  z-index: 1000;
}

.training-video{
  display: flex;
  justify-content: center;
}
.training-video img {
  width: 40%;
  z-index: 1000;

}
.training-status{
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding-top: 40px;
}
/*Progressive bars section*/

 .circle_percent {
    font-size:80px;
     width:1em;
     height:1em;
     position: relative;
     background: #d9d9d9;
     border-radius:50%;
     overflow:hidden;
     display:inline-block;
     margin:20px;
}
 .circle_inner {
    position: absolute;
     left: 0;
     top: 0;
     width: 1em;
     height: 1em;
     clip:rect(0 1em 1em .5em);
}
 .round_per {
    position: absolute;
     left: 0;
     top: 0;
     width: 1em;
     height: 1em;
     background: #2ecc71;
     clip:rect(0 1em 1em .5em);
     transform:rotate(180deg);
     transition:1.05s;
}
 .percent_more .circle_inner {
    clip:rect(0 .5em 1em 0em);
}
 .percent_more:after {
    position: absolute;
     left: .5em;
     top:0em;
     right: 0;
     bottom: 0;
     background: #2ecc71;
     content:'';
}
 .circle_inbox {
  position: absolute;
  top: 6px;
  left: 6px;
  right: 10px;
  bottom: 10px;
  background: #fff;
  z-index: 3;
  border-radius: 50%;
  width: 0.85em;
  height: 0.85em;
}
 .percent_text {
    position: absolute;
     font-size: 15px;
     font-weight: 700;
     color: #a6a6a6;
     left: 50%;
     top: 50%;
     transform: translate(-50%,-50%);
     z-index: 3;
}
.box{
  display: flex;
  align-items: center;
}
.box::after{
  content: "";
  background-color: #2ecc71;
  height: 40px;
  width: 2px;
  display: flex;
  /* flex-direction: column; */
  position: absolute;
  top: 110px;
  left: 50%;
  margin-left: -140px;
  z-index: 1000;
}

.box-final{
  display: flex;
  align-items: center;
}

.box-final .text{
  font-size: 18px;
  color: #7f7f7f;
  font-weight: 600;
}

.box-final::after{
  content: "";
  background-color: #2ecc71;
  display: flex;
  /* flex-direction: column; */
  position: absolute;
  top: 110px;
  left: 50%;
  margin-left: -140px;
  z-index: 1000;
}
.not-active::after{
  content: "";
  background-color: #d9d9d9 !important;
  height: 40px;
  width: 2px;
  display: flex;
  /* flex-direction: column; */
  position: absolute;
  top: 110px;
  left: 50%;
  margin-left: -140px;
  z-index: 1000;
}
.progress-bar:nth-child(5) .box::after {
  height: 0px;
  width: 0px;
}
.progress-bar{
  position: relative;
  min-width: 400px;
  min-height: 140px;
}
.progress-bar svg{
  width: 15px;
  height: 15px;
  margin-left: 70px;
}
.box .text{
  font-size: 18px;
  color: #7f7f7f;
  font-weight: 600;
}
.video-wrapper{
  position: relative;
  padding-bottom: 25.25%;
  z-index: 1000;
  justify-content: center;
  display: flex;

}  
.video-wrapper iframe{
  position: absolute;
  width: 45%;
  height: 100%;

}


</style>  

    <div class="training-container">
        <div class="video-wrapper">
        <iframe src="https://player.vimeo.com/video/139703445?title=0&byline=0&portrait=0" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
        <script src="https://player.vimeo.com/api/player.js"></script>
        </div>
      <div class="training-status">
        

      <?php 
      $terms = get_terms(
      array(
      'taxonomy'   => 'category',
      'exclude' => 1,
      'orderby' => 'ID',
      'hide_empty' => false,
      'hierarchical'=>1

      )
      );

// Check if any term exists
if ( ! empty( $terms ) && is_array( $terms ) ) {
  $i = 0;
  $len = count($terms);
    // Run a assigned_task and print them all
  $count = 1;
    foreach ( $terms as $term ) { 

     if ($i == $len - 1) {
      ?>

<?php  $counted_videos = $term->count; 

?>




        <!--  view-complete-course is a name of course module page make sure to use same name if you change the name of the page -->
        <a href="<?php echo get_site_url() ; ?>/index.php/view-complete-course/?cat=<?php echo $term->name; ?>">

          <div class="progress-bar">
          <div class="box-final">

            <div class="circle_percent" data-percent="<?php echo $counted_videos;?>">
              <div class="circle_inner">
                  <div class="round_per"></div>
                </div>
            </div>
            <span class="text"> <?php echo   $count; ?> <?php echo  $term->name; ?> 
          </span>

          </div>
       </a>
     </div>
    <?php
    }else{ ?>

 <!--  view-complete-course is a name of course module page make sure to use same name if you change the name of the page -->
  <a href="<?php echo get_site_url() ; ?>/index.php/view-complete-course/?cat=<?php echo $term->name; ?>">
        <?php 

global $wpdb;
// Get the count
$counted_videos2 = $term->count;
//echo $term->term_id;

$task_args = array(
'tax_query' => array(
array(
'taxonomy' => 'category',
'field' => 'slug',
'terms' => array($term->name),),),
'post_type' => 'video'
);


$assigned_task = new WP_Query($task_args);

if($assigned_task->have_posts()) {
$i=0;
$tsk_term = $wp_query->queried_object;

while($assigned_task->have_posts()) : 
  $assigned_task->the_post();


$post_id=$assigned_task->posts[$i]->ID;
$i+=1;
$countterms2 = $wpdb->get_var("SELECT * FROM $wpdb->postmeta WHERE post_id='$post_id' 
  AND meta_key='to_dos'");

echo var_dump($countterms2) .'<br>';

endwhile;
}
?>
          <div class="progress-bar">
          <div class="box">
            <div class="circle_percent" data-percent="6">
              <div class="circle_inner">
                  <div class="round_per"></div>
                </div>
            </div>

            <span class="text"> <?php echo   $count; ?> <?php echo  $term->name ?> </span>

          </div>
          <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30"><title>live-call-icon</title><path d="M2,30V21.67s.8-1.71,5.14-4c4.12-2.19,6.83-1.93,9.92-1.87,2.34.05,11,2.9,11,5S28.1,30,28.1,30Z" style="fill:#d9d9d9"/><path d="M15.66,10.87A4.9,4.9,0,0,1,20,9.75a6.5,6.5,0,0,0,.29-1.89,6.31,6.31,0,1,0-6.57,6.29A7.38,7.38,0,0,1,15.66,10.87Z" style="fill:#d9d9d9"/><path d="M18.52,12.91a3.59,3.59,0,0,0,2.87-2.5H21V6.64h.35c0-.17,0-.32-.06-.48a6.27,6.27,0,0,0-.9-2.29,6.69,6.69,0,0,0-3.6-2.72A9.48,9.48,0,0,0,14.36.7a9.83,9.83,0,0,0-2.6.21A7.23,7.23,0,0,0,8,3,6.1,6.1,0,0,0,6.45,6c0,.22-.09.44-.13.68h.6V10.4H5.81a.89.89,0,0,1-.94-.87c0-.69,0-1.38,0-2.08a.78.78,0,0,1,.58-.74.17.17,0,0,0,.12-.15,7.5,7.5,0,0,1,.84-2.67A7,7,0,0,1,9.55,1,9.16,9.16,0,0,1,13.31,0a9.6,9.6,0,0,1,5.16,1.17,6.87,6.87,0,0,1,3.26,3.69,6.74,6.74,0,0,1,.36,1.66s0,.07,0,.1a1.14,1.14,0,0,1,.63.22.79.79,0,0,1,.32.64v2a.87.87,0,0,1-.82.94.09.09,0,0,0-.09.09,3.63,3.63,0,0,1-1.33,2.09,5.06,5.06,0,0,1-2.21,1,.19.19,0,0,0-.15.12.68.68,0,0,1-.71.43c-.61,0-1.21,0-1.82,0a.63.63,0,0,1-.54-.26,1.13,1.13,0,0,1,0-1.3.61.61,0,0,1,.52-.24h1.95a.58.58,0,0,1,.53.31C18.44,12.75,18.47,12.83,18.52,12.91Z" style="fill:#d9d9d9"/></svg>
       </a>
     </div>

<?php

  }
    $i++;
     $count++;  
   }
}  
?>

      </div>
<!--this closes my main wrapper/div-->
</div>
<?php get_footer(); ?>
