<?php

/**
Template Name: List Videos
 */
 ?>
 <?php get_header(); ?>

        

        <style type="text/css">

        .last-option{
        line-height: 2.5;
        left: 0;
        }

        .last-option:after{
        content: "";
        background-color: #1097c5;
        height: 23px;
        width: 1px;
        display: flex;
        /* flex-direction: column; */
        position: absolute;
        top: 247px;
        /* left: 20%; */
        margin-left: 5px;
        z-index: 1000;
        }

        .last-option span{
        list-style-type: none;
        font-size: 16px;
        color: #5d5d5d;
        font-weight: 600;
        margin-left: 28px;
        }


        .bottom_last_option{
        line-height: 2.5;
        left: 0;
        }
        .bottom_last_option ul li{

        color: #606060;
        }
        .bottom_last_option:before {
        content: "";
        background-color: #1097c5;
        height: 20px;
        width: 1px;
        display: flex;
        flex-direction: column;
        position: relative;
        /* left: 20%; */
        margin-left: 5px;
        z-index: 1000;
        }

        .bottom_last_option span{
        list-style-type: none;
        font-size: 16px;
        color: #787878;
        font-weight: 600;
        margin-left: 28px;
        }
        </style>


        <div class="content-wrapper">
        <div class="data-container">
        <div class="left-box">
        <ul class="progress-bubble">
        <?php
//======================================================================
// Capture Video category/Module ID from Training Page
//======================================================================  
        $cat=$_GET['cat'];

//======================================================================
// Load all custom post type(video) and specified category
//======================================================================  
        $args = array(
        'tax_query' => array(
        array(
        'taxonomy' => 'category',
        'field' => 'slug',
        'hierarchical'=> true,
        'terms' => array($cat) // the selected category 
        ),
        ),
        'post_type' => 'video',
        'orderby' => 'menu_order',
        'order' => 'ASC'
        );

        $post_idx=0;  //indexing each video & this helps us to  display the default video 

        $loop = new WP_Query($args);

        if($loop->have_posts()) {

        $term = $wp_query->queried_object;

        while($loop->have_posts()) : $loop->the_post();
        //Show list of videos
        echo"<li value=".$post_idx." class='active listItem'>
        <a href='#?slug=$post_idx'>".get_the_title().'<p>'.get_post_meta($post->ID, 'video_length_', true).'</p>'.'</a> 
        </li>'; 

        $post_idx++;

        endwhile;

        //display live call at the end of all videos

        if ($post_idx >= 0) {
        ?>

        <div class="last-option">
        <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" style="width: 15px;margin-left: -2px;"><title>usericons</title><path d="M2,30V21.67s.8-1.71,5.14-4c4.12-2.19,6.83-1.93,9.92-1.87,2.34.05,11,2.9,11,5S28.1,30,28.1,30Z" style="fill:#606060"/><path d="M15.66,10.87A4.9,4.9,0,0,1,20,9.75a6.5,6.5,0,0,0,.29-1.89,6.31,6.31,0,1,0-6.57,6.29A7.38,7.38,0,0,1,15.66,10.87Z" style="fill:#606060"/><path d="M18.52,12.91a3.59,3.59,0,0,0,2.87-2.5H21V6.64h.35c0-.17,0-.32-.06-.48a6.27,6.27,0,0,0-.9-2.29,6.69,6.69,0,0,0-3.6-2.72A9.48,9.48,0,0,0,14.36.7a9.83,9.83,0,0,0-2.6.21A7.23,7.23,0,0,0,8,3,6.1,6.1,0,0,0,6.45,6c0,.22-.09.44-.13.68h.6V10.4H5.81a.89.89,0,0,1-.94-.87c0-.69,0-1.38,0-2.08a.78.78,0,0,1,.58-.74.17.17,0,0,0,.12-.15,7.5,7.5,0,0,1,.84-2.67A7,7,0,0,1,9.55,1,9.16,9.16,0,0,1,13.31,0a9.6,9.6,0,0,1,5.16,1.17,6.87,6.87,0,0,1,3.26,3.69,6.74,6.74,0,0,1,.36,1.66s0,.07,0,.1a1.14,1.14,0,0,1,.63.22.79.79,0,0,1,.32.64v2a.87.87,0,0,1-.82.94.09.09,0,0,0-.09.09,3.63,3.63,0,0,1-1.33,2.09,5.06,5.06,0,0,1-2.21,1,.19.19,0,0,0-.15.12.68.68,0,0,1-.71.43c-.61,0-1.21,0-1.82,0a.63.63,0,0,1-.54-.26,1.13,1.13,0,0,1,0-1.3.61.61,0,0,1,.52-.24h1.95a.58.58,0,0,1,.53.31C18.44,12.75,18.47,12.83,18.52,12.91Z" style="fill:#606060"/></svg>
        <span>Live call</span>
        </div>

          <?php

          }
          }

          ?>

        </ul>
    </div>

      <div class="right-box">
      <div class="video-section">
      <div class="vimeo-video"></div>  <!-- Showing video,mark as complete and Task from Ajax call -->
      </div>
      </div>
      </div>
      </div>

    <div class="bottom-left-box">
        <ul class="bottom-progress-bubble">

            <?php
            $cat=$_GET['cat'];
            $args = array(
            'tax_query' => array(
            array(
            'taxonomy' => 'category',
            'field' => 'slug',
            'terms' => array($cat)
            ),
            ),
            'post_type' => 'video'
            );

            $post_idx=0;

            $loop = new WP_Query($args);

            if($loop->have_posts()) {

            $term = $wp_query->queried_object;

            while($loop->have_posts()) : $loop->the_post();
            //Output what you want
            echo"
            <li value=".$post_idx." class='active listItem'>
            <a href='#?slug=$post_idx'>".get_the_title().'<p>'.get_post_meta($post->ID, 'video_length_', true).'</p>'.'</a>
            </li>'; 
                 
                  $post_idx++;

                endwhile;

                  if ($post_idx >= 0) {
                    ?>

                      <div class="bottom_last_option">
                      <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 30 30" style="width: 15px;margin-left: -2px;"><title>usericons</title><path d="M2,30V21.67s.8-1.71,5.14-4c4.12-2.19,6.83-1.93,9.92-1.87,2.34.05,11,2.9,11,5S28.1,30,28.1,30Z" style="fill:#606060"/><path d="M15.66,10.87A4.9,4.9,0,0,1,20,9.75a6.5,6.5,0,0,0,.29-1.89,6.31,6.31,0,1,0-6.57,6.29A7.38,7.38,0,0,1,15.66,10.87Z" style="fill:#606060"/><path d="M18.52,12.91a3.59,3.59,0,0,0,2.87-2.5H21V6.64h.35c0-.17,0-.32-.06-.48a6.27,6.27,0,0,0-.9-2.29,6.69,6.69,0,0,0-3.6-2.72A9.48,9.48,0,0,0,14.36.7a9.83,9.83,0,0,0-2.6.21A7.23,7.23,0,0,0,8,3,6.1,6.1,0,0,0,6.45,6c0,.22-.09.44-.13.68h.6V10.4H5.81a.89.89,0,0,1-.94-.87c0-.69,0-1.38,0-2.08a.78.78,0,0,1,.58-.74.17.17,0,0,0,.12-.15,7.5,7.5,0,0,1,.84-2.67A7,7,0,0,1,9.55,1,9.16,9.16,0,0,1,13.31,0a9.6,9.6,0,0,1,5.16,1.17,6.87,6.87,0,0,1,3.26,3.69,6.74,6.74,0,0,1,.36,1.66s0,.07,0,.1a1.14,1.14,0,0,1,.63.22.79.79,0,0,1,.32.64v2a.87.87,0,0,1-.82.94.09.09,0,0,0-.09.09,3.63,3.63,0,0,1-1.33,2.09,5.06,5.06,0,0,1-2.21,1,.19.19,0,0,0-.15.12.68.68,0,0,1-.71.43c-.61,0-1.21,0-1.82,0a.63.63,0,0,1-.54-.26,1.13,1.13,0,0,1,0-1.3.61.61,0,0,1,.52-.24h1.95a.58.58,0,0,1,.53.31C18.44,12.75,18.47,12.83,18.52,12.91Z" style="fill:#606060"/></svg>
                      <span>Live call</span>
                    </div>



                    <?php
                   
                  }
              }

                ?>


        </ul>
    </div>

</div>

        <script type="text/javascript">
        var ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
        </script>
        <script>
        jQuery(".next_video").click(function(){

        jQuery.get(ajaxurl,{'action': "navigate"},
        function (msg) { alert(msg);});

        });
    </script>

<!-- //======================================================================
// Show first video  before user navigate to the next 
//====================================================================== -->
      <script type="text/javascript">

      $(document).ready(function(){
      $(".listItem").click(function(){
      var id = $(this).val(); 

      $('.vimeo-video').load('<?php bloginfo('template_url'); ?>/includes/ajax-video-contents.php?id='+ id +'&cat='+'<?php echo $cat;?>'); 

     
      });

       $('.vimeo-video').load('<?php bloginfo('template_url'); ?>/includes/ajax-video-contents.php?id=0'+'&cat='+'<?php echo $cat;?>'); 
       });
     </script>






    <?php get_footer(); ?>
