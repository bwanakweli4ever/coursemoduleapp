<?php

/**
Template Name: Bibliotek
 */
 get_header(); 
 ?>

<style>
.faq-container{
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
}
/* search box */
.search {
  width: 100%;
  position: relative;
  display: flex;
  max-width: 600px;
  margin-top: -20px;
}

.searchTerm {
  width: 100%;
  border: 1px solid #d4d4d4;
  border-right: none;
  padding: 5px 20px;
  height: 40px;
  border-radius: 0px 0 0 0px;
  outline: none;
  color: #282d30;
}

.searchTerm:focus{
  color: #000000;
}

.searchButton {
  width: 40px;
  height: 40px;
  border: 1px solid #282d30;
  background: #282d30;
  text-align: center;
  color: #fff;
  border-radius: 0 0px 0px 0;
  cursor: pointer;
  font-size: 20px;
}
/* header question */
.faq-header{
padding: 3rem 2rem 3rem 2rem;
}

.faq-header h2 {
  text-align: center;
  padding: 2rem;
}
.faq-header > a > p > i{
  color: #c4c4c4;
}
.faq-header > a:hover p span{
  color: #282d30;
}
.faq-header p{
  margin-bottom: 0.4rem;
}
.faq-question{
  font-size: 14px;
  padding-left: 20px;
  color: #909090;
  font-weight: 500;
}
/* category container */
.category-container{
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  background-color: #f2f2f2;
  padding: 40px;

}
.category-box{
  background-color: #fff;
  padding: 50px 40px;
  width: 300px;
  border: 2px solid rgba(0,0,0,0.1);
  margin: 1rem;
}
.category-title a{
  font-size: 22px;
  color: #1097c5;
  padding: 5px 0;
  font-weight: 600;
  overflow-wrap: break-word;
  text-transform: uppercase;
}
.category-question-number{
  font-size: 14px;
  padding-left: 5px;
  color: #909090;
  font-weight: 500;
}
.category-box  p > i{
  color: #c4c4c4;
}

</style>
<div class="faq-container">
    <div class="search">
       <input type="text" class="searchTerm" placeholder="What are you looking for?">
       <button type="submit" class="searchButton">
         <i class="fa fa-search"></i>
      </button>
    </div>

    <!-- this is the header part -->

    <div class="faq-header">
      <h2>Latest Questions</h2>
      <a href="#">
      <p><i class="fas fa-file-alt"></i><span class="faq-question">This is a great question but it's very long and is...</span></p></a>
      <a href="#">
      <p><i class="fas fa-file-alt"></i><span class="faq-question">Another very good question and we are talking about...</span></p></a>
      <a href="#">
      <p><i class="fas fa-file-alt"></i><span class="faq-question">This is the third question in the previous and it's also..</span></p></a>
    </div>
</div>

    <!-- this is the part of flex category containers -->
    <div class="category-container">
      <div class="category-box">
        <h2 class="category-title"><a href="#">headline</a></h2>
        <p><i class="fas fa-file-alt"></i><span class="category-question-number">23Questions</span></p></a>
      </div>


      <div class="category-box">
        <h2 class="category-title"><a href="#">HEADLINE ALSO ON TWO LINES</a></h2>
        <p><i class="fas fa-file-alt"></i><span class="category-question-number">15Questions</span></p></a>
      </div>


      <div class="category-box">
        <h2 class="category-title"><a href="#">ANOTHER HEADLINE</a></h2>
        <p><i class="fas fa-file-alt"></i><span class="category-question-number">11Questions</span></p></a>
      </div>


      <div class="category-box">
        <h2 class="category-title"><a href="#">HEADLINE HERE</a></h2>
        <p><i class="fas fa-file-alt"></i><span class="category-question-number">23Questions</span></p></a>
      </div>

      <div class="category-box">
        <h2 class="category-title"><a href="#">ANOTHER HEADLINE</a></h2>
        <p><i class="fas fa-file-alt"></i><span class="category-question-number">15Questions</span></p></a>
      </div>

      <div class="category-box">
        <h2 class="category-title"><a href="#">HEADLINE ALSO ON TWO LINEO</a></h2>
        <p><i class="fas fa-file-alt"></i><span class="category-question-number">11Questions</span></p></a>
      </div>



    </div>



<!--this closes my main wrapper/div-->
</div>
<?php get_footer();
