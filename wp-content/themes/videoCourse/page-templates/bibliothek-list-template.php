
 <?php get_header(); ?>

<style>
.list-container{
  display: flex;
  flex-direction: row;
  justify-content: center;
}
.list-content{
  padding: 60px 40px;
  max-width: 50%;
}
.list-content .headline{
  color: #404040;
  font-size: 30px;
  font-weight: 600;
}
.list-content .question-list > a > p > i{
  color: #c4c4c4;
}
.list-content .question-list a p span{
  margin-bottom: 0.4rem;
  font-size: 15px;
  padding-left: 20px;
  color: #909090;
  font-weight: 500;
}
.list-content .question-list a:hover p span {
  color: #282d30;
}

.question-list{
  padding: 40px 0;
}
.question-list p{
  padding: 5px 0;
}

@media screen and (max-width: 768px) {
  .list-content{
    max-width: 100%;
  }
}

@media screen and (max-width: 480px) {
  .list-content{
    padding: 30px 10px;
  }

  .list-content .headline {
    font-size: 20px;
}
}
.list-content .question-list a p span {
    font-size: 14px;
}
</style>

<div class="list-container">

  <div class="list-content">
    <label class="headline">HEADLINE FROM THE FIRST PAGE</label>
    <br/>
      <div class="question-list">
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>
        <a href="#">
        <p><i class="fas fa-file-alt"></i><span>This is a great question but it's very long and is...</span></p></a>

      </div>
  </div>
</div>


<!--this closes my main wrapper/div-->
</div>
<?php include_once "../layout/footer.php"; ?>
