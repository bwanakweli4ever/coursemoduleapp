
  <nav class="mobile-sidenav" id="mobile-sidenav">
   <style>
    .logo-container{
      display: flex;
      flex-direction: row;
      width: 100%;

    }
    .logo-container .logo{
        position: relative;
        width: 80%;
        height: auto;
    }

    .logo-container .logo img{
      position: absolute; 
      width: 100%;
      height: auto;
      top: 0;
      left: 15px;
    }
    .logo-container .close-slide{
      width: 20%;
      height: auto;
    }

    .logo-container .close-slide img{
        width: 15px;
        height: 15px;
        top: 12px;
        left: 0; 
    }
  </style>


<div class='logo-container'>
  <div class="logo">
  <?php
if ( function_exists( 'the_custom_logo' ) ) {
 the_custom_logo();
}
   ?>
 </div>

  <div class="close-slide"  onclick="closeNav()">
      <img src="<?php bloginfo('template_url'); ?>//images/icons/menu-bars.png" />
  </div>


</div>

    <?php
wp_nav_menu( array( 
    'theme_location' => 'primary') ); 
?>

    <div class="Bottom-menu">
        <?php
wp_nav_menu( array( 
    'theme_location' => 'secondary',
    'container-class'=>'AGB') ); 
?>
  </div>


</nav>