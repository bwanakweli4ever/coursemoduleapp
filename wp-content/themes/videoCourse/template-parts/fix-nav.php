  <nav class="sidebar" id="navbar">

  <style>
    .logo-container{
      position: relative;
    }
    .logo-container img{
      position: relative; 
      width: 100%;
      height: auto;
    }
  </style>


<div class='logo-container'>
<?php
  if ( function_exists( 'the_custom_logo' ) ) {
    the_custom_logo();
  }
?>
</div>

<?php
    wp_nav_menu( array(
        'theme_location' => 'primary') );
    ?>
<div class="Bottom-menu">
            <?php
    wp_nav_menu( array(
        'theme_location' => 'secondary',
        'container-class'=>'AGB') );
?>

</div>
</nav>
  