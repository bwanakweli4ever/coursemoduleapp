<?php
/**
 * Thrive Themes - https://thrivethemes.com
 *
 * @package thrive-visual-editor
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Silence is golden!
}

class TCB_Custom_Fields_Shortcode {
	const GLOBAL_SHORTCODE_URL = 'thrive_custom_fields_shortcode_url';
	const GLOBAL_SHORTCODE_DATA = 'thrive_custom_fields_shortcode_data';

	public function __construct() {
		add_filter( 'tcb_content_allowed_shortcodes', array( $this, 'allowed_shortcodes' ) );

		add_filter( 'tcb_dynamiclink_data', array( $this, 'global_links_shortcodes' ) );

		add_shortcode( static::GLOBAL_SHORTCODE_URL, array( $this, 'global_shortcode_url_link' ) );
		add_shortcode( static::GLOBAL_SHORTCODE_DATA, array( $this, 'global_shortcode_url_data' ) );

		add_filter( 'tcb_inline_shortcodes', array( $this, 'tcb_inline_shortcodes' ), 11 );
	}

	/**
	 * Filter allowed shortcodes for tve_do_wp_shortcodes
	 *
	 * @param $shortcodes
	 *
	 * @return array
	 */
	public function allowed_shortcodes( $shortcodes ) {
		return array_merge( $shortcodes, array(
				static::GLOBAL_SHORTCODE_URL,
				static::GLOBAL_SHORTCODE_DATA,
			)
		);
	}

	/**
	 * Add global shortcodes to be used in dynamic links
	 *
	 * @param $links
	 *
	 * @return mixed
	 */
	public function global_links_shortcodes( $links ) {
		$global_links = array();
		$post_id      = get_the_ID();
		foreach ( $this->global_custom_links() as $index => $value ) {
			$value['id']    = $post_id . "::" . $index;
			$global_links[] = $value;
		}
		if ( ! empty( $global_links ) ) {
			$links['Custom Fields Global'] = array( 'links' => array( $global_links ), 'shortcode' => static::GLOBAL_SHORTCODE_URL );
		}

		return $links;
	}

	/**
	 * Global data related to the custom fields with links
	 *
	 * Gets all the custom fields for the current post or post with given id, selects only the fields that correspond to a http link
	 * and returns an array of objects with the structure of a dynamic link
	 *
	 * @param null $post_id
	 *
	 * @return array
	 */
	public function global_custom_links( $post_id = null ) {
		$post_id = $post_id === null ? get_the_ID() : intval( $post_id );
		$custom  = get_post_custom( $post_id );
		//Get all the keys that are not protected meta and are links
		$custom_keys = array_filter( ( array ) get_post_custom_keys( $post_id ), function ( $meta ) use ( $custom ) {
			return apply_filters( 'is_protected_meta', ! filter_var( $custom[ $meta ][0], FILTER_VALIDATE_URL ), $meta, null ) === false;
		} );

		$items = array();
		foreach ( $custom_keys as $val ) {
			$items[ $val ] = array(
				'name' => $val,
				'url'  => $custom[ $val ][0],
				'show' => true,
			);
		}

		return $items;
	}

	/**
	 * Global data related to the custom fields
	 *
	 * Gets all the custom fields for the current post or post with given id, selects only the fields that do not correspond to a http link
	 * and returns an array of objects with the structure of an inline shortcode
	 *
	 * @param null $post_id
	 *
	 * @return array
	 */
	public function global_custom_metadata( $post_id = null ) {
		$post_id = $post_id === null ? get_the_ID() : intval( $post_id );
		$custom  = get_post_custom( $post_id );
		//Get all the keys that are not protected meta and not links
		$custom_keys = array_filter( ( array ) get_post_custom_keys( $post_id ), function ( $meta ) use ( $custom ) {
			return apply_filters( 'is_protected_meta', filter_var( $custom[ $meta ][0], FILTER_VALIDATE_URL ), $meta, null ) === false;
		} );

		$real_data  = array();
		$value      = array();
		$value_type = array();
		foreach ( $custom_keys as $val ) {
			$real_data[ $post_id . '::' . $val ]  = $custom[ $val ][0]; //Value that will be displayed
			$value[ $post_id . '::' . $val ]      = $val;               //Value appearing as option title
			$value_type[ $post_id . '::' . $val ] = '0';                //Value type (text)
		}

		return array(
			'real_data'  => $real_data,
			'value'      => $value,
			'value_type' => $value_type,
		);
	}

	/**
	 * Add some inline shortcodes.
	 *
	 * @param $shortcodes
	 *
	 * @return array
	 */
	public function tcb_inline_shortcodes( $shortcodes ) {

		$custom_data_global = $this->global_custom_metadata();

		$custom_shortcodes = array(
			'Meta Data' => array(
				array(
					'name'        => __( 'Custom Fields Global', 'thrive-cb' ),
					'value'       => static::GLOBAL_SHORTCODE_DATA,
					'option'      => __( 'Custom Fields', 'thrive-cb' ),
					'extra_param' => 'CFG',
					'input'       => array(
						'id' => array(
							'extra_options' => array(),
							'label'         => 'Field',
							'real_data'     => $custom_data_global['real_data'],
							'type'          => 'select',
							'value'         => $custom_data_global['value'],
							'value_type'    => $custom_data_global['value_type'],
						),
					),
				),
				array(
					'name'        => __( 'Custom Fields Postlist', 'thrive-cb' ),
					'value'       => 'tcb_post_custom_field',
					'option'      => __( 'Custom Fields', 'thrive-cb' ),
					'extra_param' => 'CFP',
					'input'       => array(
						'id' => array(
							'extra_options' => array(),
							'label'         => 'Field',
							'real_data'     => array(),
							'type'          => 'select',
							'value'         => array(),
							'value_type'    => array(),
						),
					),
				),
			),
		);

		return array_merge_recursive( $shortcodes, $custom_shortcodes );

	}

	/**
	 * Replace the shortcode with its content
	 *
	 * @param $args
	 *
	 * @return mixed|string
	 */
	public function global_shortcode_url_link( $args ) {
		$data = '';
		if ( ! empty( $args['id'] ) ) {
			$post_id = explode( '::', $args['id'] );
			$groups  = $this->global_custom_links( intval( $post_id[0] ) );
			$id      = $post_id[1];
			$data    = isset( $groups[ $id ] ) ? $groups[ $id ]['url'] : '';
		}

		return $data;
	}


	/**
	 * Replace the shortcode with its content
	 *
	 * @param $args
	 *
	 * @return mixed|string
	 */
	public function global_shortcode_url_data( $args ) {
		$data = '';

		if ( ! empty( $args['id'] ) ) {
			$post_id = explode( '::', $args['id'] );
			$groups  = $this->global_custom_metadata( intval( $post_id[0] ) );
			$data    = isset( $groups['real_data'][ $args['id'] ] ) ? $groups['real_data'][ $args['id'] ] : '';
		}

		return $data;
	}

}

new TCB_Custom_Fields_Shortcode();