var rcl =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./public/src/admin.tsx");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/node_modules/regenerator-runtime/runtime.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/@babel/runtime/node_modules/regenerator-runtime/runtime.js ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {
  "use strict";

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  IteratorPrototype[iteratorSymbol] = function () {
    return this;
  };

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = Gp.constructor = GeneratorFunctionPrototype;
  GeneratorFunctionPrototype.constructor = GeneratorFunction;
  GeneratorFunctionPrototype[toStringTagSymbol] =
    GeneratorFunction.displayName = "GeneratorFunction";

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      prototype[method] = function(arg) {
        return this._invoke(method, arg);
      };
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      if (!(toStringTagSymbol in genFun)) {
        genFun[toStringTagSymbol] = "GeneratorFunction";
      }
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return Promise.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return Promise.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new Promise(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  AsyncIterator.prototype[asyncIteratorSymbol] = function () {
    return this;
  };
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList) {
    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList)
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  Gp[toStringTagSymbol] = "Generator";

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  Gp[iteratorSymbol] = function() {
    return this;
  };

  Gp.toString = function() {
    return "[object Generator]";
  };

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   true ? module.exports : undefined
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  Function("r", "regeneratorRuntime = r")(runtime);
}


/***/ }),

/***/ "./node_modules/@babel/runtime/regenerator/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@babel/runtime/regenerator/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! regenerator-runtime */ "./node_modules/@babel/runtime/node_modules/regenerator-runtime/runtime.js");


/***/ }),

/***/ "./node_modules/create-react-class/factory.js":
/*!****************************************************!*\
  !*** ./node_modules/create-react-class/factory.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */



var _assign = __webpack_require__(/*! object-assign */ "./node_modules/object-assign/index.js");

var emptyObject = __webpack_require__(/*! fbjs/lib/emptyObject */ "./node_modules/fbjs/lib/emptyObject.js");
var _invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");

if (true) {
  var warning = __webpack_require__(/*! fbjs/lib/warning */ "./node_modules/fbjs/lib/warning.js");
}

var MIXINS_KEY = 'mixins';

// Helper function to allow the creation of anonymous functions which do not
// have .name set to the name of the variable being assigned to.
function identity(fn) {
  return fn;
}

var ReactPropTypeLocationNames;
if (true) {
  ReactPropTypeLocationNames = {
    prop: 'prop',
    context: 'context',
    childContext: 'child context'
  };
} else {}

function factory(ReactComponent, isValidElement, ReactNoopUpdateQueue) {
  /**
   * Policies that describe methods in `ReactClassInterface`.
   */

  var injectedMixins = [];

  /**
   * Composite components are higher-level components that compose other composite
   * or host components.
   *
   * To create a new type of `ReactClass`, pass a specification of
   * your new class to `React.createClass`. The only requirement of your class
   * specification is that you implement a `render` method.
   *
   *   var MyComponent = React.createClass({
   *     render: function() {
   *       return <div>Hello World</div>;
   *     }
   *   });
   *
   * The class specification supports a specific protocol of methods that have
   * special meaning (e.g. `render`). See `ReactClassInterface` for
   * more the comprehensive protocol. Any other properties and methods in the
   * class specification will be available on the prototype.
   *
   * @interface ReactClassInterface
   * @internal
   */
  var ReactClassInterface = {
    /**
     * An array of Mixin objects to include when defining your component.
     *
     * @type {array}
     * @optional
     */
    mixins: 'DEFINE_MANY',

    /**
     * An object containing properties and methods that should be defined on
     * the component's constructor instead of its prototype (static methods).
     *
     * @type {object}
     * @optional
     */
    statics: 'DEFINE_MANY',

    /**
     * Definition of prop types for this component.
     *
     * @type {object}
     * @optional
     */
    propTypes: 'DEFINE_MANY',

    /**
     * Definition of context types for this component.
     *
     * @type {object}
     * @optional
     */
    contextTypes: 'DEFINE_MANY',

    /**
     * Definition of context types this component sets for its children.
     *
     * @type {object}
     * @optional
     */
    childContextTypes: 'DEFINE_MANY',

    // ==== Definition methods ====

    /**
     * Invoked when the component is mounted. Values in the mapping will be set on
     * `this.props` if that prop is not specified (i.e. using an `in` check).
     *
     * This method is invoked before `getInitialState` and therefore cannot rely
     * on `this.state` or use `this.setState`.
     *
     * @return {object}
     * @optional
     */
    getDefaultProps: 'DEFINE_MANY_MERGED',

    /**
     * Invoked once before the component is mounted. The return value will be used
     * as the initial value of `this.state`.
     *
     *   getInitialState: function() {
     *     return {
     *       isOn: false,
     *       fooBaz: new BazFoo()
     *     }
     *   }
     *
     * @return {object}
     * @optional
     */
    getInitialState: 'DEFINE_MANY_MERGED',

    /**
     * @return {object}
     * @optional
     */
    getChildContext: 'DEFINE_MANY_MERGED',

    /**
     * Uses props from `this.props` and state from `this.state` to render the
     * structure of the component.
     *
     * No guarantees are made about when or how often this method is invoked, so
     * it must not have side effects.
     *
     *   render: function() {
     *     var name = this.props.name;
     *     return <div>Hello, {name}!</div>;
     *   }
     *
     * @return {ReactComponent}
     * @required
     */
    render: 'DEFINE_ONCE',

    // ==== Delegate methods ====

    /**
     * Invoked when the component is initially created and about to be mounted.
     * This may have side effects, but any external subscriptions or data created
     * by this method must be cleaned up in `componentWillUnmount`.
     *
     * @optional
     */
    componentWillMount: 'DEFINE_MANY',

    /**
     * Invoked when the component has been mounted and has a DOM representation.
     * However, there is no guarantee that the DOM node is in the document.
     *
     * Use this as an opportunity to operate on the DOM when the component has
     * been mounted (initialized and rendered) for the first time.
     *
     * @param {DOMElement} rootNode DOM element representing the component.
     * @optional
     */
    componentDidMount: 'DEFINE_MANY',

    /**
     * Invoked before the component receives new props.
     *
     * Use this as an opportunity to react to a prop transition by updating the
     * state using `this.setState`. Current props are accessed via `this.props`.
     *
     *   componentWillReceiveProps: function(nextProps, nextContext) {
     *     this.setState({
     *       likesIncreasing: nextProps.likeCount > this.props.likeCount
     *     });
     *   }
     *
     * NOTE: There is no equivalent `componentWillReceiveState`. An incoming prop
     * transition may cause a state change, but the opposite is not true. If you
     * need it, you are probably looking for `componentWillUpdate`.
     *
     * @param {object} nextProps
     * @optional
     */
    componentWillReceiveProps: 'DEFINE_MANY',

    /**
     * Invoked while deciding if the component should be updated as a result of
     * receiving new props, state and/or context.
     *
     * Use this as an opportunity to `return false` when you're certain that the
     * transition to the new props/state/context will not require a component
     * update.
     *
     *   shouldComponentUpdate: function(nextProps, nextState, nextContext) {
     *     return !equal(nextProps, this.props) ||
     *       !equal(nextState, this.state) ||
     *       !equal(nextContext, this.context);
     *   }
     *
     * @param {object} nextProps
     * @param {?object} nextState
     * @param {?object} nextContext
     * @return {boolean} True if the component should update.
     * @optional
     */
    shouldComponentUpdate: 'DEFINE_ONCE',

    /**
     * Invoked when the component is about to update due to a transition from
     * `this.props`, `this.state` and `this.context` to `nextProps`, `nextState`
     * and `nextContext`.
     *
     * Use this as an opportunity to perform preparation before an update occurs.
     *
     * NOTE: You **cannot** use `this.setState()` in this method.
     *
     * @param {object} nextProps
     * @param {?object} nextState
     * @param {?object} nextContext
     * @param {ReactReconcileTransaction} transaction
     * @optional
     */
    componentWillUpdate: 'DEFINE_MANY',

    /**
     * Invoked when the component's DOM representation has been updated.
     *
     * Use this as an opportunity to operate on the DOM when the component has
     * been updated.
     *
     * @param {object} prevProps
     * @param {?object} prevState
     * @param {?object} prevContext
     * @param {DOMElement} rootNode DOM element representing the component.
     * @optional
     */
    componentDidUpdate: 'DEFINE_MANY',

    /**
     * Invoked when the component is about to be removed from its parent and have
     * its DOM representation destroyed.
     *
     * Use this as an opportunity to deallocate any external resources.
     *
     * NOTE: There is no `componentDidUnmount` since your component will have been
     * destroyed by that point.
     *
     * @optional
     */
    componentWillUnmount: 'DEFINE_MANY',

    /**
     * Replacement for (deprecated) `componentWillMount`.
     *
     * @optional
     */
    UNSAFE_componentWillMount: 'DEFINE_MANY',

    /**
     * Replacement for (deprecated) `componentWillReceiveProps`.
     *
     * @optional
     */
    UNSAFE_componentWillReceiveProps: 'DEFINE_MANY',

    /**
     * Replacement for (deprecated) `componentWillUpdate`.
     *
     * @optional
     */
    UNSAFE_componentWillUpdate: 'DEFINE_MANY',

    // ==== Advanced methods ====

    /**
     * Updates the component's currently mounted DOM representation.
     *
     * By default, this implements React's rendering and reconciliation algorithm.
     * Sophisticated clients may wish to override this.
     *
     * @param {ReactReconcileTransaction} transaction
     * @internal
     * @overridable
     */
    updateComponent: 'OVERRIDE_BASE'
  };

  /**
   * Similar to ReactClassInterface but for static methods.
   */
  var ReactClassStaticInterface = {
    /**
     * This method is invoked after a component is instantiated and when it
     * receives new props. Return an object to update state in response to
     * prop changes. Return null to indicate no change to state.
     *
     * If an object is returned, its keys will be merged into the existing state.
     *
     * @return {object || null}
     * @optional
     */
    getDerivedStateFromProps: 'DEFINE_MANY_MERGED'
  };

  /**
   * Mapping from class specification keys to special processing functions.
   *
   * Although these are declared like instance properties in the specification
   * when defining classes using `React.createClass`, they are actually static
   * and are accessible on the constructor instead of the prototype. Despite
   * being static, they must be defined outside of the "statics" key under
   * which all other static methods are defined.
   */
  var RESERVED_SPEC_KEYS = {
    displayName: function(Constructor, displayName) {
      Constructor.displayName = displayName;
    },
    mixins: function(Constructor, mixins) {
      if (mixins) {
        for (var i = 0; i < mixins.length; i++) {
          mixSpecIntoComponent(Constructor, mixins[i]);
        }
      }
    },
    childContextTypes: function(Constructor, childContextTypes) {
      if (true) {
        validateTypeDef(Constructor, childContextTypes, 'childContext');
      }
      Constructor.childContextTypes = _assign(
        {},
        Constructor.childContextTypes,
        childContextTypes
      );
    },
    contextTypes: function(Constructor, contextTypes) {
      if (true) {
        validateTypeDef(Constructor, contextTypes, 'context');
      }
      Constructor.contextTypes = _assign(
        {},
        Constructor.contextTypes,
        contextTypes
      );
    },
    /**
     * Special case getDefaultProps which should move into statics but requires
     * automatic merging.
     */
    getDefaultProps: function(Constructor, getDefaultProps) {
      if (Constructor.getDefaultProps) {
        Constructor.getDefaultProps = createMergedResultFunction(
          Constructor.getDefaultProps,
          getDefaultProps
        );
      } else {
        Constructor.getDefaultProps = getDefaultProps;
      }
    },
    propTypes: function(Constructor, propTypes) {
      if (true) {
        validateTypeDef(Constructor, propTypes, 'prop');
      }
      Constructor.propTypes = _assign({}, Constructor.propTypes, propTypes);
    },
    statics: function(Constructor, statics) {
      mixStaticSpecIntoComponent(Constructor, statics);
    },
    autobind: function() {}
  };

  function validateTypeDef(Constructor, typeDef, location) {
    for (var propName in typeDef) {
      if (typeDef.hasOwnProperty(propName)) {
        // use a warning instead of an _invariant so components
        // don't show up in prod but only in __DEV__
        if (true) {
          warning(
            typeof typeDef[propName] === 'function',
            '%s: %s type `%s` is invalid; it must be a function, usually from ' +
              'React.PropTypes.',
            Constructor.displayName || 'ReactClass',
            ReactPropTypeLocationNames[location],
            propName
          );
        }
      }
    }
  }

  function validateMethodOverride(isAlreadyDefined, name) {
    var specPolicy = ReactClassInterface.hasOwnProperty(name)
      ? ReactClassInterface[name]
      : null;

    // Disallow overriding of base class methods unless explicitly allowed.
    if (ReactClassMixin.hasOwnProperty(name)) {
      _invariant(
        specPolicy === 'OVERRIDE_BASE',
        'ReactClassInterface: You are attempting to override ' +
          '`%s` from your class specification. Ensure that your method names ' +
          'do not overlap with React methods.',
        name
      );
    }

    // Disallow defining methods more than once unless explicitly allowed.
    if (isAlreadyDefined) {
      _invariant(
        specPolicy === 'DEFINE_MANY' || specPolicy === 'DEFINE_MANY_MERGED',
        'ReactClassInterface: You are attempting to define ' +
          '`%s` on your component more than once. This conflict may be due ' +
          'to a mixin.',
        name
      );
    }
  }

  /**
   * Mixin helper which handles policy validation and reserved
   * specification keys when building React classes.
   */
  function mixSpecIntoComponent(Constructor, spec) {
    if (!spec) {
      if (true) {
        var typeofSpec = typeof spec;
        var isMixinValid = typeofSpec === 'object' && spec !== null;

        if (true) {
          warning(
            isMixinValid,
            "%s: You're attempting to include a mixin that is either null " +
              'or not an object. Check the mixins included by the component, ' +
              'as well as any mixins they include themselves. ' +
              'Expected object but got %s.',
            Constructor.displayName || 'ReactClass',
            spec === null ? null : typeofSpec
          );
        }
      }

      return;
    }

    _invariant(
      typeof spec !== 'function',
      "ReactClass: You're attempting to " +
        'use a component class or function as a mixin. Instead, just use a ' +
        'regular object.'
    );
    _invariant(
      !isValidElement(spec),
      "ReactClass: You're attempting to " +
        'use a component as a mixin. Instead, just use a regular object.'
    );

    var proto = Constructor.prototype;
    var autoBindPairs = proto.__reactAutoBindPairs;

    // By handling mixins before any other properties, we ensure the same
    // chaining order is applied to methods with DEFINE_MANY policy, whether
    // mixins are listed before or after these methods in the spec.
    if (spec.hasOwnProperty(MIXINS_KEY)) {
      RESERVED_SPEC_KEYS.mixins(Constructor, spec.mixins);
    }

    for (var name in spec) {
      if (!spec.hasOwnProperty(name)) {
        continue;
      }

      if (name === MIXINS_KEY) {
        // We have already handled mixins in a special case above.
        continue;
      }

      var property = spec[name];
      var isAlreadyDefined = proto.hasOwnProperty(name);
      validateMethodOverride(isAlreadyDefined, name);

      if (RESERVED_SPEC_KEYS.hasOwnProperty(name)) {
        RESERVED_SPEC_KEYS[name](Constructor, property);
      } else {
        // Setup methods on prototype:
        // The following member methods should not be automatically bound:
        // 1. Expected ReactClass methods (in the "interface").
        // 2. Overridden methods (that were mixed in).
        var isReactClassMethod = ReactClassInterface.hasOwnProperty(name);
        var isFunction = typeof property === 'function';
        var shouldAutoBind =
          isFunction &&
          !isReactClassMethod &&
          !isAlreadyDefined &&
          spec.autobind !== false;

        if (shouldAutoBind) {
          autoBindPairs.push(name, property);
          proto[name] = property;
        } else {
          if (isAlreadyDefined) {
            var specPolicy = ReactClassInterface[name];

            // These cases should already be caught by validateMethodOverride.
            _invariant(
              isReactClassMethod &&
                (specPolicy === 'DEFINE_MANY_MERGED' ||
                  specPolicy === 'DEFINE_MANY'),
              'ReactClass: Unexpected spec policy %s for key %s ' +
                'when mixing in component specs.',
              specPolicy,
              name
            );

            // For methods which are defined more than once, call the existing
            // methods before calling the new property, merging if appropriate.
            if (specPolicy === 'DEFINE_MANY_MERGED') {
              proto[name] = createMergedResultFunction(proto[name], property);
            } else if (specPolicy === 'DEFINE_MANY') {
              proto[name] = createChainedFunction(proto[name], property);
            }
          } else {
            proto[name] = property;
            if (true) {
              // Add verbose displayName to the function, which helps when looking
              // at profiling tools.
              if (typeof property === 'function' && spec.displayName) {
                proto[name].displayName = spec.displayName + '_' + name;
              }
            }
          }
        }
      }
    }
  }

  function mixStaticSpecIntoComponent(Constructor, statics) {
    if (!statics) {
      return;
    }

    for (var name in statics) {
      var property = statics[name];
      if (!statics.hasOwnProperty(name)) {
        continue;
      }

      var isReserved = name in RESERVED_SPEC_KEYS;
      _invariant(
        !isReserved,
        'ReactClass: You are attempting to define a reserved ' +
          'property, `%s`, that shouldn\'t be on the "statics" key. Define it ' +
          'as an instance property instead; it will still be accessible on the ' +
          'constructor.',
        name
      );

      var isAlreadyDefined = name in Constructor;
      if (isAlreadyDefined) {
        var specPolicy = ReactClassStaticInterface.hasOwnProperty(name)
          ? ReactClassStaticInterface[name]
          : null;

        _invariant(
          specPolicy === 'DEFINE_MANY_MERGED',
          'ReactClass: You are attempting to define ' +
            '`%s` on your component more than once. This conflict may be ' +
            'due to a mixin.',
          name
        );

        Constructor[name] = createMergedResultFunction(Constructor[name], property);

        return;
      }

      Constructor[name] = property;
    }
  }

  /**
   * Merge two objects, but throw if both contain the same key.
   *
   * @param {object} one The first object, which is mutated.
   * @param {object} two The second object
   * @return {object} one after it has been mutated to contain everything in two.
   */
  function mergeIntoWithNoDuplicateKeys(one, two) {
    _invariant(
      one && two && typeof one === 'object' && typeof two === 'object',
      'mergeIntoWithNoDuplicateKeys(): Cannot merge non-objects.'
    );

    for (var key in two) {
      if (two.hasOwnProperty(key)) {
        _invariant(
          one[key] === undefined,
          'mergeIntoWithNoDuplicateKeys(): ' +
            'Tried to merge two objects with the same key: `%s`. This conflict ' +
            'may be due to a mixin; in particular, this may be caused by two ' +
            'getInitialState() or getDefaultProps() methods returning objects ' +
            'with clashing keys.',
          key
        );
        one[key] = two[key];
      }
    }
    return one;
  }

  /**
   * Creates a function that invokes two functions and merges their return values.
   *
   * @param {function} one Function to invoke first.
   * @param {function} two Function to invoke second.
   * @return {function} Function that invokes the two argument functions.
   * @private
   */
  function createMergedResultFunction(one, two) {
    return function mergedResult() {
      var a = one.apply(this, arguments);
      var b = two.apply(this, arguments);
      if (a == null) {
        return b;
      } else if (b == null) {
        return a;
      }
      var c = {};
      mergeIntoWithNoDuplicateKeys(c, a);
      mergeIntoWithNoDuplicateKeys(c, b);
      return c;
    };
  }

  /**
   * Creates a function that invokes two functions and ignores their return vales.
   *
   * @param {function} one Function to invoke first.
   * @param {function} two Function to invoke second.
   * @return {function} Function that invokes the two argument functions.
   * @private
   */
  function createChainedFunction(one, two) {
    return function chainedFunction() {
      one.apply(this, arguments);
      two.apply(this, arguments);
    };
  }

  /**
   * Binds a method to the component.
   *
   * @param {object} component Component whose method is going to be bound.
   * @param {function} method Method to be bound.
   * @return {function} The bound method.
   */
  function bindAutoBindMethod(component, method) {
    var boundMethod = method.bind(component);
    if (true) {
      boundMethod.__reactBoundContext = component;
      boundMethod.__reactBoundMethod = method;
      boundMethod.__reactBoundArguments = null;
      var componentName = component.constructor.displayName;
      var _bind = boundMethod.bind;
      boundMethod.bind = function(newThis) {
        for (
          var _len = arguments.length,
            args = Array(_len > 1 ? _len - 1 : 0),
            _key = 1;
          _key < _len;
          _key++
        ) {
          args[_key - 1] = arguments[_key];
        }

        // User is trying to bind() an autobound method; we effectively will
        // ignore the value of "this" that the user is trying to use, so
        // let's warn.
        if (newThis !== component && newThis !== null) {
          if (true) {
            warning(
              false,
              'bind(): React component methods may only be bound to the ' +
                'component instance. See %s',
              componentName
            );
          }
        } else if (!args.length) {
          if (true) {
            warning(
              false,
              'bind(): You are binding a component method to the component. ' +
                'React does this for you automatically in a high-performance ' +
                'way, so you can safely remove this call. See %s',
              componentName
            );
          }
          return boundMethod;
        }
        var reboundMethod = _bind.apply(boundMethod, arguments);
        reboundMethod.__reactBoundContext = component;
        reboundMethod.__reactBoundMethod = method;
        reboundMethod.__reactBoundArguments = args;
        return reboundMethod;
      };
    }
    return boundMethod;
  }

  /**
   * Binds all auto-bound methods in a component.
   *
   * @param {object} component Component whose method is going to be bound.
   */
  function bindAutoBindMethods(component) {
    var pairs = component.__reactAutoBindPairs;
    for (var i = 0; i < pairs.length; i += 2) {
      var autoBindKey = pairs[i];
      var method = pairs[i + 1];
      component[autoBindKey] = bindAutoBindMethod(component, method);
    }
  }

  var IsMountedPreMixin = {
    componentDidMount: function() {
      this.__isMounted = true;
    }
  };

  var IsMountedPostMixin = {
    componentWillUnmount: function() {
      this.__isMounted = false;
    }
  };

  /**
   * Add more to the ReactClass base class. These are all legacy features and
   * therefore not already part of the modern ReactComponent.
   */
  var ReactClassMixin = {
    /**
     * TODO: This will be deprecated because state should always keep a consistent
     * type signature and the only use case for this, is to avoid that.
     */
    replaceState: function(newState, callback) {
      this.updater.enqueueReplaceState(this, newState, callback);
    },

    /**
     * Checks whether or not this composite component is mounted.
     * @return {boolean} True if mounted, false otherwise.
     * @protected
     * @final
     */
    isMounted: function() {
      if (true) {
        warning(
          this.__didWarnIsMounted,
          '%s: isMounted is deprecated. Instead, make sure to clean up ' +
            'subscriptions and pending requests in componentWillUnmount to ' +
            'prevent memory leaks.',
          (this.constructor && this.constructor.displayName) ||
            this.name ||
            'Component'
        );
        this.__didWarnIsMounted = true;
      }
      return !!this.__isMounted;
    }
  };

  var ReactClassComponent = function() {};
  _assign(
    ReactClassComponent.prototype,
    ReactComponent.prototype,
    ReactClassMixin
  );

  /**
   * Creates a composite component class given a class specification.
   * See https://facebook.github.io/react/docs/top-level-api.html#react.createclass
   *
   * @param {object} spec Class specification (which must define `render`).
   * @return {function} Component constructor function.
   * @public
   */
  function createClass(spec) {
    // To keep our warnings more understandable, we'll use a little hack here to
    // ensure that Constructor.name !== 'Constructor'. This makes sure we don't
    // unnecessarily identify a class without displayName as 'Constructor'.
    var Constructor = identity(function(props, context, updater) {
      // This constructor gets overridden by mocks. The argument is used
      // by mocks to assert on what gets mounted.

      if (true) {
        warning(
          this instanceof Constructor,
          'Something is calling a React component directly. Use a factory or ' +
            'JSX instead. See: https://fb.me/react-legacyfactory'
        );
      }

      // Wire up auto-binding
      if (this.__reactAutoBindPairs.length) {
        bindAutoBindMethods(this);
      }

      this.props = props;
      this.context = context;
      this.refs = emptyObject;
      this.updater = updater || ReactNoopUpdateQueue;

      this.state = null;

      // ReactClasses doesn't have constructors. Instead, they use the
      // getInitialState and componentWillMount methods for initialization.

      var initialState = this.getInitialState ? this.getInitialState() : null;
      if (true) {
        // We allow auto-mocks to proceed as if they're returning null.
        if (
          initialState === undefined &&
          this.getInitialState._isMockFunction
        ) {
          // This is probably bad practice. Consider warning here and
          // deprecating this convenience.
          initialState = null;
        }
      }
      _invariant(
        typeof initialState === 'object' && !Array.isArray(initialState),
        '%s.getInitialState(): must return an object or null',
        Constructor.displayName || 'ReactCompositeComponent'
      );

      this.state = initialState;
    });
    Constructor.prototype = new ReactClassComponent();
    Constructor.prototype.constructor = Constructor;
    Constructor.prototype.__reactAutoBindPairs = [];

    injectedMixins.forEach(mixSpecIntoComponent.bind(null, Constructor));

    mixSpecIntoComponent(Constructor, IsMountedPreMixin);
    mixSpecIntoComponent(Constructor, spec);
    mixSpecIntoComponent(Constructor, IsMountedPostMixin);

    // Initialize the defaultProps property after all mixins have been merged.
    if (Constructor.getDefaultProps) {
      Constructor.defaultProps = Constructor.getDefaultProps();
    }

    if (true) {
      // This is a tag to indicate that the use of these method names is ok,
      // since it's used with createClass. If it's not, then it's likely a
      // mistake so we'll warn you to use the static property, property
      // initializer or constructor respectively.
      if (Constructor.getDefaultProps) {
        Constructor.getDefaultProps.isReactClassApproved = {};
      }
      if (Constructor.prototype.getInitialState) {
        Constructor.prototype.getInitialState.isReactClassApproved = {};
      }
    }

    _invariant(
      Constructor.prototype.render,
      'createClass(...): Class specification must implement a `render` method.'
    );

    if (true) {
      warning(
        !Constructor.prototype.componentShouldUpdate,
        '%s has a method called ' +
          'componentShouldUpdate(). Did you mean shouldComponentUpdate()? ' +
          'The name is phrased as a question because the function is ' +
          'expected to return a value.',
        spec.displayName || 'A component'
      );
      warning(
        !Constructor.prototype.componentWillRecieveProps,
        '%s has a method called ' +
          'componentWillRecieveProps(). Did you mean componentWillReceiveProps()?',
        spec.displayName || 'A component'
      );
      warning(
        !Constructor.prototype.UNSAFE_componentWillRecieveProps,
        '%s has a method called UNSAFE_componentWillRecieveProps(). ' +
          'Did you mean UNSAFE_componentWillReceiveProps()?',
        spec.displayName || 'A component'
      );
    }

    // Reduce time spent doing lookups by setting these on the prototype.
    for (var methodName in ReactClassInterface) {
      if (!Constructor.prototype[methodName]) {
        Constructor.prototype[methodName] = null;
      }
    }

    return Constructor;
  }

  return createClass;
}

module.exports = factory;


/***/ }),

/***/ "./node_modules/create-react-class/index.js":
/*!**************************************************!*\
  !*** ./node_modules/create-react-class/index.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */



var React = __webpack_require__(/*! react */ "react");
var factory = __webpack_require__(/*! ./factory */ "./node_modules/create-react-class/factory.js");

if (typeof React === 'undefined') {
  throw Error(
    'create-react-class could not find the React object. If you are using script tags, ' +
      'make sure that React is being loaded before create-react-class.'
  );
}

// Hack to grab NoopUpdateQueue from isomorphic React
var ReactNoopUpdateQueue = new React.Component().updater;

module.exports = factory(
  React.Component,
  React.isValidElement,
  ReactNoopUpdateQueue
);


/***/ }),

/***/ "./node_modules/events/events.js":
/*!***************************************!*\
  !*** ./node_modules/events/events.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.



var R = typeof Reflect === 'object' ? Reflect : null
var ReflectApply = R && typeof R.apply === 'function'
  ? R.apply
  : function ReflectApply(target, receiver, args) {
    return Function.prototype.apply.call(target, receiver, args);
  }

var ReflectOwnKeys
if (R && typeof R.ownKeys === 'function') {
  ReflectOwnKeys = R.ownKeys
} else if (Object.getOwnPropertySymbols) {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target)
      .concat(Object.getOwnPropertySymbols(target));
  };
} else {
  ReflectOwnKeys = function ReflectOwnKeys(target) {
    return Object.getOwnPropertyNames(target);
  };
}

function ProcessEmitWarning(warning) {
  if (console && console.warn) console.warn(warning);
}

var NumberIsNaN = Number.isNaN || function NumberIsNaN(value) {
  return value !== value;
}

function EventEmitter() {
  EventEmitter.init.call(this);
}
module.exports = EventEmitter;

// Backwards-compat with node 0.10.x
EventEmitter.EventEmitter = EventEmitter;

EventEmitter.prototype._events = undefined;
EventEmitter.prototype._eventsCount = 0;
EventEmitter.prototype._maxListeners = undefined;

// By default EventEmitters will print a warning if more than 10 listeners are
// added to it. This is a useful default which helps finding memory leaks.
var defaultMaxListeners = 10;

Object.defineProperty(EventEmitter, 'defaultMaxListeners', {
  enumerable: true,
  get: function() {
    return defaultMaxListeners;
  },
  set: function(arg) {
    if (typeof arg !== 'number' || arg < 0 || NumberIsNaN(arg)) {
      throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received ' + arg + '.');
    }
    defaultMaxListeners = arg;
  }
});

EventEmitter.init = function() {

  if (this._events === undefined ||
      this._events === Object.getPrototypeOf(this)._events) {
    this._events = Object.create(null);
    this._eventsCount = 0;
  }

  this._maxListeners = this._maxListeners || undefined;
};

// Obviously not all Emitters should be limited to 10. This function allows
// that to be increased. Set to zero for unlimited.
EventEmitter.prototype.setMaxListeners = function setMaxListeners(n) {
  if (typeof n !== 'number' || n < 0 || NumberIsNaN(n)) {
    throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received ' + n + '.');
  }
  this._maxListeners = n;
  return this;
};

function $getMaxListeners(that) {
  if (that._maxListeners === undefined)
    return EventEmitter.defaultMaxListeners;
  return that._maxListeners;
}

EventEmitter.prototype.getMaxListeners = function getMaxListeners() {
  return $getMaxListeners(this);
};

EventEmitter.prototype.emit = function emit(type) {
  var args = [];
  for (var i = 1; i < arguments.length; i++) args.push(arguments[i]);
  var doError = (type === 'error');

  var events = this._events;
  if (events !== undefined)
    doError = (doError && events.error === undefined);
  else if (!doError)
    return false;

  // If there is no 'error' event listener then throw.
  if (doError) {
    var er;
    if (args.length > 0)
      er = args[0];
    if (er instanceof Error) {
      // Note: The comments on the `throw` lines are intentional, they show
      // up in Node's output if this results in an unhandled exception.
      throw er; // Unhandled 'error' event
    }
    // At least give some kind of context to the user
    var err = new Error('Unhandled error.' + (er ? ' (' + er.message + ')' : ''));
    err.context = er;
    throw err; // Unhandled 'error' event
  }

  var handler = events[type];

  if (handler === undefined)
    return false;

  if (typeof handler === 'function') {
    ReflectApply(handler, this, args);
  } else {
    var len = handler.length;
    var listeners = arrayClone(handler, len);
    for (var i = 0; i < len; ++i)
      ReflectApply(listeners[i], this, args);
  }

  return true;
};

function _addListener(target, type, listener, prepend) {
  var m;
  var events;
  var existing;

  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }

  events = target._events;
  if (events === undefined) {
    events = target._events = Object.create(null);
    target._eventsCount = 0;
  } else {
    // To avoid recursion in the case that type === "newListener"! Before
    // adding it to the listeners, first emit "newListener".
    if (events.newListener !== undefined) {
      target.emit('newListener', type,
                  listener.listener ? listener.listener : listener);

      // Re-assign `events` because a newListener handler could have caused the
      // this._events to be assigned to a new object
      events = target._events;
    }
    existing = events[type];
  }

  if (existing === undefined) {
    // Optimize the case of one listener. Don't need the extra array object.
    existing = events[type] = listener;
    ++target._eventsCount;
  } else {
    if (typeof existing === 'function') {
      // Adding the second element, need to change to array.
      existing = events[type] =
        prepend ? [listener, existing] : [existing, listener];
      // If we've already got an array, just append.
    } else if (prepend) {
      existing.unshift(listener);
    } else {
      existing.push(listener);
    }

    // Check for listener leak
    m = $getMaxListeners(target);
    if (m > 0 && existing.length > m && !existing.warned) {
      existing.warned = true;
      // No error code for this since it is a Warning
      // eslint-disable-next-line no-restricted-syntax
      var w = new Error('Possible EventEmitter memory leak detected. ' +
                          existing.length + ' ' + String(type) + ' listeners ' +
                          'added. Use emitter.setMaxListeners() to ' +
                          'increase limit');
      w.name = 'MaxListenersExceededWarning';
      w.emitter = target;
      w.type = type;
      w.count = existing.length;
      ProcessEmitWarning(w);
    }
  }

  return target;
}

EventEmitter.prototype.addListener = function addListener(type, listener) {
  return _addListener(this, type, listener, false);
};

EventEmitter.prototype.on = EventEmitter.prototype.addListener;

EventEmitter.prototype.prependListener =
    function prependListener(type, listener) {
      return _addListener(this, type, listener, true);
    };

function onceWrapper() {
  var args = [];
  for (var i = 0; i < arguments.length; i++) args.push(arguments[i]);
  if (!this.fired) {
    this.target.removeListener(this.type, this.wrapFn);
    this.fired = true;
    ReflectApply(this.listener, this.target, args);
  }
}

function _onceWrap(target, type, listener) {
  var state = { fired: false, wrapFn: undefined, target: target, type: type, listener: listener };
  var wrapped = onceWrapper.bind(state);
  wrapped.listener = listener;
  state.wrapFn = wrapped;
  return wrapped;
}

EventEmitter.prototype.once = function once(type, listener) {
  if (typeof listener !== 'function') {
    throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
  }
  this.on(type, _onceWrap(this, type, listener));
  return this;
};

EventEmitter.prototype.prependOnceListener =
    function prependOnceListener(type, listener) {
      if (typeof listener !== 'function') {
        throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
      }
      this.prependListener(type, _onceWrap(this, type, listener));
      return this;
    };

// Emits a 'removeListener' event if and only if the listener was removed.
EventEmitter.prototype.removeListener =
    function removeListener(type, listener) {
      var list, events, position, i, originalListener;

      if (typeof listener !== 'function') {
        throw new TypeError('The "listener" argument must be of type Function. Received type ' + typeof listener);
      }

      events = this._events;
      if (events === undefined)
        return this;

      list = events[type];
      if (list === undefined)
        return this;

      if (list === listener || list.listener === listener) {
        if (--this._eventsCount === 0)
          this._events = Object.create(null);
        else {
          delete events[type];
          if (events.removeListener)
            this.emit('removeListener', type, list.listener || listener);
        }
      } else if (typeof list !== 'function') {
        position = -1;

        for (i = list.length - 1; i >= 0; i--) {
          if (list[i] === listener || list[i].listener === listener) {
            originalListener = list[i].listener;
            position = i;
            break;
          }
        }

        if (position < 0)
          return this;

        if (position === 0)
          list.shift();
        else {
          spliceOne(list, position);
        }

        if (list.length === 1)
          events[type] = list[0];

        if (events.removeListener !== undefined)
          this.emit('removeListener', type, originalListener || listener);
      }

      return this;
    };

EventEmitter.prototype.off = EventEmitter.prototype.removeListener;

EventEmitter.prototype.removeAllListeners =
    function removeAllListeners(type) {
      var listeners, events, i;

      events = this._events;
      if (events === undefined)
        return this;

      // not listening for removeListener, no need to emit
      if (events.removeListener === undefined) {
        if (arguments.length === 0) {
          this._events = Object.create(null);
          this._eventsCount = 0;
        } else if (events[type] !== undefined) {
          if (--this._eventsCount === 0)
            this._events = Object.create(null);
          else
            delete events[type];
        }
        return this;
      }

      // emit removeListener for all listeners on all events
      if (arguments.length === 0) {
        var keys = Object.keys(events);
        var key;
        for (i = 0; i < keys.length; ++i) {
          key = keys[i];
          if (key === 'removeListener') continue;
          this.removeAllListeners(key);
        }
        this.removeAllListeners('removeListener');
        this._events = Object.create(null);
        this._eventsCount = 0;
        return this;
      }

      listeners = events[type];

      if (typeof listeners === 'function') {
        this.removeListener(type, listeners);
      } else if (listeners !== undefined) {
        // LIFO order
        for (i = listeners.length - 1; i >= 0; i--) {
          this.removeListener(type, listeners[i]);
        }
      }

      return this;
    };

function _listeners(target, type, unwrap) {
  var events = target._events;

  if (events === undefined)
    return [];

  var evlistener = events[type];
  if (evlistener === undefined)
    return [];

  if (typeof evlistener === 'function')
    return unwrap ? [evlistener.listener || evlistener] : [evlistener];

  return unwrap ?
    unwrapListeners(evlistener) : arrayClone(evlistener, evlistener.length);
}

EventEmitter.prototype.listeners = function listeners(type) {
  return _listeners(this, type, true);
};

EventEmitter.prototype.rawListeners = function rawListeners(type) {
  return _listeners(this, type, false);
};

EventEmitter.listenerCount = function(emitter, type) {
  if (typeof emitter.listenerCount === 'function') {
    return emitter.listenerCount(type);
  } else {
    return listenerCount.call(emitter, type);
  }
};

EventEmitter.prototype.listenerCount = listenerCount;
function listenerCount(type) {
  var events = this._events;

  if (events !== undefined) {
    var evlistener = events[type];

    if (typeof evlistener === 'function') {
      return 1;
    } else if (evlistener !== undefined) {
      return evlistener.length;
    }
  }

  return 0;
}

EventEmitter.prototype.eventNames = function eventNames() {
  return this._eventsCount > 0 ? ReflectOwnKeys(this._events) : [];
};

function arrayClone(arr, n) {
  var copy = new Array(n);
  for (var i = 0; i < n; ++i)
    copy[i] = arr[i];
  return copy;
}

function spliceOne(list, index) {
  for (; index + 1 < list.length; index++)
    list[index] = list[index + 1];
  list.pop();
}

function unwrapListeners(arr) {
  var ret = new Array(arr.length);
  for (var i = 0; i < ret.length; ++i) {
    ret[i] = arr[i].listener || arr[i];
  }
  return ret;
}


/***/ }),

/***/ "./node_modules/fbjs/lib/emptyFunction.js":
/*!************************************************!*\
  !*** ./node_modules/fbjs/lib/emptyFunction.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 * 
 */

function makeEmptyFunction(arg) {
  return function () {
    return arg;
  };
}

/**
 * This function accepts and discards inputs; it has no side effects. This is
 * primarily useful idiomatically for overridable function endpoints which
 * always need to be callable, since JS lacks a null-call idiom ala Cocoa.
 */
var emptyFunction = function emptyFunction() {};

emptyFunction.thatReturns = makeEmptyFunction;
emptyFunction.thatReturnsFalse = makeEmptyFunction(false);
emptyFunction.thatReturnsTrue = makeEmptyFunction(true);
emptyFunction.thatReturnsNull = makeEmptyFunction(null);
emptyFunction.thatReturnsThis = function () {
  return this;
};
emptyFunction.thatReturnsArgument = function (arg) {
  return arg;
};

module.exports = emptyFunction;

/***/ }),

/***/ "./node_modules/fbjs/lib/emptyObject.js":
/*!**********************************************!*\
  !*** ./node_modules/fbjs/lib/emptyObject.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */



var emptyObject = {};

if (true) {
  Object.freeze(emptyObject);
}

module.exports = emptyObject;

/***/ }),

/***/ "./node_modules/fbjs/lib/invariant.js":
/*!********************************************!*\
  !*** ./node_modules/fbjs/lib/invariant.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2013-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */



/**
 * Use invariant() to assert state which your program assumes to be true.
 *
 * Provide sprintf-style format (only %s is supported) and arguments
 * to provide information about what broke and what you were
 * expecting.
 *
 * The invariant message will be stripped in production, but the invariant
 * will remain to ensure logic does not differ in production.
 */

var validateFormat = function validateFormat(format) {};

if (true) {
  validateFormat = function validateFormat(format) {
    if (format === undefined) {
      throw new Error('invariant requires an error message argument');
    }
  };
}

function invariant(condition, format, a, b, c, d, e, f) {
  validateFormat(format);

  if (!condition) {
    var error;
    if (format === undefined) {
      error = new Error('Minified exception occurred; use the non-minified dev environment ' + 'for the full error message and additional helpful warnings.');
    } else {
      var args = [a, b, c, d, e, f];
      var argIndex = 0;
      error = new Error(format.replace(/%s/g, function () {
        return args[argIndex++];
      }));
      error.name = 'Invariant Violation';
    }

    error.framesToPop = 1; // we don't care about invariant's own frame
    throw error;
  }
}

module.exports = invariant;

/***/ }),

/***/ "./node_modules/fbjs/lib/warning.js":
/*!******************************************!*\
  !*** ./node_modules/fbjs/lib/warning.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 *
 */



var emptyFunction = __webpack_require__(/*! ./emptyFunction */ "./node_modules/fbjs/lib/emptyFunction.js");

/**
 * Similar to invariant but only logs a warning if the condition is not met.
 * This can be used to log issues in development environments in critical
 * paths. Removing the logging code for production environments will keep the
 * same logic and follow the same code paths.
 */

var warning = emptyFunction;

if (true) {
  var printWarning = function printWarning(format) {
    for (var _len = arguments.length, args = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    var argIndex = 0;
    var message = 'Warning: ' + format.replace(/%s/g, function () {
      return args[argIndex++];
    });
    if (typeof console !== 'undefined') {
      console.error(message);
    }
    try {
      // --- Welcome to debugging React ---
      // This error was thrown as a convenience so that you can use this stack
      // to find the callsite that caused this warning to fire.
      throw new Error(message);
    } catch (x) {}
  };

  warning = function warning(condition, format) {
    if (format === undefined) {
      throw new Error('`warning(condition, format, ...args)` requires a warning ' + 'message argument');
    }

    if (format.indexOf('Failed Composite propType: ') === 0) {
      return; // Ignore CompositeComponent proptype check.
    }

    if (!condition) {
      for (var _len2 = arguments.length, args = Array(_len2 > 2 ? _len2 - 2 : 0), _key2 = 2; _key2 < _len2; _key2++) {
        args[_key2 - 2] = arguments[_key2];
      }

      printWarning.apply(undefined, [format].concat(args));
    }
  };
}

module.exports = warning;

/***/ }),

/***/ "./node_modules/hash.js/lib/hash/common.js":
/*!*************************************************!*\
  !*** ./node_modules/hash.js/lib/hash/common.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ./utils */ "./node_modules/hash.js/lib/hash/utils.js");
var assert = __webpack_require__(/*! minimalistic-assert */ "./node_modules/minimalistic-assert/index.js");

function BlockHash() {
  this.pending = null;
  this.pendingTotal = 0;
  this.blockSize = this.constructor.blockSize;
  this.outSize = this.constructor.outSize;
  this.hmacStrength = this.constructor.hmacStrength;
  this.padLength = this.constructor.padLength / 8;
  this.endian = 'big';

  this._delta8 = this.blockSize / 8;
  this._delta32 = this.blockSize / 32;
}
exports.BlockHash = BlockHash;

BlockHash.prototype.update = function update(msg, enc) {
  // Convert message to array, pad it, and join into 32bit blocks
  msg = utils.toArray(msg, enc);
  if (!this.pending)
    this.pending = msg;
  else
    this.pending = this.pending.concat(msg);
  this.pendingTotal += msg.length;

  // Enough data, try updating
  if (this.pending.length >= this._delta8) {
    msg = this.pending;

    // Process pending data in blocks
    var r = msg.length % this._delta8;
    this.pending = msg.slice(msg.length - r, msg.length);
    if (this.pending.length === 0)
      this.pending = null;

    msg = utils.join32(msg, 0, msg.length - r, this.endian);
    for (var i = 0; i < msg.length; i += this._delta32)
      this._update(msg, i, i + this._delta32);
  }

  return this;
};

BlockHash.prototype.digest = function digest(enc) {
  this.update(this._pad());
  assert(this.pending === null);

  return this._digest(enc);
};

BlockHash.prototype._pad = function pad() {
  var len = this.pendingTotal;
  var bytes = this._delta8;
  var k = bytes - ((len + this.padLength) % bytes);
  var res = new Array(k + this.padLength);
  res[0] = 0x80;
  for (var i = 1; i < k; i++)
    res[i] = 0;

  // Append length
  len <<= 3;
  if (this.endian === 'big') {
    for (var t = 8; t < this.padLength; t++)
      res[i++] = 0;

    res[i++] = 0;
    res[i++] = 0;
    res[i++] = 0;
    res[i++] = 0;
    res[i++] = (len >>> 24) & 0xff;
    res[i++] = (len >>> 16) & 0xff;
    res[i++] = (len >>> 8) & 0xff;
    res[i++] = len & 0xff;
  } else {
    res[i++] = len & 0xff;
    res[i++] = (len >>> 8) & 0xff;
    res[i++] = (len >>> 16) & 0xff;
    res[i++] = (len >>> 24) & 0xff;
    res[i++] = 0;
    res[i++] = 0;
    res[i++] = 0;
    res[i++] = 0;

    for (t = 8; t < this.padLength; t++)
      res[i++] = 0;
  }

  return res;
};


/***/ }),

/***/ "./node_modules/hash.js/lib/hash/sha/1.js":
/*!************************************************!*\
  !*** ./node_modules/hash.js/lib/hash/sha/1.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ../utils */ "./node_modules/hash.js/lib/hash/utils.js");
var common = __webpack_require__(/*! ../common */ "./node_modules/hash.js/lib/hash/common.js");
var shaCommon = __webpack_require__(/*! ./common */ "./node_modules/hash.js/lib/hash/sha/common.js");

var rotl32 = utils.rotl32;
var sum32 = utils.sum32;
var sum32_5 = utils.sum32_5;
var ft_1 = shaCommon.ft_1;
var BlockHash = common.BlockHash;

var sha1_K = [
  0x5A827999, 0x6ED9EBA1,
  0x8F1BBCDC, 0xCA62C1D6
];

function SHA1() {
  if (!(this instanceof SHA1))
    return new SHA1();

  BlockHash.call(this);
  this.h = [
    0x67452301, 0xefcdab89, 0x98badcfe,
    0x10325476, 0xc3d2e1f0 ];
  this.W = new Array(80);
}

utils.inherits(SHA1, BlockHash);
module.exports = SHA1;

SHA1.blockSize = 512;
SHA1.outSize = 160;
SHA1.hmacStrength = 80;
SHA1.padLength = 64;

SHA1.prototype._update = function _update(msg, start) {
  var W = this.W;

  for (var i = 0; i < 16; i++)
    W[i] = msg[start + i];

  for(; i < W.length; i++)
    W[i] = rotl32(W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16], 1);

  var a = this.h[0];
  var b = this.h[1];
  var c = this.h[2];
  var d = this.h[3];
  var e = this.h[4];

  for (i = 0; i < W.length; i++) {
    var s = ~~(i / 20);
    var t = sum32_5(rotl32(a, 5), ft_1(s, b, c, d), e, W[i], sha1_K[s]);
    e = d;
    d = c;
    c = rotl32(b, 30);
    b = a;
    a = t;
  }

  this.h[0] = sum32(this.h[0], a);
  this.h[1] = sum32(this.h[1], b);
  this.h[2] = sum32(this.h[2], c);
  this.h[3] = sum32(this.h[3], d);
  this.h[4] = sum32(this.h[4], e);
};

SHA1.prototype._digest = function digest(enc) {
  if (enc === 'hex')
    return utils.toHex32(this.h, 'big');
  else
    return utils.split32(this.h, 'big');
};


/***/ }),

/***/ "./node_modules/hash.js/lib/hash/sha/common.js":
/*!*****************************************************!*\
  !*** ./node_modules/hash.js/lib/hash/sha/common.js ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var utils = __webpack_require__(/*! ../utils */ "./node_modules/hash.js/lib/hash/utils.js");
var rotr32 = utils.rotr32;

function ft_1(s, x, y, z) {
  if (s === 0)
    return ch32(x, y, z);
  if (s === 1 || s === 3)
    return p32(x, y, z);
  if (s === 2)
    return maj32(x, y, z);
}
exports.ft_1 = ft_1;

function ch32(x, y, z) {
  return (x & y) ^ ((~x) & z);
}
exports.ch32 = ch32;

function maj32(x, y, z) {
  return (x & y) ^ (x & z) ^ (y & z);
}
exports.maj32 = maj32;

function p32(x, y, z) {
  return x ^ y ^ z;
}
exports.p32 = p32;

function s0_256(x) {
  return rotr32(x, 2) ^ rotr32(x, 13) ^ rotr32(x, 22);
}
exports.s0_256 = s0_256;

function s1_256(x) {
  return rotr32(x, 6) ^ rotr32(x, 11) ^ rotr32(x, 25);
}
exports.s1_256 = s1_256;

function g0_256(x) {
  return rotr32(x, 7) ^ rotr32(x, 18) ^ (x >>> 3);
}
exports.g0_256 = g0_256;

function g1_256(x) {
  return rotr32(x, 17) ^ rotr32(x, 19) ^ (x >>> 10);
}
exports.g1_256 = g1_256;


/***/ }),

/***/ "./node_modules/hash.js/lib/hash/utils.js":
/*!************************************************!*\
  !*** ./node_modules/hash.js/lib/hash/utils.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var assert = __webpack_require__(/*! minimalistic-assert */ "./node_modules/minimalistic-assert/index.js");
var inherits = __webpack_require__(/*! inherits */ "./node_modules/inherits/inherits_browser.js");

exports.inherits = inherits;

function isSurrogatePair(msg, i) {
  if ((msg.charCodeAt(i) & 0xFC00) !== 0xD800) {
    return false;
  }
  if (i < 0 || i + 1 >= msg.length) {
    return false;
  }
  return (msg.charCodeAt(i + 1) & 0xFC00) === 0xDC00;
}

function toArray(msg, enc) {
  if (Array.isArray(msg))
    return msg.slice();
  if (!msg)
    return [];
  var res = [];
  if (typeof msg === 'string') {
    if (!enc) {
      // Inspired by stringToUtf8ByteArray() in closure-library by Google
      // https://github.com/google/closure-library/blob/8598d87242af59aac233270742c8984e2b2bdbe0/closure/goog/crypt/crypt.js#L117-L143
      // Apache License 2.0
      // https://github.com/google/closure-library/blob/master/LICENSE
      var p = 0;
      for (var i = 0; i < msg.length; i++) {
        var c = msg.charCodeAt(i);
        if (c < 128) {
          res[p++] = c;
        } else if (c < 2048) {
          res[p++] = (c >> 6) | 192;
          res[p++] = (c & 63) | 128;
        } else if (isSurrogatePair(msg, i)) {
          c = 0x10000 + ((c & 0x03FF) << 10) + (msg.charCodeAt(++i) & 0x03FF);
          res[p++] = (c >> 18) | 240;
          res[p++] = ((c >> 12) & 63) | 128;
          res[p++] = ((c >> 6) & 63) | 128;
          res[p++] = (c & 63) | 128;
        } else {
          res[p++] = (c >> 12) | 224;
          res[p++] = ((c >> 6) & 63) | 128;
          res[p++] = (c & 63) | 128;
        }
      }
    } else if (enc === 'hex') {
      msg = msg.replace(/[^a-z0-9]+/ig, '');
      if (msg.length % 2 !== 0)
        msg = '0' + msg;
      for (i = 0; i < msg.length; i += 2)
        res.push(parseInt(msg[i] + msg[i + 1], 16));
    }
  } else {
    for (i = 0; i < msg.length; i++)
      res[i] = msg[i] | 0;
  }
  return res;
}
exports.toArray = toArray;

function toHex(msg) {
  var res = '';
  for (var i = 0; i < msg.length; i++)
    res += zero2(msg[i].toString(16));
  return res;
}
exports.toHex = toHex;

function htonl(w) {
  var res = (w >>> 24) |
            ((w >>> 8) & 0xff00) |
            ((w << 8) & 0xff0000) |
            ((w & 0xff) << 24);
  return res >>> 0;
}
exports.htonl = htonl;

function toHex32(msg, endian) {
  var res = '';
  for (var i = 0; i < msg.length; i++) {
    var w = msg[i];
    if (endian === 'little')
      w = htonl(w);
    res += zero8(w.toString(16));
  }
  return res;
}
exports.toHex32 = toHex32;

function zero2(word) {
  if (word.length === 1)
    return '0' + word;
  else
    return word;
}
exports.zero2 = zero2;

function zero8(word) {
  if (word.length === 7)
    return '0' + word;
  else if (word.length === 6)
    return '00' + word;
  else if (word.length === 5)
    return '000' + word;
  else if (word.length === 4)
    return '0000' + word;
  else if (word.length === 3)
    return '00000' + word;
  else if (word.length === 2)
    return '000000' + word;
  else if (word.length === 1)
    return '0000000' + word;
  else
    return word;
}
exports.zero8 = zero8;

function join32(msg, start, end, endian) {
  var len = end - start;
  assert(len % 4 === 0);
  var res = new Array(len / 4);
  for (var i = 0, k = start; i < res.length; i++, k += 4) {
    var w;
    if (endian === 'big')
      w = (msg[k] << 24) | (msg[k + 1] << 16) | (msg[k + 2] << 8) | msg[k + 3];
    else
      w = (msg[k + 3] << 24) | (msg[k + 2] << 16) | (msg[k + 1] << 8) | msg[k];
    res[i] = w >>> 0;
  }
  return res;
}
exports.join32 = join32;

function split32(msg, endian) {
  var res = new Array(msg.length * 4);
  for (var i = 0, k = 0; i < msg.length; i++, k += 4) {
    var m = msg[i];
    if (endian === 'big') {
      res[k] = m >>> 24;
      res[k + 1] = (m >>> 16) & 0xff;
      res[k + 2] = (m >>> 8) & 0xff;
      res[k + 3] = m & 0xff;
    } else {
      res[k + 3] = m >>> 24;
      res[k + 2] = (m >>> 16) & 0xff;
      res[k + 1] = (m >>> 8) & 0xff;
      res[k] = m & 0xff;
    }
  }
  return res;
}
exports.split32 = split32;

function rotr32(w, b) {
  return (w >>> b) | (w << (32 - b));
}
exports.rotr32 = rotr32;

function rotl32(w, b) {
  return (w << b) | (w >>> (32 - b));
}
exports.rotl32 = rotl32;

function sum32(a, b) {
  return (a + b) >>> 0;
}
exports.sum32 = sum32;

function sum32_3(a, b, c) {
  return (a + b + c) >>> 0;
}
exports.sum32_3 = sum32_3;

function sum32_4(a, b, c, d) {
  return (a + b + c + d) >>> 0;
}
exports.sum32_4 = sum32_4;

function sum32_5(a, b, c, d, e) {
  return (a + b + c + d + e) >>> 0;
}
exports.sum32_5 = sum32_5;

function sum64(buf, pos, ah, al) {
  var bh = buf[pos];
  var bl = buf[pos + 1];

  var lo = (al + bl) >>> 0;
  var hi = (lo < al ? 1 : 0) + ah + bh;
  buf[pos] = hi >>> 0;
  buf[pos + 1] = lo;
}
exports.sum64 = sum64;

function sum64_hi(ah, al, bh, bl) {
  var lo = (al + bl) >>> 0;
  var hi = (lo < al ? 1 : 0) + ah + bh;
  return hi >>> 0;
}
exports.sum64_hi = sum64_hi;

function sum64_lo(ah, al, bh, bl) {
  var lo = al + bl;
  return lo >>> 0;
}
exports.sum64_lo = sum64_lo;

function sum64_4_hi(ah, al, bh, bl, ch, cl, dh, dl) {
  var carry = 0;
  var lo = al;
  lo = (lo + bl) >>> 0;
  carry += lo < al ? 1 : 0;
  lo = (lo + cl) >>> 0;
  carry += lo < cl ? 1 : 0;
  lo = (lo + dl) >>> 0;
  carry += lo < dl ? 1 : 0;

  var hi = ah + bh + ch + dh + carry;
  return hi >>> 0;
}
exports.sum64_4_hi = sum64_4_hi;

function sum64_4_lo(ah, al, bh, bl, ch, cl, dh, dl) {
  var lo = al + bl + cl + dl;
  return lo >>> 0;
}
exports.sum64_4_lo = sum64_4_lo;

function sum64_5_hi(ah, al, bh, bl, ch, cl, dh, dl, eh, el) {
  var carry = 0;
  var lo = al;
  lo = (lo + bl) >>> 0;
  carry += lo < al ? 1 : 0;
  lo = (lo + cl) >>> 0;
  carry += lo < cl ? 1 : 0;
  lo = (lo + dl) >>> 0;
  carry += lo < dl ? 1 : 0;
  lo = (lo + el) >>> 0;
  carry += lo < el ? 1 : 0;

  var hi = ah + bh + ch + dh + eh + carry;
  return hi >>> 0;
}
exports.sum64_5_hi = sum64_5_hi;

function sum64_5_lo(ah, al, bh, bl, ch, cl, dh, dl, eh, el) {
  var lo = al + bl + cl + dl + el;

  return lo >>> 0;
}
exports.sum64_5_lo = sum64_5_lo;

function rotr64_hi(ah, al, num) {
  var r = (al << (32 - num)) | (ah >>> num);
  return r >>> 0;
}
exports.rotr64_hi = rotr64_hi;

function rotr64_lo(ah, al, num) {
  var r = (ah << (32 - num)) | (al >>> num);
  return r >>> 0;
}
exports.rotr64_lo = rotr64_lo;

function shr64_hi(ah, al, num) {
  return ah >>> num;
}
exports.shr64_hi = shr64_hi;

function shr64_lo(ah, al, num) {
  var r = (ah << (32 - num)) | (al >>> num);
  return r >>> 0;
}
exports.shr64_lo = shr64_lo;


/***/ }),

/***/ "./node_modules/i18n-calypso/index.js":
/*!********************************************!*\
  !*** ./node_modules/i18n-calypso/index.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * Internal dependencies
 */
var I18N = __webpack_require__( /*! ./lib */ "./node_modules/i18n-calypso/lib/index.js" ),
	i18n = new I18N();

module.exports = {
	moment: i18n.moment,
	numberFormat: i18n.numberFormat.bind( i18n ),
	translate: i18n.translate.bind( i18n ),
	configure: i18n.configure.bind( i18n ),
	setLocale: i18n.setLocale.bind( i18n ),
	getLocale: i18n.getLocale.bind( i18n ),
	getLocaleSlug: i18n.getLocaleSlug.bind( i18n ),
	addTranslations: i18n.addTranslations.bind( i18n ),
	reRenderTranslations: i18n.reRenderTranslations.bind( i18n ),
	registerComponentUpdateHook: i18n.registerComponentUpdateHook.bind( i18n ),
	registerTranslateHook: i18n.registerTranslateHook.bind( i18n ),
	state: i18n.state,
	stateObserver: i18n.stateObserver,
	on: i18n.stateObserver.on.bind(i18n.stateObserver),
	off: i18n.stateObserver.removeListener.bind(i18n.stateObserver),
	emit: i18n.stateObserver.emit.bind(i18n.stateObserver),
	localize: __webpack_require__( /*! ./lib/localize */ "./node_modules/i18n-calypso/lib/localize/index.js" )( i18n ),
	$this: i18n,
	I18N: I18N
};



/***/ }),

/***/ "./node_modules/i18n-calypso/lib/index.js":
/*!************************************************!*\
  !*** ./node_modules/i18n-calypso/lib/index.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * External dependencies
 */
var debug = __webpack_require__( /*! debug */ "./node_modules/i18n-calypso/node_modules/debug/src/browser.js" )( 'i18n-calypso' ),
	Jed = __webpack_require__( /*! jed */ "./node_modules/jed/jed.js" ),
	moment = __webpack_require__( /*! moment-timezone */ "moment-timezone" ),
	sha1 = __webpack_require__( /*! hash.js/lib/hash/sha/1 */ "./node_modules/hash.js/lib/hash/sha/1.js" ),
	EventEmitter = __webpack_require__( /*! events */ "./node_modules/events/events.js" ).EventEmitter,
	interpolateComponents = __webpack_require__( /*! interpolate-components */ "./node_modules/interpolate-components/lib/index.js" ).default,
	LRU = __webpack_require__( /*! lru */ "./node_modules/lru/index.js" ),
	assign = __webpack_require__( /*! lodash.assign */ "./node_modules/lodash.assign/index.js" );

/**
 * Internal dependencies
 */
var numberFormatPHPJS = __webpack_require__( /*! ./number-format */ "./node_modules/i18n-calypso/lib/number-format.js" );

/**
 * Constants
 */
var decimal_point_translation_key = 'number_format_decimals',
	thousands_sep_translation_key = 'number_format_thousands_sep';

var translationLookup = [
	// By default don't modify the options when looking up translations.
	function( options ) {
		return options;
	}
];

var hashCache = {};

// raise a console warning
function warn() {
	if ( ! I18N.throwErrors ) {
		return;
	}
	if ( 'undefined' !== typeof window && window.console && window.console.warn ) {
		window.console.warn.apply( window.console, arguments );
	}
}

// turns Function.arguments into an array
function simpleArguments( args ) {
	return Array.prototype.slice.call( args );
}

/**
 * Coerce the possible arguments and normalize to a single object
 * @param  {arguments} args - arguments passed in from `translate()`
 * @return {object}         - a single object describing translation needs
 */
function normalizeTranslateArguments( args ) {
	var original = args[ 0 ],
		options = {},
		i;

	// warn about older deprecated syntax
	if ( typeof original !== 'string' || args.length > 3 || ( args.length > 2 && typeof args[ 1 ] === 'object' && typeof args[ 2 ] === 'object' ) ) {
		warn( 'Deprecated Invocation: `translate()` accepts ( string, [string], [object] ). These arguments passed:', simpleArguments( args ), '. See https://github.com/Automattic/i18n-calypso#translate-method' );
	}
	if ( args.length === 2 && typeof original === 'string' && typeof args[ 1 ] === 'string' ) {
		warn( 'Invalid Invocation: `translate()` requires an options object for plural translations, but passed:', simpleArguments( args ) );
	}

	// options could be in position 0, 1, or 2
	// sending options as the first object is deprecated and will raise a warning
	for ( i = 0; i < args.length; i++ ) {
		if ( typeof args[ i ] === 'object' ) {
			options = args[ i ];
		}
	}

	// `original` can be passed as first parameter or as part of the options object
	// though passing original as part of the options is a deprecated approach and will be removed
	if ( typeof original === 'string' ) {
		options.original = original;
	} else if ( typeof options.original === 'object' ) {
		options.plural = options.original.plural;
		options.count = options.original.count;
		options.original = options.original.single;
	}
	if ( typeof args[ 1 ] === 'string' ) {
		options.plural = args[ 1 ];
	}

	if ( typeof options.original === 'undefined' ) {
		throw new Error( 'Translate called without a `string` value as first argument.' );
	}

	return options;
}

/**
 * Pull the right set of arguments for the Jed method
 * @param  {string} jedMethod Name of jed gettext-style method. [See docs](http://slexaxton.github.io/Jed/)
 * @param  {[object]} props     properties passed into `translate()` method
 * @return {[array]}           array of properties to pass into gettext-style method
 */
function getJedArgs( jedMethod, props ) {
	switch ( jedMethod ) {
		case 'gettext':
			return [ props.original ];
		case 'ngettext':
			return [ props.original, props.plural, props.count ];
		case 'npgettext':
			return [ props.context, props.original, props.plural, props.count ];
		case 'pgettext':
			return [ props.context, props.original ];
	}

	return [];
}

/**
 * Takes translate options object and coerces to a Jed request to retrieve translation
 * @param  {object} jed     - jed data object
 * @param  {object} options - object describing translation
 * @return {string}         - the returned translation from Jed
 */
function getTranslationFromJed( jed, options ) {
	var jedMethod = 'gettext',
		jedArgs;

	if ( options.context ) {
		jedMethod = 'p' + jedMethod;
	}

	if ( typeof options.original === 'string' && typeof options.plural === 'string' ) {
		jedMethod = 'n' + jedMethod;
	}

	jedArgs = getJedArgs( jedMethod, options );

	return jed[ jedMethod ].apply( jed, jedArgs );
}

function getTranslation( i18n, options ) {
	var i, lookup;

	for ( i = translationLookup.length - 1; i >= 0; i-- ) {
		lookup = translationLookup[ i ]( assign( {}, options ) );
		// Only get the translation from jed if it exists.
		if ( i18n.state.locale[ lookup.original ] ) {
			return getTranslationFromJed( i18n.state.jed, lookup );
		}
	}

	return null;
}


function I18N() {
	if( ! ( this instanceof I18N ) ) {
		return new I18N();
	}
	this.defaultLocaleSlug = 'en';
	this.state = {
		numberFormatSettings: {},
		jed: undefined,
		locale: undefined,
		localeSlug: undefined,
		translations: LRU( { max: 100 } )
	};
	this.componentUpdateHooks = [];
	this.translateHooks = [];
	this.stateObserver = new EventEmitter();
	// Because the higher-order component can wrap a ton of React components,
	// we need to bump the number of listeners to infinity and beyond
	// FIXME: still valid?
	this.stateObserver.setMaxListeners( 0 );
	// default configuration
	this.configure();
}

I18N.throwErrors = false;
I18N.prototype.moment = moment;

/**
 * Formats numbers using locale settings and/or passed options
 * @param  {String|Number|Int}  number to format (required)
 * @param  {Int|object} options  Number of decimal places or options object (optional)
 * @return {string}         Formatted number as string
 */
I18N.prototype.numberFormat = function( number ) {
	var options = arguments[ 1 ] || {},
		decimals = ( typeof options === 'number' ) ? options : options.decimals || 0,
		decPoint = options.decPoint || this.state.numberFormatSettings.decimal_point || '.',
		thousandsSep = options.thousandsSep || this.state.numberFormatSettings.thousands_sep || ',';

	return numberFormatPHPJS( number, decimals, decPoint, thousandsSep );
};

I18N.prototype.configure = function( options ) {
	assign( this, options || {} );
	this.setLocale();
};

I18N.prototype.setLocale = function( localeData ) {
	if ( localeData && localeData[ '' ] && localeData[ '' ][ 'key-hash' ] ) {
		var hashLength, minHashLength, maxHashLength, keyHash = localeData[ '' ][ 'key-hash' ];

		var transform = function( string, hashLength ) {
			const lookupPrefix = hashLength === false ? '' : String( hashLength );
			if ( typeof hashCache[ lookupPrefix + string ] !== 'undefined' ) {
				return hashCache[ lookupPrefix + string ];
			}
			var hash = sha1().update( string ).digest('hex');

			if ( hashLength ) {
				return hashCache[ lookupPrefix + string ] = hash.substr( 0, hashLength );
			}

			return hashCache[ lookupPrefix + string ] = hash;
		};

		var generateLookup = function( hashLength ) {
			return function( options ) {
				if ( options.context ) {
					options.original = transform( options.context + String.fromCharCode( 4 ) + options.original, hashLength );
					delete options.context;
				} else {
					options.original = transform( options.original, hashLength );
				}

				return options;
			};
		}

		if ( keyHash.substr( 0, 4 ) === 'sha1' ) {
			if ( keyHash.length === 4 ) {
				translationLookup.push( generateLookup( false ) );
			} else {
				var variableHashLengthPos = keyHash.substr( 5 ).indexOf( '-' );
				if ( variableHashLengthPos < 0 ) {
					hashLength = Number( keyHash.substr( 5 ) );
					translationLookup.push( generateLookup( hashLength ) );
				} else {
					minHashLength = Number( keyHash.substr( 5, variableHashLengthPos ) );
					maxHashLength = Number( keyHash.substr( 6 + variableHashLengthPos ) );

					for ( hashLength = minHashLength; hashLength <= maxHashLength; hashLength++ ) {
						translationLookup.push( generateLookup( hashLength ) );
					}
				}
			}
		}
	}

	// if localeData is not given, assumes default locale and reset
	if ( ! localeData || ! localeData[ '' ].localeSlug ) {
		this.state.locale = { '': { localeSlug: this.defaultLocaleSlug } };
	} else if ( localeData[ '' ].localeSlug === this.state.localeSlug ) {
		// Exit if same data as current (comparing references only)
		if ( localeData === this.state.locale ) {
			return;
		}

		// merge new data into existing one
		assign( this.state.locale, localeData );
	} else {
		this.state.locale = assign( {}, localeData );
	}

	this.state.localeSlug = this.state.locale[ '' ].localeSlug;

	this.state.jed = new Jed( {
		locale_data: {
			messages: this.state.locale
		}
	} );

	moment.locale( this.state.localeSlug );

	// Updates numberFormat preferences with settings from translations
	this.state.numberFormatSettings.decimal_point = getTranslationFromJed(
		this.state.jed,
		normalizeTranslateArguments( [ decimal_point_translation_key ] )
	);
	this.state.numberFormatSettings.thousands_sep = getTranslationFromJed(
		this.state.jed,
		normalizeTranslateArguments( [ thousands_sep_translation_key ] )
	);

	// If translation isn't set, define defaults.
	if ( this.state.numberFormatSettings.decimal_point === decimal_point_translation_key ) {
		this.state.numberFormatSettings.decimal_point = '.';
	}

	if ( this.state.numberFormatSettings.thousands_sep === thousands_sep_translation_key ) {
		this.state.numberFormatSettings.thousands_sep = ',';
	}

	this.state.translations.clear();
	this.stateObserver.emit( 'change' );
};

I18N.prototype.getLocale = function() {
	return this.state.locale;
};

/**
 * Get the current locale slug.
 * @returns {string} The string representing the currently loaded locale
 **/
I18N.prototype.getLocaleSlug = function() {
	return this.state.localeSlug;
};


/**
 * Adds new translations to the locale data, overwriting any existing translations with a matching key
 **/
I18N.prototype.addTranslations = function( localeData ) {
	for ( var prop in localeData ) {
		if ( prop !== '' ) {
			this.state.jed.options.locale_data.messages[prop] = localeData[prop];
		}
	}

	this.state.translations.clear();
	this.stateObserver.emit( 'change' );
};


/**
 * Checks whether the given original has a translation. Parameters are the same as for translate().
 *
 * @param  {string} original  the string to translate
 * @param  {string} plural    the plural string to translate (if applicable), original used as singular
 * @param  {object} options   properties describing translation requirements for given text
 * @return {boolean} whether a translation exists
 */
I18N.prototype.hasTranslation = function() {
	return !! getTranslation( this, normalizeTranslateArguments( arguments ) );
}

/**
 * Exposes single translation method, which is converted into its respective Jed method.
 * See sibling README
 * @param  {string} original  the string to translate
 * @param  {string} plural    the plural string to translate (if applicable), original used as singular
 * @param  {object} options   properties describing translation requirements for given text
 * @return {string|React-components} translated text or an object containing React children that can be inserted into a parent component
 */
I18N.prototype.translate = function() {
	var options, translation, sprintfArgs, errorMethod, optionsString, cacheable;

	options = normalizeTranslateArguments( arguments );

	cacheable = ! options.components;
	if ( cacheable ) {
		// Safe JSON stringification here to catch Circular JSON error
		// caused by passing a React component into args where only scalars are allowed
		try {
			optionsString = JSON.stringify( options );
		} catch ( e ) {
			cacheable = false;
		}

		if ( optionsString ) {
			translation = this.state.translations.get( optionsString );
			// Return the cached translation.
			if ( translation ) {
				return translation;
			}
		}
	}

	translation = getTranslation( this, options );
	if ( ! translation ) {
		// This purposefully calls jed for a case where there is no translation,
		// so that jed gives us the expected object with English text.
		translation = getTranslationFromJed( this.state.jed, options );
	}

	// handle any string substitution
	if ( options.args ) {
		sprintfArgs = ( Array.isArray( options.args ) ) ? options.args.slice( 0 ) : [ options.args ];
		sprintfArgs.unshift( translation );
		try {
			translation = Jed.sprintf.apply( Jed, sprintfArgs );
		} catch ( error ) {
			if ( ! window || ! window.console ) {
				return;
			}
			errorMethod = this.throwErrors ? 'error' : 'warn';
			if ( typeof error !== 'string' ) {
				window.console[ errorMethod ]( error );
			} else {
				window.console[ errorMethod ]( 'i18n sprintf error:', sprintfArgs );
			}
		}
	}

	// interpolate any components
	if ( options.components ) {
		translation = interpolateComponents( {
			mixedString: translation,
			components: options.components,
			throwErrors: this.throwErrors
		} );
	}

	// run any necessary hooks
	this.translateHooks.forEach( function( hook ) {
		translation = hook( translation, options );
	} );

	if ( cacheable ) {
		this.state.translations.set( optionsString, translation );
	}

	return translation;
};

/**
 * Causes i18n to re-render all translations.
 *
 * This can be necessary if an extension makes changes that i18n is unaware of
 * and needs those changes manifested immediately (e.g. adding an important
 * translation hook, or modifying the behaviour of an existing hook).
 *
 * If at all possible, react components should try to use the more local
 * updateTranslation() function inherited from the mixin.
 */
I18N.prototype.reRenderTranslations = function() {
	debug( 'Re-rendering all translations due to external request' );
	this.state.translations.clear();
	this.stateObserver.emit( 'change' );
};

I18N.prototype.registerComponentUpdateHook = function( callback ) {
	this.componentUpdateHooks.push( callback );
};

I18N.prototype.registerTranslateHook = function( callback ) {
	this.translateHooks.push( callback );
};

module.exports = I18N;


/***/ }),

/***/ "./node_modules/i18n-calypso/lib/localize/index.js":
/*!*********************************************************!*\
  !*** ./node_modules/i18n-calypso/lib/localize/index.js ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var React = __webpack_require__( /*! react */ "react" ),
	assign = __webpack_require__( /*! lodash.assign */ "./node_modules/lodash.assign/index.js" ),
	createClass = __webpack_require__( /*! create-react-class */ "./node_modules/create-react-class/index.js" );

/**
 * Localize a React component
 * @param ComposedComponent
 * @returns A new Localized React Component
 */
module.exports = function( i18n ) {
	var i18nProps = {
		moment: i18n.moment,
		numberFormat: i18n.numberFormat.bind( i18n ),
		translate: i18n.translate.bind( i18n )
	};

	return function( ComposedComponent ) {
		var componentName = ComposedComponent.displayName || ComposedComponent.name || '';

		var component = createClass({
			displayName: 'Localized(' + componentName + ')',

			componentDidMount: function() {
				this.boundForceUpdate = this.forceUpdate.bind( this );
				i18n.stateObserver.addListener( 'change', this.boundForceUpdate );
			},

			componentWillUnmount: function() {
				// in some cases, componentWillUnmount is called before componentDidMount
				// Supposedly fixed in React 15.1.0: https://github.com/facebook/react/issues/2410
				if ( this.boundForceUpdate ) {
					i18n.stateObserver.removeListener( 'change', this.boundForceUpdate );
				}
			},

			render: function() {
				var props = assign( {
					locale: i18n.getLocaleSlug()
				}, this.props, i18nProps );
				return React.createElement( ComposedComponent, props );
			}
		} );
		component._composedComponent = ComposedComponent;
		return component;
	};
};


/***/ }),

/***/ "./node_modules/i18n-calypso/lib/number-format.js":
/*!********************************************************!*\
  !*** ./node_modules/i18n-calypso/lib/number-format.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Exposes number format capability
 *
 * @copyright Copyright (c) 2013 Kevin van Zonneveld (http://kvz.io) and Contributors (http://phpjs.org/authors).
 * @license See CREDITS.md
 * @see https://github.com/kvz/phpjs/blob/ffe1356af23a6f2512c84c954dd4e828e92579fa/functions/strings/number_format.js
 */
function number_format( number, decimals, dec_point, thousands_sep ) {
  number = ( number + '' )
    .replace( /[^0-9+\-Ee.]/g, '' );
  var n = ! isFinite( +number ) ? 0 : +number,
    prec = ! isFinite( +decimals ) ? 0 : Math.abs( decimals ),
    sep = ( typeof thousands_sep === 'undefined' ) ? ',' : thousands_sep,
    dec = ( typeof dec_point === 'undefined' ) ? '.' : dec_point,
    s = '',
    toFixedFix = function( n, prec ) {
      var k = Math.pow( 10, prec );
      return '' + ( Math.round( n * k ) / k )
          .toFixed( prec );
    };
  // Fix for IE parseFloat(0.55).toFixed(0) = 0;
  s = ( prec ? toFixedFix( n, prec ) : '' + Math.round( n ) )
    .split( '.' );
  if ( s[ 0 ].length > 3 ) {
    s[ 0 ] = s[ 0 ].replace( /\B(?=(?:\d{3})+(?!\d))/g, sep );
  }
  if ( ( s[ 1 ] || '' )
        .length < prec ) {
    s[ 1 ] = s[ 1 ] || '';
    s[ 1 ] += new Array( prec - s[ 1 ].length + 1 )
      .join( '0' );
  }
  return s.join( dec );
}

module.exports = number_format;


/***/ }),

/***/ "./node_modules/i18n-calypso/node_modules/debug/src/browser.js":
/*!*********************************************************************!*\
  !*** ./node_modules/i18n-calypso/node_modules/debug/src/browser.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(process) {

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/* eslint-env browser */

/**
 * This is the web browser implementation of `debug()`.
 */
exports.log = log;
exports.formatArgs = formatArgs;
exports.save = save;
exports.load = load;
exports.useColors = useColors;
exports.storage = localstorage();
/**
 * Colors.
 */

exports.colors = ['#0000CC', '#0000FF', '#0033CC', '#0033FF', '#0066CC', '#0066FF', '#0099CC', '#0099FF', '#00CC00', '#00CC33', '#00CC66', '#00CC99', '#00CCCC', '#00CCFF', '#3300CC', '#3300FF', '#3333CC', '#3333FF', '#3366CC', '#3366FF', '#3399CC', '#3399FF', '#33CC00', '#33CC33', '#33CC66', '#33CC99', '#33CCCC', '#33CCFF', '#6600CC', '#6600FF', '#6633CC', '#6633FF', '#66CC00', '#66CC33', '#9900CC', '#9900FF', '#9933CC', '#9933FF', '#99CC00', '#99CC33', '#CC0000', '#CC0033', '#CC0066', '#CC0099', '#CC00CC', '#CC00FF', '#CC3300', '#CC3333', '#CC3366', '#CC3399', '#CC33CC', '#CC33FF', '#CC6600', '#CC6633', '#CC9900', '#CC9933', '#CCCC00', '#CCCC33', '#FF0000', '#FF0033', '#FF0066', '#FF0099', '#FF00CC', '#FF00FF', '#FF3300', '#FF3333', '#FF3366', '#FF3399', '#FF33CC', '#FF33FF', '#FF6600', '#FF6633', '#FF9900', '#FF9933', '#FFCC00', '#FFCC33'];
/**
 * Currently only WebKit-based Web Inspectors, Firefox >= v31,
 * and the Firebug extension (any Firefox version) are known
 * to support "%c" CSS customizations.
 *
 * TODO: add a `localStorage` variable to explicitly enable/disable colors
 */
// eslint-disable-next-line complexity

function useColors() {
  // NB: In an Electron preload script, document will be defined but not fully
  // initialized. Since we know we're in Chrome, we'll just detect this case
  // explicitly
  if (typeof window !== 'undefined' && window.process && (window.process.type === 'renderer' || window.process.__nwjs)) {
    return true;
  } // Internet Explorer and Edge do not support colors.


  if (typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/(edge|trident)\/(\d+)/)) {
    return false;
  } // Is webkit? http://stackoverflow.com/a/16459606/376773
  // document is undefined in react-native: https://github.com/facebook/react-native/pull/1632


  return typeof document !== 'undefined' && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance || // Is firebug? http://stackoverflow.com/a/398120/376773
  typeof window !== 'undefined' && window.console && (window.console.firebug || window.console.exception && window.console.table) || // Is firefox >= v31?
  // https://developer.mozilla.org/en-US/docs/Tools/Web_Console#Styling_messages
  typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31 || // Double check webkit in userAgent just in case we are in a worker
  typeof navigator !== 'undefined' && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/);
}
/**
 * Colorize log arguments if enabled.
 *
 * @api public
 */


function formatArgs(args) {
  args[0] = (this.useColors ? '%c' : '') + this.namespace + (this.useColors ? ' %c' : ' ') + args[0] + (this.useColors ? '%c ' : ' ') + '+' + module.exports.humanize(this.diff);

  if (!this.useColors) {
    return;
  }

  var c = 'color: ' + this.color;
  args.splice(1, 0, c, 'color: inherit'); // The final "%c" is somewhat tricky, because there could be other
  // arguments passed either before or after the %c, so we need to
  // figure out the correct index to insert the CSS into

  var index = 0;
  var lastC = 0;
  args[0].replace(/%[a-zA-Z%]/g, function (match) {
    if (match === '%%') {
      return;
    }

    index++;

    if (match === '%c') {
      // We only are interested in the *last* %c
      // (the user may have provided their own)
      lastC = index;
    }
  });
  args.splice(lastC, 0, c);
}
/**
 * Invokes `console.log()` when available.
 * No-op when `console.log` is not a "function".
 *
 * @api public
 */


function log() {
  var _console;

  // This hackery is required for IE8/9, where
  // the `console.log` function doesn't have 'apply'
  return (typeof console === "undefined" ? "undefined" : _typeof(console)) === 'object' && console.log && (_console = console).log.apply(_console, arguments);
}
/**
 * Save `namespaces`.
 *
 * @param {String} namespaces
 * @api private
 */


function save(namespaces) {
  try {
    if (namespaces) {
      exports.storage.setItem('debug', namespaces);
    } else {
      exports.storage.removeItem('debug');
    }
  } catch (error) {// Swallow
    // XXX (@Qix-) should we be logging these?
  }
}
/**
 * Load `namespaces`.
 *
 * @return {String} returns the previously persisted debug modes
 * @api private
 */


function load() {
  var r;

  try {
    r = exports.storage.getItem('debug');
  } catch (error) {} // Swallow
  // XXX (@Qix-) should we be logging these?
  // If debug isn't set in LS, and we're in Electron, try to load $DEBUG


  if (!r && typeof process !== 'undefined' && 'env' in process) {
    r = Object({"NODE_ENV":"development","PLUGIN_CTX":"pro","env":"development"}).DEBUG;
  }

  return r;
}
/**
 * Localstorage attempts to return the localstorage.
 *
 * This is necessary because safari throws
 * when a user disables cookies/localstorage
 * and you attempt to access it.
 *
 * @return {LocalStorage}
 * @api private
 */


function localstorage() {
  try {
    // TVMLKit (Apple TV JS Runtime) does not have a window object, just localStorage in the global context
    // The Browser also has localStorage in the global context.
    return localStorage;
  } catch (error) {// Swallow
    // XXX (@Qix-) should we be logging these?
  }
}

module.exports = __webpack_require__(/*! ./common */ "./node_modules/i18n-calypso/node_modules/debug/src/common.js")(exports);
var formatters = module.exports.formatters;
/**
 * Map %j to `JSON.stringify()`, since no Web Inspectors do that by default.
 */

formatters.j = function (v) {
  try {
    return JSON.stringify(v);
  } catch (error) {
    return '[UnexpectedJSONParseError]: ' + error.message;
  }
};


/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/i18n-calypso/node_modules/debug/src/common.js":
/*!********************************************************************!*\
  !*** ./node_modules/i18n-calypso/node_modules/debug/src/common.js ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/**
 * This is the common logic for both the Node.js and web browser
 * implementations of `debug()`.
 */
function setup(env) {
  createDebug.debug = createDebug;
  createDebug.default = createDebug;
  createDebug.coerce = coerce;
  createDebug.disable = disable;
  createDebug.enable = enable;
  createDebug.enabled = enabled;
  createDebug.humanize = __webpack_require__(/*! ms */ "./node_modules/ms/index.js");
  Object.keys(env).forEach(function (key) {
    createDebug[key] = env[key];
  });
  /**
  * Active `debug` instances.
  */

  createDebug.instances = [];
  /**
  * The currently active debug mode names, and names to skip.
  */

  createDebug.names = [];
  createDebug.skips = [];
  /**
  * Map of special "%n" handling functions, for the debug "format" argument.
  *
  * Valid key names are a single, lower or upper-case letter, i.e. "n" and "N".
  */

  createDebug.formatters = {};
  /**
  * Selects a color for a debug namespace
  * @param {String} namespace The namespace string for the for the debug instance to be colored
  * @return {Number|String} An ANSI color code for the given namespace
  * @api private
  */

  function selectColor(namespace) {
    var hash = 0;

    for (var i = 0; i < namespace.length; i++) {
      hash = (hash << 5) - hash + namespace.charCodeAt(i);
      hash |= 0; // Convert to 32bit integer
    }

    return createDebug.colors[Math.abs(hash) % createDebug.colors.length];
  }

  createDebug.selectColor = selectColor;
  /**
  * Create a debugger with the given `namespace`.
  *
  * @param {String} namespace
  * @return {Function}
  * @api public
  */

  function createDebug(namespace) {
    var prevTime;

    function debug() {
      // Disabled?
      if (!debug.enabled) {
        return;
      }

      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      var self = debug; // Set `diff` timestamp

      var curr = Number(new Date());
      var ms = curr - (prevTime || curr);
      self.diff = ms;
      self.prev = prevTime;
      self.curr = curr;
      prevTime = curr;
      args[0] = createDebug.coerce(args[0]);

      if (typeof args[0] !== 'string') {
        // Anything else let's inspect with %O
        args.unshift('%O');
      } // Apply any `formatters` transformations


      var index = 0;
      args[0] = args[0].replace(/%([a-zA-Z%])/g, function (match, format) {
        // If we encounter an escaped % then don't increase the array index
        if (match === '%%') {
          return match;
        }

        index++;
        var formatter = createDebug.formatters[format];

        if (typeof formatter === 'function') {
          var val = args[index];
          match = formatter.call(self, val); // Now we need to remove `args[index]` since it's inlined in the `format`

          args.splice(index, 1);
          index--;
        }

        return match;
      }); // Apply env-specific formatting (colors, etc.)

      createDebug.formatArgs.call(self, args);
      var logFn = self.log || createDebug.log;
      logFn.apply(self, args);
    }

    debug.namespace = namespace;
    debug.enabled = createDebug.enabled(namespace);
    debug.useColors = createDebug.useColors();
    debug.color = selectColor(namespace);
    debug.destroy = destroy;
    debug.extend = extend; // Debug.formatArgs = formatArgs;
    // debug.rawLog = rawLog;
    // env-specific initialization logic for debug instances

    if (typeof createDebug.init === 'function') {
      createDebug.init(debug);
    }

    createDebug.instances.push(debug);
    return debug;
  }

  function destroy() {
    var index = createDebug.instances.indexOf(this);

    if (index !== -1) {
      createDebug.instances.splice(index, 1);
      return true;
    }

    return false;
  }

  function extend(namespace, delimiter) {
    return createDebug(this.namespace + (typeof delimiter === 'undefined' ? ':' : delimiter) + namespace);
  }
  /**
  * Enables a debug mode by namespaces. This can include modes
  * separated by a colon and wildcards.
  *
  * @param {String} namespaces
  * @api public
  */


  function enable(namespaces) {
    createDebug.save(namespaces);
    createDebug.names = [];
    createDebug.skips = [];
    var i;
    var split = (typeof namespaces === 'string' ? namespaces : '').split(/[\s,]+/);
    var len = split.length;

    for (i = 0; i < len; i++) {
      if (!split[i]) {
        // ignore empty strings
        continue;
      }

      namespaces = split[i].replace(/\*/g, '.*?');

      if (namespaces[0] === '-') {
        createDebug.skips.push(new RegExp('^' + namespaces.substr(1) + '$'));
      } else {
        createDebug.names.push(new RegExp('^' + namespaces + '$'));
      }
    }

    for (i = 0; i < createDebug.instances.length; i++) {
      var instance = createDebug.instances[i];
      instance.enabled = createDebug.enabled(instance.namespace);
    }
  }
  /**
  * Disable debug output.
  *
  * @api public
  */


  function disable() {
    createDebug.enable('');
  }
  /**
  * Returns true if the given mode name is enabled, false otherwise.
  *
  * @param {String} name
  * @return {Boolean}
  * @api public
  */


  function enabled(name) {
    if (name[name.length - 1] === '*') {
      return true;
    }

    var i;
    var len;

    for (i = 0, len = createDebug.skips.length; i < len; i++) {
      if (createDebug.skips[i].test(name)) {
        return false;
      }
    }

    for (i = 0, len = createDebug.names.length; i < len; i++) {
      if (createDebug.names[i].test(name)) {
        return true;
      }
    }

    return false;
  }
  /**
  * Coerce `val`.
  *
  * @param {Mixed} val
  * @return {Mixed}
  * @api private
  */


  function coerce(val) {
    if (val instanceof Error) {
      return val.stack || val.message;
    }

    return val;
  }

  createDebug.enable(createDebug.load());
  return createDebug;
}

module.exports = setup;



/***/ }),

/***/ "./node_modules/inherits/inherits_browser.js":
/*!***************************************************!*\
  !*** ./node_modules/inherits/inherits_browser.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

if (typeof Object.create === 'function') {
  // implementation from standard node.js 'util' module
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    ctor.prototype = Object.create(superCtor.prototype, {
      constructor: {
        value: ctor,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
  };
} else {
  // old school shim for old browsers
  module.exports = function inherits(ctor, superCtor) {
    ctor.super_ = superCtor
    var TempCtor = function () {}
    TempCtor.prototype = superCtor.prototype
    ctor.prototype = new TempCtor()
    ctor.prototype.constructor = ctor
  }
}


/***/ }),

/***/ "./node_modules/interpolate-components/lib/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/interpolate-components/lib/index.js ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
	value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; /**
                                                                                                                                                                                                                                                                               * External Dependencies
                                                                                                                                                                                                                                                                               */


/**
 * Internal Dependencies
 */


var _react = __webpack_require__(/*! react */ "react");

var _react2 = _interopRequireDefault(_react);

var _reactAddonsCreateFragment = __webpack_require__(/*! react-addons-create-fragment */ "./node_modules/react-addons-create-fragment/index.js");

var _reactAddonsCreateFragment2 = _interopRequireDefault(_reactAddonsCreateFragment);

var _tokenize = __webpack_require__(/*! ./tokenize */ "./node_modules/interpolate-components/lib/tokenize.js");

var _tokenize2 = _interopRequireDefault(_tokenize);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var currentMixedString = void 0;

function getCloseIndex(openIndex, tokens) {
	var openToken = tokens[openIndex],
	    nestLevel = 0,
	    token,
	    i;
	for (i = openIndex + 1; i < tokens.length; i++) {
		token = tokens[i];
		if (token.value === openToken.value) {
			if (token.type === 'componentOpen') {
				nestLevel++;
				continue;
			}
			if (token.type === 'componentClose') {
				if (nestLevel === 0) {
					return i;
				}
				nestLevel--;
			}
		}
	}
	// if we get this far, there was no matching close token
	throw new Error('Missing closing component token `' + openToken.value + '`');
}

function buildChildren(tokens, components) {
	var children = [],
	    childrenObject = {},
	    openComponent,
	    clonedOpenComponent,
	    openIndex,
	    closeIndex,
	    token,
	    i,
	    grandChildTokens,
	    grandChildren,
	    siblingTokens,
	    siblings;

	for (i = 0; i < tokens.length; i++) {
		token = tokens[i];
		if (token.type === 'string') {
			children.push(token.value);
			continue;
		}
		// component node should at least be set
		if (!components.hasOwnProperty(token.value) || typeof components[token.value] === 'undefined') {
			throw new Error('Invalid interpolation, missing component node: `' + token.value + '`');
		}
		// should be either ReactElement or null (both type "object"), all other types deprecated
		if (_typeof(components[token.value]) !== 'object') {
			throw new Error('Invalid interpolation, component node must be a ReactElement or null: `' + token.value + '`', '\n> ' + currentMixedString);
		}
		// we should never see a componentClose token in this loop
		if (token.type === 'componentClose') {
			throw new Error('Missing opening component token: `' + token.value + '`');
		}
		if (token.type === 'componentOpen') {
			openComponent = components[token.value];
			openIndex = i;
			break;
		}
		// componentSelfClosing token
		children.push(components[token.value]);
		continue;
	}

	if (openComponent) {
		closeIndex = getCloseIndex(openIndex, tokens);
		grandChildTokens = tokens.slice(openIndex + 1, closeIndex);
		grandChildren = buildChildren(grandChildTokens, components);
		clonedOpenComponent = _react2.default.cloneElement(openComponent, {}, grandChildren);
		children.push(clonedOpenComponent);

		if (closeIndex < tokens.length - 1) {
			siblingTokens = tokens.slice(closeIndex + 1);
			siblings = buildChildren(siblingTokens, components);
			children = children.concat(siblings);
		}
	}

	if (children.length === 1) {
		return children[0];
	}

	children.forEach(function (child, index) {
		if (child) {
			childrenObject['interpolation-child-' + index] = child;
		}
	});

	return (0, _reactAddonsCreateFragment2.default)(childrenObject);
}

function interpolate(options) {
	var mixedString = options.mixedString,
	    components = options.components,
	    throwErrors = options.throwErrors;


	currentMixedString = mixedString;

	if (!components) {
		return mixedString;
	}

	if ((typeof components === 'undefined' ? 'undefined' : _typeof(components)) !== 'object') {
		if (throwErrors) {
			throw new Error('Interpolation Error: unable to process `' + mixedString + '` because components is not an object');
		}

		return mixedString;
	}

	var tokens = (0, _tokenize2.default)(mixedString);

	try {
		return buildChildren(tokens, components);
	} catch (error) {
		if (throwErrors) {
			throw new Error('Interpolation Error: unable to process `' + mixedString + '` because of error `' + error.message + '`');
		}

		return mixedString;
	}
};

exports.default = interpolate;
//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/interpolate-components/lib/tokenize.js":
/*!*************************************************************!*\
  !*** ./node_modules/interpolate-components/lib/tokenize.js ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function identifyToken(item) {
	// {{/example}}
	if (item.match(/^\{\{\//)) {
		return {
			type: 'componentClose',
			value: item.replace(/\W/g, '')
		};
	}
	// {{example /}}
	if (item.match(/\/\}\}$/)) {
		return {
			type: 'componentSelfClosing',
			value: item.replace(/\W/g, '')
		};
	}
	// {{example}}
	if (item.match(/^\{\{/)) {
		return {
			type: 'componentOpen',
			value: item.replace(/\W/g, '')
		};
	}
	return {
		type: 'string',
		value: item
	};
}

module.exports = function (mixedString) {
	var tokenStrings = mixedString.split(/(\{\{\/?\s*\w+\s*\/?\}\})/g); // split to components and strings
	return tokenStrings.map(identifyToken);
};
//# sourceMappingURL=tokenize.js.map

/***/ }),

/***/ "./node_modules/jed/jed.js":
/*!*********************************!*\
  !*** ./node_modules/jed/jed.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/**
 * @preserve jed.js https://github.com/SlexAxton/Jed
 */
/*
-----------
A gettext compatible i18n library for modern JavaScript Applications

by Alex Sexton - AlexSexton [at] gmail - @SlexAxton

MIT License

A jQuery Foundation project - requires CLA to contribute -
https://contribute.jquery.org/CLA/



Jed offers the entire applicable GNU gettext spec'd set of
functions, but also offers some nicer wrappers around them.
The api for gettext was written for a language with no function
overloading, so Jed allows a little more of that.

Many thanks to Joshua I. Miller - unrtst@cpan.org - who wrote
gettext.js back in 2008. I was able to vet a lot of my ideas
against his. I also made sure Jed passed against his tests
in order to offer easy upgrades -- jsgettext.berlios.de
*/
(function (root, undef) {

  // Set up some underscore-style functions, if you already have
  // underscore, feel free to delete this section, and use it
  // directly, however, the amount of functions used doesn't
  // warrant having underscore as a full dependency.
  // Underscore 1.3.0 was used to port and is licensed
  // under the MIT License by Jeremy Ashkenas.
  var ArrayProto    = Array.prototype,
      ObjProto      = Object.prototype,
      slice         = ArrayProto.slice,
      hasOwnProp    = ObjProto.hasOwnProperty,
      nativeForEach = ArrayProto.forEach,
      breaker       = {};

  // We're not using the OOP style _ so we don't need the
  // extra level of indirection. This still means that you
  // sub out for real `_` though.
  var _ = {
    forEach : function( obj, iterator, context ) {
      var i, l, key;
      if ( obj === null ) {
        return;
      }

      if ( nativeForEach && obj.forEach === nativeForEach ) {
        obj.forEach( iterator, context );
      }
      else if ( obj.length === +obj.length ) {
        for ( i = 0, l = obj.length; i < l; i++ ) {
          if ( i in obj && iterator.call( context, obj[i], i, obj ) === breaker ) {
            return;
          }
        }
      }
      else {
        for ( key in obj) {
          if ( hasOwnProp.call( obj, key ) ) {
            if ( iterator.call (context, obj[key], key, obj ) === breaker ) {
              return;
            }
          }
        }
      }
    },
    extend : function( obj ) {
      this.forEach( slice.call( arguments, 1 ), function ( source ) {
        for ( var prop in source ) {
          obj[prop] = source[prop];
        }
      });
      return obj;
    }
  };
  // END Miniature underscore impl

  // Jed is a constructor function
  var Jed = function ( options ) {
    // Some minimal defaults
    this.defaults = {
      "locale_data" : {
        "messages" : {
          "" : {
            "domain"       : "messages",
            "lang"         : "en",
            "plural_forms" : "nplurals=2; plural=(n != 1);"
          }
          // There are no default keys, though
        }
      },
      // The default domain if one is missing
      "domain" : "messages",
      // enable debug mode to log untranslated strings to the console
      "debug" : false
    };

    // Mix in the sent options with the default options
    this.options = _.extend( {}, this.defaults, options );
    this.textdomain( this.options.domain );

    if ( options.domain && ! this.options.locale_data[ this.options.domain ] ) {
      throw new Error('Text domain set to non-existent domain: `' + options.domain + '`');
    }
  };

  // The gettext spec sets this character as the default
  // delimiter for context lookups.
  // e.g.: context\u0004key
  // If your translation company uses something different,
  // just change this at any time and it will use that instead.
  Jed.context_delimiter = String.fromCharCode( 4 );

  function getPluralFormFunc ( plural_form_string ) {
    return Jed.PF.compile( plural_form_string || "nplurals=2; plural=(n != 1);");
  }

  function Chain( key, i18n ){
    this._key = key;
    this._i18n = i18n;
  }

  // Create a chainable api for adding args prettily
  _.extend( Chain.prototype, {
    onDomain : function ( domain ) {
      this._domain = domain;
      return this;
    },
    withContext : function ( context ) {
      this._context = context;
      return this;
    },
    ifPlural : function ( num, pkey ) {
      this._val = num;
      this._pkey = pkey;
      return this;
    },
    fetch : function ( sArr ) {
      if ( {}.toString.call( sArr ) != '[object Array]' ) {
        sArr = [].slice.call(arguments, 0);
      }
      return ( sArr && sArr.length ? Jed.sprintf : function(x){ return x; } )(
        this._i18n.dcnpgettext(this._domain, this._context, this._key, this._pkey, this._val),
        sArr
      );
    }
  });

  // Add functions to the Jed prototype.
  // These will be the functions on the object that's returned
  // from creating a `new Jed()`
  // These seem redundant, but they gzip pretty well.
  _.extend( Jed.prototype, {
    // The sexier api start point
    translate : function ( key ) {
      return new Chain( key, this );
    },

    textdomain : function ( domain ) {
      if ( ! domain ) {
        return this._textdomain;
      }
      this._textdomain = domain;
    },

    gettext : function ( key ) {
      return this.dcnpgettext.call( this, undef, undef, key );
    },

    dgettext : function ( domain, key ) {
     return this.dcnpgettext.call( this, domain, undef, key );
    },

    dcgettext : function ( domain , key /*, category */ ) {
      // Ignores the category anyways
      return this.dcnpgettext.call( this, domain, undef, key );
    },

    ngettext : function ( skey, pkey, val ) {
      return this.dcnpgettext.call( this, undef, undef, skey, pkey, val );
    },

    dngettext : function ( domain, skey, pkey, val ) {
      return this.dcnpgettext.call( this, domain, undef, skey, pkey, val );
    },

    dcngettext : function ( domain, skey, pkey, val/*, category */) {
      return this.dcnpgettext.call( this, domain, undef, skey, pkey, val );
    },

    pgettext : function ( context, key ) {
      return this.dcnpgettext.call( this, undef, context, key );
    },

    dpgettext : function ( domain, context, key ) {
      return this.dcnpgettext.call( this, domain, context, key );
    },

    dcpgettext : function ( domain, context, key/*, category */) {
      return this.dcnpgettext.call( this, domain, context, key );
    },

    npgettext : function ( context, skey, pkey, val ) {
      return this.dcnpgettext.call( this, undef, context, skey, pkey, val );
    },

    dnpgettext : function ( domain, context, skey, pkey, val ) {
      return this.dcnpgettext.call( this, domain, context, skey, pkey, val );
    },

    // The most fully qualified gettext function. It has every option.
    // Since it has every option, we can use it from every other method.
    // This is the bread and butter.
    // Technically there should be one more argument in this function for 'Category',
    // but since we never use it, we might as well not waste the bytes to define it.
    dcnpgettext : function ( domain, context, singular_key, plural_key, val ) {
      // Set some defaults

      plural_key = plural_key || singular_key;

      // Use the global domain default if one
      // isn't explicitly passed in
      domain = domain || this._textdomain;

      var fallback;

      // Handle special cases

      // No options found
      if ( ! this.options ) {
        // There's likely something wrong, but we'll return the correct key for english
        // We do this by instantiating a brand new Jed instance with the default set
        // for everything that could be broken.
        fallback = new Jed();
        return fallback.dcnpgettext.call( fallback, undefined, undefined, singular_key, plural_key, val );
      }

      // No translation data provided
      if ( ! this.options.locale_data ) {
        throw new Error('No locale data provided.');
      }

      if ( ! this.options.locale_data[ domain ] ) {
        throw new Error('Domain `' + domain + '` was not found.');
      }

      if ( ! this.options.locale_data[ domain ][ "" ] ) {
        throw new Error('No locale meta information provided.');
      }

      // Make sure we have a truthy key. Otherwise we might start looking
      // into the empty string key, which is the options for the locale
      // data.
      if ( ! singular_key ) {
        throw new Error('No translation key found.');
      }

      var key  = context ? context + Jed.context_delimiter + singular_key : singular_key,
          locale_data = this.options.locale_data,
          dict = locale_data[ domain ],
          defaultConf = (locale_data.messages || this.defaults.locale_data.messages)[""],
          pluralForms = dict[""].plural_forms || dict[""]["Plural-Forms"] || dict[""]["plural-forms"] || defaultConf.plural_forms || defaultConf["Plural-Forms"] || defaultConf["plural-forms"],
          val_list,
          res;

      var val_idx;
      if (val === undefined) {
        // No value passed in; assume singular key lookup.
        val_idx = 0;

      } else {
        // Value has been passed in; use plural-forms calculations.

        // Handle invalid numbers, but try casting strings for good measure
        if ( typeof val != 'number' ) {
          val = parseInt( val, 10 );

          if ( isNaN( val ) ) {
            throw new Error('The number that was passed in is not a number.');
          }
        }

        val_idx = getPluralFormFunc(pluralForms)(val);
      }

      // Throw an error if a domain isn't found
      if ( ! dict ) {
        throw new Error('No domain named `' + domain + '` could be found.');
      }

      val_list = dict[ key ];

      // If there is no match, then revert back to
      // english style singular/plural with the keys passed in.
      if ( ! val_list || val_idx > val_list.length ) {
        if (this.options.missing_key_callback) {
          this.options.missing_key_callback(key, domain);
        }
        res = [ singular_key, plural_key ];

        // collect untranslated strings
        if (this.options.debug===true) {
          console.log(res[ getPluralFormFunc(pluralForms)( val ) ]);
        }
        return res[ getPluralFormFunc()( val ) ];
      }

      res = val_list[ val_idx ];

      // This includes empty strings on purpose
      if ( ! res  ) {
        res = [ singular_key, plural_key ];
        return res[ getPluralFormFunc()( val ) ];
      }
      return res;
    }
  });


  // We add in sprintf capabilities for post translation value interolation
  // This is not internally used, so you can remove it if you have this
  // available somewhere else, or want to use a different system.

  // We _slightly_ modify the normal sprintf behavior to more gracefully handle
  // undefined values.

  /**
   sprintf() for JavaScript 0.7-beta1
   http://www.diveintojavascript.com/projects/javascript-sprintf

   Copyright (c) Alexandru Marasteanu <alexaholic [at) gmail (dot] com>
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions are met:
       * Redistributions of source code must retain the above copyright
         notice, this list of conditions and the following disclaimer.
       * Redistributions in binary form must reproduce the above copyright
         notice, this list of conditions and the following disclaimer in the
         documentation and/or other materials provided with the distribution.
       * Neither the name of sprintf() for JavaScript nor the
         names of its contributors may be used to endorse or promote products
         derived from this software without specific prior written permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
   ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
   WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
   DISCLAIMED. IN NO EVENT SHALL Alexandru Marasteanu BE LIABLE FOR ANY
   DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
   (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
   LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
   (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  */
  var sprintf = (function() {
    function get_type(variable) {
      return Object.prototype.toString.call(variable).slice(8, -1).toLowerCase();
    }
    function str_repeat(input, multiplier) {
      for (var output = []; multiplier > 0; output[--multiplier] = input) {/* do nothing */}
      return output.join('');
    }

    var str_format = function() {
      if (!str_format.cache.hasOwnProperty(arguments[0])) {
        str_format.cache[arguments[0]] = str_format.parse(arguments[0]);
      }
      return str_format.format.call(null, str_format.cache[arguments[0]], arguments);
    };

    str_format.format = function(parse_tree, argv) {
      var cursor = 1, tree_length = parse_tree.length, node_type = '', arg, output = [], i, k, match, pad, pad_character, pad_length;
      for (i = 0; i < tree_length; i++) {
        node_type = get_type(parse_tree[i]);
        if (node_type === 'string') {
          output.push(parse_tree[i]);
        }
        else if (node_type === 'array') {
          match = parse_tree[i]; // convenience purposes only
          if (match[2]) { // keyword argument
            arg = argv[cursor];
            for (k = 0; k < match[2].length; k++) {
              if (!arg.hasOwnProperty(match[2][k])) {
                throw(sprintf('[sprintf] property "%s" does not exist', match[2][k]));
              }
              arg = arg[match[2][k]];
            }
          }
          else if (match[1]) { // positional argument (explicit)
            arg = argv[match[1]];
          }
          else { // positional argument (implicit)
            arg = argv[cursor++];
          }

          if (/[^s]/.test(match[8]) && (get_type(arg) != 'number')) {
            throw(sprintf('[sprintf] expecting number but found %s', get_type(arg)));
          }

          // Jed EDIT
          if ( typeof arg == 'undefined' || arg === null ) {
            arg = '';
          }
          // Jed EDIT

          switch (match[8]) {
            case 'b': arg = arg.toString(2); break;
            case 'c': arg = String.fromCharCode(arg); break;
            case 'd': arg = parseInt(arg, 10); break;
            case 'e': arg = match[7] ? arg.toExponential(match[7]) : arg.toExponential(); break;
            case 'f': arg = match[7] ? parseFloat(arg).toFixed(match[7]) : parseFloat(arg); break;
            case 'o': arg = arg.toString(8); break;
            case 's': arg = ((arg = String(arg)) && match[7] ? arg.substring(0, match[7]) : arg); break;
            case 'u': arg = Math.abs(arg); break;
            case 'x': arg = arg.toString(16); break;
            case 'X': arg = arg.toString(16).toUpperCase(); break;
          }
          arg = (/[def]/.test(match[8]) && match[3] && arg >= 0 ? '+'+ arg : arg);
          pad_character = match[4] ? match[4] == '0' ? '0' : match[4].charAt(1) : ' ';
          pad_length = match[6] - String(arg).length;
          pad = match[6] ? str_repeat(pad_character, pad_length) : '';
          output.push(match[5] ? arg + pad : pad + arg);
        }
      }
      return output.join('');
    };

    str_format.cache = {};

    str_format.parse = function(fmt) {
      var _fmt = fmt, match = [], parse_tree = [], arg_names = 0;
      while (_fmt) {
        if ((match = /^[^\x25]+/.exec(_fmt)) !== null) {
          parse_tree.push(match[0]);
        }
        else if ((match = /^\x25{2}/.exec(_fmt)) !== null) {
          parse_tree.push('%');
        }
        else if ((match = /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(_fmt)) !== null) {
          if (match[2]) {
            arg_names |= 1;
            var field_list = [], replacement_field = match[2], field_match = [];
            if ((field_match = /^([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
              field_list.push(field_match[1]);
              while ((replacement_field = replacement_field.substring(field_match[0].length)) !== '') {
                if ((field_match = /^\.([a-z_][a-z_\d]*)/i.exec(replacement_field)) !== null) {
                  field_list.push(field_match[1]);
                }
                else if ((field_match = /^\[(\d+)\]/.exec(replacement_field)) !== null) {
                  field_list.push(field_match[1]);
                }
                else {
                  throw('[sprintf] huh?');
                }
              }
            }
            else {
              throw('[sprintf] huh?');
            }
            match[2] = field_list;
          }
          else {
            arg_names |= 2;
          }
          if (arg_names === 3) {
            throw('[sprintf] mixing positional and named placeholders is not (yet) supported');
          }
          parse_tree.push(match);
        }
        else {
          throw('[sprintf] huh?');
        }
        _fmt = _fmt.substring(match[0].length);
      }
      return parse_tree;
    };

    return str_format;
  })();

  var vsprintf = function(fmt, argv) {
    argv.unshift(fmt);
    return sprintf.apply(null, argv);
  };

  Jed.parse_plural = function ( plural_forms, n ) {
    plural_forms = plural_forms.replace(/n/g, n);
    return Jed.parse_expression(plural_forms);
  };

  Jed.sprintf = function ( fmt, args ) {
    if ( {}.toString.call( args ) == '[object Array]' ) {
      return vsprintf( fmt, [].slice.call(args) );
    }
    return sprintf.apply(this, [].slice.call(arguments) );
  };

  Jed.prototype.sprintf = function () {
    return Jed.sprintf.apply(this, arguments);
  };
  // END sprintf Implementation

  // Start the Plural forms section
  // This is a full plural form expression parser. It is used to avoid
  // running 'eval' or 'new Function' directly against the plural
  // forms.
  //
  // This can be important if you get translations done through a 3rd
  // party vendor. I encourage you to use this instead, however, I
  // also will provide a 'precompiler' that you can use at build time
  // to output valid/safe function representations of the plural form
  // expressions. This means you can build this code out for the most
  // part.
  Jed.PF = {};

  Jed.PF.parse = function ( p ) {
    var plural_str = Jed.PF.extractPluralExpr( p );
    return Jed.PF.parser.parse.call(Jed.PF.parser, plural_str);
  };

  Jed.PF.compile = function ( p ) {
    // Handle trues and falses as 0 and 1
    function imply( val ) {
      return (val === true ? 1 : val ? val : 0);
    }

    var ast = Jed.PF.parse( p );
    return function ( n ) {
      return imply( Jed.PF.interpreter( ast )( n ) );
    };
  };

  Jed.PF.interpreter = function ( ast ) {
    return function ( n ) {
      var res;
      switch ( ast.type ) {
        case 'GROUP':
          return Jed.PF.interpreter( ast.expr )( n );
        case 'TERNARY':
          if ( Jed.PF.interpreter( ast.expr )( n ) ) {
            return Jed.PF.interpreter( ast.truthy )( n );
          }
          return Jed.PF.interpreter( ast.falsey )( n );
        case 'OR':
          return Jed.PF.interpreter( ast.left )( n ) || Jed.PF.interpreter( ast.right )( n );
        case 'AND':
          return Jed.PF.interpreter( ast.left )( n ) && Jed.PF.interpreter( ast.right )( n );
        case 'LT':
          return Jed.PF.interpreter( ast.left )( n ) < Jed.PF.interpreter( ast.right )( n );
        case 'GT':
          return Jed.PF.interpreter( ast.left )( n ) > Jed.PF.interpreter( ast.right )( n );
        case 'LTE':
          return Jed.PF.interpreter( ast.left )( n ) <= Jed.PF.interpreter( ast.right )( n );
        case 'GTE':
          return Jed.PF.interpreter( ast.left )( n ) >= Jed.PF.interpreter( ast.right )( n );
        case 'EQ':
          return Jed.PF.interpreter( ast.left )( n ) == Jed.PF.interpreter( ast.right )( n );
        case 'NEQ':
          return Jed.PF.interpreter( ast.left )( n ) != Jed.PF.interpreter( ast.right )( n );
        case 'MOD':
          return Jed.PF.interpreter( ast.left )( n ) % Jed.PF.interpreter( ast.right )( n );
        case 'VAR':
          return n;
        case 'NUM':
          return ast.val;
        default:
          throw new Error("Invalid Token found.");
      }
    };
  };

  Jed.PF.extractPluralExpr = function ( p ) {
    // trim first
    p = p.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

    if (! /;\s*$/.test(p)) {
      p = p.concat(';');
    }

    var nplurals_re = /nplurals\=(\d+);/,
        plural_re = /plural\=(.*);/,
        nplurals_matches = p.match( nplurals_re ),
        res = {},
        plural_matches;

    // Find the nplurals number
    if ( nplurals_matches.length > 1 ) {
      res.nplurals = nplurals_matches[1];
    }
    else {
      throw new Error('nplurals not found in plural_forms string: ' + p );
    }

    // remove that data to get to the formula
    p = p.replace( nplurals_re, "" );
    plural_matches = p.match( plural_re );

    if (!( plural_matches && plural_matches.length > 1 ) ) {
      throw new Error('`plural` expression not found: ' + p);
    }
    return plural_matches[ 1 ];
  };

  /* Jison generated parser */
  Jed.PF.parser = (function(){

var parser = {trace: function trace() { },
yy: {},
symbols_: {"error":2,"expressions":3,"e":4,"EOF":5,"?":6,":":7,"||":8,"&&":9,"<":10,"<=":11,">":12,">=":13,"!=":14,"==":15,"%":16,"(":17,")":18,"n":19,"NUMBER":20,"$accept":0,"$end":1},
terminals_: {2:"error",5:"EOF",6:"?",7:":",8:"||",9:"&&",10:"<",11:"<=",12:">",13:">=",14:"!=",15:"==",16:"%",17:"(",18:")",19:"n",20:"NUMBER"},
productions_: [0,[3,2],[4,5],[4,3],[4,3],[4,3],[4,3],[4,3],[4,3],[4,3],[4,3],[4,3],[4,3],[4,1],[4,1]],
performAction: function anonymous(yytext,yyleng,yylineno,yy,yystate,$$,_$) {

var $0 = $$.length - 1;
switch (yystate) {
case 1: return { type : 'GROUP', expr: $$[$0-1] };
break;
case 2:this.$ = { type: 'TERNARY', expr: $$[$0-4], truthy : $$[$0-2], falsey: $$[$0] };
break;
case 3:this.$ = { type: "OR", left: $$[$0-2], right: $$[$0] };
break;
case 4:this.$ = { type: "AND", left: $$[$0-2], right: $$[$0] };
break;
case 5:this.$ = { type: 'LT', left: $$[$0-2], right: $$[$0] };
break;
case 6:this.$ = { type: 'LTE', left: $$[$0-2], right: $$[$0] };
break;
case 7:this.$ = { type: 'GT', left: $$[$0-2], right: $$[$0] };
break;
case 8:this.$ = { type: 'GTE', left: $$[$0-2], right: $$[$0] };
break;
case 9:this.$ = { type: 'NEQ', left: $$[$0-2], right: $$[$0] };
break;
case 10:this.$ = { type: 'EQ', left: $$[$0-2], right: $$[$0] };
break;
case 11:this.$ = { type: 'MOD', left: $$[$0-2], right: $$[$0] };
break;
case 12:this.$ = { type: 'GROUP', expr: $$[$0-1] };
break;
case 13:this.$ = { type: 'VAR' };
break;
case 14:this.$ = { type: 'NUM', val: Number(yytext) };
break;
}
},
table: [{3:1,4:2,17:[1,3],19:[1,4],20:[1,5]},{1:[3]},{5:[1,6],6:[1,7],8:[1,8],9:[1,9],10:[1,10],11:[1,11],12:[1,12],13:[1,13],14:[1,14],15:[1,15],16:[1,16]},{4:17,17:[1,3],19:[1,4],20:[1,5]},{5:[2,13],6:[2,13],7:[2,13],8:[2,13],9:[2,13],10:[2,13],11:[2,13],12:[2,13],13:[2,13],14:[2,13],15:[2,13],16:[2,13],18:[2,13]},{5:[2,14],6:[2,14],7:[2,14],8:[2,14],9:[2,14],10:[2,14],11:[2,14],12:[2,14],13:[2,14],14:[2,14],15:[2,14],16:[2,14],18:[2,14]},{1:[2,1]},{4:18,17:[1,3],19:[1,4],20:[1,5]},{4:19,17:[1,3],19:[1,4],20:[1,5]},{4:20,17:[1,3],19:[1,4],20:[1,5]},{4:21,17:[1,3],19:[1,4],20:[1,5]},{4:22,17:[1,3],19:[1,4],20:[1,5]},{4:23,17:[1,3],19:[1,4],20:[1,5]},{4:24,17:[1,3],19:[1,4],20:[1,5]},{4:25,17:[1,3],19:[1,4],20:[1,5]},{4:26,17:[1,3],19:[1,4],20:[1,5]},{4:27,17:[1,3],19:[1,4],20:[1,5]},{6:[1,7],8:[1,8],9:[1,9],10:[1,10],11:[1,11],12:[1,12],13:[1,13],14:[1,14],15:[1,15],16:[1,16],18:[1,28]},{6:[1,7],7:[1,29],8:[1,8],9:[1,9],10:[1,10],11:[1,11],12:[1,12],13:[1,13],14:[1,14],15:[1,15],16:[1,16]},{5:[2,3],6:[2,3],7:[2,3],8:[2,3],9:[1,9],10:[1,10],11:[1,11],12:[1,12],13:[1,13],14:[1,14],15:[1,15],16:[1,16],18:[2,3]},{5:[2,4],6:[2,4],7:[2,4],8:[2,4],9:[2,4],10:[1,10],11:[1,11],12:[1,12],13:[1,13],14:[1,14],15:[1,15],16:[1,16],18:[2,4]},{5:[2,5],6:[2,5],7:[2,5],8:[2,5],9:[2,5],10:[2,5],11:[2,5],12:[2,5],13:[2,5],14:[2,5],15:[2,5],16:[1,16],18:[2,5]},{5:[2,6],6:[2,6],7:[2,6],8:[2,6],9:[2,6],10:[2,6],11:[2,6],12:[2,6],13:[2,6],14:[2,6],15:[2,6],16:[1,16],18:[2,6]},{5:[2,7],6:[2,7],7:[2,7],8:[2,7],9:[2,7],10:[2,7],11:[2,7],12:[2,7],13:[2,7],14:[2,7],15:[2,7],16:[1,16],18:[2,7]},{5:[2,8],6:[2,8],7:[2,8],8:[2,8],9:[2,8],10:[2,8],11:[2,8],12:[2,8],13:[2,8],14:[2,8],15:[2,8],16:[1,16],18:[2,8]},{5:[2,9],6:[2,9],7:[2,9],8:[2,9],9:[2,9],10:[2,9],11:[2,9],12:[2,9],13:[2,9],14:[2,9],15:[2,9],16:[1,16],18:[2,9]},{5:[2,10],6:[2,10],7:[2,10],8:[2,10],9:[2,10],10:[2,10],11:[2,10],12:[2,10],13:[2,10],14:[2,10],15:[2,10],16:[1,16],18:[2,10]},{5:[2,11],6:[2,11],7:[2,11],8:[2,11],9:[2,11],10:[2,11],11:[2,11],12:[2,11],13:[2,11],14:[2,11],15:[2,11],16:[2,11],18:[2,11]},{5:[2,12],6:[2,12],7:[2,12],8:[2,12],9:[2,12],10:[2,12],11:[2,12],12:[2,12],13:[2,12],14:[2,12],15:[2,12],16:[2,12],18:[2,12]},{4:30,17:[1,3],19:[1,4],20:[1,5]},{5:[2,2],6:[1,7],7:[2,2],8:[1,8],9:[1,9],10:[1,10],11:[1,11],12:[1,12],13:[1,13],14:[1,14],15:[1,15],16:[1,16],18:[2,2]}],
defaultActions: {6:[2,1]},
parseError: function parseError(str, hash) {
    throw new Error(str);
},
parse: function parse(input) {
    var self = this,
        stack = [0],
        vstack = [null], // semantic value stack
        lstack = [], // location stack
        table = this.table,
        yytext = '',
        yylineno = 0,
        yyleng = 0,
        recovering = 0,
        TERROR = 2,
        EOF = 1;

    //this.reductionCount = this.shiftCount = 0;

    this.lexer.setInput(input);
    this.lexer.yy = this.yy;
    this.yy.lexer = this.lexer;
    if (typeof this.lexer.yylloc == 'undefined')
        this.lexer.yylloc = {};
    var yyloc = this.lexer.yylloc;
    lstack.push(yyloc);

    if (typeof this.yy.parseError === 'function')
        this.parseError = this.yy.parseError;

    function popStack (n) {
        stack.length = stack.length - 2*n;
        vstack.length = vstack.length - n;
        lstack.length = lstack.length - n;
    }

    function lex() {
        var token;
        token = self.lexer.lex() || 1; // $end = 1
        // if token isn't its numeric value, convert
        if (typeof token !== 'number') {
            token = self.symbols_[token] || token;
        }
        return token;
    }

    var symbol, preErrorSymbol, state, action, a, r, yyval={},p,len,newState, expected;
    while (true) {
        // retreive state number from top of stack
        state = stack[stack.length-1];

        // use default actions if available
        if (this.defaultActions[state]) {
            action = this.defaultActions[state];
        } else {
            if (symbol == null)
                symbol = lex();
            // read action for current state and first input
            action = table[state] && table[state][symbol];
        }

        // handle parse error
        _handle_error:
        if (typeof action === 'undefined' || !action.length || !action[0]) {

            if (!recovering) {
                // Report error
                expected = [];
                for (p in table[state]) if (this.terminals_[p] && p > 2) {
                    expected.push("'"+this.terminals_[p]+"'");
                }
                var errStr = '';
                if (this.lexer.showPosition) {
                    errStr = 'Parse error on line '+(yylineno+1)+":\n"+this.lexer.showPosition()+"\nExpecting "+expected.join(', ') + ", got '" + this.terminals_[symbol]+ "'";
                } else {
                    errStr = 'Parse error on line '+(yylineno+1)+": Unexpected " +
                                  (symbol == 1 /*EOF*/ ? "end of input" :
                                              ("'"+(this.terminals_[symbol] || symbol)+"'"));
                }
                this.parseError(errStr,
                    {text: this.lexer.match, token: this.terminals_[symbol] || symbol, line: this.lexer.yylineno, loc: yyloc, expected: expected});
            }

            // just recovered from another error
            if (recovering == 3) {
                if (symbol == EOF) {
                    throw new Error(errStr || 'Parsing halted.');
                }

                // discard current lookahead and grab another
                yyleng = this.lexer.yyleng;
                yytext = this.lexer.yytext;
                yylineno = this.lexer.yylineno;
                yyloc = this.lexer.yylloc;
                symbol = lex();
            }

            // try to recover from error
            while (1) {
                // check for error recovery rule in this state
                if ((TERROR.toString()) in table[state]) {
                    break;
                }
                if (state == 0) {
                    throw new Error(errStr || 'Parsing halted.');
                }
                popStack(1);
                state = stack[stack.length-1];
            }

            preErrorSymbol = symbol; // save the lookahead token
            symbol = TERROR;         // insert generic error symbol as new lookahead
            state = stack[stack.length-1];
            action = table[state] && table[state][TERROR];
            recovering = 3; // allow 3 real symbols to be shifted before reporting a new error
        }

        // this shouldn't happen, unless resolve defaults are off
        if (action[0] instanceof Array && action.length > 1) {
            throw new Error('Parse Error: multiple actions possible at state: '+state+', token: '+symbol);
        }

        switch (action[0]) {

            case 1: // shift
                //this.shiftCount++;

                stack.push(symbol);
                vstack.push(this.lexer.yytext);
                lstack.push(this.lexer.yylloc);
                stack.push(action[1]); // push state
                symbol = null;
                if (!preErrorSymbol) { // normal execution/no error
                    yyleng = this.lexer.yyleng;
                    yytext = this.lexer.yytext;
                    yylineno = this.lexer.yylineno;
                    yyloc = this.lexer.yylloc;
                    if (recovering > 0)
                        recovering--;
                } else { // error just occurred, resume old lookahead f/ before error
                    symbol = preErrorSymbol;
                    preErrorSymbol = null;
                }
                break;

            case 2: // reduce
                //this.reductionCount++;

                len = this.productions_[action[1]][1];

                // perform semantic action
                yyval.$ = vstack[vstack.length-len]; // default to $$ = $1
                // default location, uses first token for firsts, last for lasts
                yyval._$ = {
                    first_line: lstack[lstack.length-(len||1)].first_line,
                    last_line: lstack[lstack.length-1].last_line,
                    first_column: lstack[lstack.length-(len||1)].first_column,
                    last_column: lstack[lstack.length-1].last_column
                };
                r = this.performAction.call(yyval, yytext, yyleng, yylineno, this.yy, action[1], vstack, lstack);

                if (typeof r !== 'undefined') {
                    return r;
                }

                // pop off stack
                if (len) {
                    stack = stack.slice(0,-1*len*2);
                    vstack = vstack.slice(0, -1*len);
                    lstack = lstack.slice(0, -1*len);
                }

                stack.push(this.productions_[action[1]][0]);    // push nonterminal (reduce)
                vstack.push(yyval.$);
                lstack.push(yyval._$);
                // goto new state = table[STATE][NONTERMINAL]
                newState = table[stack[stack.length-2]][stack[stack.length-1]];
                stack.push(newState);
                break;

            case 3: // accept
                return true;
        }

    }

    return true;
}};/* Jison generated lexer */
var lexer = (function(){

var lexer = ({EOF:1,
parseError:function parseError(str, hash) {
        if (this.yy.parseError) {
            this.yy.parseError(str, hash);
        } else {
            throw new Error(str);
        }
    },
setInput:function (input) {
        this._input = input;
        this._more = this._less = this.done = false;
        this.yylineno = this.yyleng = 0;
        this.yytext = this.matched = this.match = '';
        this.conditionStack = ['INITIAL'];
        this.yylloc = {first_line:1,first_column:0,last_line:1,last_column:0};
        return this;
    },
input:function () {
        var ch = this._input[0];
        this.yytext+=ch;
        this.yyleng++;
        this.match+=ch;
        this.matched+=ch;
        var lines = ch.match(/\n/);
        if (lines) this.yylineno++;
        this._input = this._input.slice(1);
        return ch;
    },
unput:function (ch) {
        this._input = ch + this._input;
        return this;
    },
more:function () {
        this._more = true;
        return this;
    },
pastInput:function () {
        var past = this.matched.substr(0, this.matched.length - this.match.length);
        return (past.length > 20 ? '...':'') + past.substr(-20).replace(/\n/g, "");
    },
upcomingInput:function () {
        var next = this.match;
        if (next.length < 20) {
            next += this._input.substr(0, 20-next.length);
        }
        return (next.substr(0,20)+(next.length > 20 ? '...':'')).replace(/\n/g, "");
    },
showPosition:function () {
        var pre = this.pastInput();
        var c = new Array(pre.length + 1).join("-");
        return pre + this.upcomingInput() + "\n" + c+"^";
    },
next:function () {
        if (this.done) {
            return this.EOF;
        }
        if (!this._input) this.done = true;

        var token,
            match,
            col,
            lines;
        if (!this._more) {
            this.yytext = '';
            this.match = '';
        }
        var rules = this._currentRules();
        for (var i=0;i < rules.length; i++) {
            match = this._input.match(this.rules[rules[i]]);
            if (match) {
                lines = match[0].match(/\n.*/g);
                if (lines) this.yylineno += lines.length;
                this.yylloc = {first_line: this.yylloc.last_line,
                               last_line: this.yylineno+1,
                               first_column: this.yylloc.last_column,
                               last_column: lines ? lines[lines.length-1].length-1 : this.yylloc.last_column + match[0].length}
                this.yytext += match[0];
                this.match += match[0];
                this.matches = match;
                this.yyleng = this.yytext.length;
                this._more = false;
                this._input = this._input.slice(match[0].length);
                this.matched += match[0];
                token = this.performAction.call(this, this.yy, this, rules[i],this.conditionStack[this.conditionStack.length-1]);
                if (token) return token;
                else return;
            }
        }
        if (this._input === "") {
            return this.EOF;
        } else {
            this.parseError('Lexical error on line '+(this.yylineno+1)+'. Unrecognized text.\n'+this.showPosition(),
                    {text: "", token: null, line: this.yylineno});
        }
    },
lex:function lex() {
        var r = this.next();
        if (typeof r !== 'undefined') {
            return r;
        } else {
            return this.lex();
        }
    },
begin:function begin(condition) {
        this.conditionStack.push(condition);
    },
popState:function popState() {
        return this.conditionStack.pop();
    },
_currentRules:function _currentRules() {
        return this.conditions[this.conditionStack[this.conditionStack.length-1]].rules;
    },
topState:function () {
        return this.conditionStack[this.conditionStack.length-2];
    },
pushState:function begin(condition) {
        this.begin(condition);
    }});
lexer.performAction = function anonymous(yy,yy_,$avoiding_name_collisions,YY_START) {

var YYSTATE=YY_START;
switch($avoiding_name_collisions) {
case 0:/* skip whitespace */
break;
case 1:return 20
break;
case 2:return 19
break;
case 3:return 8
break;
case 4:return 9
break;
case 5:return 6
break;
case 6:return 7
break;
case 7:return 11
break;
case 8:return 13
break;
case 9:return 10
break;
case 10:return 12
break;
case 11:return 14
break;
case 12:return 15
break;
case 13:return 16
break;
case 14:return 17
break;
case 15:return 18
break;
case 16:return 5
break;
case 17:return 'INVALID'
break;
}
};
lexer.rules = [/^\s+/,/^[0-9]+(\.[0-9]+)?\b/,/^n\b/,/^\|\|/,/^&&/,/^\?/,/^:/,/^<=/,/^>=/,/^</,/^>/,/^!=/,/^==/,/^%/,/^\(/,/^\)/,/^$/,/^./];
lexer.conditions = {"INITIAL":{"rules":[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],"inclusive":true}};return lexer;})()
parser.lexer = lexer;
return parser;
})();
// End parser

  // Handle node, amd, and global systems
  if (true) {
    if ( true && module.exports) {
      exports = module.exports = Jed;
    }
    exports.Jed = Jed;
  }
  else {}

})(this);


/***/ }),

/***/ "./node_modules/lil-uri/uri.js":
/*!*************************************!*\
  !*** ./node_modules/lil-uri/uri.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*! lil-uri - v0.2.0 - MIT License - https://github.com/lil-js/uri */
;(function (root, factory) {
  if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [exports], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__))
  } else {}
}(this, function (exports) {
  'use strict'

  var VERSION = '0.2.2'
  var REGEX = /^(?:([^:\/?#]+):\/\/)?((?:([^\/?#@]*)@)?([^\/?#:]*)(?:\:(\d*))?)?([^?#]*)(?:\?([^#]*))?(?:#((?:.|\n)*))?/i

  function isStr (o) {
    return typeof o === 'string'
  }
  
  function decode (uri) {
    return decodeURIComponent(escape(uri))
  }

  function mapSearchParams(search) {
    var map = {}
    if (typeof search === 'string') {
      search.split('&').forEach(function (values) {
        values = values.split('=')
        if (map.hasOwnProperty(values[0])) {
          map[values[0]] = Array.isArray(map[values[0]]) ? map[values[0]] : [ map[values[0]] ]
          map[values[0]].push(values[1])
        } else {
          map[values[0]] = values[1]
        }
      })
      return map
    }
  }

  function accessor(type) {
    return function (value) {
      if (value) {
        this.parts[type] = isStr(value) ? decode(value) : value
        return this
      }
      this.parts = this.parse(this.build())
      return this.parts[type]
    }
  }

  function URI(uri) {
    this.uri = uri || null
    if (isStr(uri) && uri.length) {
      this.parts = this.parse(uri)
    } else {
      this.parts = {}
    }
  }

  URI.prototype.parse = function (uri) {
    var parts = decode(uri || '').match(REGEX)
    var auth = (parts[3] || '').split(':')
    var host = auth.length ? (parts[2] || '').replace(/(.*\@)/, '') : parts[2]
    return {
      uri: parts[0],
      protocol: parts[1],
      host: host,
      hostname: parts[4],
      port: parts[5],
      auth: parts[3],
      user: auth[0],
      password: auth[1],
      path: parts[6],
      search: parts[7],
      query: mapSearchParams(parts[7]),
      hash: parts[8]
    }
  }

  URI.prototype.protocol = function (host) {
    return accessor('protocol').call(this, host)
  }

  URI.prototype.host = function (host) {
    return accessor('host').call(this, host)
  }

  URI.prototype.hostname = function (hostname) {
    return accessor('hostname').call(this, hostname)
  }

  URI.prototype.port = function (port) {
    return accessor('port').call(this, port)
  }

  URI.prototype.auth = function (auth) {
    return accessor('host').call(this, auth)
  }

  URI.prototype.user = function (user) {
    return accessor('user').call(this, user)
  }

  URI.prototype.password = function (password) {
    return accessor('password').call(this, password)
  }

  URI.prototype.path = function (path) {
    return accessor('path').call(this, path)
  }

  URI.prototype.search = function (search) {
    return accessor('search').call(this, search)
  }

  URI.prototype.query = function (query) {
    return query && typeof query === 'object' ? accessor('query').call(this, query) : this.parts.query
  }

  URI.prototype.hash = function (hash) {
    return accessor('hash').call(this, hash)
  }

  URI.prototype.get = function (value) {
    return this.parts[value] || ''
  }

  URI.prototype.build = URI.prototype.toString = URI.prototype.valueOf = function () {
    var p = this.parts, buf = []

    if (p.protocol) buf.push(p.protocol + '://')
    if (p.auth) buf.push(p.auth + '@')
    else if (p.user) buf.push(p.user + (p.password ? ':' + p.password : '') + '@')

    if (p.host) {
      buf.push(p.host)
    } else {
      if (p.hostname) buf.push(p.hostname)
      if (p.port) buf.push(':' + p.port)
    }

    if (p.path) buf.push(p.path)
    if (p.query && typeof p.query === 'object') {
      if (!p.path) buf.push('/')
      buf.push('?' + (Object.keys(p.query).map(function (name) {
        if (Array.isArray(p.query[name])) {
          return p.query[name].map(function (value) {
            return name + (value ? '=' + value : '')
          }).join('&')
        } else {
          return name + (p.query[name] ? '=' + p.query[name] : '')
        }
      }).join('&')))
    } else if (p.search) {
      buf.push('?' + p.search)
    }

    if (p.hash) {
      if (!p.path) buf.push('/')
      buf.push('#' + p.hash)
    }

    return this.url = buf.filter(function (part) { return isStr(part) }).join('')
  }

  function uri(uri) {
    return new URI(uri)
  }

  function isURL(uri) {
    return typeof uri === 'string' && REGEX.test(uri)
  }

  uri.VERSION = VERSION
  uri.is = uri.isURL = isURL
  uri.URI = URI

  return exports.uri = uri
}));


/***/ }),

/***/ "./node_modules/lodash.assign/index.js":
/*!*********************************************!*\
  !*** ./node_modules/lodash.assign/index.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * lodash (Custom Build) <https://lodash.com/>
 * Build: `lodash modularize exports="npm" -o ./`
 * Copyright jQuery Foundation and other contributors <https://jquery.org/>
 * Released under MIT license <https://lodash.com/license>
 * Based on Underscore.js 1.8.3 <http://underscorejs.org/LICENSE>
 * Copyright Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 */

/** Used as references for various `Number` constants. */
var MAX_SAFE_INTEGER = 9007199254740991;

/** `Object#toString` result references. */
var argsTag = '[object Arguments]',
    funcTag = '[object Function]',
    genTag = '[object GeneratorFunction]';

/** Used to detect unsigned integer values. */
var reIsUint = /^(?:0|[1-9]\d*)$/;

/**
 * A faster alternative to `Function#apply`, this function invokes `func`
 * with the `this` binding of `thisArg` and the arguments of `args`.
 *
 * @private
 * @param {Function} func The function to invoke.
 * @param {*} thisArg The `this` binding of `func`.
 * @param {Array} args The arguments to invoke `func` with.
 * @returns {*} Returns the result of `func`.
 */
function apply(func, thisArg, args) {
  switch (args.length) {
    case 0: return func.call(thisArg);
    case 1: return func.call(thisArg, args[0]);
    case 2: return func.call(thisArg, args[0], args[1]);
    case 3: return func.call(thisArg, args[0], args[1], args[2]);
  }
  return func.apply(thisArg, args);
}

/**
 * The base implementation of `_.times` without support for iteratee shorthands
 * or max array length checks.
 *
 * @private
 * @param {number} n The number of times to invoke `iteratee`.
 * @param {Function} iteratee The function invoked per iteration.
 * @returns {Array} Returns the array of results.
 */
function baseTimes(n, iteratee) {
  var index = -1,
      result = Array(n);

  while (++index < n) {
    result[index] = iteratee(index);
  }
  return result;
}

/**
 * Creates a unary function that invokes `func` with its argument transformed.
 *
 * @private
 * @param {Function} func The function to wrap.
 * @param {Function} transform The argument transform.
 * @returns {Function} Returns the new function.
 */
function overArg(func, transform) {
  return function(arg) {
    return func(transform(arg));
  };
}

/** Used for built-in method references. */
var objectProto = Object.prototype;

/** Used to check objects for own properties. */
var hasOwnProperty = objectProto.hasOwnProperty;

/**
 * Used to resolve the
 * [`toStringTag`](http://ecma-international.org/ecma-262/7.0/#sec-object.prototype.tostring)
 * of values.
 */
var objectToString = objectProto.toString;

/** Built-in value references. */
var propertyIsEnumerable = objectProto.propertyIsEnumerable;

/* Built-in method references for those with the same name as other `lodash` methods. */
var nativeKeys = overArg(Object.keys, Object),
    nativeMax = Math.max;

/** Detect if properties shadowing those on `Object.prototype` are non-enumerable. */
var nonEnumShadows = !propertyIsEnumerable.call({ 'valueOf': 1 }, 'valueOf');

/**
 * Creates an array of the enumerable property names of the array-like `value`.
 *
 * @private
 * @param {*} value The value to query.
 * @param {boolean} inherited Specify returning inherited property names.
 * @returns {Array} Returns the array of property names.
 */
function arrayLikeKeys(value, inherited) {
  // Safari 8.1 makes `arguments.callee` enumerable in strict mode.
  // Safari 9 makes `arguments.length` enumerable in strict mode.
  var result = (isArray(value) || isArguments(value))
    ? baseTimes(value.length, String)
    : [];

  var length = result.length,
      skipIndexes = !!length;

  for (var key in value) {
    if ((inherited || hasOwnProperty.call(value, key)) &&
        !(skipIndexes && (key == 'length' || isIndex(key, length)))) {
      result.push(key);
    }
  }
  return result;
}

/**
 * Assigns `value` to `key` of `object` if the existing value is not equivalent
 * using [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * for equality comparisons.
 *
 * @private
 * @param {Object} object The object to modify.
 * @param {string} key The key of the property to assign.
 * @param {*} value The value to assign.
 */
function assignValue(object, key, value) {
  var objValue = object[key];
  if (!(hasOwnProperty.call(object, key) && eq(objValue, value)) ||
      (value === undefined && !(key in object))) {
    object[key] = value;
  }
}

/**
 * The base implementation of `_.keys` which doesn't treat sparse arrays as dense.
 *
 * @private
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 */
function baseKeys(object) {
  if (!isPrototype(object)) {
    return nativeKeys(object);
  }
  var result = [];
  for (var key in Object(object)) {
    if (hasOwnProperty.call(object, key) && key != 'constructor') {
      result.push(key);
    }
  }
  return result;
}

/**
 * The base implementation of `_.rest` which doesn't validate or coerce arguments.
 *
 * @private
 * @param {Function} func The function to apply a rest parameter to.
 * @param {number} [start=func.length-1] The start position of the rest parameter.
 * @returns {Function} Returns the new function.
 */
function baseRest(func, start) {
  start = nativeMax(start === undefined ? (func.length - 1) : start, 0);
  return function() {
    var args = arguments,
        index = -1,
        length = nativeMax(args.length - start, 0),
        array = Array(length);

    while (++index < length) {
      array[index] = args[start + index];
    }
    index = -1;
    var otherArgs = Array(start + 1);
    while (++index < start) {
      otherArgs[index] = args[index];
    }
    otherArgs[start] = array;
    return apply(func, this, otherArgs);
  };
}

/**
 * Copies properties of `source` to `object`.
 *
 * @private
 * @param {Object} source The object to copy properties from.
 * @param {Array} props The property identifiers to copy.
 * @param {Object} [object={}] The object to copy properties to.
 * @param {Function} [customizer] The function to customize copied values.
 * @returns {Object} Returns `object`.
 */
function copyObject(source, props, object, customizer) {
  object || (object = {});

  var index = -1,
      length = props.length;

  while (++index < length) {
    var key = props[index];

    var newValue = customizer
      ? customizer(object[key], source[key], key, object, source)
      : undefined;

    assignValue(object, key, newValue === undefined ? source[key] : newValue);
  }
  return object;
}

/**
 * Creates a function like `_.assign`.
 *
 * @private
 * @param {Function} assigner The function to assign values.
 * @returns {Function} Returns the new assigner function.
 */
function createAssigner(assigner) {
  return baseRest(function(object, sources) {
    var index = -1,
        length = sources.length,
        customizer = length > 1 ? sources[length - 1] : undefined,
        guard = length > 2 ? sources[2] : undefined;

    customizer = (assigner.length > 3 && typeof customizer == 'function')
      ? (length--, customizer)
      : undefined;

    if (guard && isIterateeCall(sources[0], sources[1], guard)) {
      customizer = length < 3 ? undefined : customizer;
      length = 1;
    }
    object = Object(object);
    while (++index < length) {
      var source = sources[index];
      if (source) {
        assigner(object, source, index, customizer);
      }
    }
    return object;
  });
}

/**
 * Checks if `value` is a valid array-like index.
 *
 * @private
 * @param {*} value The value to check.
 * @param {number} [length=MAX_SAFE_INTEGER] The upper bounds of a valid index.
 * @returns {boolean} Returns `true` if `value` is a valid index, else `false`.
 */
function isIndex(value, length) {
  length = length == null ? MAX_SAFE_INTEGER : length;
  return !!length &&
    (typeof value == 'number' || reIsUint.test(value)) &&
    (value > -1 && value % 1 == 0 && value < length);
}

/**
 * Checks if the given arguments are from an iteratee call.
 *
 * @private
 * @param {*} value The potential iteratee value argument.
 * @param {*} index The potential iteratee index or key argument.
 * @param {*} object The potential iteratee object argument.
 * @returns {boolean} Returns `true` if the arguments are from an iteratee call,
 *  else `false`.
 */
function isIterateeCall(value, index, object) {
  if (!isObject(object)) {
    return false;
  }
  var type = typeof index;
  if (type == 'number'
        ? (isArrayLike(object) && isIndex(index, object.length))
        : (type == 'string' && index in object)
      ) {
    return eq(object[index], value);
  }
  return false;
}

/**
 * Checks if `value` is likely a prototype object.
 *
 * @private
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a prototype, else `false`.
 */
function isPrototype(value) {
  var Ctor = value && value.constructor,
      proto = (typeof Ctor == 'function' && Ctor.prototype) || objectProto;

  return value === proto;
}

/**
 * Performs a
 * [`SameValueZero`](http://ecma-international.org/ecma-262/7.0/#sec-samevaluezero)
 * comparison between two values to determine if they are equivalent.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to compare.
 * @param {*} other The other value to compare.
 * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
 * @example
 *
 * var object = { 'a': 1 };
 * var other = { 'a': 1 };
 *
 * _.eq(object, object);
 * // => true
 *
 * _.eq(object, other);
 * // => false
 *
 * _.eq('a', 'a');
 * // => true
 *
 * _.eq('a', Object('a'));
 * // => false
 *
 * _.eq(NaN, NaN);
 * // => true
 */
function eq(value, other) {
  return value === other || (value !== value && other !== other);
}

/**
 * Checks if `value` is likely an `arguments` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an `arguments` object,
 *  else `false`.
 * @example
 *
 * _.isArguments(function() { return arguments; }());
 * // => true
 *
 * _.isArguments([1, 2, 3]);
 * // => false
 */
function isArguments(value) {
  // Safari 8.1 makes `arguments.callee` enumerable in strict mode.
  return isArrayLikeObject(value) && hasOwnProperty.call(value, 'callee') &&
    (!propertyIsEnumerable.call(value, 'callee') || objectToString.call(value) == argsTag);
}

/**
 * Checks if `value` is classified as an `Array` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array, else `false`.
 * @example
 *
 * _.isArray([1, 2, 3]);
 * // => true
 *
 * _.isArray(document.body.children);
 * // => false
 *
 * _.isArray('abc');
 * // => false
 *
 * _.isArray(_.noop);
 * // => false
 */
var isArray = Array.isArray;

/**
 * Checks if `value` is array-like. A value is considered array-like if it's
 * not a function and has a `value.length` that's an integer greater than or
 * equal to `0` and less than or equal to `Number.MAX_SAFE_INTEGER`.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is array-like, else `false`.
 * @example
 *
 * _.isArrayLike([1, 2, 3]);
 * // => true
 *
 * _.isArrayLike(document.body.children);
 * // => true
 *
 * _.isArrayLike('abc');
 * // => true
 *
 * _.isArrayLike(_.noop);
 * // => false
 */
function isArrayLike(value) {
  return value != null && isLength(value.length) && !isFunction(value);
}

/**
 * This method is like `_.isArrayLike` except that it also checks if `value`
 * is an object.
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an array-like object,
 *  else `false`.
 * @example
 *
 * _.isArrayLikeObject([1, 2, 3]);
 * // => true
 *
 * _.isArrayLikeObject(document.body.children);
 * // => true
 *
 * _.isArrayLikeObject('abc');
 * // => false
 *
 * _.isArrayLikeObject(_.noop);
 * // => false
 */
function isArrayLikeObject(value) {
  return isObjectLike(value) && isArrayLike(value);
}

/**
 * Checks if `value` is classified as a `Function` object.
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a function, else `false`.
 * @example
 *
 * _.isFunction(_);
 * // => true
 *
 * _.isFunction(/abc/);
 * // => false
 */
function isFunction(value) {
  // The use of `Object#toString` avoids issues with the `typeof` operator
  // in Safari 8-9 which returns 'object' for typed array and other constructors.
  var tag = isObject(value) ? objectToString.call(value) : '';
  return tag == funcTag || tag == genTag;
}

/**
 * Checks if `value` is a valid array-like length.
 *
 * **Note:** This method is loosely based on
 * [`ToLength`](http://ecma-international.org/ecma-262/7.0/#sec-tolength).
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is a valid length, else `false`.
 * @example
 *
 * _.isLength(3);
 * // => true
 *
 * _.isLength(Number.MIN_VALUE);
 * // => false
 *
 * _.isLength(Infinity);
 * // => false
 *
 * _.isLength('3');
 * // => false
 */
function isLength(value) {
  return typeof value == 'number' &&
    value > -1 && value % 1 == 0 && value <= MAX_SAFE_INTEGER;
}

/**
 * Checks if `value` is the
 * [language type](http://www.ecma-international.org/ecma-262/7.0/#sec-ecmascript-language-types)
 * of `Object`. (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
 *
 * @static
 * @memberOf _
 * @since 0.1.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is an object, else `false`.
 * @example
 *
 * _.isObject({});
 * // => true
 *
 * _.isObject([1, 2, 3]);
 * // => true
 *
 * _.isObject(_.noop);
 * // => true
 *
 * _.isObject(null);
 * // => false
 */
function isObject(value) {
  var type = typeof value;
  return !!value && (type == 'object' || type == 'function');
}

/**
 * Checks if `value` is object-like. A value is object-like if it's not `null`
 * and has a `typeof` result of "object".
 *
 * @static
 * @memberOf _
 * @since 4.0.0
 * @category Lang
 * @param {*} value The value to check.
 * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
 * @example
 *
 * _.isObjectLike({});
 * // => true
 *
 * _.isObjectLike([1, 2, 3]);
 * // => true
 *
 * _.isObjectLike(_.noop);
 * // => false
 *
 * _.isObjectLike(null);
 * // => false
 */
function isObjectLike(value) {
  return !!value && typeof value == 'object';
}

/**
 * Assigns own enumerable string keyed properties of source objects to the
 * destination object. Source objects are applied from left to right.
 * Subsequent sources overwrite property assignments of previous sources.
 *
 * **Note:** This method mutates `object` and is loosely based on
 * [`Object.assign`](https://mdn.io/Object/assign).
 *
 * @static
 * @memberOf _
 * @since 0.10.0
 * @category Object
 * @param {Object} object The destination object.
 * @param {...Object} [sources] The source objects.
 * @returns {Object} Returns `object`.
 * @see _.assignIn
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 * }
 *
 * function Bar() {
 *   this.c = 3;
 * }
 *
 * Foo.prototype.b = 2;
 * Bar.prototype.d = 4;
 *
 * _.assign({ 'a': 0 }, new Foo, new Bar);
 * // => { 'a': 1, 'c': 3 }
 */
var assign = createAssigner(function(object, source) {
  if (nonEnumShadows || isPrototype(source) || isArrayLike(source)) {
    copyObject(source, keys(source), object);
    return;
  }
  for (var key in source) {
    if (hasOwnProperty.call(source, key)) {
      assignValue(object, key, source[key]);
    }
  }
});

/**
 * Creates an array of the own enumerable property names of `object`.
 *
 * **Note:** Non-object values are coerced to objects. See the
 * [ES spec](http://ecma-international.org/ecma-262/7.0/#sec-object.keys)
 * for more details.
 *
 * @static
 * @since 0.1.0
 * @memberOf _
 * @category Object
 * @param {Object} object The object to query.
 * @returns {Array} Returns the array of property names.
 * @example
 *
 * function Foo() {
 *   this.a = 1;
 *   this.b = 2;
 * }
 *
 * Foo.prototype.c = 3;
 *
 * _.keys(new Foo);
 * // => ['a', 'b'] (iteration order is not guaranteed)
 *
 * _.keys('hi');
 * // => ['0', '1']
 */
function keys(object) {
  return isArrayLike(object) ? arrayLikeKeys(object) : baseKeys(object);
}

module.exports = assign;


/***/ }),

/***/ "./node_modules/lru/index.js":
/*!***********************************!*\
  !*** ./node_modules/lru/index.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var events = __webpack_require__(/*! events */ "./node_modules/events/events.js")
var inherits = __webpack_require__(/*! inherits */ "./node_modules/inherits/inherits_browser.js")

module.exports = LRU

function LRU (opts) {
  if (!(this instanceof LRU)) return new LRU(opts)
  if (typeof opts === 'number') opts = {max: opts}
  if (!opts) opts = {}
  events.EventEmitter.call(this)
  this.cache = {}
  this.head = this.tail = null
  this.length = 0
  this.max = opts.max || 1000
  this.maxAge = opts.maxAge || 0
}

inherits(LRU, events.EventEmitter)

Object.defineProperty(LRU.prototype, 'keys', {
  get: function () { return Object.keys(this.cache) }
})

LRU.prototype.clear = function () {
  this.cache = {}
  this.head = this.tail = null
  this.length = 0
}

LRU.prototype.remove = function (key) {
  if (typeof key !== 'string') key = '' + key
  if (!this.cache.hasOwnProperty(key)) return

  var element = this.cache[key]
  delete this.cache[key]
  this._unlink(key, element.prev, element.next)
  return element.value
}

LRU.prototype._unlink = function (key, prev, next) {
  this.length--

  if (this.length === 0) {
    this.head = this.tail = null
  } else {
    if (this.head === key) {
      this.head = prev
      this.cache[this.head].next = null
    } else if (this.tail === key) {
      this.tail = next
      this.cache[this.tail].prev = null
    } else {
      this.cache[prev].next = next
      this.cache[next].prev = prev
    }
  }
}

LRU.prototype.peek = function (key) {
  if (!this.cache.hasOwnProperty(key)) return

  var element = this.cache[key]

  if (!this._checkAge(key, element)) return
  return element.value
}

LRU.prototype.set = function (key, value) {
  if (typeof key !== 'string') key = '' + key

  var element

  if (this.cache.hasOwnProperty(key)) {
    element = this.cache[key]
    element.value = value
    if (this.maxAge) element.modified = Date.now()

    // If it's already the head, there's nothing more to do:
    if (key === this.head) return value
    this._unlink(key, element.prev, element.next)
  } else {
    element = {value: value, modified: 0, next: null, prev: null}
    if (this.maxAge) element.modified = Date.now()
    this.cache[key] = element

    // Eviction is only possible if the key didn't already exist:
    if (this.length === this.max) this.evict()
  }

  this.length++
  element.next = null
  element.prev = this.head

  if (this.head) this.cache[this.head].next = key
  this.head = key

  if (!this.tail) this.tail = key
  return value
}

LRU.prototype._checkAge = function (key, element) {
  if (this.maxAge && (Date.now() - element.modified) > this.maxAge) {
    this.remove(key)
    this.emit('evict', {key: key, value: element.value})
    return false
  }
  return true
}

LRU.prototype.get = function (key) {
  if (typeof key !== 'string') key = '' + key
  if (!this.cache.hasOwnProperty(key)) return

  var element = this.cache[key]

  if (!this._checkAge(key, element)) return

  if (this.head !== key) {
    if (key === this.tail) {
      this.tail = element.next
      this.cache[this.tail].prev = null
    } else {
      // Set prev.next -> element.next:
      this.cache[element.prev].next = element.next
    }

    // Set element.next.prev -> element.prev:
    this.cache[element.next].prev = element.prev

    // Element is the new head
    this.cache[this.head].next = key
    element.prev = this.head
    element.next = null
    this.head = key
  }

  return element.value
}

LRU.prototype.evict = function () {
  if (!this.tail) return
  var key = this.tail
  var value = this.remove(this.tail)
  this.emit('evict', {key: key, value: value})
}


/***/ }),

/***/ "./node_modules/minimalistic-assert/index.js":
/*!***************************************************!*\
  !*** ./node_modules/minimalistic-assert/index.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = assert;

function assert(val, msg) {
  if (!val)
    throw new Error(msg || 'Assertion failed');
}

assert.equal = function assertEqual(l, r, msg) {
  if (l != r)
    throw new Error(msg || ('Assertion failed: ' + l + ' != ' + r));
};


/***/ }),

/***/ "./node_modules/mobx-react-lite/dist/index.module.js":
/*!***********************************************************!*\
  !*** ./node_modules/mobx-react-lite/dist/index.module.js ***!
  \***********************************************************/
/*! exports provided: Observer, isUsingStaticRendering, observer, optimizeScheduler, useAsObservableSource, useComputed, useDisposable, useForceUpdate, useLocalStore, useObservable, useObserver, useStaticRendering */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Observer", function() { return ObserverComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isUsingStaticRendering", function() { return isUsingStaticRendering; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "observer", function() { return observer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "optimizeScheduler", function() { return optimizeScheduler; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useAsObservableSource", function() { return useAsObservableSource; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useComputed", function() { return useComputed; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useDisposable", function() { return useDisposable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useForceUpdate", function() { return useForceUpdate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useLocalStore", function() { return useLocalStore; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useObservable", function() { return useObservable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useObserver", function() { return useObserver; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "useStaticRendering", function() { return useStaticRendering; });
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! mobx */ "mobx");
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(mobx__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);



if (!react__WEBPACK_IMPORTED_MODULE_1__["useState"]) {
    throw new Error("mobx-react-lite requires React with Hooks support");
}
if (!mobx__WEBPACK_IMPORTED_MODULE_0__["spy"]) {
    throw new Error("mobx-react-lite requires mobx at least version 4 to be available");
}

function useObservable(initialValue) {
    var observableRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
    if (!observableRef.current) {
        observableRef.current = Object(mobx__WEBPACK_IMPORTED_MODULE_0__["observable"])(initialValue);
    }
    return observableRef.current;
}

function useComputed(func, inputs) {
    if (inputs === void 0) { inputs = []; }
    var computed$1 = Object(react__WEBPACK_IMPORTED_MODULE_1__["useMemo"])(function () { return Object(mobx__WEBPACK_IMPORTED_MODULE_0__["computed"])(func); }, inputs);
    return computed$1.get();
}

var doNothingDisposer = function () {
    // empty
};
/**
 * Adds an observable effect (reaction, autorun, or anything else that returns a disposer) that will be registered upon component creation and disposed upon unmounting.
 * Returns the generated disposer for early disposal.
 *
 * @export
 * @template D
 * @param {() => D} disposerGenerator A function that returns the disposer of the wanted effect.
 * @param {ReadonlyArray<any>} [inputs=[]] If you want the effect to be automatically re-created when some variable(s) are changed then pass them in this array.
 * @returns {D}
 */
function useDisposable(disposerGenerator, inputs) {
    if (inputs === void 0) { inputs = []; }
    var disposerRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
    var earlyDisposedRef = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(false);
    Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () {
        return lazyCreateDisposer(false);
    }, inputs);
    function lazyCreateDisposer(earlyDisposal) {
        // ensure that we won't create a new disposer if it was early disposed
        if (earlyDisposedRef.current) {
            return doNothingDisposer;
        }
        if (!disposerRef.current) {
            var newDisposer = disposerGenerator();
            if (typeof newDisposer !== "function") {
                var error = new Error("generated disposer must be a function");
                {
                    // tslint:disable-next-line:no-console
                    console.error(error);
                    return doNothingDisposer;
                }
            }
            disposerRef.current = newDisposer;
        }
        return function () {
            if (disposerRef.current) {
                disposerRef.current();
                disposerRef.current = null;
            }
            if (earlyDisposal) {
                earlyDisposedRef.current = true;
            }
        };
    }
    return lazyCreateDisposer(true);
}

var globalIsUsingStaticRendering = false;
function useStaticRendering(enable) {
    globalIsUsingStaticRendering = enable;
}
function isUsingStaticRendering() {
    return globalIsUsingStaticRendering;
}

/*! *****************************************************************************
Copyright (c) Microsoft Corporation. All rights reserved.
Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at http://www.apache.org/licenses/LICENSE-2.0

THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
MERCHANTABLITY OR NON-INFRINGEMENT.

See the Apache Version 2.0 License for specific language governing permissions
and limitations under the License.
***************************************************************************** */

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function printDebugValue(v) {
    if (!v.current) {
        return "<unknown>";
    }
    return Object(mobx__WEBPACK_IMPORTED_MODULE_0__["getDependencyTree"])(v.current);
}

var EMPTY_ARRAY = [];
function useUnmount(fn) {
    Object(react__WEBPACK_IMPORTED_MODULE_1__["useEffect"])(function () { return fn; }, EMPTY_ARRAY);
}
function useForceUpdate() {
    var _a = __read(Object(react__WEBPACK_IMPORTED_MODULE_1__["useState"])(0), 2), setTick = _a[1];
    var update = Object(react__WEBPACK_IMPORTED_MODULE_1__["useCallback"])(function () {
        setTick(function (tick) { return tick + 1; });
    }, []);
    return update;
}
function isPlainObject(value) {
    if (!value || typeof value !== "object") {
        return false;
    }
    var proto = Object.getPrototypeOf(value);
    return !proto || proto === Object.prototype;
}

var EMPTY_OBJECT = {};
function useObserver(fn, baseComponentName, options) {
    if (baseComponentName === void 0) { baseComponentName = "observed"; }
    if (options === void 0) { options = EMPTY_OBJECT; }
    if (isUsingStaticRendering()) {
        return fn();
    }
    var wantedForceUpdateHook = options.useForceUpdate || useForceUpdate;
    var forceUpdate = wantedForceUpdateHook();
    var reaction = Object(react__WEBPACK_IMPORTED_MODULE_1__["useRef"])(null);
    if (!reaction.current) {
        reaction.current = new mobx__WEBPACK_IMPORTED_MODULE_0__["Reaction"]("observer(" + baseComponentName + ")", function () {
            forceUpdate();
        });
    }
    var dispose = function () {
        if (reaction.current && !reaction.current.isDisposed) {
            reaction.current.dispose();
            reaction.current = null;
        }
    };
    Object(react__WEBPACK_IMPORTED_MODULE_1__["useDebugValue"])(reaction, printDebugValue);
    useUnmount(function () {
        dispose();
    });
    // render the original component, but have the
    // reaction track the observables, so that rendering
    // can be invalidated (see above) once a dependency changes
    var rendering;
    var exception;
    reaction.current.track(function () {
        try {
            rendering = fn();
        }
        catch (e) {
            exception = e;
        }
    });
    if (exception) {
        dispose();
        throw exception; // re-throw any exceptions catched during rendering
    }
    return rendering;
}

// n.b. base case is not used for actual typings or exported in the typing files
function observer(baseComponent, options) {
    // The working of observer is explained step by step in this talk: https://www.youtube.com/watch?v=cPF4iBedoF0&feature=youtu.be&t=1307
    if (isUsingStaticRendering()) {
        return baseComponent;
    }
    var realOptions = __assign({ forwardRef: false }, options);
    var baseComponentName = baseComponent.displayName || baseComponent.name;
    var wrappedComponent = function (props, ref) {
        return useObserver(function () { return baseComponent(props, ref); }, baseComponentName);
    };
    wrappedComponent.displayName = baseComponentName;
    // memo; we are not intested in deep updates
    // in props; we assume that if deep objects are changed,
    // this is in observables, which would have been tracked anyway
    var memoComponent;
    if (realOptions.forwardRef) {
        // we have to use forwardRef here because:
        // 1. it cannot go before memo, only after it
        // 2. forwardRef converts the function into an actual component, so we can't let the baseComponent do it
        //    since it wouldn't be a callable function anymore
        memoComponent = Object(react__WEBPACK_IMPORTED_MODULE_1__["memo"])(Object(react__WEBPACK_IMPORTED_MODULE_1__["forwardRef"])(wrappedComponent));
    }
    else {
        memoComponent = Object(react__WEBPACK_IMPORTED_MODULE_1__["memo"])(wrappedComponent);
    }
    copyStaticProperties(baseComponent, memoComponent);
    memoComponent.displayName = baseComponentName;
    return memoComponent;
}
// based on https://github.com/mridgway/hoist-non-react-statics/blob/master/src/index.js
var hoistBlackList = {
    $$typeof: true,
    render: true,
    compare: true,
    type: true
};
function copyStaticProperties(base, target) {
    Object.keys(base).forEach(function (key) {
        if (base.hasOwnProperty(key) && !hoistBlackList[key]) {
            Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(base, key));
        }
    });
}

function ObserverComponent(_a) {
    var children = _a.children, render = _a.render;
    var component = children || render;
    if (typeof component !== "function") {
        return null;
    }
    return useObserver(component);
}
ObserverComponent.propTypes = {
    children: ObserverPropsCheck,
    render: ObserverPropsCheck
};
ObserverComponent.displayName = "Observer";
function ObserverPropsCheck(props, key, componentName, location, propFullName) {
    var extraKey = key === "children" ? "render" : "children";
    var hasProp = typeof props[key] === "function";
    var hasExtraProp = typeof props[extraKey] === "function";
    if (hasProp && hasExtraProp) {
        return new Error("MobX Observer: Do not use children and render in the same time in`" + componentName);
    }
    if (hasProp || hasExtraProp) {
        return null;
    }
    return new Error("Invalid prop `" +
        propFullName +
        "` of type `" +
        typeof props[key] +
        "` supplied to" +
        " `" +
        componentName +
        "`, expected `function`.");
}

function useAsObservableSourceInternal(current, usedByLocalStore) {
    if (usedByLocalStore && current === undefined) {
        return undefined;
    }
    var _a = __read(react__WEBPACK_IMPORTED_MODULE_1___default.a.useState(function () { return Object(mobx__WEBPACK_IMPORTED_MODULE_0__["observable"])(current, {}, { deep: false }); }), 1), res = _a[0];
    Object(mobx__WEBPACK_IMPORTED_MODULE_0__["runInAction"])(function () {
        Object.assign(res, current);
    });
    return res;
}
function useAsObservableSource(current) {
    return useAsObservableSourceInternal(current, false);
}

function useLocalStore(initializer, current) {
    var source = useAsObservableSourceInternal(current, true);
    return react__WEBPACK_IMPORTED_MODULE_1___default.a.useState(function () {
        var local = Object(mobx__WEBPACK_IMPORTED_MODULE_0__["observable"])(initializer(source));
        if (isPlainObject(local)) {
            Object(mobx__WEBPACK_IMPORTED_MODULE_0__["runInAction"])(function () {
                Object.keys(local).forEach(function (key) {
                    var value = local[key];
                    if (typeof value === "function") {
                        // @ts-ignore No idea why ts2536 is popping out here
                        local[key] = wrapInTransaction(value, local);
                    }
                });
            });
        }
        return local;
    })[0];
}
// tslint:disable-next-line: ban-types
function wrapInTransaction(fn, context) {
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return Object(mobx__WEBPACK_IMPORTED_MODULE_0__["transaction"])(function () { return fn.apply(context, args); });
    };
}

var optimizeScheduler = function (reactionScheduler) {
    if (typeof reactionScheduler === "function") {
        Object(mobx__WEBPACK_IMPORTED_MODULE_0__["configure"])({ reactionScheduler: reactionScheduler });
    }
};




/***/ }),

/***/ "./node_modules/mobx-react/dist/mobx-react.module.js":
/*!***********************************************************!*\
  !*** ./node_modules/mobx-react/dist/mobx-react.module.js ***!
  \***********************************************************/
/*! exports provided: Observer, useObserver, useAsObservableSource, useLocalStore, isUsingStaticRendering, useStaticRendering, observer, Provider, MobXProviderContext, inject, disposeOnUnmount, PropTypes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "observer", function() { return W; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Provider", function() { return X; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MobXProviderContext", function() { return L; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "inject", function() { return Y; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "disposeOnUnmount", function() { return G; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PropTypes", function() { return ee; });
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react-dom */ "react-dom");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_react_lite__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! mobx-react-lite */ "./node_modules/mobx-react-lite/dist/index.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Observer", function() { return mobx_react_lite__WEBPACK_IMPORTED_MODULE_1__["Observer"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "useObserver", function() { return mobx_react_lite__WEBPACK_IMPORTED_MODULE_1__["useObserver"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "useAsObservableSource", function() { return mobx_react_lite__WEBPACK_IMPORTED_MODULE_1__["useAsObservableSource"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "useLocalStore", function() { return mobx_react_lite__WEBPACK_IMPORTED_MODULE_1__["useLocalStore"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isUsingStaticRendering", function() { return mobx_react_lite__WEBPACK_IMPORTED_MODULE_1__["isUsingStaticRendering"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "useStaticRendering", function() { return mobx_react_lite__WEBPACK_IMPORTED_MODULE_1__["useStaticRendering"]; });

/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! mobx */ "mobx");
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(mobx__WEBPACK_IMPORTED_MODULE_3__);
var g=0,w={};function j(e){return w[e]||(w[e]=function(e){if("function"==typeof Symbol)return Symbol(e);var r="__$mobx-react "+e+" ("+g+")";return g++,r}(e)),w[e]}function x(e,r){if(P(e,r))return!0;if("object"!=typeof e||null===e||"object"!=typeof r||null===r)return!1;var t=Object.keys(e),o=Object.keys(r);if(t.length!==o.length)return!1;for(var n=0;n<t.length;n++)if(!hasOwnProperty.call(r,t[n])||!P(e[t[n]],r[t[n]]))return!1;return!0}function P(e,r){return e===r?0!==e||1/e==1/r:e!=e&&r!=r}var E={$$typeof:1,render:1,compare:1,type:1,childContextTypes:1,contextType:1,contextTypes:1,defaultProps:1,getDefaultProps:1,getDerivedStateFromError:1,getDerivedStateFromProps:1,mixins:1,propTypes:1};function S(e,r,t){Object.hasOwnProperty.call(e,r)?e[r]=t:Object.defineProperty(e,r,{enumerable:!1,configurable:!0,writable:!0,value:t})}var R=j("patchMixins"),k=j("patchedDefinition");function A(e,r){for(var t=this,o=[],n=arguments.length-2;n-- >0;)o[n]=arguments[n+2];r.locks++;try{var i;return null!=e&&(i=e.apply(this,o)),i}finally{r.locks--,0===r.locks&&r.methods.forEach(function(e){e.apply(t,o)})}}function C(e,r){return function(){for(var t=[],o=arguments.length;o--;)t[o]=arguments[o];A.call.apply(A,[this,e,r].concat(t))}}function U(e,r,t){var o=function(e,r){var t=e[R]=e[R]||{},o=t[r]=t[r]||{};return o.locks=o.locks||0,o.methods=o.methods||[],o}(e,r);o.methods.indexOf(t)<0&&o.methods.push(t);var n=Object.getOwnPropertyDescriptor(e,r);if(!n||!n[k]){var i=function e(r,t,o,n,i){var a,c=C(i,n);return(a={})[k]=!0,a.get=function(){return c},a.set=function(i){if(this===r)c=C(i,n);else{var a=e(this,t,o,n,i);Object.defineProperty(this,t,a)}},a.configurable=!0,a.enumerable=o,a}(e,r,n?n.enumerable:void 0,o,e[r]);Object.defineProperty(e,r,i)}}var M=mobx__WEBPACK_IMPORTED_MODULE_3__["$mobx"]||"$mobx",$=j("isUnmounted"),_=j("skipRender"),T=j("isForcingUpdate");function N(e,t){return Object(mobx_react_lite__WEBPACK_IMPORTED_MODULE_1__["isUsingStaticRendering"])()&&console.warn("[mobx-react] It seems that a re-rendering of a React component is triggered while in static (server-side) mode. Please make sure components are rendered only once server-side."),this.state!==t||!x(this.props,e)}function D(e,r){var t=j("reactProp_"+r+"_valueHolder"),o=j("reactProp_"+r+"_atomHolder");function n(){return this[o]||S(this,o,Object(mobx__WEBPACK_IMPORTED_MODULE_3__["createAtom"])("reactive "+r)),this[o]}Object.defineProperty(e,r,{configurable:!0,enumerable:!0,get:function(){return n.call(this).reportObserved(),this[t]},set:function(e){this[T]||x(this[t],e)?S(this,t,e):(S(this,t,e),S(this,_,!0),n.call(this).reportChanged(),S(this,_,!1))}})}var I="function"==typeof Symbol&&Symbol.for,q=I?Symbol.for("react.forward_ref"):"function"==typeof react__WEBPACK_IMPORTED_MODULE_2__["forwardRef"]&&Object(react__WEBPACK_IMPORTED_MODULE_2__["forwardRef"])(function(){}).$$typeof,F=I?Symbol.for("react.memo"):"function"==typeof react__WEBPACK_IMPORTED_MODULE_2__["memo"]&&Object(react__WEBPACK_IMPORTED_MODULE_2__["memo"])(function(){}).$$typeof;function W(e){if(!0===e.isMobxInjector&&console.warn("Mobx observer: You are trying to use 'observer' on a component that already has 'inject'. Please apply 'observer' before applying 'inject'"),F&&e.$$typeof===F)throw new Error("Mobx observer: You are trying to use 'observer' on function component wrapped to either another observer or 'React.memo'. The observer already applies 'React.memo' for you.");if(q&&e.$$typeof===q){var s=e.render;if("function"!=typeof s)throw new Error("render property of ForwardRef was not a function");return Object(react__WEBPACK_IMPORTED_MODULE_2__["forwardRef"])(function(){var e=arguments;return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(mobx_react_lite__WEBPACK_IMPORTED_MODULE_1__["Observer"],null,function(){return s.apply(void 0,e)})})}return"function"!=typeof e||e.prototype&&e.prototype.render||e.isReactClass||Object.prototype.isPrototypeOf.call(react__WEBPACK_IMPORTED_MODULE_2__["Component"],e)?function(e){var t=e.prototype;if(t.componentWillReact)throw new Error("The componentWillReact life-cycle event is no longer supported");if(e.__proto__!==react__WEBPACK_IMPORTED_MODULE_2__["PureComponent"])if(t.shouldComponentUpdate){if(t.shouldComponentUpdate!==N)throw new Error("It is not allowed to use shouldComponentUpdate in observer based components.")}else t.shouldComponentUpdate=N;D(t,"props"),D(t,"state");var o=t.render;return t.render=function(){return function(e){var t=this;if(!0===Object(mobx_react_lite__WEBPACK_IMPORTED_MODULE_1__["isUsingStaticRendering"])())return e.call(this);S(this,_,!1),S(this,T,!1);var o=this.displayName||this.name||this.constructor&&(this.constructor.displayName||this.constructor.name)||"<component>",n=e.bind(this),i=!1,c=new mobx__WEBPACK_IMPORTED_MODULE_3__["Reaction"](o+".render()",function(){if(!i&&(i=!0,!0!==t[$])){var e=!0;try{S(t,T,!0),t[_]||react__WEBPACK_IMPORTED_MODULE_2__["Component"].prototype.forceUpdate.call(t),e=!1}finally{S(t,T,!1),e&&c.dispose()}}});function s(){i=!1;var e=void 0,r=void 0;if(c.track(function(){try{r=Object(mobx__WEBPACK_IMPORTED_MODULE_3__["_allowStateChanges"])(!1,n)}catch(r){e=r}}),e)throw e;return r}return c.reactComponent=this,s[M]=c,this.render=s,s.call(this)}.call(this,o)},U(t,"componentWillUnmount",function(){!0!==Object(mobx_react_lite__WEBPACK_IMPORTED_MODULE_1__["isUsingStaticRendering"])()&&(this.render[M]&&this.render[M].dispose(),this[$]=!0)}),e}(e):Object(mobx_react_lite__WEBPACK_IMPORTED_MODULE_1__["observer"])(e)}var L=react__WEBPACK_IMPORTED_MODULE_2___default.a.createContext({});function X(e){var r=e.children,t=function(e,r){var t={};for(var o in e)Object.prototype.hasOwnProperty.call(e,o)&&-1===r.indexOf(o)&&(t[o]=e[o]);return t}(e,["children"]),o=react__WEBPACK_IMPORTED_MODULE_2___default.a.useContext(L),i=react__WEBPACK_IMPORTED_MODULE_2___default.a.useRef(Object.assign({},o,t)).current;if( true&&!x(i,Object.assign({},i,t)))throw new Error("MobX Provider: The set of provided stores has changed. See: https://github.com/mobxjs/mobx-react#the-set-of-provided-stores-has-changed-error.");return react__WEBPACK_IMPORTED_MODULE_2___default.a.createElement(L.Provider,{value:i},r)}function H(e,r,t,o){var i,a,c,s=react__WEBPACK_IMPORTED_MODULE_2___default.a.forwardRef(function(t,o){var i=Object.assign({},t),a=react__WEBPACK_IMPORTED_MODULE_2___default.a.useContext(L);return Object.assign(i,e(a||{},i)||{}),o&&(i.ref=o),Object(react__WEBPACK_IMPORTED_MODULE_2__["createElement"])(r,i)});return o&&(s=W(s)),s.isMobxInjector=!0,i=r,a=s,c=Object.getOwnPropertyNames(Object.getPrototypeOf(i)),Object.getOwnPropertyNames(i).forEach(function(e){E[e]||-1!==c.indexOf(e)||Object.defineProperty(a,e,Object.getOwnPropertyDescriptor(i,e))}),s.wrappedComponent=r,s.displayName=function(e,r){var t=e.displayName||e.name||e.constructor&&e.constructor.name||"Component";return r?"inject-with-"+r+"("+t+")":"inject("+t+")"}(r,t),s}function Y(){for(var e,r=[],t=arguments.length;t--;)r[t]=arguments[t];return"function"==typeof arguments[0]?(e=arguments[0],function(r){return H(e,r,e.name,!0)}):function(e){return H(function(e){return function(r,t){return e.forEach(function(e){if(!(e in t)){if(!(e in r))throw new Error("MobX injector: Store '"+e+"' is not available! Make sure it is provided by some Provider");t[e]=r[e]}}),t}}(r),e,r.join("-"),!1)}}X.displayName="MobXProvider";var V=j("disposeOnUnmountProto"),z=j("disposeOnUnmountInst");function B(){var e=this;(this[V]||[]).concat(this[z]||[]).forEach(function(r){var t="string"==typeof r?e[r]:r;null!=t&&(Array.isArray(t)?t.map(function(e){return e()}):t())})}function G(e,r){if(Array.isArray(r))return r.map(function(r){return G(e,r)});var t=Object.getPrototypeOf(e).constructor||Object.getPrototypeOf(e.constructor),o=Object.getPrototypeOf(e.constructor);if(t!==react__WEBPACK_IMPORTED_MODULE_2__["Component"]&&t!==react__WEBPACK_IMPORTED_MODULE_2__["PureComponent"]&&o!==react__WEBPACK_IMPORTED_MODULE_2__["Component"]&&o!==react__WEBPACK_IMPORTED_MODULE_2__["PureComponent"])throw new Error("[mobx-react] disposeOnUnmount only supports direct subclasses of React.Component or React.PureComponent.");if("string"!=typeof r&&"function"!=typeof r&&!Array.isArray(r))throw new Error("[mobx-react] disposeOnUnmount only works if the parameter is either a property key or a function.");var n=!!e[V]||!!e[z];return("string"==typeof r?e[V]||(e[V]=[]):e[z]||(e[z]=[])).push(r),n||U(e,"componentWillUnmount",B),"string"!=typeof r?r:void 0}function J(e){function r(r,t,o,n,i,a){for(var c=[],s=arguments.length-6;s-- >0;)c[s]=arguments[s+6];return Object(mobx__WEBPACK_IMPORTED_MODULE_3__["untracked"])(function(){return n=n||"<<anonymous>>",a=a||o,null==t[o]?r?new Error("The "+i+" `"+a+"` is marked as required in `"+n+"`, but its value is `"+(null===t[o]?"null":"undefined")+"`."):null:e.apply(void 0,[t,o,n,i,a].concat(c))})}var t=r.bind(null,!1);return t.isRequired=r.bind(null,!0),t}function K(e){var r=typeof e;return Array.isArray(e)?"array":e instanceof RegExp?"object":function(e,r){return"symbol"===e||"Symbol"===r["@@toStringTag"]||"function"==typeof Symbol&&r instanceof Symbol}(r,e)?"symbol":r}function Q(e,r){return J(function(t,o,n,i,a){return Object(mobx__WEBPACK_IMPORTED_MODULE_3__["untracked"])(function(){if(e&&K(t[o])===r.toLowerCase())return null;var i;switch(r){case"Array":i=mobx__WEBPACK_IMPORTED_MODULE_3__["isObservableArray"];break;case"Object":i=mobx__WEBPACK_IMPORTED_MODULE_3__["isObservableObject"];break;case"Map":i=mobx__WEBPACK_IMPORTED_MODULE_3__["isObservableMap"];break;default:throw new Error("Unexpected mobxType: "+r)}var c=t[o];if(!i(c)){var s=function(e){var r=K(e);if("object"===r){if(e instanceof Date)return"date";if(e instanceof RegExp)return"regexp"}return r}(c),u=e?" or javascript `"+r.toLowerCase()+"`":"";return new Error("Invalid prop `"+a+"` of type `"+s+"` supplied to `"+n+"`, expected `mobx.Observable"+r+"`"+u+".")}return null})})}function Z(e,r){return J(function(t,o,n,i,a){for(var c=[],s=arguments.length-5;s-- >0;)c[s]=arguments[s+5];return Object(mobx__WEBPACK_IMPORTED_MODULE_3__["untracked"])(function(){if("function"!=typeof r)return new Error("Property `"+a+"` of component `"+n+"` has invalid PropType notation.");var s=Q(e,"Array")(t,o,n);if(s instanceof Error)return s;for(var u=t[o],f=0;f<u.length;f++)if((s=r.apply(void 0,[u,f,n,i,a+"["+f+"]"].concat(c)))instanceof Error)return s;return null})})}var ee={observableArray:Q(!1,"Array"),observableArrayOf:Z.bind(null,!1),observableMap:Q(!1,"Map"),observableObject:Q(!1,"Object"),arrayOrObservableArray:Q(!0,"Array"),arrayOrObservableArrayOf:Z.bind(null,!0),objectOrObservableObject:Q(!0,"Object")};if(!react__WEBPACK_IMPORTED_MODULE_2__["Component"])throw new Error("mobx-react requires React to be available");if(!mobx__WEBPACK_IMPORTED_MODULE_3__["observable"])throw new Error("mobx-react requires mobx to be available");"function"==typeof react_dom__WEBPACK_IMPORTED_MODULE_0__["unstable_batchedUpdates"]&&Object(mobx__WEBPACK_IMPORTED_MODULE_3__["configure"])({reactionScheduler:react_dom__WEBPACK_IMPORTED_MODULE_0__["unstable_batchedUpdates"]});
//# sourceMappingURL=mobx-react.module.js.map


/***/ }),

/***/ "./node_modules/ms/index.js":
/*!**********************************!*\
  !*** ./node_modules/ms/index.js ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

/**
 * Helpers.
 */

var s = 1000;
var m = s * 60;
var h = m * 60;
var d = h * 24;
var w = d * 7;
var y = d * 365.25;

/**
 * Parse or format the given `val`.
 *
 * Options:
 *
 *  - `long` verbose formatting [false]
 *
 * @param {String|Number} val
 * @param {Object} [options]
 * @throws {Error} throw an error if val is not a non-empty string or a number
 * @return {String|Number}
 * @api public
 */

module.exports = function(val, options) {
  options = options || {};
  var type = typeof val;
  if (type === 'string' && val.length > 0) {
    return parse(val);
  } else if (type === 'number' && isNaN(val) === false) {
    return options.long ? fmtLong(val) : fmtShort(val);
  }
  throw new Error(
    'val is not a non-empty string or a valid number. val=' +
      JSON.stringify(val)
  );
};

/**
 * Parse the given `str` and return milliseconds.
 *
 * @param {String} str
 * @return {Number}
 * @api private
 */

function parse(str) {
  str = String(str);
  if (str.length > 100) {
    return;
  }
  var match = /^((?:\d+)?\-?\d?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|weeks?|w|years?|yrs?|y)?$/i.exec(
    str
  );
  if (!match) {
    return;
  }
  var n = parseFloat(match[1]);
  var type = (match[2] || 'ms').toLowerCase();
  switch (type) {
    case 'years':
    case 'year':
    case 'yrs':
    case 'yr':
    case 'y':
      return n * y;
    case 'weeks':
    case 'week':
    case 'w':
      return n * w;
    case 'days':
    case 'day':
    case 'd':
      return n * d;
    case 'hours':
    case 'hour':
    case 'hrs':
    case 'hr':
    case 'h':
      return n * h;
    case 'minutes':
    case 'minute':
    case 'mins':
    case 'min':
    case 'm':
      return n * m;
    case 'seconds':
    case 'second':
    case 'secs':
    case 'sec':
    case 's':
      return n * s;
    case 'milliseconds':
    case 'millisecond':
    case 'msecs':
    case 'msec':
    case 'ms':
      return n;
    default:
      return undefined;
  }
}

/**
 * Short format for `ms`.
 *
 * @param {Number} ms
 * @return {String}
 * @api private
 */

function fmtShort(ms) {
  var msAbs = Math.abs(ms);
  if (msAbs >= d) {
    return Math.round(ms / d) + 'd';
  }
  if (msAbs >= h) {
    return Math.round(ms / h) + 'h';
  }
  if (msAbs >= m) {
    return Math.round(ms / m) + 'm';
  }
  if (msAbs >= s) {
    return Math.round(ms / s) + 's';
  }
  return ms + 'ms';
}

/**
 * Long format for `ms`.
 *
 * @param {Number} ms
 * @return {String}
 * @api private
 */

function fmtLong(ms) {
  var msAbs = Math.abs(ms);
  if (msAbs >= d) {
    return plural(ms, msAbs, d, 'day');
  }
  if (msAbs >= h) {
    return plural(ms, msAbs, h, 'hour');
  }
  if (msAbs >= m) {
    return plural(ms, msAbs, m, 'minute');
  }
  if (msAbs >= s) {
    return plural(ms, msAbs, s, 'second');
  }
  return ms + ' ms';
}

/**
 * Pluralization helper.
 */

function plural(ms, msAbs, n, name) {
  var isPlural = msAbs >= n * 1.5;
  return Math.round(ms / n) + ' ' + name + (isPlural ? 's' : '');
}


/***/ }),

/***/ "./node_modules/object-assign/index.js":
/*!*********************************************!*\
  !*** ./node_modules/object-assign/index.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*
object-assign
(c) Sindre Sorhus
@license MIT
*/


/* eslint-disable no-unused-vars */
var getOwnPropertySymbols = Object.getOwnPropertySymbols;
var hasOwnProperty = Object.prototype.hasOwnProperty;
var propIsEnumerable = Object.prototype.propertyIsEnumerable;

function toObject(val) {
	if (val === null || val === undefined) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

function shouldUseNative() {
	try {
		if (!Object.assign) {
			return false;
		}

		// Detect buggy property enumeration order in older V8 versions.

		// https://bugs.chromium.org/p/v8/issues/detail?id=4118
		var test1 = new String('abc');  // eslint-disable-line no-new-wrappers
		test1[5] = 'de';
		if (Object.getOwnPropertyNames(test1)[0] === '5') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test2 = {};
		for (var i = 0; i < 10; i++) {
			test2['_' + String.fromCharCode(i)] = i;
		}
		var order2 = Object.getOwnPropertyNames(test2).map(function (n) {
			return test2[n];
		});
		if (order2.join('') !== '0123456789') {
			return false;
		}

		// https://bugs.chromium.org/p/v8/issues/detail?id=3056
		var test3 = {};
		'abcdefghijklmnopqrst'.split('').forEach(function (letter) {
			test3[letter] = letter;
		});
		if (Object.keys(Object.assign({}, test3)).join('') !==
				'abcdefghijklmnopqrst') {
			return false;
		}

		return true;
	} catch (err) {
		// We don't expect any of the above to throw, but better to be safe.
		return false;
	}
}

module.exports = shouldUseNative() ? Object.assign : function (target, source) {
	var from;
	var to = toObject(target);
	var symbols;

	for (var s = 1; s < arguments.length; s++) {
		from = Object(arguments[s]);

		for (var key in from) {
			if (hasOwnProperty.call(from, key)) {
				to[key] = from[key];
			}
		}

		if (getOwnPropertySymbols) {
			symbols = getOwnPropertySymbols(from);
			for (var i = 0; i < symbols.length; i++) {
				if (propIsEnumerable.call(from, symbols[i])) {
					to[symbols[i]] = from[symbols[i]];
				}
			}
		}
	}

	return to;
};


/***/ }),

/***/ "./node_modules/process/browser.js":
/*!*****************************************!*\
  !*** ./node_modules/process/browser.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./node_modules/react-addons-create-fragment/index.js":
/*!************************************************************!*\
  !*** ./node_modules/react-addons-create-fragment/index.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Copyright (c) 2015-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */



var React = __webpack_require__(/*! react */ "react");

var REACT_ELEMENT_TYPE =
  (typeof Symbol === 'function' && Symbol.for && Symbol.for('react.element')) ||
  0xeac7;

var emptyFunction = __webpack_require__(/*! fbjs/lib/emptyFunction */ "./node_modules/fbjs/lib/emptyFunction.js");
var invariant = __webpack_require__(/*! fbjs/lib/invariant */ "./node_modules/fbjs/lib/invariant.js");
var warning = __webpack_require__(/*! fbjs/lib/warning */ "./node_modules/fbjs/lib/warning.js");

var SEPARATOR = '.';
var SUBSEPARATOR = ':';

var didWarnAboutMaps = false;

var ITERATOR_SYMBOL = typeof Symbol === 'function' && Symbol.iterator;
var FAUX_ITERATOR_SYMBOL = '@@iterator'; // Before Symbol spec.

function getIteratorFn(maybeIterable) {
  var iteratorFn =
    maybeIterable &&
    ((ITERATOR_SYMBOL && maybeIterable[ITERATOR_SYMBOL]) ||
      maybeIterable[FAUX_ITERATOR_SYMBOL]);
  if (typeof iteratorFn === 'function') {
    return iteratorFn;
  }
}

function escape(key) {
  var escapeRegex = /[=:]/g;
  var escaperLookup = {
    '=': '=0',
    ':': '=2'
  };
  var escapedString = ('' + key).replace(escapeRegex, function(match) {
    return escaperLookup[match];
  });

  return '$' + escapedString;
}

function getComponentKey(component, index) {
  // Do some typechecking here since we call this blindly. We want to ensure
  // that we don't block potential future ES APIs.
  if (component && typeof component === 'object' && component.key != null) {
    // Explicit key
    return escape(component.key);
  }
  // Implicit key determined by the index in the set
  return index.toString(36);
}

function traverseAllChildrenImpl(
  children,
  nameSoFar,
  callback,
  traverseContext
) {
  var type = typeof children;

  if (type === 'undefined' || type === 'boolean') {
    // All of the above are perceived as null.
    children = null;
  }

  if (
    children === null ||
    type === 'string' ||
    type === 'number' ||
    // The following is inlined from ReactElement. This means we can optimize
    // some checks. React Fiber also inlines this logic for similar purposes.
    (type === 'object' && children.$$typeof === REACT_ELEMENT_TYPE)
  ) {
    callback(
      traverseContext,
      children,
      // If it's the only child, treat the name as if it was wrapped in an array
      // so that it's consistent if the number of children grows.
      nameSoFar === '' ? SEPARATOR + getComponentKey(children, 0) : nameSoFar
    );
    return 1;
  }

  var child;
  var nextName;
  var subtreeCount = 0; // Count of children found in the current subtree.
  var nextNamePrefix = nameSoFar === '' ? SEPARATOR : nameSoFar + SUBSEPARATOR;

  if (Array.isArray(children)) {
    for (var i = 0; i < children.length; i++) {
      child = children[i];
      nextName = nextNamePrefix + getComponentKey(child, i);
      subtreeCount += traverseAllChildrenImpl(
        child,
        nextName,
        callback,
        traverseContext
      );
    }
  } else {
    var iteratorFn = getIteratorFn(children);
    if (iteratorFn) {
      if (true) {
        // Warn about using Maps as children
        if (iteratorFn === children.entries) {
          warning(
            didWarnAboutMaps,
            'Using Maps as children is unsupported and will likely yield ' +
              'unexpected results. Convert it to a sequence/iterable of keyed ' +
              'ReactElements instead.'
          );
          didWarnAboutMaps = true;
        }
      }

      var iterator = iteratorFn.call(children);
      var step;
      var ii = 0;
      while (!(step = iterator.next()).done) {
        child = step.value;
        nextName = nextNamePrefix + getComponentKey(child, ii++);
        subtreeCount += traverseAllChildrenImpl(
          child,
          nextName,
          callback,
          traverseContext
        );
      }
    } else if (type === 'object') {
      var addendum = '';
      if (true) {
        addendum =
          ' If you meant to render a collection of children, use an array ' +
          'instead or wrap the object using createFragment(object) from the ' +
          'React add-ons.';
      }
      var childrenString = '' + children;
      invariant(
        false,
        'Objects are not valid as a React child (found: %s).%s',
        childrenString === '[object Object]'
          ? 'object with keys {' + Object.keys(children).join(', ') + '}'
          : childrenString,
        addendum
      );
    }
  }

  return subtreeCount;
}

function traverseAllChildren(children, callback, traverseContext) {
  if (children == null) {
    return 0;
  }

  return traverseAllChildrenImpl(children, '', callback, traverseContext);
}

var userProvidedKeyEscapeRegex = /\/+/g;
function escapeUserProvidedKey(text) {
  return ('' + text).replace(userProvidedKeyEscapeRegex, '$&/');
}

function cloneAndReplaceKey(oldElement, newKey) {
  return React.cloneElement(
    oldElement,
    {key: newKey},
    oldElement.props !== undefined ? oldElement.props.children : undefined
  );
}

var DEFAULT_POOL_SIZE = 10;
var DEFAULT_POOLER = oneArgumentPooler;

var oneArgumentPooler = function(copyFieldsFrom) {
  var Klass = this;
  if (Klass.instancePool.length) {
    var instance = Klass.instancePool.pop();
    Klass.call(instance, copyFieldsFrom);
    return instance;
  } else {
    return new Klass(copyFieldsFrom);
  }
};

var addPoolingTo = function addPoolingTo(CopyConstructor, pooler) {
  // Casting as any so that flow ignores the actual implementation and trusts
  // it to match the type we declared
  var NewKlass = CopyConstructor;
  NewKlass.instancePool = [];
  NewKlass.getPooled = pooler || DEFAULT_POOLER;
  if (!NewKlass.poolSize) {
    NewKlass.poolSize = DEFAULT_POOL_SIZE;
  }
  NewKlass.release = standardReleaser;
  return NewKlass;
};

var standardReleaser = function standardReleaser(instance) {
  var Klass = this;
  invariant(
    instance instanceof Klass,
    'Trying to release an instance into a pool of a different type.'
  );
  instance.destructor();
  if (Klass.instancePool.length < Klass.poolSize) {
    Klass.instancePool.push(instance);
  }
};

var fourArgumentPooler = function fourArgumentPooler(a1, a2, a3, a4) {
  var Klass = this;
  if (Klass.instancePool.length) {
    var instance = Klass.instancePool.pop();
    Klass.call(instance, a1, a2, a3, a4);
    return instance;
  } else {
    return new Klass(a1, a2, a3, a4);
  }
};

function MapBookKeeping(mapResult, keyPrefix, mapFunction, mapContext) {
  this.result = mapResult;
  this.keyPrefix = keyPrefix;
  this.func = mapFunction;
  this.context = mapContext;
  this.count = 0;
}
MapBookKeeping.prototype.destructor = function() {
  this.result = null;
  this.keyPrefix = null;
  this.func = null;
  this.context = null;
  this.count = 0;
};
addPoolingTo(MapBookKeeping, fourArgumentPooler);

function mapSingleChildIntoContext(bookKeeping, child, childKey) {
  var result = bookKeeping.result;
  var keyPrefix = bookKeeping.keyPrefix;
  var func = bookKeeping.func;
  var context = bookKeeping.context;

  var mappedChild = func.call(context, child, bookKeeping.count++);
  if (Array.isArray(mappedChild)) {
    mapIntoWithKeyPrefixInternal(
      mappedChild,
      result,
      childKey,
      emptyFunction.thatReturnsArgument
    );
  } else if (mappedChild != null) {
    if (React.isValidElement(mappedChild)) {
      mappedChild = cloneAndReplaceKey(
        mappedChild,
        // Keep both the (mapped) and old keys if they differ, just as
        // traverseAllChildren used to do for objects as children
        keyPrefix +
          (mappedChild.key && (!child || child.key !== mappedChild.key)
            ? escapeUserProvidedKey(mappedChild.key) + '/'
            : '') +
          childKey
      );
    }
    result.push(mappedChild);
  }
}

function mapIntoWithKeyPrefixInternal(children, array, prefix, func, context) {
  var escapedPrefix = '';
  if (prefix != null) {
    escapedPrefix = escapeUserProvidedKey(prefix) + '/';
  }
  var traverseContext = MapBookKeeping.getPooled(
    array,
    escapedPrefix,
    func,
    context
  );
  traverseAllChildren(children, mapSingleChildIntoContext, traverseContext);
  MapBookKeeping.release(traverseContext);
}

var numericPropertyRegex = /^\d+$/;

var warnedAboutNumeric = false;

function createReactFragment(object) {
  if (typeof object !== 'object' || !object || Array.isArray(object)) {
    warning(
      false,
      'React.addons.createFragment only accepts a single object. Got: %s',
      object
    );
    return object;
  }
  if (React.isValidElement(object)) {
    warning(
      false,
      'React.addons.createFragment does not accept a ReactElement ' +
        'without a wrapper object.'
    );
    return object;
  }

  invariant(
    object.nodeType !== 1,
    'React.addons.createFragment(...): Encountered an invalid child; DOM ' +
      'elements are not valid children of React components.'
  );

  var result = [];

  for (var key in object) {
    if (true) {
      if (!warnedAboutNumeric && numericPropertyRegex.test(key)) {
        warning(
          false,
          'React.addons.createFragment(...): Child objects should have ' +
            'non-numeric keys so ordering is preserved.'
        );
        warnedAboutNumeric = true;
      }
    }
    mapIntoWithKeyPrefixInternal(
      object[key],
      result,
      key,
      emptyFunction.thatReturnsArgument
    );
  }

  return result;
}

module.exports = createReactFragment;


/***/ }),

/***/ "./node_modules/react-aiot/src/style/theme-wordpress.scss":
/*!****************************************************************!*\
  !*** ./node_modules/react-aiot/src/style/theme-wordpress.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./node_modules/setimmediate/setImmediate.js":
/*!***************************************************!*\
  !*** ./node_modules/setimmediate/setImmediate.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var registerImmediate;

    function setImmediate(callback) {
      // Callback can either be a function or a string
      if (typeof callback !== "function") {
        callback = new Function("" + callback);
      }
      // Copy function arguments
      var args = new Array(arguments.length - 1);
      for (var i = 0; i < args.length; i++) {
          args[i] = arguments[i + 1];
      }
      // Store and register the task
      var task = { callback: callback, args: args };
      tasksByHandle[nextHandle] = task;
      registerImmediate(nextHandle);
      return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
        case 0:
            callback();
            break;
        case 1:
            callback(args[0]);
            break;
        case 2:
            callback(args[0], args[1]);
            break;
        case 3:
            callback(args[0], args[1], args[2]);
            break;
        default:
            callback.apply(undefined, args);
            break;
        }
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function installNextTickImplementation() {
        registerImmediate = function(handle) {
            process.nextTick(function () { runIfPresent(handle); });
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function() {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        registerImmediate = function(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function(handle) {
            channel.port2.postMessage(handle);
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        registerImmediate = function(handle) {
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
        };
    }

    function installSetTimeoutImplementation() {
        registerImmediate = function(handle) {
            setTimeout(runIfPresent, 0, handle);
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();

    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();

    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();

    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();

    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
}(typeof self === "undefined" ? typeof global === "undefined" ? this : global : self));

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js"), __webpack_require__(/*! ./../process/browser.js */ "./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./public/src/AppTree.tsx":
/*!********************************!*\
  !*** ./public/src/AppTree.tsx ***!
  \********************************/
/*! exports provided: AppTree */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppTree", function() { return AppTree; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./util */ "./public/src/util/index.tsx");
/* harmony import */ var react_aiot__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-aiot */ "react-aiot");
/* harmony import */ var react_aiot__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_aiot__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! mobx-react */ "./node_modules/mobx-react/dist/mobx-react.module.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _util_i18n__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./util/i18n */ "./public/src/util/i18n.tsx");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! jquery */ "jquery");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _components__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components */ "./public/src/components/index.tsx");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./store */ "./public/src/store/index.tsx");
/* harmony import */ var _rest__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./rest */ "./public/src/rest/index.tsx");
/* harmony import */ var _util_dragdrop__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./util/dragdrop */ "./public/src/util/dragdrop.tsx");
/* harmony import */ var _util_fastModeContent__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./util/fastModeContent */ "./public/src/util/fastModeContent.tsx");
/* harmony import */ var lil_uri__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! lil-uri */ "./node_modules/lil-uri/uri.js");
/* harmony import */ var lil_uri__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(lil_uri__WEBPACK_IMPORTED_MODULE_12__);


function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }













var DEFAULT_CREATABLE_CLASSES = "page-title-action add-new-h2";
react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].config({
  top: 50
});
/**
 * The application tree handler for Real Categories Management.
 */

var AppTree = Object(mobx_react__WEBPACK_IMPORTED_MODULE_3__["observer"])(function (props) {
  var mobile = Object(_util__WEBPACK_IMPORTED_MODULE_1__["isMobile"])(),
      store = Object(react__WEBPACK_IMPORTED_MODULE_4__["useContext"])(_store__WEBPACK_IMPORTED_MODULE_8__["storeContext"]),
      staticTree = store.staticTree,
      tree = store.tree,
      selectedId = store.selectedId,
      methodMoved301Endpoint = store.methodMoved301Endpoint,
      methodNotAllowed405Endpoint = store.methodNotAllowed405Endpoint; // States

  var _useState = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(),
      _useState2 = _slicedToArray(_useState, 2),
      createRoot = _useState2[0],
      setCreateRoot = _useState2[1],
      _useState3 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(props.typenow),
      _useState4 = _slicedToArray(_useState3, 1),
      typenow = _useState4[0],
      _useState5 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(props.taxnow),
      _useState6 = _slicedToArray(_useState5, 2),
      taxnow = _useState6[0],
      setTaxnow = _useState6[1],
      _useState7 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(props.taxos),
      _useState8 = _slicedToArray(_useState7, 1),
      taxos = _useState8[0],
      _useState9 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(!mobile),
      _useState10 = _slicedToArray(_useState9, 2),
      isSticky = _useState10[0],
      setIsSticky = _useState10[1],
      _useState11 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(!mobile),
      _useState12 = _slicedToArray(_useState11, 2),
      isStickyHeader = _useState12[0],
      setIsStickyHeader = _useState12[1],
      _useState13 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(!mobile),
      _useState14 = _slicedToArray(_useState13, 2),
      isResizable = _useState14[0],
      setIsResizable = _useState14[1],
      _useState15 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(!mobile),
      _useState16 = _slicedToArray(_useState15, 2),
      isFullWidth = _useState16[0],
      setIsFullWidth = _useState16[1],
      _useState17 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(mobile ? {
    marginLeft: 10
  } : {}),
      _useState18 = _slicedToArray(_useState17, 2),
      style = _useState18[0],
      setStyle = _useState18[1],
      _useState19 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(true),
      _useState20 = _slicedToArray(_useState19, 2),
      isToolbarActive = _useState20[0],
      setIsToolbarActive = _useState20[1],
      _useState21 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(false),
      _useState22 = _slicedToArray(_useState21, 2),
      isToolbarBusy = _useState22[0],
      setIsToolbarBusy = _useState22[1],
      _useState23 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(true),
      _useState24 = _slicedToArray(_useState23, 2),
      isSortable = _useState24[0],
      setIsSortable = _useState24[1],
      _useState25 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(true),
      _useState26 = _slicedToArray(_useState25, 2),
      isSortableDisabled = _useState26[0],
      setIsSortableDisabled = _useState26[1],
      _useState27 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(false),
      _useState28 = _slicedToArray(_useState27, 2),
      isSortableBusy = _useState28[0],
      setIsSortableBusy = _useState28[1],
      _useState29 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(false),
      _useState30 = _slicedToArray(_useState29, 2),
      isTreeBusy = _useState30[0],
      setIsTreeBusy = _useState30[1],
      _useState31 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(false),
      _useState32 = _slicedToArray(_useState31, 2),
      isCreatableLinkDisabled = _useState32[0],
      setIsCreatableLinkDisabled = _useState32[1],
      _useState33 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(false),
      _useState34 = _slicedToArray(_useState33, 2),
      isCreatableLinkCancel = _useState34[0],
      setIsCreatableLinkCancel = _useState34[1],
      _useState35 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(false),
      _useState36 = _slicedToArray(_useState35, 2),
      isTreeLinkDisabled = _useState36[0],
      setIsTreeLinkDisabled = _useState36[1],
      _useState37 = Object(react__WEBPACK_IMPORTED_MODULE_4__["useState"])(_util__WEBPACK_IMPORTED_MODULE_1__["isSortMode"] ? "order" : undefined),
      _useState38 = _slicedToArray(_useState37, 2),
      toolbarActiveButton = _useState38[0],
      setToolbarActiveButton = _useState38[1];
  /**
   * Handle tax switching.
   */


  var handleTaxSwitch = function handleTaxSwitch(_ref) {
    var key = _ref.key;
    return setTaxnow(key);
  };
  /**
   * Dismiss the license notice for a given time (transient).
   */


  var handleDismissLicenseNotice =
  /*#__PURE__*/
  function () {
    var _ref2 = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return Object(_util__WEBPACK_IMPORTED_MODULE_1__["ajax"])({
                location: _rest__WEBPACK_IMPORTED_MODULE_9__["locationRestNoticeLicenseDelete"]
              });

            case 2:
              window.location.reload();

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function handleDismissLicenseNotice() {
      return _ref2.apply(this, arguments);
    };
  }();
  /**
   * A node item must be an observer (mobx).
   */


  var handleTreeNodeRender = function handleTreeNodeRender(createTreeNode, TreeNodeS, node) {
    return React.createElement(mobx_react__WEBPACK_IMPORTED_MODULE_3__["Observer"], {
      key: node.id
    }, function () {
      return createTreeNode(node);
    });
  };
  /**
   * MobX states should only hold serializable data so use strings for icons
   * and render it manually.
   */


  var handleRenderIcon = function handleRenderIcon(icon) {
    switch (icon) {
      case "folder-open":
        return _util__WEBPACK_IMPORTED_MODULE_1__["ICON_OBJ_FOLDER_OPEN"];

      case "folder":
        return _util__WEBPACK_IMPORTED_MODULE_1__["ICON_OBJ_FOLDER_CLOSED"];

      default:
        return React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Icon"], {
          type: icon
        });
    }
  };
  /**
   * Fetch category tree for the current taxonomy.
   *
   * @param remember If true the taxonomy gets saved for the next page reload
   */


  var fetchTree = function fetchTree() {
    var remember = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
    var taxonomy = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : taxnow;
    var type = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : typenow;
    var currentUrl = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : window.location.href;
    store.setTree([]); // Reset tree for rerendering performance for massive tree's

    setIsTreeBusy(true);
    store.fetchTree({
      remember: remember,
      taxonomy: taxonomy,
      type: type,
      currentUrl: currentUrl
    }, function () {
      return setIsTreeBusy(false);
    });
  }; // componentDidMount


  Object(react__WEBPACK_IMPORTED_MODULE_4__["useEffect"])(function () {
    // Make posts draggable
    Object(_util_dragdrop__WEBPACK_IMPORTED_MODULE_10__["draggable"])(); // Responsiveness

    jquery__WEBPACK_IMPORTED_MODULE_6___default()(window).resize(function () {
      var rmobile = Object(_util__WEBPACK_IMPORTED_MODULE_1__["isMobile"])();
      setIsSticky(!rmobile);
      setIsStickyHeader(!rmobile);
      setIsResizable(!rmobile);
      setIsFullWidth(rmobile);
      setStyle(rmobile ? {
        marginLeft: 10
      } : {});
    });
  }, []); // Listen to taxonomy changes and reload the tree

  Object(react__WEBPACK_IMPORTED_MODULE_4__["useEffect"])(function () {
    return fetchTree(true, taxnow);
  }, [taxnow]); // componentDidUpdate

  Object(react__WEBPACK_IMPORTED_MODULE_4__["useEffect"])(function () {
    Object(_util_dragdrop__WEBPACK_IMPORTED_MODULE_10__["droppable"])(props.id, function () {
      return fetchTree();
    });
  });
  /**
   * A node gets selected. Depending on the fast mode the page gets reloaded
   * or the wp list table gets reloaded.
   */

  var handleSelect =
  /*#__PURE__*/
  function () {
    var _ref3 = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(id) {
      var url, node, setter;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              if (!(toolbarActiveButton === "sort")) {
                _context2.next = 2;
                break;
              }

              return _context2.abrupt("return");

            case 2:
              // Generate URL with query args
              url = window.location.href;

              if (id === _store__WEBPACK_IMPORTED_MODULE_8__["STATIC_TREE_ID_ALL"]) {
                url = Object(_util__WEBPACK_IMPORTED_MODULE_1__["addUrlParam"])(window.location.pathname, "post_type", typenow);
              } else {
                node = store.getTreeItemById(id);
                jquery__WEBPACK_IMPORTED_MODULE_6___default.a.each(node.properties.queryArgs, function (key, value) {
                  url = Object(_util__WEBPACK_IMPORTED_MODULE_1__["addUrlParam"])(url, key, value);
                });
              }

              +Object(_util__WEBPACK_IMPORTED_MODULE_1__["urlParam"])("paged") > 1 && (url = Object(_util__WEBPACK_IMPORTED_MODULE_1__["addUrlParam"])(url, "paged", 1)); // Update busy state for selected item

              setter = function setter($busy) {
                return store.getTreeItemById(id, false).setter(function (node) {
                  node.selected = true;
                  node.$busy = $busy;
                });
              };

              if (props.isFastMode && !_util__WEBPACK_IMPORTED_MODULE_1__["isSortMode"]) {
                // Fast mode active so load URL and get content
                setter(false);
                setTimeout(function () {
                  return Object(_util_fastModeContent__WEBPACK_IMPORTED_MODULE_11__["load"])(url).then(function () {
                    return Object(_util_dragdrop__WEBPACK_IMPORTED_MODULE_10__["draggable"])();
                  });
                }, 0);
              } else {
                window.location.href = url;
                setter(true);
              }

            case 7:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function handleSelect(_x) {
      return _ref3.apply(this, arguments);
    };
  }();
  /**
   * Handle rename node states (helper).
   */


  var handleRenameNode = function handleRenameNode(shToolbarActiveButton, shIsCreatableLinkDisabled, shIsTreeLinkDisabled, nodeRename) {
    // Make other nodes editable / not editable
    setIsCreatableLinkDisabled(shIsCreatableLinkDisabled);
    setIsTreeLinkDisabled(shIsTreeLinkDisabled);
    setToolbarActiveButton(shToolbarActiveButton); // Make selected node editable / not editable

    store.getTreeItemById(store.selectedId).setter(function (node) {
      node.$rename = nodeRename;
    });
  };
  /**
   * Handle rename click and enable the input field if necessery.
   */


  var handleRenameClick = function handleRenameClick() {
    return handleRenameNode("rename", true, true, true);
  };
  /**
   * Cancel current rename proces.
   */


  var handleRenameCancel = function handleRenameCancel() {
    return handleRenameNode(undefined, false, false, undefined);
  };
  /**
   * F2 also starts the rename process.
   */


  var handleNodePressF2 = function handleNodePressF2(_ref4) {
    var id = _ref4.id;
    return id !== _store__WEBPACK_IMPORTED_MODULE_8__["STATIC_TREE_ID_ALL"] && handleRenameClick(undefined);
  };
  /**
   * Handle rename close and depending on the save state create the new node.
   */


  var handleRenameClose =
  /*#__PURE__*/
  function () {
    var _ref5 = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(save, inputValue, _ref6) {
      var id, hide;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              id = _ref6.id;

              if (!(save && inputValue.length)) {
                _context3.next = 19;
                break;
              }

              hide = react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].loading(Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])('Renaming "%s"...', inputValue));
              _context3.prev = 3;
              _context3.next = 6;
              return store.getTreeItemById(id).setName(inputValue);

            case 6:
              react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].success(Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])('"%s" successfully renamed.', inputValue));
              handleRenameCancel(undefined, undefined);
              _context3.next = 14;
              break;

            case 10:
              _context3.prev = 10;
              _context3.t0 = _context3["catch"](3);
              react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].error(_context3.t0.responseJSON.message);
              store.getTreeItemById(id).setter(function (node) {
                return node.$busy = false;
              });

            case 14:
              _context3.prev = 14;
              hide();
              return _context3.finish(14);

            case 17:
              _context3.next = 20;
              break;

            case 19:
              handleRenameCancel(undefined, undefined);

            case 20:
            case "end":
              return _context3.stop();
          }
        }
      }, _callee3, null, [[3, 10, 14, 17]]);
    }));

    return function handleRenameClose(_x2, _x3, _x4) {
      return _ref5.apply(this, arguments);
    };
  }();
  /**
   * Handles the creatable click and creates a new node depending on the selected one.
   */


  var handleCreatable = function handleCreatable(type) {
    var shCreateRoot, $create;

    if (type) {
      // Activate create
      var newNode = {
        $rename: true,
        $busy: false,
        icon: "folder-open"
      };

      if (selectedId === _store__WEBPACK_IMPORTED_MODULE_8__["STATIC_TREE_ID_ALL"]) {
        shCreateRoot = newNode;
      } else {
        $create = newNode;
        $create.parent = selectedId;
      }
    }

    setIsTreeLinkDisabled(!!type);
    setIsCreatableLinkCancel(!!type);
    setIsToolbarActive(!type);
    setCreateRoot(shCreateRoot);
    store.getTreeItemById(selectedId, false).setter(function (node) {
      return node.$create = $create;
    });
  };

  var handleCreatableClick = function handleCreatableClick(type) {
    return handleCreatable(type);
  };

  var handleCreatableCancel = function handleCreatableCancel() {
    return handleCreatable();
  };
  /**
   * Updates the create node. That's the node without id and the input field.
   */


  var updateCreateNode = function updateCreateNode(callback) {
    createRoot && setCreateRoot(react_aiot__WEBPACK_IMPORTED_MODULE_2__["immer"].produce(createRoot, callback));
    var node = store.getTreeItemById(selectedId);
    node && node.setter(function (shNode) {
      if (shNode.$create) {
        var obj = _objectSpread({}, shNode.$create);

        callback(obj);
        shNode.$create = obj;
      }
    });
  };
  /**
   * Handle add close and remove the new node.
   */


  var handleAddClose =
  /*#__PURE__*/
  function () {
    var _ref7 = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4(save, name, node) {
      var parent, hide;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              parent = node.parent || 0;

              if (!save) {
                _context4.next = 21;
                break;
              }

              updateCreateNode(function (shNode) {
                shNode.$busy = true;
              });
              hide = react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].loading(Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])('Creating "%s"...', name));
              _context4.prev = 4;
              _context4.next = 7;
              return store.persist({
                name: name,
                parent: parent,
                type: typenow,
                taxonomy: taxnow
              });

            case 7:
              handleCreatableCancel(undefined);
              react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].success(Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])('"%s" successfully created.', name));
              Object(_util_dragdrop__WEBPACK_IMPORTED_MODULE_10__["droppable"])(props.id, function () {
                return fetchTree();
              });
              _context4.next = 16;
              break;

            case 12:
              _context4.prev = 12;
              _context4.t0 = _context4["catch"](4);
              react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].error(_context4.t0.responseJSON.message);
              updateCreateNode(function (shNode) {
                shNode.$busy = false;
              });

            case 16:
              _context4.prev = 16;
              hide();
              return _context4.finish(16);

            case 19:
              _context4.next = 22;
              break;

            case 21:
              handleCreatableCancel(undefined);

            case 22:
            case "end":
              return _context4.stop();
          }
        }
      }, _callee4, null, [[4, 12, 16, 19]]);
    }));

    return function handleAddClose(_x5, _x6, _x7) {
      return _ref7.apply(this, arguments);
    };
  }();
  /**
   * Reload the current view (hard page reload).
   */


  var handleReload = function handleReload() {
    return window.location.reload();
  };
  /**
   * Handle trashing of a category. If the category has subcategories the
   * trash is forbidden.
   */


  var handleTrash =
  /*#__PURE__*/
  function () {
    var _ref8 = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
      var node, hide, parentId;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              node = store.getTreeItemById(store.selectedId); // Check if subdirectories

              if (!node.childNodes.filter(function (n) {
                return n.$visible;
              }).length) {
                _context5.next = 3;
                break;
              }

              return _context5.abrupt("return", react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].error(Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])('You can not delete "%s" because it contains subcategories.', [node.title])));

            case 3:
              hide = react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].loading(Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])('Deleting "%s"...', node.title));
              _context5.prev = 4;
              _context5.next = 7;
              return node.trash();

            case 7:
              react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].success(Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])('"%s" successfully deleted.', node.title)); // Select parent

              parentId = Object(react_aiot__WEBPACK_IMPORTED_MODULE_2__["getTreeParentById"])(node.id, tree);
              handleSelect(parentId === 0 ? _store__WEBPACK_IMPORTED_MODULE_8__["STATIC_TREE_ID_ALL"] : parentId);
              _context5.next = 15;
              break;

            case 12:
              _context5.prev = 12;
              _context5.t0 = _context5["catch"](4);
              react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].error(_context5.t0.responseJSON.message);

            case 15:
              _context5.prev = 15;
              hide();
              return _context5.finish(15);

            case 18:
            case "end":
              return _context5.stop();
          }
        }
      }, _callee5, null, [[4, 12, 15, 18]]);
    }));

    return function handleTrash() {
      return _ref8.apply(this, arguments);
    };
  }();
  /**
   * When opening the trash symbol in the toolbar do a Popconfirm instead of simple click.
   */


  var handleTrashModifier = function handleTrashModifier(body) {
    var node = store.getTreeItemById(store.selectedId);
    return node ? React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Popconfirm"], {
      overlayStyle: {
        maxWidth: 254
      },
      placement: "bottom",
      onConfirm: handleTrash,
      title: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])('Are you sure to delete "%s"? Note: The posts in this category are NOT deleted automatically', [node.title]),
      okText: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Ok"),
      cancelText: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Cancel")
    }, body) : body;
  };
  /**
   * When opening the order symbol show a hint when the plugin is not installed / activated
   * or the pages can not be ordered (no hierarchy).
   */


  var handleOrderModifier = function handleOrderModifier(body) {
    if (!_util__WEBPACK_IMPORTED_MODULE_1__["isSortModeAvailable"]) {
      if (!_util__WEBPACK_IMPORTED_MODULE_1__["pluginOptions"].others.simplePageOrdering) {
        // Install plugin
        body = React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Popconfirm"], {
          placement: "bottom",
          overlayStyle: {
            maxWidth: 254
          },
          onConfirm: function onConfirm() {
            return window.location.href = _util__WEBPACK_IMPORTED_MODULE_1__["pluginOptions"].others.simplePageOrderingLink;
          },
          title: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])('If you want to use custom order functionality for this post type you must install and activate the plugin "Simple Page Ordering".'),
          okText: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Install plugin"),
          cancelText: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Cancel")
        }, body);
      } else {
        // Post type can not be reordered
        body = React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Popconfirm"], {
          placement: "bottom",
          overlayStyle: {
            maxWidth: 254
          },
          onConfirm: function onConfirm() {
            return window.open("https://wordpress.org/plugins/simple-page-ordering/#faq-header", "_blank");
          },
          title: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])('"Simple Page Ordering" is not available here. You can only reorder post types with hierarchical structure and an "Order" attribute.'),
          okText: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Learn more"),
          cancelText: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Cancel")
        }, body);
      }
    }

    return body;
  };
  /**
   * When clicking the order button check if ordering is possible here and
   * redirect to the given parameterized url.
   */


  var handleOrderClick = function handleOrderClick() {
    _util__WEBPACK_IMPORTED_MODULE_1__["isSortModeAvailable"] && (window.location.href = Object(_util__WEBPACK_IMPORTED_MODULE_1__["$byOrder"])().find("a").attr("href"));
  };
  /**
   * Detects automatically if Simple Page Ordering is currently open and
   * cancel with a given parameterized url.
   */


  var handleOrderCancel = function handleOrderCancel() {
    var url = lil_uri__WEBPACK_IMPORTED_MODULE_12___default()(window.location.href),
        query = url.query();
    delete query.orderby;
    delete query.order;
    url.query(query);
    window.location.href = url.build();
  };
  /**
   * Handle the sort toolbar button to activate the sortable tree.
   */


  var handleSortNode = function handleSortNode(shToolbarActiveButton, isBusy) {
    setIsCreatableLinkDisabled(!!shToolbarActiveButton);
    setToolbarActiveButton(shToolbarActiveButton);
    setIsSortableDisabled(!shToolbarActiveButton);
    typeof isBusy === "boolean" && setIsSortableBusy(isBusy);
    typeof isBusy === "boolean" && setIsToolbarBusy(isBusy);
  };

  var handleSortClick = function handleSortClick() {
    return handleSortNode("sort");
  };

  var handleSortCancel = function handleSortCancel() {
    return handleSortNode();
  };
  /**
   * Handle categories sorting and update the tree so the changes are visible. If sorting
   * is cancelled the old tree gets restored.
   */


  var handleSort =
  /*#__PURE__*/
  function () {
    var _ref9 = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6(_ref10) {
      var id, oldIndex, newIndex, parentFromId, parentToId, nextId, hide;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              id = _ref10.id, oldIndex = _ref10.oldIndex, newIndex = _ref10.newIndex, parentFromId = _ref10.parentFromId, parentToId = _ref10.parentToId, nextId = _ref10.nextId;
              setIsSortableBusy(true);
              setIsToolbarBusy(true);
              hide = react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].loading(Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("The category tree will be reordered soon..."));
              _context6.prev = 4;
              _context6.next = 7;
              return store.handleSort({
                id: +id,
                oldIndex: oldIndex,
                newIndex: newIndex,
                parentFromId: +parentFromId,
                parentToId: +parentToId,
                nextId: +nextId
              }, typenow, taxnow);

            case 7:
              react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].success(Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("The category tree is successfully reordered."));
              _context6.next = 13;
              break;

            case 10:
              _context6.prev = 10;
              _context6.t0 = _context6["catch"](4);
              react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].error(_context6.t0.responseJSON.message);

            case 13:
              _context6.prev = 13;
              hide();
              handleSortNode(toolbarActiveButton, false);
              return _context6.finish(13);

            case 17:
            case "end":
              return _context6.stop();
          }
        }
      }, _callee6, null, [[4, 10, 13, 17]]);
    }));

    return function handleSort(_x8) {
      return _ref9.apply(this, arguments);
    };
  }(); // Rendering process


  var treeProps = {
    staticTree: staticTree,
    tree: tree,
    id: props.id,
    createRoot: createRoot,
    isSticky: isSticky,
    isStickyHeader: isStickyHeader,
    isResizable: isResizable,
    isFullWidth: isFullWidth,
    style: style,
    isToolbarActive: isToolbarActive,
    isToolbarBusy: isToolbarBusy,
    isSortable: isSortable,
    isSortableDisabled: isSortableDisabled,
    isSortableBusy: isSortableBusy,
    isTreeBusy: isTreeBusy,
    isCreatableLinkDisabled: isCreatableLinkDisabled,
    isCreatableLinkCancel: isCreatableLinkCancel,
    isTreeLinkDisabled: isTreeLinkDisabled,
    toolbarActiveButton: toolbarActiveButton
  };
  return React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2___default.a, _extends({}, treeProps, {
    onSelect: handleSelect,
    headline: React.createElement("span", {
      style: {
        paddingRight: 5
      }
    }, Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Categories")),
    opposite: document.getElementById("wpbody-content"),
    attr: {
      "data-type": typenow,
      "data-tax": taxnow
    },
    renameSaveText: React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Icon"], {
      type: "save"
    }),
    renameAddText: React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Icon"], {
      type: "save"
    }),
    noFoldersTitle: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("No category found"),
    noFoldersDescription: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Click the above button to create a new category."),
    noSearchResult: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("No search results found"),
    innerClassName: "wrap",
    theme: "wordpress",
    sortableDelay: 0,
    headerStickyAttr: {
      top: "#wpadminbar"
    },
    renderItem: handleTreeNodeRender,
    renderIcon: handleRenderIcon,
    onRenameClose: handleRenameClose,
    onAddClose: handleAddClose,
    onNodePressF2: handleNodePressF2,
    onNodeExpand: function onNodeExpand() {
      return setTimeout(function () {
        return Object(_util_dragdrop__WEBPACK_IMPORTED_MODULE_10__["droppable"])(props.id, function () {
          return fetchTree();
        });
      }, 200);
    },
    onSort: handleSort,
    creatable: {
      backButton: {
        cssClasses: DEFAULT_CREATABLE_CLASSES,
        label: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Cancel"),
        onClick: handleCreatableCancel
      },
      buttons: {
        folder: {
          icon: React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Icon"], {
            type: "folder-add"
          }),
          cssClasses: DEFAULT_CREATABLE_CLASSES,
          toolTipTitle: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Click this to create a new category"),
          toolTipText: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("If you want to create a sub-category simply select a category from the list and click this button."),
          label: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("New"),
          onClick: handleCreatableClick
        }
      }
    },
    toolbar: {
      backButton: {
        label: _util__WEBPACK_IMPORTED_MODULE_1__["isSortMode"] || toolbarActiveButton === "sort" ? Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Back") : Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Cancel"),
        save: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Save")
      },
      buttons: {
        order: {
          content: React.createElement(_components__WEBPACK_IMPORTED_MODULE_7__["DashIcon"], {
            name: "move"
          }),
          toolTipTitle: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Reorder entries"),
          toolTipText: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["_i"])(Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Start to reorder the entries with the help of {{strong}}Simple Page Ordering{{/strong}}."), {
            strong: React.createElement("strong", null)
          }),
          modifier: handleOrderModifier,
          onClick: handleOrderClick,
          onCancel: handleOrderCancel
        },
        reload: {
          content: React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Icon"], {
            type: "reload"
          }),
          toolTipTitle: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Refresh"),
          toolTipText: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Refreshes the current category view."),
          onClick: handleReload
        },
        rename: {
          content: React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Icon"], {
            type: "edit"
          }),
          toolTipTitle: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Rename"),
          toolTipText: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Rename the current selected category."),
          disabled: selectedId === _store__WEBPACK_IMPORTED_MODULE_8__["STATIC_TREE_ID_ALL"],
          onClick: handleRenameClick,
          onCancel: handleRenameCancel
        },
        trash: {
          content: React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Icon"], {
            type: "delete"
          }),
          toolTipTitle: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Delete"),
          toolTipText: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Delete the current selected category."),
          disabled: selectedId === _store__WEBPACK_IMPORTED_MODULE_8__["STATIC_TREE_ID_ALL"],
          modifier: handleTrashModifier
        },
        sort: {
          content: React.createElement(_components__WEBPACK_IMPORTED_MODULE_7__["DashIcon"], {
            name: "sort"
          }),
          toolTipTitle: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Rearrange"),
          toolTipText: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Change the hierarchical order of the categories."),
          onClick: handleSortClick,
          onCancel: handleSortCancel
        }
      }
    },
    forceSortableFallback: true
  }), !!methodNotAllowed405Endpoint.length && React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Alert"], {
    message: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["_i"])(Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("An error occured during requesting the Real Category Management REST endpoint {{strong}}%s{{/strong}} from the server (405 Method not allowed). One reason can be that your server configuration is missing something. Learn more about it {{a1}}here{{/a1}} and {{a2}}here{{/a2}} (external links).", methodNotAllowed405Endpoint), {
      strong: React.createElement("strong", null),
      a1: React.createElement("a", {
        href: "https://airbrake.io/blog/http-errors/405-method-not-allowed",
        target: "_blank"
      }),
      a2: React.createElement("a", {
        href: "https://matthias-web.com/2018/05/31/release-4-0-complete-code-rewrite/#Troubleshooting",
        target: "_blank"
      })
    }),
    type: "error",
    showIcon: true,
    style: {
      marginBottom: "10px"
    }
  }), _util__WEBPACK_IMPORTED_MODULE_1__["pluginOptions"].others.showLicenseNotice && React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Alert"], {
    message: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["_i"])(Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("Product not activated, yet. {{a}}Activate now{{/a}} or {{d}}dismiss notice{{/d}}."), {
      a: React.createElement("a", {
        href: _util__WEBPACK_IMPORTED_MODULE_1__["pluginOptions"].others.pluginsUrl
      }),
      d: React.createElement("a", {
        href: "#",
        onClick: handleDismissLicenseNotice
      })
    }),
    type: "info",
    style: {
      marginBottom: "10px"
    }
  }), methodMoved301Endpoint && React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Alert"], {
    message: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])('It seems the tree could not be fetched because the WP REST API endpoint "tree" is not reachable. Do you use a plugin which disables the WP REST API like "Clearfy"?'),
    type: "error",
    showIcon: true,
    style: {
      marginBottom: "10px"
    }
  }), !_util__WEBPACK_IMPORTED_MODULE_1__["isPro"] && _util__WEBPACK_IMPORTED_MODULE_1__["showProHints"] && _util__WEBPACK_IMPORTED_MODULE_1__["pluginOptions"].others.showLiteNotice && React.createElement(_components__WEBPACK_IMPORTED_MODULE_7__["ProFooter"], {
    dismissable: true
  }), Object.keys(taxos).length > 1 && React.createElement("div", {
    style: {
      margin: "2px 0px 9px 0",
      textAlign: "right"
    }
  }, React.createElement(_components__WEBPACK_IMPORTED_MODULE_7__["TaxSwitcher"], {
    disabled: !!toolbarActiveButton,
    taxnow: taxnow,
    taxos: taxos,
    onClick: handleTaxSwitch
  })));
});


/***/ }),

/***/ "./public/src/admin.tsx":
/*!******************************!*\
  !*** ./public/src/admin.tsx ***!
  \******************************/
/*! exports provided: pluginOptions, isPro, showProHints, process, trailingslashit, untrailingslashit, urlParam, addUrlParam, applyNodeDefaults, hooks, isMobile, ICON_OBJ_FOLDER_OPEN, ICON_OBJ_FOLDER_CLOSED, $byOrder, isSortModeAvailable, isSortMode, StoredAppTree, STATIC_TREE_ID_ALL, Store, store, storeContext, StoreContextProvider, ERouteHttpVerb, urlBuilder, ajax, WP_REST_API_ENDPOINT, Hooks, _n, _nx, __, _x, _i, TreeNode */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _style_style_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./style/style.scss */ "./public/src/style/style.scss");
/* harmony import */ var _style_style_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_style_style_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_aiot_src_style_theme_wordpress_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-aiot/src/style/theme-wordpress.scss */ "./node_modules/react-aiot/src/style/theme-wordpress.scss");
/* harmony import */ var react_aiot_src_style_theme_wordpress_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_aiot_src_style_theme_wordpress_scss__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./util */ "./public/src/util/index.tsx");
/* harmony import */ var _util_dragdrop__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./util/dragdrop */ "./public/src/util/dragdrop.tsx");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jquery */ "jquery");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var setimmediate__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! setimmediate */ "./node_modules/setimmediate/setImmediate.js");
/* harmony import */ var setimmediate__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(setimmediate__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./store */ "./public/src/store/index.tsx");
/* harmony import */ var _util_fastModeContent__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./util/fastModeContent */ "./public/src/util/fastModeContent.tsx");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-dom */ "react-dom");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! mobx */ "mobx");
/* harmony import */ var mobx__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(mobx__WEBPACK_IMPORTED_MODULE_10__);
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "pluginOptions", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["pluginOptions"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isPro", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["isPro"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "showProHints", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["showProHints"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "process", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["process"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "trailingslashit", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["trailingslashit"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "untrailingslashit", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["untrailingslashit"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "urlParam", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["urlParam"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "addUrlParam", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["addUrlParam"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "applyNodeDefaults", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["applyNodeDefaults"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "hooks", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["hooks"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isMobile", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["isMobile"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ICON_OBJ_FOLDER_OPEN", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["ICON_OBJ_FOLDER_OPEN"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ICON_OBJ_FOLDER_CLOSED", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["ICON_OBJ_FOLDER_CLOSED"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "$byOrder", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["$byOrder"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isSortModeAvailable", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["isSortModeAvailable"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "isSortMode", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["isSortMode"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ERouteHttpVerb", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["ERouteHttpVerb"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "urlBuilder", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["urlBuilder"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ajax", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["ajax"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "WP_REST_API_ENDPOINT", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["WP_REST_API_ENDPOINT"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Hooks", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["Hooks"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "_n", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["_n"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "_nx", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["_nx"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "__", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["__"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "_x", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["_x"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "_i", function() { return _util__WEBPACK_IMPORTED_MODULE_3__["_i"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StoredAppTree", function() { return _store__WEBPACK_IMPORTED_MODULE_7__["StoredAppTree"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "STATIC_TREE_ID_ALL", function() { return _store__WEBPACK_IMPORTED_MODULE_7__["STATIC_TREE_ID_ALL"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Store", function() { return _store__WEBPACK_IMPORTED_MODULE_7__["Store"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "store", function() { return _store__WEBPACK_IMPORTED_MODULE_7__["store"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "storeContext", function() { return _store__WEBPACK_IMPORTED_MODULE_7__["storeContext"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StoreContextProvider", function() { return _store__WEBPACK_IMPORTED_MODULE_7__["StoreContextProvider"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TreeNode", function() { return _store__WEBPACK_IMPORTED_MODULE_7__["TreeNode"]; });



function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

/**
 * The entry point for the admin side wp-admin resource.
 */





 // Polyfill for yielding





Object(mobx__WEBPACK_IMPORTED_MODULE_10__["configure"])({
  isolateGlobalState: true
});
/**
 * General event when script for RCL is ready to load.
 *
 * @event module:util/hooks#general
 */

_util__WEBPACK_IMPORTED_MODULE_3__["hooks"].call("general");
jquery__WEBPACK_IMPORTED_MODULE_5___default()(document).ready(
/*#__PURE__*/
_asyncToGenerator(
/*#__PURE__*/
_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
  var containerId, _pluginOptions$others, typenow, taxnow, taxos, allowsFastMode, $container;

  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          if (_util__WEBPACK_IMPORTED_MODULE_3__["pluginOptions"] && _util__WEBPACK_IMPORTED_MODULE_3__["pluginOptions"].others.screenSettings.isActive) {
            containerId = "rcl" + _util__WEBPACK_IMPORTED_MODULE_3__["pluginOptions"].others.blogId, _pluginOptions$others = _util__WEBPACK_IMPORTED_MODULE_3__["pluginOptions"].others, typenow = _pluginOptions$others.typenow, taxnow = _pluginOptions$others.taxnow, taxos = _pluginOptions$others.taxos, allowsFastMode = !!(_util__WEBPACK_IMPORTED_MODULE_3__["pluginOptions"].others.screenSettings.isFastMode && window.history && window.history.pushState);
            /**
             * General event when DOM is ready and a list table is available.
             *
             * @event module:admin#ready
             */

            _util__WEBPACK_IMPORTED_MODULE_3__["hooks"].call("ready"); // Create the container sidebar

            jquery__WEBPACK_IMPORTED_MODULE_5___default()("body").addClass("activate-aiot");
            $container = jquery__WEBPACK_IMPORTED_MODULE_5___default()("<div/>").prependTo("body.wp-admin #wpbody").addClass("rcl-container"); // Deactivate Simple Page Ordering

            !_util__WEBPACK_IMPORTED_MODULE_3__["isSortMode"] && jquery__WEBPACK_IMPORTED_MODULE_5___default()(".wp-list-table tbody.ui-sortable").sortable("destroy"); // Create the wrapper and React component

            Object(react_dom__WEBPACK_IMPORTED_MODULE_9__["render"])(React.createElement(_store__WEBPACK_IMPORTED_MODULE_7__["StoredAppTree"], {
              id: containerId,
              typenow: typenow,
              taxnow: taxnow,
              taxos: taxos,
              isFastMode: allowsFastMode
            }), $container.get(0));
            Object(_util_dragdrop__WEBPACK_IMPORTED_MODULE_4__["anyKeyHolding"])(); // Handler for pagination

            allowsFastMode && jquery__WEBPACK_IMPORTED_MODULE_5___default()(document).on("click", ".pagination-links a", function (e) {
              Object(_util_fastModeContent__WEBPACK_IMPORTED_MODULE_8__["load"])(jquery__WEBPACK_IMPORTED_MODULE_5___default()(this).attr("href")).then(function () {
                Object(_util_dragdrop__WEBPACK_IMPORTED_MODULE_4__["draggable"])();
              });
              e.preventDefault();
              return false;
            });
          }

        case 1:
        case "end":
          return _context.stop();
      }
    }
  }, _callee);
}))); // Warn message when something went wrong with AJAX response (405 not allowed)

jquery__WEBPACK_IMPORTED_MODULE_5___default()(document).on("ajaxComplete", function (event, _ref2, _ref3) {
  var status = _ref2.status;
  var url = _ref3.url,
      type = _ref3.type;

  if (url.indexOf(_util__WEBPACK_IMPORTED_MODULE_3__["WP_REST_API_ENDPOINT"]) > -1 && status === 405) {
    _store__WEBPACK_IMPORTED_MODULE_7__["store"].setter(function (node) {
      return node.methodNotAllowed405Endpoint = type + " " + url;
    });
  }
});
jquery__WEBPACK_IMPORTED_MODULE_5___default()("link#dark_mode-css").length && jquery__WEBPACK_IMPORTED_MODULE_5___default()("body").addClass("aiot-wp-dark-mode");



/***/ }),

/***/ "./public/src/components/ProFooter.tsx":
/*!*********************************************!*\
  !*** ./public/src/components/ProFooter.tsx ***!
  \*********************************************/
/*! exports provided: ProFooter, ProLink */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProFooter", function() { return ProFooter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProLink", function() { return ProLink; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../util */ "./public/src/util/index.tsx");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _rest__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../rest */ "./public/src/rest/index.tsx");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





function dismiss() {
  return _dismiss.apply(this, arguments);
}

function _dismiss() {
  _dismiss = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return Object(_util__WEBPACK_IMPORTED_MODULE_1__["ajax"])({
              location: _rest__WEBPACK_IMPORTED_MODULE_3__["locationRestNoticeLiteDelete"]
            });

          case 2:
            window.location.reload();

          case 3:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _dismiss.apply(this, arguments);
}

var ProFooter = function ProFooter(_ref) {
  var _ref$dismissable = _ref.dismissable,
      dismissable = _ref$dismissable === void 0 ? false : _ref$dismissable;
  return React.createElement("div", {
    className: "notice inline",
    style: {
      margin: "0 0 5px",
      textAlign: "left"
    }
  }, React.createElement("p", {
    style: {
      fontSize: 11
    },
    className: "wp-clearfix"
  }, Object(_util__WEBPACK_IMPORTED_MODULE_1__["__"])("Thanks for using Real Categories Management Lite."), " \xB7", " ", React.createElement("a", {
    href: _util__WEBPACK_IMPORTED_MODULE_1__["pluginOptions"].others.proUrl,
    target: "_blank"
  }, Object(_util__WEBPACK_IMPORTED_MODULE_1__["__"])("Learn more about Pro (external link)")), dismissable && React.createElement("a", {
    style: {
      marginLeft: 10,
      "float": "right"
    },
    onClick: dismiss,
    href: "#"
  }, Object(_util__WEBPACK_IMPORTED_MODULE_1__["__"])("Dismiss 20 days"))));
};

function redirect(e) {
  if (!window.confirm(Object(_util__WEBPACK_IMPORTED_MODULE_1__["__"])("You will redirected to the external website of Pro version, confirm to continue."))) {
    return e.preventDefault();
  }
}

var ProLink = function ProLink(_ref2) {
  var _ref2$brackets = _ref2.brackets,
      brackets = _ref2$brackets === void 0 ? false : _ref2$brackets;
  return React.createElement(react__WEBPACK_IMPORTED_MODULE_2__["Fragment"], null, brackets && "(", React.createElement("a", {
    href: _util__WEBPACK_IMPORTED_MODULE_1__["pluginOptions"].others.proUrl,
    onClick: redirect,
    target: "_blank"
  }, Object(_util__WEBPACK_IMPORTED_MODULE_1__["__"])("Pro feature")), brackets && ")");
};



/***/ }),

/***/ "./public/src/components/TaxSwitcher.tsx":
/*!***********************************************!*\
  !*** ./public/src/components/TaxSwitcher.tsx ***!
  \***********************************************/
/*! exports provided: TaxSwitcher */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TaxSwitcher", function() { return TaxSwitcher; });
/* harmony import */ var react_aiot__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react-aiot */ "react-aiot");
/* harmony import */ var react_aiot__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_aiot__WEBPACK_IMPORTED_MODULE_0__);


/**
 * Create a tax switcher if more than one taxonomy is available for the post type.
 *
 * @param props.taxos Key value map of available taxonomies
 * @param props.taxnow The current taxonomy
 * @param props.disabled If true the taxonomy can not be switched
 * @param props.onClick A taxonomy is selected
 */
var TaxSwitcher = function TaxSwitcher(_ref) {
  var taxos = _ref.taxos,
      taxnow = _ref.taxnow,
      disabled = _ref.disabled,
      onClick = _ref.onClick;

  if (true) {
    var keys = Object.keys(taxos);

    if (keys.length < 1) {
      return null;
    } // Create tax switcher overlay


    var overlayMenu = keys.map(function (key) {
      var label = taxos[key];
      return React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_0__["Menu"].Item, {
        key: key
      }, label);
    }),
        overlay = React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_0__["Menu"], {
      selectedKeys: [taxnow],
      onClick: onClick
    }, overlayMenu);
    return React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_0__["Dropdown"], {
      placement: "bottomRight",
      overlay: overlay,
      disabled: disabled
    }, React.createElement("a", {
      className: "ant-dropdown-link",
      href: "javascript:void(0)",
      style: {
        textDecoration: "none"
      }
    }, taxos[taxnow], " ", React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_0__["Icon"], {
      type: "down"
    })));
  } else {}
};



/***/ }),

/***/ "./public/src/components/index.tsx":
/*!*****************************************!*\
  !*** ./public/src/components/index.tsx ***!
  \*****************************************/
/*! exports provided: DashIcon, TaxSwitcher, ProFooter, ProLink */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashIcon", function() { return DashIcon; });
/* harmony import */ var _TaxSwitcher__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TaxSwitcher */ "./public/src/components/TaxSwitcher.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TaxSwitcher", function() { return _TaxSwitcher__WEBPACK_IMPORTED_MODULE_0__["TaxSwitcher"]; });

/* harmony import */ var _ProFooter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProFooter */ "./public/src/components/ProFooter.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ProFooter", function() { return _ProFooter__WEBPACK_IMPORTED_MODULE_1__["ProFooter"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ProLink", function() { return _ProFooter__WEBPACK_IMPORTED_MODULE_1__["ProLink"]; });

/**
 * Create a WordPress dash icon.
 *
 * @see https://developer.wordpress.org/resource/dashicons/ Available icons
 */
var DashIcon = function DashIcon(_ref) {
  var name = _ref.name;
  return React.createElement("span", {
    className: "dashicons dashicons-" + name
  });
};





/***/ }),

/***/ "./public/src/rest/hierarchy.put.tsx":
/*!*******************************************!*\
  !*** ./public/src/rest/hierarchy.put.tsx ***!
  \*******************************************/
/*! exports provided: locationRestHierarchyPut */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locationRestHierarchyPut", function() { return locationRestHierarchyPut; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../util */ "./public/src/util/index.tsx");

var locationRestHierarchyPut = {
  path: "hierarchy/:id",
  method: _util__WEBPACK_IMPORTED_MODULE_0__["ERouteHttpVerb"].PUT
};

/***/ }),

/***/ "./public/src/rest/index.tsx":
/*!***********************************!*\
  !*** ./public/src/rest/index.tsx ***!
  \***********************************/
/*! exports provided: locationRestPluginGet, locationRestPostsBulkMovePut, locationRestTermsPut, locationRestTermsDelete, locationRestHierarchyPut, locationRestTermsPost, locationRestTreeGet, locationRestNoticeLicenseDelete, locationRestNoticeLiteDelete */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _plugin_get__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./plugin.get */ "./public/src/rest/plugin.get.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "locationRestPluginGet", function() { return _plugin_get__WEBPACK_IMPORTED_MODULE_0__["locationRestPluginGet"]; });

/* harmony import */ var _posts_bulk_move_put__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./posts-bulk-move.put */ "./public/src/rest/posts-bulk-move.put.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "locationRestPostsBulkMovePut", function() { return _posts_bulk_move_put__WEBPACK_IMPORTED_MODULE_1__["locationRestPostsBulkMovePut"]; });

/* harmony import */ var _terms_put__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./terms.put */ "./public/src/rest/terms.put.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "locationRestTermsPut", function() { return _terms_put__WEBPACK_IMPORTED_MODULE_2__["locationRestTermsPut"]; });

/* harmony import */ var _terms_delete__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./terms.delete */ "./public/src/rest/terms.delete.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "locationRestTermsDelete", function() { return _terms_delete__WEBPACK_IMPORTED_MODULE_3__["locationRestTermsDelete"]; });

/* harmony import */ var _hierarchy_put__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./hierarchy.put */ "./public/src/rest/hierarchy.put.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "locationRestHierarchyPut", function() { return _hierarchy_put__WEBPACK_IMPORTED_MODULE_4__["locationRestHierarchyPut"]; });

/* harmony import */ var _terms_post__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./terms.post */ "./public/src/rest/terms.post.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "locationRestTermsPost", function() { return _terms_post__WEBPACK_IMPORTED_MODULE_5__["locationRestTermsPost"]; });

/* harmony import */ var _tree_get__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tree.get */ "./public/src/rest/tree.get.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "locationRestTreeGet", function() { return _tree_get__WEBPACK_IMPORTED_MODULE_6__["locationRestTreeGet"]; });

/* harmony import */ var _notice_license_delete__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./notice-license.delete */ "./public/src/rest/notice-license.delete.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "locationRestNoticeLicenseDelete", function() { return _notice_license_delete__WEBPACK_IMPORTED_MODULE_7__["locationRestNoticeLicenseDelete"]; });

/* harmony import */ var _notice_lite_delete__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./notice-lite.delete */ "./public/src/rest/notice-lite.delete.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "locationRestNoticeLiteDelete", function() { return _notice_lite_delete__WEBPACK_IMPORTED_MODULE_8__["locationRestNoticeLiteDelete"]; });











/***/ }),

/***/ "./public/src/rest/notice-license.delete.tsx":
/*!***************************************************!*\
  !*** ./public/src/rest/notice-license.delete.tsx ***!
  \***************************************************/
/*! exports provided: locationRestNoticeLicenseDelete */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locationRestNoticeLicenseDelete", function() { return locationRestNoticeLicenseDelete; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../util */ "./public/src/util/index.tsx");

var locationRestNoticeLicenseDelete = {
  path: "notice/license",
  method: _util__WEBPACK_IMPORTED_MODULE_0__["ERouteHttpVerb"].DELETE
};

/***/ }),

/***/ "./public/src/rest/notice-lite.delete.tsx":
/*!************************************************!*\
  !*** ./public/src/rest/notice-lite.delete.tsx ***!
  \************************************************/
/*! exports provided: locationRestNoticeLiteDelete */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locationRestNoticeLiteDelete", function() { return locationRestNoticeLiteDelete; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../util */ "./public/src/util/index.tsx");

var locationRestNoticeLiteDelete = {
  path: "notice/lite",
  method: _util__WEBPACK_IMPORTED_MODULE_0__["ERouteHttpVerb"].DELETE
};

/***/ }),

/***/ "./public/src/rest/plugin.get.tsx":
/*!****************************************!*\
  !*** ./public/src/rest/plugin.get.tsx ***!
  \****************************************/
/*! exports provided: locationRestPluginGet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locationRestPluginGet", function() { return locationRestPluginGet; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../util */ "./public/src/util/index.tsx");

var locationRestPluginGet = {
  path: "plugin",
  method: _util__WEBPACK_IMPORTED_MODULE_0__["ERouteHttpVerb"].GET
};

/***/ }),

/***/ "./public/src/rest/posts-bulk-move.put.tsx":
/*!*************************************************!*\
  !*** ./public/src/rest/posts-bulk-move.put.tsx ***!
  \*************************************************/
/*! exports provided: locationRestPostsBulkMovePut */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locationRestPostsBulkMovePut", function() { return locationRestPostsBulkMovePut; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../util */ "./public/src/util/index.tsx");

var locationRestPostsBulkMovePut = {
  path: "posts/bulk/move",
  method: _util__WEBPACK_IMPORTED_MODULE_0__["ERouteHttpVerb"].PUT
};

/***/ }),

/***/ "./public/src/rest/terms.delete.tsx":
/*!******************************************!*\
  !*** ./public/src/rest/terms.delete.tsx ***!
  \******************************************/
/*! exports provided: locationRestTermsDelete */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locationRestTermsDelete", function() { return locationRestTermsDelete; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../util */ "./public/src/util/index.tsx");

var locationRestTermsDelete = {
  path: "terms/:id",
  method: _util__WEBPACK_IMPORTED_MODULE_0__["ERouteHttpVerb"].DELETE
};

/***/ }),

/***/ "./public/src/rest/terms.post.tsx":
/*!****************************************!*\
  !*** ./public/src/rest/terms.post.tsx ***!
  \****************************************/
/*! exports provided: locationRestTermsPost */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locationRestTermsPost", function() { return locationRestTermsPost; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../util */ "./public/src/util/index.tsx");

var locationRestTermsPost = {
  path: "terms",
  method: _util__WEBPACK_IMPORTED_MODULE_0__["ERouteHttpVerb"].POST
};

/***/ }),

/***/ "./public/src/rest/terms.put.tsx":
/*!***************************************!*\
  !*** ./public/src/rest/terms.put.tsx ***!
  \***************************************/
/*! exports provided: locationRestTermsPut */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locationRestTermsPut", function() { return locationRestTermsPut; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../util */ "./public/src/util/index.tsx");

var locationRestTermsPut = {
  path: "terms/:id",
  method: _util__WEBPACK_IMPORTED_MODULE_0__["ERouteHttpVerb"].PUT
};

/***/ }),

/***/ "./public/src/rest/tree.get.tsx":
/*!**************************************!*\
  !*** ./public/src/rest/tree.get.tsx ***!
  \**************************************/
/*! exports provided: locationRestTreeGet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "locationRestTreeGet", function() { return locationRestTreeGet; });
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../util */ "./public/src/util/index.tsx");

var locationRestTreeGet = {
  path: "tree",
  method: _util__WEBPACK_IMPORTED_MODULE_0__["ERouteHttpVerb"].GET
};

/***/ }),

/***/ "./public/src/store/TreeNode.tsx":
/*!***************************************!*\
  !*** ./public/src/store/TreeNode.tsx ***!
  \***************************************/
/*! exports provided: TreeNode */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TreeNode", function() { return TreeNode; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../util */ "./public/src/util/index.tsx");
/* harmony import */ var mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! mobx-state-tree */ "mobx-state-tree");
/* harmony import */ var mobx_state_tree__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jquery */ "jquery");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_aiot__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react-aiot */ "react-aiot");
/* harmony import */ var react_aiot__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_aiot__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _rest__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../rest */ "./public/src/rest/index.tsx");






/**
 * The store holding general data for categories.
 * See React AIOT TreeNode documentation for properties and defaults
 */

var TreeNode = mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__["types"].model("RCLTreeNode", {
  id: mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__["types"].union(mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__["types"].identifierNumber, mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__["types"].identifier),
  hash: "",
  className: "",
  icon: "",
  iconActive: "",
  childNodes: mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__["types"].optional(mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__["types"].array(mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__["types"].late(function () {
    return TreeNode;
  })), []),
  title: "",
  count: 0,
  isTreeLinkDisabled: false,
  selected: false,
  $busy: false,
  $droppable: true,
  $visible: true,
  $rename: false,
  $create: mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__["types"].optional(mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__["types"].frozen(), undefined),
  properties: mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__["types"].optional(mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__["types"].frozen(), undefined),
  isQueried: true
}).actions(function (self) {
  function getTypedChildNodes() {
    return self.childNodes;
  }
  /**
   * Update this node attributes.
   *
   * @param callback The callback with one argument (node draft)
   * @param setHash If true the hash node is changed so a rerender is forced
   */


  function setter(callback) {
    var setHash = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
    callback(self);
    setHash && (self.hash = Object(react_aiot__WEBPACK_IMPORTED_MODULE_4__["uuid"])());
  }
  /**
   * Rename tree node.
   */


  var setName = Object(mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__["flow"])(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(inputValue) {
    var result;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            setter(function (node) {
              node.$busy = true;
            });
            _context.prev = 1;
            _context.next = 4;
            return Object(_util__WEBPACK_IMPORTED_MODULE_1__["ajax"])({
              location: _rest__WEBPACK_IMPORTED_MODULE_5__["locationRestTermsPut"],
              params: {
                id: +self.id
              },
              request: {
                name: inputValue,
                taxonomy: self.properties.taxonomy
              }
            });

          case 4:
            result = _context.sent;
            setter(function (node) {
              node.title = inputValue;
              node.properties = jquery__WEBPACK_IMPORTED_MODULE_3___default.a.extend({}, node.properties, result);
              node.$busy = false;
            });
            return _context.abrupt("return", result);

          case 9:
            _context.prev = 9;
            _context.t0 = _context["catch"](1);
            setter(function (node) {
              node.$busy = false;
            }, !!self.id);
            throw _context.t0;

          case 13:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[1, 9]]);
  }));
  /**
   * Permanently delete category.
   */

  var trash = Object(mobx_state_tree__WEBPACK_IMPORTED_MODULE_2__["flow"])(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            setter(function (node) {
              node.$busy = true;
            });
            _context2.prev = 1;
            _context2.next = 4;
            return Object(_util__WEBPACK_IMPORTED_MODULE_1__["ajax"])({
              location: _rest__WEBPACK_IMPORTED_MODULE_5__["locationRestTermsDelete"],
              params: {
                id: +self.id,
                taxonomy: self.properties.taxonomy
              }
            });

          case 4:
            setter(function (node) {
              node.$visible = false;
            });
            _context2.next = 10;
            break;

          case 7:
            _context2.prev = 7;
            _context2.t0 = _context2["catch"](1);
            throw _context2.t0;

          case 10:
            _context2.prev = 10;
            setter(function (node) {
              node.$busy = false;
            });
            return _context2.finish(10);

          case 13:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2, null, [[1, 7, 10, 13]]);
  }));
  return {
    getTypedChildNodes: getTypedChildNodes,
    setter: setter,
    setName: setName,
    trash: trash
  };
});


/***/ }),

/***/ "./public/src/store/index.tsx":
/*!************************************!*\
  !*** ./public/src/store/index.tsx ***!
  \************************************/
/*! exports provided: StoredAppTree, STATIC_TREE_ID_ALL, Store, store, storeContext, StoreContextProvider, TreeNode */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoredAppTree", function() { return StoredAppTree; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "STATIC_TREE_ID_ALL", function() { return STATIC_TREE_ID_ALL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Store", function() { return Store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "store", function() { return store; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "storeContext", function() { return storeContext; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoreContextProvider", function() { return StoreContextProvider; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! mobx-state-tree */ "mobx-state-tree");
/* harmony import */ var mobx_state_tree__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _TreeNode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./TreeNode */ "./public/src/store/TreeNode.tsx");
/* harmony import */ var _util__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../util */ "./public/src/util/index.tsx");
/* harmony import */ var _rest__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../rest */ "./public/src/rest/index.tsx");
/* harmony import */ var _util_i18n__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../util/i18n */ "./public/src/util/i18n.tsx");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _AppTree__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../AppTree */ "./public/src/AppTree.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "TreeNode", function() { return _TreeNode__WEBPACK_IMPORTED_MODULE_2__["TreeNode"]; });









var STATIC_TREE_ID_ALL = "ALL";
/**
 * The main store for the RCL application. It holds a static tree and
 * the fetched tree from the server.
 */

var Store = mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["types"].model("RCLStore", {
  staticTree: mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["types"].array(_TreeNode__WEBPACK_IMPORTED_MODULE_2__["TreeNode"]),
  tree: mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["types"].optional(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["types"].array(_TreeNode__WEBPACK_IMPORTED_MODULE_2__["TreeNode"]), []),
  refs: mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["types"].optional(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["types"].map(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["types"].reference(_TreeNode__WEBPACK_IMPORTED_MODULE_2__["TreeNode"])), {}),
  selectedId: mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["types"].optional(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["types"].union(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["types"].string, mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["types"].number), 0),
  // Do not fill manually, it is filled in afterCreated through onPatch
  methodNotAllowed405Endpoint: mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["types"].optional(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["types"].string, ""),
  methodMoved301Endpoint: false
}).views(function (self) {
  /**
   * Get tree item by id.
   */
  function getTreeItemById(id) {
    var excludeStatic = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;
    var result = Object(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["resolveIdentifier"])(_TreeNode__WEBPACK_IMPORTED_MODULE_2__["TreeNode"], self, !!+id ? +id : id);

    if (excludeStatic && self.staticTree.indexOf(result) > -1) {
      return undefined;
    }

    return result;
  }

  return {
    getTreeItemById: getTreeItemById,

    get selected() {
      return getTreeItemById(self.selectedId, false);
    }

  };
}).actions(function (self) {
  /**
   * The model is created so watch for specific properties. For example set
   * the selected property.
   */
  function afterCreate() {
    Object(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["onPatch"])(self, function (_ref) {
      var op = _ref.op,
          path = _ref.path,
          value = _ref.value;

      // A new selected item is setted
      if ((path.startsWith("/tree/") || path.startsWith("/staticTree/")) && path.endsWith("/selected") && value === true) {
        var currentSelected = self.selected;
        currentSelected && currentSelected.setter(function (node) {
          node.selected = false;
        }); // There is a bug in MobX when using actions inside afterCreate together with onPatch
        // so use unprotect and protect.

        Object(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["unprotect"])(self);
        self.selectedId = Object(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["resolvePath"])(self, path.slice(0, path.length - 9)).id;
        Object(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["protect"])(self);
      }
    });
  }
  /**
   * Update this node attributes.
   */


  function setter(callback) {
    callback(self);
  }
  /**
   * Set the tree.
   */


  function setTree(tree) {
    var isStatic = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    if (isStatic) {
      self.staticTree.replace(tree);
    } else {
      self.tree.replace(tree);
    }
  }
  /**
   * Handle sort mechanism.
   *
   * @returns {boolean}
   * @throws
   */


  var handleSort = Object(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["flow"])(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref2, type, taxonomy) {
    var id, oldIndex, newIndex, parentFromId, parentToId, nextId, _ref2$request, request, tree, treeItem;

    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            id = _ref2.id, oldIndex = _ref2.oldIndex, newIndex = _ref2.newIndex, parentFromId = _ref2.parentFromId, parentToId = _ref2.parentToId, nextId = _ref2.nextId, _ref2$request = _ref2.request, request = _ref2$request === void 0 ? true : _ref2$request;
            tree = self.tree; // Find parent trees with children

            if (parentFromId === 0) {
              treeItem = tree[oldIndex]; // tree.splice(oldIndex, 1);
            } else {
              self.getTreeItemById(parentFromId).setter(function (node) {
                treeItem = node.getTypedChildNodes()[oldIndex]; // node.childNodes.splice(oldIndex, 1);
              }, true);
            }

            Object(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["detach"])(treeItem); // Find destination tree

            if (parentToId === 0) {
              tree.splice(newIndex, 0, treeItem);
            } else {
              self.getTreeItemById(parentToId).setter(function (node) {
                node.childNodes.splice(newIndex, 0, treeItem);
              }, true);
            }

            if (request) {
              _context.next = 7;
              break;
            }

            return _context.abrupt("return", true);

          case 7:
            _context.prev = 7;
            _context.next = 10;
            return Object(_util__WEBPACK_IMPORTED_MODULE_3__["ajax"])({
              location: _rest__WEBPACK_IMPORTED_MODULE_4__["locationRestHierarchyPut"],
              params: {
                id: id
              },
              request: {
                nextId: nextId,
                parent: parentToId,
                type: type,
                taxonomy: taxonomy
              }
            });

          case 10:
            return _context.abrupt("return", true);

          case 13:
            _context.prev = 13;
            _context.t0 = _context["catch"](7);
            _context.next = 17;
            return handleSort({
              id: id,
              oldIndex: newIndex,
              newIndex: oldIndex,
              parentFromId: parentToId,
              parentToId: parentFromId,
              nextId: nextId,
              request: false
            }, type, taxonomy);

          case 17:
            throw _context.t0;

          case 18:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, null, [[7, 13]]);
  }));
  /**
   * Create a new tree node.
   */

  var persist = Object(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["flow"])(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(request) {
    var result, newObj, parent;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return Object(_util__WEBPACK_IMPORTED_MODULE_3__["ajax"])({
              location: _rest__WEBPACK_IMPORTED_MODULE_4__["locationRestTermsPost"],
              request: request
            });

          case 2:
            result = _context2.sent;
            newObj = Object(_util__WEBPACK_IMPORTED_MODULE_3__["applyNodeDefaults"])([result])[0];
            // Add it to the tree
            parent = request.parent;

            if (parent === 0) {
              self.tree.push(newObj);
            } else {
              self.getTreeItemById(parent).setter(function (node) {
                node.childNodes.push(newObj);
              }, true);
            }

            return _context2.abrupt("return", self.getTreeItemById(newObj.id));

          case 7:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  /**
   * Fetch the folder tree.
   */

  var fetchTree = Object(mobx_state_tree__WEBPACK_IMPORTED_MODULE_1__["flow"])(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3(params, callback) {
    var result, selectedId, tree;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return Object(_util__WEBPACK_IMPORTED_MODULE_3__["ajax"])({
              location: _rest__WEBPACK_IMPORTED_MODULE_4__["locationRestTreeGet"],
              params: params
            });

          case 2:
            result = _context3.sent;
            selectedId = result.selectedId;
            // Check if result received
            !result.tree && (self.methodMoved301Endpoint = true);
            tree = Object(_util__WEBPACK_IMPORTED_MODULE_3__["applyNodeDefaults"])(result.tree);
            setTree(tree);
            self.getTreeItemById(selectedId, false).setter(function (node) {
              node.selected = true;
            });
            callback && callback(result);
            return _context3.abrupt("return", result);

          case 10:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return {
    afterCreate: afterCreate,
    setter: setter,
    setTree: setTree,
    handleSort: handleSort,
    persist: persist,
    fetchTree: fetchTree
  };
});
var store = Store.create({
  staticTree: [// @ts-ignore See https://git.io/fjiRW, https://git.io/fjiRl and https://git.io/JeU5v
  {
    id: STATIC_TREE_ID_ALL,
    title: Object(_util_i18n__WEBPACK_IMPORTED_MODULE_5__["__"])("All posts"),
    icon: "copy",
    count: _util__WEBPACK_IMPORTED_MODULE_3__["pluginOptions"].others.allPostCnt
  }]
});
/**
 * Context for mobx-react so it can be used with hooks
 */

var storeContext = Object(react__WEBPACK_IMPORTED_MODULE_6__["createContext"])(null);
/**
 * Make {@link storeContext} available in function components.
 */

var StoreContextProvider = function StoreContextProvider(_ref3) {
  var children = _ref3.children;
  return React.createElement(storeContext.Provider, {
    value: store
  }, children);
};
/**
 * An AppTree implementation with store provided. This means you have no longer
 * implement the Provider of mobx here.
 */


var StoredAppTree = function StoredAppTree(opts) {
  return React.createElement(StoreContextProvider, null, React.createElement(_AppTree__WEBPACK_IMPORTED_MODULE_7__["AppTree"], opts));
};



/***/ }),

/***/ "./public/src/style/style.scss":
/*!*************************************!*\
  !*** ./public/src/style/style.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./public/src/util/ajax.tsx":
/*!**********************************!*\
  !*** ./public/src/util/ajax.tsx ***!
  \**********************************/
/*! exports provided: ERouteHttpVerb, urlBuilder, ajax, WP_REST_API_ENDPOINT */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ERouteHttpVerb", function() { return ERouteHttpVerb; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "urlBuilder", function() { return urlBuilder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ajax", function() { return ajax; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WP_REST_API_ENDPOINT", function() { return WP_REST_API_ENDPOINT; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! . */ "./public/src/util/index.tsx");
/* harmony import */ var lil_uri__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! lil-uri */ "./node_modules/lil-uri/uri.js");
/* harmony import */ var lil_uri__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lil_uri__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jquery */ "jquery");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_3__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



 // Use _method instead of Http verb

var WP_REST_API_USE_GLOBAL_METHOD = true,
    WP_REST_API_ENDPOINT = "realcategorylibrary/v1";
var ERouteHttpVerb;

(function (ERouteHttpVerb) {
  ERouteHttpVerb["GET"] = "GET";
  ERouteHttpVerb["POST"] = "POST";
  ERouteHttpVerb["PUT"] = "PUT";
  ERouteHttpVerb["DELETE"] = "DELETE";
})(ERouteHttpVerb || (ERouteHttpVerb = {}));

// Result in JSON-format
function urlBuilder(_ref) {
  var location = _ref.location,
      _ref$params = _ref.params,
      params = _ref$params === void 0 ? {} : _ref$params,
      _ref$nonce = _ref.nonce,
      nonce = _ref$nonce === void 0 ? true : _ref$nonce;
  var apiUrl = lil_uri__WEBPACK_IMPORTED_MODULE_2___default()(___WEBPACK_IMPORTED_MODULE_1__["pluginOptions"].restRoot),
      windowProtocol = lil_uri__WEBPACK_IMPORTED_MODULE_2___default()(window.location.href).protocol(),
      query = apiUrl.query() || {},
      permalinkPath = query.rest_route || apiUrl.path(); // Determine path from permalink settings
  // Generate dynamic path

  var foundParams = [],
      path = location.path.replace(/\:([A-Za-z0-9-_]+)/g, function (match, group) {
    foundParams.push(group);
    return params[group];
  }),
      getParams = {};
  Object.keys(params).filter(function (x) {
    return foundParams.indexOf(x) === -1;
  }).forEach(function (x) {
    getParams[x] = encodeURIComponent(params[x]);
  });
  var usePath = Object(___WEBPACK_IMPORTED_MODULE_1__["trailingslashit"])(permalinkPath) + Object(___WEBPACK_IMPORTED_MODULE_1__["trailingslashit"])(location.namespace || WP_REST_API_ENDPOINT) + path; // Set https if site url is SSL

  if (windowProtocol === "https") {
    apiUrl.protocol("https");
  } // Set path depending on permalink settings


  if (query.rest_route) {
    query.rest_route = usePath;
  } else {
    apiUrl.path(usePath); // Set path
  } // Append others


  if (nonce) {
    query._wpnonce = ___WEBPACK_IMPORTED_MODULE_1__["pluginOptions"].restNonce;
  }

  if (WP_REST_API_USE_GLOBAL_METHOD && location.method !== ERouteHttpVerb.GET) {
    query._method = location.method;
  }

  return apiUrl.query(jquery__WEBPACK_IMPORTED_MODULE_3___default.a.extend(true, {}, ___WEBPACK_IMPORTED_MODULE_1__["pluginOptions"].restQuery, getParams, query));
}

function ajax(_x) {
  return _ajax.apply(this, arguments);
}

function _ajax() {
  _ajax = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(_ref2) {
    var location, request, params, _ref2$settings, settings, builtUrl, result;

    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            location = _ref2.location, request = _ref2.request, params = _ref2.params, _ref2$settings = _ref2.settings, settings = _ref2$settings === void 0 ? {} : _ref2$settings;
            builtUrl = urlBuilder({
              location: location,
              params: params,
              nonce: false
            }); // Use global parameter (see https://developer.wordpress.org/rest-api/using-the-rest-api/global-parameters/)

            if (WP_REST_API_USE_GLOBAL_METHOD && location.method !== ERouteHttpVerb.GET) {
              settings.method = "POST";
            }

            _context.next = 5;
            return jquery__WEBPACK_IMPORTED_MODULE_3___default.a.ajax(jquery__WEBPACK_IMPORTED_MODULE_3___default.a.extend(true, settings, {
              url: builtUrl.build(),
              headers: {
                "X-WP-Nonce": ___WEBPACK_IMPORTED_MODULE_1__["pluginOptions"].restNonce
              },
              data: request
            }));

          case 5:
            result = _context.sent;
            return _context.abrupt("return", result);

          case 7:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _ajax.apply(this, arguments);
}



/***/ }),

/***/ "./public/src/util/dragdrop.tsx":
/*!**************************************!*\
  !*** ./public/src/util/dragdrop.tsx ***!
  \**************************************/
/*! exports provided: anyKeyHolding, droppable, draggable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "anyKeyHolding", function() { return anyKeyHolding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "droppable", function() { return droppable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "draggable", function() { return draggable; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-dom */ "react-dom");
/* harmony import */ var react_dom__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_dom__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_aiot__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-aiot */ "react-aiot");
/* harmony import */ var react_aiot__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_aiot__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! . */ "./public/src/util/index.tsx");
/* harmony import */ var _i18n__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./i18n */ "./public/src/util/i18n.tsx");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! jquery */ "jquery");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _rest__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../rest */ "./public/src/rest/index.tsx");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }







var CLASS_NAME_APPEND = "aiot-helper-method-append",
    CLASS_NAME_MOVE = "aiot-helper-method-move";
/**
 * On CTRL holding add class 'aiot-helper-method-append' to document body.
 */

function anyKeyHolding() {
  jquery__WEBPACK_IMPORTED_MODULE_5___default()(document).on("keydown", function (e) {
    return jquery__WEBPACK_IMPORTED_MODULE_5___default()("body").addClass(CLASS_NAME_APPEND);
  });
  jquery__WEBPACK_IMPORTED_MODULE_5___default()(document).on("keyup", function (e) {
    return jquery__WEBPACK_IMPORTED_MODULE_5___default()("body").removeClass(CLASS_NAME_APPEND);
  });
}
/**
 * Enables / Reinitializes the droppable nodes. If a draggable item is dropped
 * here the given posts are moved to the category. You have to provide a ReactJS
 * element to reload the tree.
 */


function droppable(id, onSuccess) {
  var dom = jquery__WEBPACK_IMPORTED_MODULE_5___default()("#" + id + " li.aiot-sortable .aiot-node.aiot-droppable");
  dom.droppable({
    activeClass: "aiot-state-default",
    hoverClass: "aiot-state-hover",
    tolerance: "pointer",
    drop: function drop(event, ui) {
      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var ids, to, removeElements, isCopy, taxonomy, elementsIterate, iterateChecked, i18nProps, hide;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                ids = [], to = +jquery__WEBPACK_IMPORTED_MODULE_5___default()(event.target).attr("data-id"), removeElements = [], isCopy = jquery__WEBPACK_IMPORTED_MODULE_5___default()("body").hasClass(CLASS_NAME_APPEND), taxonomy = jquery__WEBPACK_IMPORTED_MODULE_5___default()(event.target).parents(".aiot-tree").attr("data-tax"), elementsIterate = function elementsIterate(cb) {
                  for (var i = 0; i < removeElements.length; i++) {
                    cb(removeElements[i]);
                  }
                }, iterateChecked = function iterateChecked(elem, cb) {
                  // List-mode
                  var trs = jquery__WEBPACK_IMPORTED_MODULE_5___default()('input[name="post[]"]:checked'); // Multiselect

                  if (trs.length) {
                    trs.each(function () {
                      cb(jquery__WEBPACK_IMPORTED_MODULE_5___default()(this).parents("tr"));
                    });
                  } else {
                    // One selected
                    cb(elem);
                  }
                }; // Parse the item ids and remove them

                iterateChecked(ui.draggable, function (tr) {
                  ids.push(parseInt(tr.find('input[name="post[]"]').attr("value"), 10));
                  removeElements.push(tr);
                }); // The function to progress the move

                elementsIterate(function (obj) {
                  return jquery__WEBPACK_IMPORTED_MODULE_5___default()(obj).fadeTo(250, 0.3);
                });
                i18nProps = {
                  count: ids.length,
                  category: jquery__WEBPACK_IMPORTED_MODULE_5___default()(event.target).find(".aiot-node-name").html()
                };
                hide = react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].loading(isCopy ? Object(_i18n__WEBPACK_IMPORTED_MODULE_4__["_n"])("Copy post to %(category)s...", "Copy %(count)d posts to %(category)s...", ids.length, i18nProps) : Object(_i18n__WEBPACK_IMPORTED_MODULE_4__["_n"])("Moving post to %(category)s...", "Moving %(count)d posts to %(category)s...", ids.length, i18nProps));
                _context.prev = 5;
                _context.next = 8;
                return Object(___WEBPACK_IMPORTED_MODULE_3__["ajax"])({
                  location: _rest__WEBPACK_IMPORTED_MODULE_6__["locationRestPostsBulkMovePut"],
                  request: {
                    ids: ids,
                    isCopy: isCopy,
                    taxonomy: taxonomy,
                    to: to
                  }
                });

              case 8:
                hide();
                react_aiot__WEBPACK_IMPORTED_MODULE_2__["message"].success(isCopy ? Object(_i18n__WEBPACK_IMPORTED_MODULE_4__["_n"])("Post successfully copied.", "%(count)d posts successfully copied.", ids.length, i18nProps) : Object(_i18n__WEBPACK_IMPORTED_MODULE_4__["_n"])("Post successfully moved.", "%(count)d posts successfully moved.", ids.length, i18nProps));
                onSuccess(); // Update WP Table

                if (isCopy) {
                  // Reload the WP List table
                  jquery__WEBPACK_IMPORTED_MODULE_5___default.a.get(window.location.href, {}, function (response) {
                    var doc = jquery__WEBPACK_IMPORTED_MODULE_5___default()(response),
                        tableRows = doc.find(".wp-list-table tbody tr");

                    if (tableRows.length) {
                      var postId, tableRow;
                      elementsIterate(function (obj) {
                        postId = jquery__WEBPACK_IMPORTED_MODULE_5___default()(obj).attr("id");
                        tableRow = doc.find("#" + postId);
                        jquery__WEBPACK_IMPORTED_MODULE_5___default()(obj).replaceWith(tableRow);
                      }); // Reinit draggable

                      draggable();
                    }
                  });
                } else {
                  // Remove items
                  elementsIterate(function (obj) {
                    return jquery__WEBPACK_IMPORTED_MODULE_5___default()(obj).remove();
                  });
                } // Add no media


                if (!jquery__WEBPACK_IMPORTED_MODULE_5___default()(".wp-list-table tbody tr").length) {
                  jquery__WEBPACK_IMPORTED_MODULE_5___default()(".wp-list-table tbody").html('<tr class="no-items"><td class="colspanchange" colspan="6">' + Object(_i18n__WEBPACK_IMPORTED_MODULE_4__["__"])("No entries") + "</td></tr></tbody>");
                }

                _context.next = 18;
                break;

              case 15:
                _context.prev = 15;
                _context.t0 = _context["catch"](5);
                console.log(_context.t0); // Usually there should be no error... Silence is golden.

              case 18:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[5, 15]]);
      }))();
    }
  });
}
/**
 * Make the list table draggable if sort mode is not active.
 */


function draggable() {
  // No dragging in sort mode
  if (___WEBPACK_IMPORTED_MODULE_3__["isSortMode"]) {
    return;
  }

  jquery__WEBPACK_IMPORTED_MODULE_5___default()("#wpbody-content .wp-list-table tbody tr:not(.no-items)").draggable({
    revert: "invalid",
    revertDuration: 0,
    appendTo: "body",
    cursorAt: {
      top: 0,
      left: 0
    },
    distance: 10,
    refreshPositions: true,
    helper: function helper() {
      var helperId = Object(react_aiot__WEBPACK_IMPORTED_MODULE_2__["uuid"])(),
          helper = jquery__WEBPACK_IMPORTED_MODULE_5___default()('<div id="' + helperId + '" class="aiot-helper"></div>'),
          count = jquery__WEBPACK_IMPORTED_MODULE_5___default()('input[name="post[]"]:checked').length || 1;
      helper.appendTo(jquery__WEBPACK_IMPORTED_MODULE_5___default()("body"));
      react_dom__WEBPACK_IMPORTED_MODULE_1___default.a.render(React.createElement("div", null, React.createElement("div", {
        className: CLASS_NAME_MOVE
      }, React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Icon"], {
        type: "swap"
      }), " ", Object(_i18n__WEBPACK_IMPORTED_MODULE_4__["_n"])("Move post", "Move %d posts", count, [count]), React.createElement("p", null, Object(_i18n__WEBPACK_IMPORTED_MODULE_4__["__"])("Hold any key to copy to category"))), React.createElement("div", {
        className: CLASS_NAME_APPEND
      }, React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_2__["Icon"], {
        type: "copy"
      }), " ", Object(_i18n__WEBPACK_IMPORTED_MODULE_4__["_n"])("Copy post", "Copy %d posts", count, [count]), React.createElement("p", null, Object(_i18n__WEBPACK_IMPORTED_MODULE_4__["__"])("Release key to move")))), document.getElementById(helperId));
      return helper;
    },
    start: function start() {
      jquery__WEBPACK_IMPORTED_MODULE_5___default()("body").addClass("aiot-currently-dragging").removeClass(CLASS_NAME_APPEND); // FIX https://bugs.jqueryui.com/ticket/4261

      jquery__WEBPACK_IMPORTED_MODULE_5___default()(document.activeElement).blur();
    },
    stop: function stop() {
      jquery__WEBPACK_IMPORTED_MODULE_5___default()("body").removeClass("aiot-currently-dragging");
    }
  });
}



/***/ }),

/***/ "./public/src/util/fastModeContent.tsx":
/*!*********************************************!*\
  !*** ./public/src/util/fastModeContent.tsx ***!
  \*********************************************/
/*! exports provided: load */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "load", function() { return load; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! jquery */ "jquery");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var load =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function load() {
    return _ref.apply(this, arguments);
  };
}();

if (true) {
  load =
  /*#__PURE__*/
  function () {
    var _ref2 = _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(href) {
      var tableFilter, headerFilter;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              tableFilter = jquery__WEBPACK_IMPORTED_MODULE_1___default()("form#posts-filter"), headerFilter = tableFilter.parent().children("ul.subsubsub");
              tableFilter.stop().fadeTo(250, 0.5);
              window.history.pushState({}, "", href); // Get the new table item rows

              _context2.next = 5;
              return jquery__WEBPACK_IMPORTED_MODULE_1___default.a.get(href, {}, function (response) {
                var doc = jquery__WEBPACK_IMPORTED_MODULE_1___default()(response),
                    newTable = doc.find("form#posts-filter"),
                    newHeader = newTable.parent().children("ul.subsubsub"),
                    iep = window.inlineEditPost;
                tableFilter.replaceWith(newTable);
                headerFilter.replaceWith(newHeader);
                /**
                 * ported from inline-edit-post.js
                 */
                // inlineEditPost.init();

                var qeRow = jquery__WEBPACK_IMPORTED_MODULE_1___default()("#inline-edit"),
                    bulkRow = jquery__WEBPACK_IMPORTED_MODULE_1___default()("#bulk-edit");
                iep.type = jquery__WEBPACK_IMPORTED_MODULE_1___default()("table.widefat").hasClass("pages") ? "page" : "post";
                iep.what = "#post-"; // prepare the edit rows

                qeRow.keyup(function (e) {
                  if (e.which === 27) {
                    return iep.revert();
                  }
                });
                bulkRow.keyup(function (e) {
                  if (e.which === 27) {
                    return iep.revert();
                  }
                });
                jquery__WEBPACK_IMPORTED_MODULE_1___default()(".cancel", qeRow).click(function () {
                  return iep.revert();
                });
                jquery__WEBPACK_IMPORTED_MODULE_1___default()(".save", qeRow).click(function () {
                  return iep.save(this);
                });
                jquery__WEBPACK_IMPORTED_MODULE_1___default()("td", qeRow).keydown(function (e) {
                  if (e.which === 13 && !jquery__WEBPACK_IMPORTED_MODULE_1___default()(e.target).hasClass("cancel")) {
                    return iep.save(this);
                  }
                });
                jquery__WEBPACK_IMPORTED_MODULE_1___default()(".cancel", bulkRow).click(function () {
                  return iep.revert();
                });
                jquery__WEBPACK_IMPORTED_MODULE_1___default()('#inline-edit .inline-edit-private input[value="private"]').click(function () {
                  var pw = jquery__WEBPACK_IMPORTED_MODULE_1___default()("input.inline-edit-password-input");

                  if (jquery__WEBPACK_IMPORTED_MODULE_1___default()(this).prop("checked")) {
                    pw.val("").prop("disabled", true);
                  } else {
                    pw.prop("disabled", false);
                  }
                }); // add events

                jquery__WEBPACK_IMPORTED_MODULE_1___default()("#the-list").on("click", "a.editinline", function (e) {
                  e.preventDefault();
                  iep.edit(this);
                });
                jquery__WEBPACK_IMPORTED_MODULE_1___default()('select[name="_status"] option[value="future"]', bulkRow).remove();
                jquery__WEBPACK_IMPORTED_MODULE_1___default()("#doaction, #doaction2").click(function (e) {
                  var n;
                  iep.whichBulkButtonId = jquery__WEBPACK_IMPORTED_MODULE_1___default()(this).attr("id");
                  n = iep.whichBulkButtonId.substr(2);

                  if ("edit" === jquery__WEBPACK_IMPORTED_MODULE_1___default()('select[name="' + n + '"]').val()) {
                    e.preventDefault();
                    iep.setBulk();
                  } else if (jquery__WEBPACK_IMPORTED_MODULE_1___default()("form#posts-filter tr.inline-editor").length > 0) {
                    iep.revert();
                  }
                });
              });

            case 5:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2);
    }));

    return function load(_x) {
      return _ref2.apply(this, arguments);
    };
  }();
}



/***/ }),

/***/ "./public/src/util/hooks.tsx":
/*!***********************************!*\
  !*** ./public/src/util/hooks.tsx ***!
  \***********************************/
/*! exports provided: Hooks */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Hooks", function() { return Hooks; });
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! jquery */ "jquery");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_0__);
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

 // interface IDemoHook extends IHook {
//    args: [string, number];
//    context: Window;
// }
//
// const h = new Hooks();
// h.register<IDemoHook>("general", function(d1, d2) {
//    this.console.log(d1, d2);
// }).call<IDemoHook>("general", ["1", 2], window);

var Hooks =
/*#__PURE__*/
function () {
  function Hooks() {
    _classCallCheck(this, Hooks);

    this.registry = {};
  }

  _createClass(Hooks, [{
    key: "register",

    /**
     * Registers a callback to a given event name.
     *
     * @param names Can be multiple names with "," split
     * @param callback
     */
    value: function register(names, callback) {
      var _this = this;

      names.split(" ").forEach(function (n) {
        _this.registry[n] = _this.registry[n] || [];

        _this.registry[n].push(callback);
      });
      return this;
    }
    /**
     * Call an event.
     *
     * @param name
     * @param args
     * @param context
     */

  }, {
    key: "call",
    value: function call(name, args, context) {
      if (this.exists(name)) {
        var useArguments = args ? [].concat(_toConsumableArray(args), [jquery__WEBPACK_IMPORTED_MODULE_0___default.a]) : [jquery__WEBPACK_IMPORTED_MODULE_0___default.a];
        this.registry[name].forEach(function (element) {
          return element.apply(context, useArguments) !== false;
        });
      }

      return this;
    }
    /**
     * Checks if a event name is registered.
     *
     * @param name
     */

  }, {
    key: "exists",
    value: function exists(name) {
      return !!this.registry[name];
    }
  }]);

  return Hooks;
}();



/***/ }),

/***/ "./public/src/util/i18n.tsx":
/*!**********************************!*\
  !*** ./public/src/util/i18n.tsx ***!
  \**********************************/
/*! exports provided: _n, _nx, __, _x, _i */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "_n", function() { return _n; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "_nx", function() { return _nx; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__", function() { return __; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "_x", function() { return _x; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "_i", function() { return _i; });
/* harmony import */ var i18n_calypso__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! i18n-calypso */ "./node_modules/i18n-calypso/index.js");
/* harmony import */ var i18n_calypso__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(i18n_calypso__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/i18n */ "@wordpress/i18n");
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__);
// @see https://github.com/Automattic/wp-calypso/blob/master/packages/i18n-calypso/src/index.js

/**
 * Internal dependencies
 */
// @ts-ignore i18n-calpyso has no types, yet

// @ts-ignore @wordpress/i18n has no types, yet

/**
 * We need to use this as constant for the domain because
 * in the lite version it will be "real-category-library-lite".
 */

var useSlug = "real-category-library";
/**
 * Translates and retrieves the singular or plural form based on the supplied number.
 * For arguments sprintf is used, see http://www.diveintojavascript.com/projects/javascript-sprintf for
 * specification and usage.
 *
 * @see https://github.com/WordPress/gutenberg/tree/master/packages/i18n#_n
 * @see https://github.com/WordPress/gutenberg/tree/master/packages/i18n#sprintf
 */

function _n(single, plural, count) {
  for (var _len = arguments.length, args = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
    args[_key - 3] = arguments[_key];
  }

  return _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["sprintf"].apply(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__, [_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["_n"](single, plural, count, useSlug)].concat(args));
}
/**
 * Translates and retrieves the singular or plural form based on the supplied number, with gettext context.
 * For arguments sprintf is used, see http://www.diveintojavascript.com/projects/javascript-sprintf for
 * specification and usage.
 *
 * @see https://github.com/WordPress/gutenberg/tree/master/packages/i18n#_n
 * @see https://github.com/WordPress/gutenberg/tree/master/packages/i18n#sprintf
 */


function _nx(single, plural, count, context) {
  for (var _len2 = arguments.length, args = new Array(_len2 > 4 ? _len2 - 4 : 0), _key2 = 4; _key2 < _len2; _key2++) {
    args[_key2 - 4] = arguments[_key2];
  }

  return _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["sprintf"].apply(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__, [_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["_nx"](single, plural, count, context, useSlug)].concat(args));
}
/**
 * Retrieve the translation of text.
 * For arguments sprintf is used, see http://www.diveintojavascript.com/projects/javascript-sprintf for
 * specification and usage.
 *
 * @see https://github.com/WordPress/gutenberg/tree/master/packages/i18n#_n
 * @see https://github.com/WordPress/gutenberg/tree/master/packages/i18n#sprintf
 */


function __(single) {
  for (var _len3 = arguments.length, args = new Array(_len3 > 1 ? _len3 - 1 : 0), _key3 = 1; _key3 < _len3; _key3++) {
    args[_key3 - 1] = arguments[_key3];
  }

  return _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["sprintf"].apply(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__, [_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["__"](single, useSlug)].concat(args));
}
/**
 * Retrieve translated string with gettext context.
 * For arguments sprintf is used, see http://www.diveintojavascript.com/projects/javascript-sprintf for
 * specification and usage.
 *
 * @see https://github.com/WordPress/gutenberg/tree/master/packages/i18n#_n
 * @see https://github.com/WordPress/gutenberg/tree/master/packages/i18n#sprintf
 */


function _x(single, context) {
  for (var _len4 = arguments.length, args = new Array(_len4 > 2 ? _len4 - 2 : 0), _key4 = 2; _key4 < _len4; _key4++) {
    args[_key4 - 2] = arguments[_key4];
  }

  return _wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["sprintf"].apply(_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__, [_wordpress_i18n__WEBPACK_IMPORTED_MODULE_1__["_x"](single, context, useSlug)].concat(args));
}

var i18n = new i18n_calypso__WEBPACK_IMPORTED_MODULE_0__["I18N"]();
/**
 * This function allows you to interpolate react components to your translations.
 * You have to pass an already translated string as argument! For this you can use the other
 * i18n functions like _n() or __().
 *
 * A translation can look like this: "Hello {{a}}click me{{/a}}." and you have to pass
 * a component with key "a".
 */

function _i(translation, components) {
  return i18n.translate(translation, {
    components: components
  });
}



/***/ }),

/***/ "./public/src/util/index.tsx":
/*!***********************************!*\
  !*** ./public/src/util/index.tsx ***!
  \***********************************/
/*! exports provided: pluginOptions, isPro, showProHints, process, trailingslashit, untrailingslashit, urlParam, addUrlParam, applyNodeDefaults, hooks, isMobile, ICON_OBJ_FOLDER_OPEN, ICON_OBJ_FOLDER_CLOSED, $byOrder, isSortModeAvailable, isSortMode, ERouteHttpVerb, urlBuilder, ajax, WP_REST_API_ENDPOINT, Hooks, _n, _nx, __, _x, _i */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "pluginOptions", function() { return pluginOptions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isPro", function() { return isPro; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "showProHints", function() { return showProHints; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "process", function() { return process; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "trailingslashit", function() { return trailingslashit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "untrailingslashit", function() { return untrailingslashit; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "urlParam", function() { return urlParam; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addUrlParam", function() { return addUrlParam; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "applyNodeDefaults", function() { return applyNodeDefaults; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "hooks", function() { return hooks; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isMobile", function() { return isMobile; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ICON_OBJ_FOLDER_OPEN", function() { return ICON_OBJ_FOLDER_OPEN; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ICON_OBJ_FOLDER_CLOSED", function() { return ICON_OBJ_FOLDER_CLOSED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "$byOrder", function() { return $byOrder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isSortModeAvailable", function() { return isSortModeAvailable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isSortMode", function() { return isSortMode; });
/* harmony import */ var lil_uri__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lil-uri */ "./node_modules/lil-uri/uri.js");
/* harmony import */ var lil_uri__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lil_uri__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_aiot__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-aiot */ "react-aiot");
/* harmony import */ var react_aiot__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_aiot__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _hooks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./hooks */ "./public/src/util/hooks.tsx");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jquery */ "jquery");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ajax__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ajax */ "./public/src/util/ajax.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ERouteHttpVerb", function() { return _ajax__WEBPACK_IMPORTED_MODULE_4__["ERouteHttpVerb"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "urlBuilder", function() { return _ajax__WEBPACK_IMPORTED_MODULE_4__["urlBuilder"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ajax", function() { return _ajax__WEBPACK_IMPORTED_MODULE_4__["ajax"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "WP_REST_API_ENDPOINT", function() { return _ajax__WEBPACK_IMPORTED_MODULE_4__["WP_REST_API_ENDPOINT"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "Hooks", function() { return _hooks__WEBPACK_IMPORTED_MODULE_2__["Hooks"]; });

/* harmony import */ var _i18n__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./i18n */ "./public/src/util/i18n.tsx");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "_n", function() { return _i18n__WEBPACK_IMPORTED_MODULE_5__["_n"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "_nx", function() { return _i18n__WEBPACK_IMPORTED_MODULE_5__["_nx"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "__", function() { return _i18n__WEBPACK_IMPORTED_MODULE_5__["__"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "_x", function() { return _i18n__WEBPACK_IMPORTED_MODULE_5__["_x"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "_i", function() { return _i18n__WEBPACK_IMPORTED_MODULE_5__["_i"]; });

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }






/**
 * Default Real Categories Management hook system.
 */
var hooks = new _hooks__WEBPACK_IMPORTED_MODULE_2__["Hooks"]();
/**
 * See PHP file inc/general/Assets.class.php.
 */

var pluginOptions = window.rclOpts,
    process = window.process,
    isPro = pluginOptions.others.isPro,
    showProHints = pluginOptions.others.showProHints;

var untrailingslashit = function untrailingslashit(str) {
  return str.endsWith("/") || str.endsWith("\\") ? untrailingslashit(str.slice(0, -1)) : str;
};

var trailingslashit = function trailingslashit(str) {
  return untrailingslashit(str) + "/";
};
/**
 * Get URL parameter of current url.
 *
 * @param name The parameter name
 * @param url
 * @returns
 */


function urlParam(name) {
  var url = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : window.location.href;
  return (lil_uri__WEBPACK_IMPORTED_MODULE_0___default()(url).query() || {})[name];
}
/**
 * Add an URL parameter to a given url. This function does not allow to remove a parameter.
 *
 * @param url
 * @param param
 * @param value
 */


function addUrlParam(url, param, value) {
  var b = lil_uri__WEBPACK_IMPORTED_MODULE_0___default()(url),
      query = b.query() || {};
  query[param] = value;
  return b.query(query).build();
}
/**
 * Handle tree node defaults for loaded folder items and new items.
 */


function applyNodeDefaults(arr) {
  return arr.map(function (_ref) {
    var term_id = _ref.term_id,
        name = _ref.name,
        count = _ref.count,
        childNodes = _ref.childNodes,
        rest = _objectWithoutProperties(_ref, ["term_id", "name", "count", "childNodes"]);

    return function (node) {
      hooks.call("tree/node", [node]);
      return node;
    }(jquery__WEBPACK_IMPORTED_MODULE_3___default.a.extend({}, react_aiot__WEBPACK_IMPORTED_MODULE_1__["TreeNode"].defaultProps, {
      // Default node
      id: term_id,
      title: name,
      icon: "folder",
      iconActive: "folder-open",
      count: count,
      childNodes: childNodes ? applyNodeDefaults(childNodes) : [],
      properties: rest,
      $visible: true
    }));
  });
}

var isMobile = function isMobile() {
  return jquery__WEBPACK_IMPORTED_MODULE_3___default()(window).width() <= 700;
};
/**
 * Icon showing a opened folder.
 */


var ICON_OBJ_FOLDER_OPEN = React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
  type: "folder-open"
});
/**
 * Icon showing a closed folder.
 */

var ICON_OBJ_FOLDER_CLOSED = React.createElement(react_aiot__WEBPACK_IMPORTED_MODULE_1__["Icon"], {
  type: "folder"
});
/**
 * A jQuery object selecting the byorder link above the wp list table.
 */

var $byOrder = function $byOrder() {
  return jquery__WEBPACK_IMPORTED_MODULE_3___default()("ul.subsubsub li.byorder");
};
/**
 * If true there is a sort mode available.
 */


var isSortModeAvailable = !!$byOrder().length;
/**
 * If true the sort mode is currently active (selected).
 */

var isSortMode = isSortModeAvailable && pluginOptions.others.editOrderBy.toLowerCase() === "menu_order title" && pluginOptions.others.editOrder.toLowerCase() === "asc";





/***/ }),

/***/ "@wordpress/i18n":
/*!*****************************!*\
  !*** external "wp['i18n']" ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = wp['i18n'];

/***/ }),

/***/ "jquery":
/*!*************************!*\
  !*** external "jQuery" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = jQuery;

/***/ }),

/***/ "mobx":
/*!***********************!*\
  !*** external "mobx" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = mobx;

/***/ }),

/***/ "mobx-state-tree":
/*!********************************!*\
  !*** external "mobxStateTree" ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = mobxStateTree;

/***/ }),

/***/ "moment-timezone":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = moment;

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "React" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = React;

/***/ }),

/***/ "react-aiot":
/*!****************************!*\
  !*** external "ReactAIOT" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ReactAIOT;

/***/ }),

/***/ "react-dom":
/*!***************************!*\
  !*** external "ReactDOM" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ReactDOM;

/***/ })

/******/ });
//# sourceMappingURL=rcl.pro.js.map