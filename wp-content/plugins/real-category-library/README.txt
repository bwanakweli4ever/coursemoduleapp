=== Real Categories Management - Start organizing your categories! ===
Contributors: mguenter
Tags: folders, administration, tree view, content management, page organization, custom post type organization, categories
Donate link:
Stable tag: trunk
Requires at least: 5.0.0
Requires PHP: 5.4.0
Tested up to: 5.2
License: GPLv2
License:
License URI:

Organize your WordPress categories in a nice and organized way.

== Description ==

**Turn your Wordpress categories to the next level with ultra fast organization mode. Manage categories in an explorer-like tree view and create a custom category order.**

RCM (Real Categories Management) allows you to organize all your wordpress categories within the main screen – easy and flexible. Use your mouse (or touch) to drag and drop your posts. Create, rename, delete or reorder your categories. It works fine with every custom post type and WooCommerce products. Just install this plugin and it works fine with all your written posts. It also supports multisite.

**Features (Lite):**

- WordPress 5.0 / Gutenberg ready
- Flexible view of your categories structure
- Drag and drop your posts
- Just install and it works with your current categories
- Move or append to category
- Full control for your folders in one toolbar (create, rename, delete, rearrange)
- Custom term order for your front end order (drag & drop)
- Compatible with touch devices
- Supports multisite

**[PRO Version](https://matthias-web.com/go/codecanyon/13580393) available with additional features:**

- Supports custom post types
- Works fine with WooCommerce products and product attributes
- Supports multiple taxonomies and WooCommerce product attributes
- No annoying page reload while switching the content (pagination, category switch, taxonomy switch) – it works like infinite scroll for your WP List tables
- One time payment, forever free updates
- Fast support

[→ Interested in the Pro version?](https://matthias-web.com/go/codecanyon/13580393)

== Installation ==

1. Goto your wordpress backend
2. Navigate to Plugins > Add new
3. Search for "WP Real Categories Management"
4. "Install"

OR 

1. Download the plugin from this site (wordpress.org)
2. Copy the extracted folder into your `wp-content/plugins` folder
3. Activate the "WP Real Categories Management" plugin via the plugins admin page

== Frequently Asked Questions ==


== Screenshots ==
1. Drag&Drop your categories order
2. Create or rename categories

== Changelog ==
= 07 November 2019 - Version 3.2.7 =
* Fixed drag&drop of categories now represents the correct order after movement

= 04 October 2019 - Version 3.2.6 =
* (developers only) Fixed bug with two instances of MobX loaded

= 20 August 2019 - Version 3.2.5 =
* Improved experience when sorting post entries
* Fixed bug with sort mode in subcategories
* Fixed bug with search box height in some cases that it needed too much space

= 02 June 2019 - Version 3.2.4 =
* Fixed bug when copy post that it is draggable again

= 07 May 2019 - Version 3.2.3 =
* Added "title" attribute to tree node for accessibility
* (developers only) Updated to latest AIOT version

= 19 March 2019 - Version 3.2.2 =
* Added button to expand/collapse all node items
* Fixed bug with style/script dependencies
* Fixed bug with missing animations
* Improved performance: Loading a tree with 10,000 nodes in 1s (the old way in 30s)

= 10 December 2018 - Version 3.2.1 =
* Added notice to the tree if the product is not yet registered

= 27 October 2018 - Version 3.2.0 =
* Added auto update functionality
* Fixed bug with new created folders and droppable posts
* (developers only) Fixed bug with WPML API requests

= 17 August 2018 - Version 3.1.1 HOTFIX =
* Fixed bug with relocating categories to a category with no childs yet

= 05 August 2018 - Version 3.1.0 =
* Improved the custom order performance
* Improved the way of handling custom order
* Fixed bug with mass categories
* Fixed bug with "Plain" permalink structure
* Fixed bug with collapsable/expandable folders

= 20 July 2018 - Version 3.0.6 =
* Improved error handling with plugins like Clearfy
* Fixed bug with "&" in category names
* Fixed bug with PHP 5.3
* Fixed bug with non-SSL API root urls
* Fixed bug with pagination in list mode after switching folder
* (developers only) Fixed bug with Gutenberg 3.1.x (https://git.io/f4SXU)

= 15 June 2018 - Version 3.0.5 =
* Added compatibility with WP Dark Mode plugin
* Added help message if WP REST API is not reachable through HTTP verbs
* Fixed bug with scroll container in media modal in IE/Edge/Firefox
* (developers only) Use global WP REST API parameters instead of DELETE / PUT

= 4 June 2018 - Version 3.0.4 =
* Fixed bug with spinning loader when permalink structure is "Plain"
* Fixed bug with german translation
* Fixed bug with IE11/Edge browser

= 17 May 2018 - Version 3.0.3 =
* Fixed bug with WPML and fetching a tree from another language within admin dashboard

= 08 May 2018 - Version 3.0.2 =
* Improved performance
* Fixed bug with switching from category to "All posts"
* (developers only) Added Mobx State Tree for frontend state management

= 09 March 2018 - Version 3.0.1 =
* Fixed bug with mobile devices

= 28 February 2018 - Version 3.0.0 =
* Complete code rewrite
* ... Same functionality with improved performance
* ... with an eye on smooth user interface and experience
* The plugin is now available in the following languages: English, German
* Fixed bug with WooCommerce 3.3.x product attributes
* (developers only) Sidebar is now fully written in ReactJS v16
* (developers only) The plugin is now bundled with webpack v3
* (developers only) Minimum of PHP 5.3 required now (in each update you'll find v2.4 for PHP 5.0+ compatibility)
* (developers only) Minimum of WordPress 4.4 required now (in each update you'll find v2.4 for 4.0+ compatibility)
* (developers only) PHP Classes modernized with autoloading and namespaces
* (developers only) WP REST API v2 for API programming, no longer use admin-ajax.php for your CRUD operations
* (developers only) Implemented cachebuster to avoid cache problems
* (developers only) ApiGen for PHP Documentation
* (developers only) JSDoc for JavaScript Documentation
* (developers only) apiDoc for API Documentation
* (developers only) WP HookDoc for Filters & Actions Documentation
* (developers only) Custom filters and actions which affected the tree ouput are now removed, you have to do this in JS now
* (developers only) All JavaScript events / hooks are removed now - contact me so I can implement for you

= 16 January 2018 - Version 2.4 =
* Added support for WooCommerce attributes (through an option)
* Improved the tax switcher (when multiple category types are available)

= 24 November 2017 - Version 2.3.2 =
* Fixed bug with hidden sidebar without resized before
* (developers only) Added filter to hide category try for specific taxonomies (RCL/Available)

= 31 October 2017 - Version 2.3.1 HOTFIX =
* Fixed bug after creating a new post the nodes are not clickable
* Fixed bug when switching taxonomy when fast mode is deactivated

= 28 October 2017 - Version 2.3 =
* Added ability to expand/collapse the complete sidebar by doubleclick the resize button
* Fixed bug with WooCommerce 3.x
* Fixed bug with touch devices (no dropdown was touchable)
* Fixed bug with ESC key in rename mode
* Fixed bug with creating a new folder and switch back to previous
* Fixed bug with taxonomy switcher (especially WooCommerce products)
* (developers only) Improved the save of localStorage items within one row per tree instance

= 22 September 2017 - Version 2.2.1 =
* Improved the tax switcher when more than two taxonomies are available
* Fixed bug when switching to an taxonomy with no categories
* (developers only) Added new filter to disable RCL sorting mechanism

= 24 June 2017 - Version 2.2 =
* Added full compatibility with WordPress 4.8
* Added ESC to close the rename category action
* Added F2 handler to rename a category
* Added double click event to open category hierarchy
* Added search input field for categories
* Fixed bug with some browsers when local storage is disabled

= 24 March 2017 - Version 2.1.1 =
* Added https://matthias-web.com as author url
* Improved the way of rearrange mode, the folders gets expand after 700ms of hover
* Fixed bug with > 600 categories
* Fixed bug with styles and scripts
* Fixed bug with rearrange

= 24 November 2016 - Version 2.1 =
* Added new version of AIO tree view (1.3.1)
* Added the MatthiasWeb promotion dialog
* Added responsivness
* Improved performance with lazy loading of categories
* Improved changelog
* (developers only) Use rootParentId in jQuery AIO tree
* (developers only) Fixed bug with jQuery AIO tree version when RML is enabled

= 09 September 2016 - Version 2.0.2 HOTFIX =
* (developers only) Conflict with jQuery.allInOneTree

= 02 September 2016 - Version 2.0.1 =
* Added minified scripts and styles
* Fixed capability bug while logged out
* (developers only) Added Javascript polyfill's to avoid browser incompatibility
* (developers only) Fixed bug for crashed safari browser
* (developers only) Fixed bug with boolval function

= 08 August 2016 - Version 2.0 =
* Added more userfriendly toolbar (ported from RML)
* Added fixed header
* Added "fast mode" for switching between taxonomies without page reload
* Added "fast mode" for switching between categories without page reload
* Added "fast mode" for switching between pages without page reload
* Added taxonomy to pages
* Added custom order for taxonomies
* Added new advertisment system for MatthiasWeb
* (developers only) Complete recode of PHP and Javascript

= 20 January 2016 - Version 1.1.1 =
* Added facebook advert on plugin activation
* Fixed count of categories

= 28 November 2015 - Version 1.1 =
* Fixed conditional tag to create / sort items
* Fixed hierarchical read of categories
* Fixed append method with CTRL - now tap and hold any key to append

= 13 November 2015 - Version 1.0.2 =
* (developers only) Removed unnecessary code
* (developers only) Fixed jquery conflict

= 10 November 2015 - Version 1.0.1 =
* (developers only) Fixed javascript error for firefox, ie and opera

= 08 November 2015 - Version 1.0 =
* Initial Release

== Upgrade Notice ==

