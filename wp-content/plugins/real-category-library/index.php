<?php
/**
 * @wordpress-plugin
 * Plugin Name: 	WP Real Categories Management
 * Plugin URI:		https://codecanyon.net/item/wordpress-real-category-management-custom-category-order-tree-view/13580393
 * Description: 	Organize your WordPress categories in a nice way.
 * Author:          Matthias Guenter
 * Author URI:		https://matthias-web.com
 * Version: 		3.2.7
 * Text Domain:		real-category-library
 * Domain Path:		/languages
 */

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

update_site_option( 'wpls_license_real-category-library', 'nulled' );
update_site_option('wpls_activation_id_real-category-library', 'nulled');

/**
 * Plugin constants. This file is procedural coding style for initialization of
 * the plugin core and definition of plugin configuration.
 */
if (defined('RCL_PATH')) {
    require_once path_join(dirname(__FILE__), 'inc/others/fallback-already.php');
    return;
}

define('RCL_FILE', __FILE__);
define('RCL_PATH', dirname(RCL_FILE));
define('RCL_SLUG', defined('RCL_LITE') && RCL_LITE ? basename(RCL_PATH) . '-lite' : basename(RCL_PATH));
define('RCL_INC', trailingslashit(path_join(RCL_PATH, 'inc')));
define('RCL_IS_PRO', is_dir(path_join(RCL_INC, 'overrides/pro')) && !(defined('RCL_LITE') && RCL_LITE));
define('RCL_OVERRIDES_INC', trailingslashit(path_join(RCL_INC, 'overrides/' . (RCL_IS_PRO ? 'pro' : 'lite'))));
define('RCL_MIN_PHP', '5.4.0'); // Minimum of PHP 5.3 required for autoloading and namespacing
define('RCL_MIN_WP', '5.0.0'); // Minimum of WordPress 5.0 required
define('RCL_NS', 'MatthiasWeb\\RealCategoryLibrary');
define('RCL_PRO_VERSION', 'http://matthias-web.com/go/codecanyon/13580393');
define('RCL_DB_PREFIX', 'rcl'); // The table name prefix wp_{prefix}
define('RCL_OPT_PREFIX', 'rcl'); // The option name prefix in wp_options
//define('RCL_TD', ''); This constant is defined in the core class. Use this constant in all your __() methods
//define('RCL_VERSION', ''); This constant is defined in the core class
//define('RCL_DEBUG', true); This constant should be defined in wp-config.php to enable the Base::debug() method

// Check PHP Version and print notice if minimum not reached, otherwise start the plugin core
require_once RCL_INC . 'others/' . (version_compare(phpversion(), RCL_MIN_PHP, '>=') ? 'start.php' : 'phpfallback.php');
