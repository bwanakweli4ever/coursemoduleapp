<?php
namespace MatthiasWeb\RealCategoryLibrary\rest;
use MatthiasWeb\RealCategoryLibrary\base;
use MatthiasWeb\RealCategoryLibrary\general;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Create a REST Service.
 */
class Service extends base\Base {
    /**
     * The namespace for this service.
     *
     * @see getUrl()
     */
    const SERVICE_NAMESPACE = 'realcategorylibrary/v1';

    /**
     * Register endpoints.
     */
    public function rest_api_init() {
        register_rest_route(Service::SERVICE_NAMESPACE, '/plugin', [
            'methods' => 'GET',
            'callback' => [$this, 'routePlugin']
        ]);

        register_rest_route(Service::SERVICE_NAMESPACE, '/notice/license', [
            'methods' => 'DELETE',
            'callback' => [$this, 'routeNoticeDismissLicense']
        ]);

        register_rest_route(Service::SERVICE_NAMESPACE, '/notice/lite', [
            'methods' => 'DELETE',
            'callback' => [$this, 'routeNoticeDismissLite']
        ]);

        register_rest_route(Service::SERVICE_NAMESPACE, '/tree', [
            'methods' => 'GET',
            'callback' => [$this, 'routeTree']
        ]);

        register_rest_route(Service::SERVICE_NAMESPACE, '/hierarchy/(?P<id>\d+)', [
            'methods' => 'PUT',
            'callback' => [$this, 'routeHierarchy']
        ]);
    }

    /**
     * @api {delete} /realcategorylibrary/v1/notice/license Dismiss the license notice for a given time (transient)
     * @apiName DismissLicenseNotice
     * @apiGroup Tree
     * @apiVersion 3.2.1
     * @since 3.2.1
     * @apiPermission install_plugins
     */
    public function routeNoticeDismissLicense() {
        if (($permit = Service::permit('install_plugins')) !== null) {
            return $permit;
        }
        $this->getCore()->isLicenseNoticeDismissed(true);
        return new \WP_REST_Response(true);
    }

    /**
     * @api {delete} /realcategorylibrary/v1/notice/lite Dismiss the lite notice for a given time (transient)
     * @apiName DismissLiteNotice
     * @apiGroup Tree
     * @apiVersion 3.2.6
     * @since 3.2.6
     * @apiPermission install_plugins
     */
    public function routeNoticeDismissLite() {
        if (($permit = Service::permit('install_plugins')) !== null) {
            return $permit;
        }
        $this->getCore()->isLiteNoticeDismissed(true);
        return new \WP_REST_Response(true);
    }

    /**
     * @api {put} /realcategorylibrary/v1/hierarchy/:id Change a folder position within the hierarchy
     * @apiParam {int} id The folder id
     * @apiParam {int} parent The parent
     * @apiParam {int} totalNextId The total next id to the folder
     * @apiParam {string} type The post type
     * @apiParam {string} taxonomy The taxonomy
     * @apiName PutHierarchy
     * @apiGroup Tree
     * @apiVersion 3.1.0
     * @since 3.1.0 The hierarchy is now single relocate, that means you send a term id with next id
     * @apiPermission manage_categories
     */
    public function routeHierarchy($request) {
        if (($permit = Service::permit()) !== null) {
            return $permit;
        }

        $id = intval($request->get_param('id'));
        $type = $request->get_param('type');
        $taxonomy = $request->get_param('taxonomy');
        $parent = intval($request->get_param('parent'));
        $nextId = intval($request->get_param('nextId'));

        // Check type and taxonomy
        if (!$type || !$taxonomy || !is_taxonomy_hierarchical($taxonomy)) {
            return new \WP_Error('rest_rcl_hierarchy_no_type', __('No type or valid taxonomy provided.', RCL_TD), [
                'status' => 500
            ]);
        }

        $taxOrder = $this->getCore()->getTaxOrder();
        $hierarchy = $taxOrder->relocate($type, $taxonomy, $id, $parent, $nextId);
        if (is_wp_error($hierarchy)) {
            return $hierarchy;
        }
        return new \WP_REST_Response(true);
    }

    /**
     * @api {get} /realcategorylibrary/v1/tree Get the full categories tree
     * @apiParam {string} type The post type
     * @apiParam {string} taxonomy The taxonomy
     * @apiParam {integer} [id] Only return this term id as result
     * @apiParam {string} [currentUrl] The current url to detect the active item
     * @apiParam {boolean} [remember] If true the selected taxonomy is saved to the user option
     * @apiName GetTree
     * @apiGroup Tree
     * @apiVersion 1.0.0
     * @apiPermission manage_categories
     */
    public function routeTree($request) {
        if (($permit = Service::permit()) !== null) {
            return $permit;
        }

        $type = $request->get_param('type');
        $taxonomy = $request->get_param('taxonomy');
        $id = $request->get_param('id');
        $currentUrl = $request->get_param('currentUrl');
        $remember = (bool) $request->get_param('remember');
        if (empty($type) || empty($taxonomy)) {
            return new \WP_Error('rest_rcl_tree_no_type', __('No type or taxonomy provided.', RCL_TD), [
                'status' => 500
            ]);
        }

        // Receive tree
        $taxTree = new general\TaxTree($type, $taxonomy, $currentUrl);

        // Save taxonomy to the user
        if ($remember === true) {
            update_user_option(get_current_user_id(), 'rcl_tax_' . $type, $taxonomy);
        }

        if (is_numeric($id)) {
            $term = get_term((int) $id);
            if ($term) {
                return new \WP_REST_Response($taxTree->enrichTerm($term));
            } else {
                return new \WP_Error('rest_rcl_tree_term_not_found', __('The passed term id was not found.', RCL_TD), [
                    'status' => 404
                ]);
            }
        } else {
            return new \WP_REST_Response([
                'tree' => $taxTree->getCats(),
                'selectedId' => $taxTree->getSelectedId()
            ]);
        }
    }

    /**
     * @api {get} /realcategorylibrary/v1/plugin Get plugin information
     * @apiHeader {string} X-WP-Nonce
     * @apiName GetPlugin
     * @apiGroup Plugin
     *
     * @apiSuccessExample {json} Success-Response:
     * {
     *     WC requires at least: "",
     *     WC tested up to: "",
     *     Name: "WP ReactJS Starter",
     *     PluginURI: "https://matthias-web.com/wordpress",
     *     Version: "0.1.0",
     *     Description: "This WordPress plugin demonstrates how to setup a plugin that uses React and ES6 in a WordPress plugin. <cite>By <a href="https://matthias-web.com">Matthias Guenter</a>.</cite>",
     *     Author: "<a href="https://matthias-web.com">Matthias Guenter</a>",
     *     AuthorURI: "https://matthias-web.com",
     *     TextDomain: "wp-reactjs-starter",
     *     DomainPath: "/languages",
     *     Network: false,
     *     Title: "<a href="https://matthias-web.com/wordpress">WP ReactJS Starter</a>",
     *     AuthorName: "Matthias Guenter"
     * }
     * @apiVersion 0.1.0
     */
    public function routePlugin() {
        return new \WP_REST_Response(general\Core::getInstance()->getPluginData());
    }

    /**
     * Get the wp-json URL for a defined REST service.
     *
     * @param string $namespace The prefix for REST service
     * @param string $endpoint The path appended to the prefix
     * @return String Example: https://example.com/wp-json
     * @example Service::url(Service::SERVICE_NAMESPACE) // => main path
     */
    public static function getUrl($namespace, $endpoint = '') {
        return site_url(rest_get_url_prefix()) . '/' . $namespace . '/' . $endpoint;
    }

    /**
     * Checks if the current user has a given capability and throws an error if not.
     *
     * @param string $cap The capability
     * @throws \WP_Error
     */
    public static function permit($cap = 'manage_categories') {
        if (!current_user_can($cap)) {
            return new \WP_Error('rest_rcl_forbidden', __('Forbidden'), ['status' => 403]);
        }
        if (!wp_rcl_active()) {
            return new \WP_Error(
                'rest_rcl_not_activated',
                __('Real Categories Management is not active for the current user.', RCL_TD),
                ['status' => 500]
            );
        }
        return null;
    }
}
