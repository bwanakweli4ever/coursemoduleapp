<?php
namespace MatthiasWeb\RealCategoryLibrary\rest;
use MatthiasWeb\RealCategoryLibrary\base;
use MatthiasWeb\RealCategoryLibrary\general;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Enables the /posts REST.
 */
class Post extends base\Base {
    /**
     * Register endpoints.
     */
    public function rest_api_init() {
        register_rest_route(Service::SERVICE_NAMESPACE, '/posts/bulk/move', [
            'methods' => 'PUT',
            'callback' => [$this, 'updateItem']
        ]);
    }

    /**
     * @api {put} /realcategorylibrary/v1/posts/bulk/move Move/Copy multipe posts
     * @apiParam {int[]} ids The post ids to move / copy
     * @apiParam {int} to The term id
     * @apiParam {boolean} isCopy If true the post is appended to the category
     * @apiParam {string} taxonomy The taxonomy
     * @apiName UpdatePostBulkMove
     * @apiGroup Post
     * @apiVersion 1.0.0
     * @apiPermission manage_categories
     */
    public function updateItem($request) {
        if (($permit = Service::permit()) !== null) {
            return $permit;
        }

        $ids = $request->get_param('ids');
        $to = intval($request->get_param('to'));
        $isCopy = $request->get_param('isCopy');
        $isCopy = gettype($isCopy) === 'string' ? $isCopy === 'true' : $isCopy;
        $taxonomy = $request->get_param('taxonomy');

        if (!is_array($ids) || count($ids) == 0 || $to == null) {
            return new \WP_Error('rest_rcl_posts_bulk_move_failed', __('Something went wrong.', RCL_TD), [
                'status' => 500
            ]);
        }

        foreach ($ids as $value) {
            wp_set_object_terms(intval($value), $to, $taxonomy, $isCopy);
        }

        return new \WP_REST_Response(true);
    }
}
