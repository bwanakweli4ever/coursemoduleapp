<?php
namespace MatthiasWeb\RealCategoryLibrary\general;
use MatthiasWeb\RealCategoryLibrary\base;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Different compatibility implementations.
 */
class Comp extends base\Base {
    public function localize_wpml($arr) {
        global $sitepress;
        if (isset($sitepress)) {
            $current = $sitepress->get_current_language();
            $default = $sitepress->get_default_language();
            if ($current !== $default) {
                $arr['restQuery']['lang'] = $current;
            }
        }
        return $arr;
    }
}
