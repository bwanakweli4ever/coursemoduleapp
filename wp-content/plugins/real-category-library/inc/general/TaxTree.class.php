<?php
namespace MatthiasWeb\RealCategoryLibrary\general;
use MatthiasWeb\RealCategoryLibrary\base;
use MatthiasWeb\RealCategoryLibrary\lite;
use MatthiasWeb\RealCategoryLibrary\overrides\interfce;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Tree structure management.
 */
class TaxTree extends base\Base implements interfce\general\IOverrideTaxTree {
    use lite\general\TaxTree;

    /**
     * The post type.
     */
    private $typenow;

    /**
     * The taxonomy.
     */
    private $taxnow;

    /**
     * The query variable for the taxonomy.
     */
    private $query_var;

    /**
     * Query arguments for the current url.
     */
    private $query_args = [];

    /**
     * The categories for the taxonomy. The categories are lazy loaded.
     */
    private $cats = null;

    /**
     * Array of available taxonomies for the post type.
     */
    private $taxos = null;

    /**
     * The count of the categories.
     */
    private $cnt = 0;

    /**
     * The count of all posts.
     */
    private $cntPosts = 0;

    /**
     * Determines if the current taxonomy is a WC taxonomy.
     */
    private $isWcTaxonomy = false;

    /**
     * The selected id if currentUrl is set.
     */
    private $selectedId = 'ALL';

    /**
     * Read the category tree flat (getCats()
     *
     * @since 3.1.0
     */
    private $flat = false;

    /**
     * Read the category tree within this parent, this does not include child nodes (getCats()
     *
     * @since 3.1.0
     */
    private $parent = null;

    /**
     * Create a new tree instance.
     *
     * @param string $typecustom Use this post type instead of current
     * @param string $taxcustom Use this taxonomy instead of current
     * @param string $currentUrl The current url to detect the active category
     * @param boolean $flat Read the category tree flat (getCats(), since 3.1.0)
     * @param int $parent Read the category tree within this parent, this does not include child nodes (getCats(), since 3.1.0)
     */
    public function __construct(
        $typecustom = false,
        $taxcustom = false,
        $currentUrl = null,
        $flat = false,
        $parent = null
    ) {
        global $typenow;
        $this->typenow = $typecustom !== false ? $typecustom : $typenow;
        $this->flat = $flat;
        $this->parent = $parent;

        // Parse current url arguments
        if (gettype($currentUrl) === 'string') {
            $args = [];
            $parsedUrl = parse_url($currentUrl);
            if (isset($parsedUrl['query'])) {
                parse_str($parsedUrl['query'], $args);
                $this->query_args = $args;
            }
        }

        // Fetch the taxonomies which are able to filter
        $taxonomy_objects = get_object_taxonomies($this->typenow, 'objects');
        $this->taxos = [];
        foreach ($taxonomy_objects as $key => $value) {
            if ($value->hierarchical == 1) {
                $this->taxos[$key] = $value;
                $this->taxos[$key]->objkey = $key;
            }
        }

        // Check if taxonomy is ready and set it to users' current
        if (count($this->taxos) > 0) {
            reset($this->taxos);

            // Get current taxonomy
            $taxnow = $taxcustom !== false ? $taxcustom : get_user_option('rcl_tax_' . $this->typenow);

            if ($taxnow !== false && isset($this->taxos[$taxnow])) {
                $this->taxnow = $this->taxos[$taxnow];
            } else {
                $this->taxnow = current($this->taxos);
            }
            $this->init();
        }
    }

    /**
     * Initialize the tree.
     */
    private function init() {
        // Initialize post count
        $num_posts = wp_count_posts($this->typenow, 'readable');
        $total_posts = array_sum((array) $num_posts);

        foreach (get_post_stati(['show_in_admin_all_list' => false]) as $state) {
            $total_posts -= $num_posts->$state;
        }
        $this->cntPosts = $total_posts;

        // Get query var of current tax
        $category_args = get_taxonomy($this->taxnow->objkey);
        if ($category_args->query_var === false || $category_args === true) {
            $this->query_var = $this->taxnow->objkey;
        } else {
            $this->query_var = $category_args->query_var;
        }

        // Save if WC attribute
        $this->isWcTaxonomy = $this->getCore()
            ->getWooCommerce()
            ->isWoocommerceTaxonomy($this->query_var);
    }

    /**
     * Enrich the category object with additional data.
     *
     * @param {WP_Term} $category The category
     */
    public function enrichTerm($category) {
        if (is_object($category)) {
            $category->queryArgs = $this->buildQueryUrl($category->slug);
            $category->editableSlug = apply_filters('editable_slug', $category->slug); // urldecode
            $category->name = htmlspecialchars_decode($category->name, ENT_NOQUOTES);
        }
        return $category;
    }

    /**
     * Minify a WP_Term object.
     *
     * @see TreeNode.js (and search for ".properties" to ensure ...rest usage)
     * @see enrichTerm for additional data
     */
    private function minify($term) {
        $result = [
            'term_id' => $term->term_id,
            'name' => $term->name,
            'count' => $term->count,
            'taxonomy' => $term->taxonomy,
            'slug' => $term->slug // needed in enrichTerm, should be removed for response ...
        ];
        return (object) $result;
    }

    /**
     * Get the categories tree for the current post type and taxonomy. This is a
     * rescursive method.
     *
     * @param int $parent The parent
     * @param array $categories All available categories
     * @param boolean $flat Read the category flat (since 3.1.0)
     * @param boolean $readChildNodes Read child nodes (since 3.1.0)
     * @return array
     */
    public function getCategoryTree($parent, $categories, $flat = false, $readChildNodes = true) {
        // Initially build a parent -> children tree (performance)
        if (!isset($categories['parents']) && $readChildNodes) {
            $parents = [];
            foreach ($categories as &$category) {
                $parentIdx = 'p' . $category->category_parent;
                if (!isset($parents[$parentIdx])) {
                    $parents[$parentIdx] = [];
                }
                $parents[$parentIdx][] = $this->minify($category);
            }
            $categories = ['parents' => $parents];
        }

        $result = [];
        if ($readChildNodes) {
            // This is the usual way to read children
            $parentIdx = 'p' . $parent;
            if (isset($categories['parents'][$parentIdx])) {
                foreach ($categories['parents'][$parentIdx] as $category) {
                    $children = $this->getCategoryTree($category->term_id, $categories, $flat);

                    if (!$flat) {
                        $category->childNodes = $children;
                    }

                    $this->enrichTerm($category);
                    unset($category->slug);

                    // Check if active
                    if ($this->isActive($category)) {
                        $this->selectedId = $category->term_id;
                    }

                    $result[] = $category;

                    if ($flat) {
                        foreach ($children as $child) {
                            $result[] = $child;
                        }
                    }
                }
            }
        } else {
            // Read all children as flat array
            foreach ($categories as $category) {
                $category = $this->minify($category);
                $this->enrichTerm($category);

                // Check if active
                if ($this->isActive($category)) {
                    $this->selectedId = $category->cat_ID;
                }

                $result[] = $category;
            }
        }
        return $result;
    }

    /**
     * Check if this post type and taxonomy is available for the category view.
     *
     * @return boolean
     */
    public function isAvailable() {
        $available =
            $this->getTypeNow() !== null &&
            $this->getTaxNow() !== null &&
            $this->getTaxNow()->objkey != 'attachment' &&
            get_current_screen()->base == 'edit';
        if ($available) {
            /**
             * Allows you to disable the taxonomy tree (i.e. the whole plugin) for a
             * type or taxonomy.
             *
             * @param {boolean} $enabled If true the plugin is available for this post type / taxonomy
             * @param {string} $type The post type
             * @param {object} $taxonomy The taxonomy
             * @return {boolean}
             * @hook RCL/Available
             */
            $available = apply_filters('RCL/Available', true, $this->getTypeNow(), $this->getTaxNow());
        }
        return $available;
    }

    /**
     * Get the count for the available categories for the post type and taxonomy.
     *
     * @return int
     */
    public function getCnt() {
        return $this->cnt;
    }

    /**
     * Get the posts count for all posts in this post type and taxonomy.
     *
     * @return int
     */
    public function getPostCnt() {
        return $this->cntPosts;
    }

    /**
     * Get all available categories for this post type and taxonomy.
     *
     * @return array
     */
    public function getCats() {
        if ($this->cats === null) {
            // New Initialization of categories so lazy loading is allowed
            $cats = get_categories([
                'hide_empty' => 0,
                //'number' => 100,
                'type' => $this->typenow,
                'taxonomy' => $this->taxnow->objkey,
                'parent' => $this->parent !== null ? $this->parent : ''
            ]);
            $this->cnt = count($cats);
            $this->cats = $this->getCategoryTree(0, $cats, $this->flat, $this->parent === null);
        }
        return $this->cats;
    }

    /**
     * Get the post type.
     *
     * @return string
     */
    public function getTypeNow() {
        return $this->typenow;
    }

    /**
     * Get the taxonomy.
     *
     * @return string
     */
    public function getTaxNow() {
        return $this->taxnow;
    }

    /**
     * Get all available taxonomies.
     *
     * @param boolean $prepared If true only key -> label is returned
     * @return array
     */
    public function getTaxos($prepared = false) {
        $resut = $this->taxos;
        if ($prepared) {
            $result = [];
            foreach ($this->taxos as $key => $value) {
                $result[$key] = $value->label;
            }
        }
        return $result;
    }

    /**
     * Get the query variable name.
     *
     * @return string
     */
    public function getQueryVar() {
        return $this->query_var;
    }

    /**
     * Get the query arguments.
     *
     * @return mixed
     */
    public function getQueryArgs() {
        return $this->query_args;
    }

    /**
     * Get the selected id. You have to pass a currentUrl for this attribute.
     *
     * @return string
     */
    public function getSelectedId() {
        return $this->isCurrentAllPosts() || $this->selectedId === 'ALL' ? 'ALL' : $this->selectedId;
    }
}
