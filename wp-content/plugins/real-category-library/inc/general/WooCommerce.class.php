<?php
namespace MatthiasWeb\RealCategoryLibrary\general;
use MatthiasWeb\RealCategoryLibrary\base;
use MatthiasWeb\RealCategoryLibrary\overrides\interfce;
use MatthiasWeb\RealCategoryLibrary\lite;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Add compatibility with WooCommerce 3.x.
 */
class WooCommerce extends base\Base implements interfce\general\IOverrideWooCommerce {
    use lite\general\WooCommerce;

    /**
     * Add woocommerce product options.
     */
    public function woocommerce_products_general_settings($settings) {
        $settings[] = [
            'title' => __('Real Category Management', RCL_TD),
            'type' => 'title',
            'desc' => '',
            'id' => 'rcl_section'
        ];

        $settings[] = [
            'title' => __('Product attributes', RCL_TD),
            'desc' =>
                __('Enable hierarchical product attributes (recommenend for > 3.x)', RCL_TD) .
                ($this->isPro()
                    ? ''
                    : '<p style="font-weight:bold;">' .
                        __('You are using Real Categories Management Lite, you need Pro to use this feature.', RCL_TD) .
                        ' &middot; <a href="' .
                        RCL_PRO_VERSION .
                        '" target="_blank">' .
                        __('Learn more about Pro (external link)', RCL_TD) .
                        '</a></p>'),
            'id' => 'woocommerce_rcl_hierarchical',
            'default' => 'no',
            'type' => 'checkbox',
            'custom_attributes' => $this->isPro() ? [] : ['disabled' => 'disabled']
        ];

        $settings[] = [
            'type' => 'sectionend',
            'id' => 'rcl_section'
        ];
        return $settings;
    }
}

?>
