<?php
namespace MatthiasWeb\RealCategoryLibrary\general;
use MatthiasWeb\RealCategoryLibrary\base;
use MatthiasWeb\RealCategoryLibrary\rest;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Asset management for frontend scripts and styles.
 */
class Assets extends base\Assets {
    /**
     * Enqueue scripts and styles depending on the type. This function is called
     * from both admin_enqueue_scripts and wp_enqueue_scripts. You can check the
     * type through the $type parameter. In this function you can include your
     * external libraries from public/lib, too.
     *
     * @param string $type The type (see base\Assets constants)
     */
    public function enqueue_scripts_and_styles($type) {
        if (!wp_rcl_active()) {
            return;
        }

        $publicFolder = $this->getPublicFolder();
        $isDebug = $this->isScriptDebug();
        $dpSuffix = $isDebug ? 'development' : 'production.min';
        $minSuffix = $isDebug ? '' : '.min';

        // React (first check version is minium 16.8 so hooks work correctly)
        if ($type === base\Assets::TYPE_ADMIN) {
            // jQuery scripts (Helper) core.js, widget.js, mouse.js, draggable.js, droppable.js, sortable.js
            $requires = array(
                'jquery',
                'jquery-ui-core',
                'jquery-ui-widget',
                'jquery-ui-mouse',
                'jquery-ui-draggable',
                'jquery-ui-droppable',
                'jquery-ui-sortable',
                'jquery-touch-punch'
            );
            array_walk($requires, 'wp_enqueue_script');

            $coreReact = wp_scripts()->query('react');
            if ($coreReact !== false && version_compare($coreReact->ver, '16.8', '<')) {
                wp_deregister_script('react');
                wp_deregister_script('react-dom');
            }

            // ES6/ES7 polyfill
            $this->enqueueLibraryScript('es6-shim', 'es6-shim/es6-shim.min.js');
            $this->enqueueLibraryScript('es7-shim', 'es7-shim/dist/es7-shim.min.js', ['es6-shim']);

            // Both in admin interface (page) and frontend (widgets)
            $this->enqueueLibraryScript('react', 'react/umd/react.' . $dpSuffix . '.js', ['es7-shim']);
            $this->enqueueLibraryScript('react-dom', 'react-dom/umd/react-dom.' . $dpSuffix . '.js', 'react', [
                'es7-shim'
            ]);

            // mobx
            $this->enqueueLibraryScript('mobx', 'mobx/lib/mobx.umd' . $minSuffix . '.js');

            // mobx-state-tree
            $this->enqueueLibraryScript('mobx-state-tree', 'mobx-state-tree/dist/mobx-state-tree.umd.js', ['mobx']);

            // React AIOT
            $this->enqueueLibraryScript('react-aiot.vendor', 'react-aiot/umd/react-aiot.vendor.umd.js', ['react-dom']);
            $this->enqueueLibraryStyle('react-aiot.vendor', 'react-aiot/umd/react-aiot.vendor.umd.css');
            $this->enqueueLibraryScript('react-aiot', 'react-aiot/umd/react-aiot.umd.js', ['react-aiot.vendor']);
            $this->enqueueLibraryStyle('react-aiot', 'react-aiot/umd/react-aiot.umd.css');

            // Your assets implementation here... See base\Assets for enqueue* methods
            $scriptDeps = ['react-dom', 'lodash', 'moment', 'wp-i18n'];
            $handle = RCL_SLUG . '-rcl';
            $this->enqueueScript($handle, 'rcl.' . ($this->isPro() ? 'pro' : 'lite') . '.js', $scriptDeps);
            $this->enqueueStyle($handle, 'rcl.css');
            wp_set_script_translations($handle, RCL_TD, path_join(RCL_PATH, base\Assets::PUBLIC_JSON_I18N));
            wp_localize_script($handle, RCL_OPT_PREFIX . 'Opts', $this->localizeScript($type));
        }

        // Localize with a window.process.env variable for libraries relying on it (MST for example)
        wp_localize_script('react', 'process', [
            'env' => ['NODE_ENV' => $isDebug ? 'development' : 'production']
        ]);
    }

    /**
     * Localize the WordPress backend and frontend. If you want to provide URLs to the
     * frontend you have to consider that some JS libraries do not support umlauts
     * in their URI builder. For this you can use base\Assets#getAsciiUrl.
     *
     * @return array
     */
    public function localizeScript($context) {
        $core = $this->getCore();
        $defaultTaxTree = $core->getDefaultTaxTree();
        $screenSettings = $core->getScreenSettings();
        $isLicenseActivated = $this->isPro() ? $core->getUpdater()->isActivated() : true;
        $isLicenseNoticeDismissed = $core->isLicenseNoticeDismissed();
        $isLiteNoticeDismissed = $core->isLiteNoticeDismissed();

        $common = [
            'slug' => RCL_SLUG,
            'textDomain' => RCL_TD,
            'version' => RCL_VERSION
        ];

        // We put custom variables to "others" because if you put for example
        // a boolean to the first-level it is interpreted as "1" instead of true.
        if ($context === base\Assets::TYPE_ADMIN) {
            return array_merge($common, [
                'others' => [
                    'isPro' => $this->isPro(),
                    'showProHints' => current_user_can('install_plugins'),
                    'canManageOptions' => current_user_can('manage_options'),
                    'proUrl' => RCL_PRO_VERSION,
                    'isAvailable' => $defaultTaxTree->isAvailable(),
                    'screenSettings' => [
                        'isActive' => $defaultTaxTree->isAvailable() && $screenSettings->isActive($defaultTaxTree),
                        'isFastMode' => $defaultTaxTree->isAvailable() && $screenSettings->isFastMode($defaultTaxTree)
                    ],
                    'blogId' => (int) get_current_blog_id(),
                    'simplePageOrdering' => is_plugin_active('simple-page-ordering/simple-page-ordering.php'),
                    'simplePageOrderingLink' => admin_url('plugin-install.php?tab=search&s=simple+page+ordering'),
                    'editOrderBy' => isset($_GET['orderby']) ? sanitize_text_field($_GET['orderby']) : '',
                    'editOrder' => isset($_GET['order']) ? sanitize_text_field($_GET['order']) : '',
                    'typenow' => $defaultTaxTree->getTypeNow(),
                    'taxnow' => $defaultTaxTree->getTaxNow() !== null ? $defaultTaxTree->getTaxNow()->objkey : '',
                    'allPostCnt' => $defaultTaxTree->getPostCnt(),
                    'taxos' => $defaultTaxTree->getTaxos(true),
                    'showLicenseNotice' =>
                        !$isLicenseActivated && !$isLicenseNoticeDismissed && current_user_can('install_plugins'),
                    'showLiteNotice' => !$isLiteNoticeDismissed,
                    'pluginsUrl' => admin_url('plugins.php')
                ],
                'restUrl' => $this->getAsciiUrl(rest\Service::getUrl(rest\Service::SERVICE_NAMESPACE)),
                'restRoot' => $this->getAsciiUrl(get_rest_url()),
                'restQuery' => ['_v' => RCL_VERSION],
                'restNonce' => wp_installing() && !is_multisite() ? '' : wp_create_nonce('wp_rest'),
                'publicUrl' => trailingslashit(plugins_url('public', RCL_FILE))
            ]);
        }
    }

    /**
     * Enqueue scripts and styles for admin pages.
     */
    public function admin_enqueue_scripts() {
        $this->enqueue_scripts_and_styles(base\Assets::TYPE_ADMIN);
    }

    /**
     * Enqueue scripts and styles for frontend pages.
     */
    public function wp_enqueue_scripts() {
        $this->enqueue_scripts_and_styles(base\Assets::TYPE_FRONTEND);
    }
}
