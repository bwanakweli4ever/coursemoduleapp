<?php
namespace MatthiasWeb\RealCategoryLibrary\general;
use MatthiasWeb\RealCategoryLibrary\base;
use MatthiasWeb\RealCategoryLibrary\rest;
use MatthiasWeb\RealCategoryLibrary\options;
use MatthiasWeb\RealCategoryLibrary\overrides\interfce;
use MatthiasWeb\RealCategoryLibrary\lite;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

// Include files, where autoloading is not possible, yet
require_once RCL_INC . 'base/Core.class.php';
require_once RCL_INC . 'overrides/interfce/IOverrideCore.interface.php';
require_once RCL_OVERRIDES_INC . 'Core.trait.php';

// Just for development purposes, you can ignore this!
if (defined('RCL_DEVELOPMENT_ENV') && RCL_DEVELOPMENT_ENV) {
    require_once RCL_INC . 'others/development-env.php';
}

/**
 * Singleton core class which handles the main system for plugin. It includes
 * registering of the autoloader, all hooks (actions & filters) (see base\Core class).
 */
class Core extends base\Core implements interfce\IOverrideCore {
    use lite\Core;

    const TERMS_PRIORITY = 9999999;

    /**
     * The taxonomy tree.
     *
     * @see general\TaxTree
     */
    private $taxTree = null;

    /**
     * The tax order.
     *
     * @see TaxOrder
     */
    private $taxOrder;

    /**
     * The screen settings.
     *
     * @see options\ScreenSettings
     */
    private $screenSettings;

    /**
     * The WooCommerce handler.
     *
     * @see WooCommerce
     */
    private $wooCommerce;

    /**
     * Singleton instance.
     */
    private static $me;

    /**
     * The main service.
     *
     * @see rest\Service
     */
    private $service;

    /**
     * Application core constructor.
     */
    protected function __construct() {
        parent::__construct();

        // Load no-namespace API functions
        foreach (['general'] as $apiInclude) {
            require_once RCL_INC . 'api/' . $apiInclude . '.php';
        }

        $this->getUpdater(); // Initially load the updater
        $this->screenSettings = new options\ScreenSettings();
        $this->wooCommerce = new WooCommerce();

        // Register all your before init hooks here
        add_action('plugins_loaded', [$this, 'updateDbCheck']);
        add_action('init', [$this->getWooCommerce(), 'init'], 2);
        add_filter('check_admin_referer', [$this->getScreenSettings(), 'check_admin_referer'], 10, 2);

        $this->overrideConstruct();
    }

    /**
     * The init function is fired even the init hook of WordPress. If possible
     * it should register all hooks to have them in one place.
     */
    public function init() {
        $this->service = new rest\Service();
        $this->taxOrder = new TaxOrder();
        $comp = new Comp();

        // Register all your hooks here
        add_action('rest_api_init', [$this->getService(), 'rest_api_init']);
        add_action('rest_api_init', [new rest\Term(), 'rest_api_init']);
        add_action('rest_api_init', [new rest\Post(), 'rest_api_init']);
        add_action('admin_enqueue_scripts', [$this->getAssets(), 'admin_enqueue_scripts']);
        add_action('created_term', [$this->getTaxOrder(), 'created_term'], 10, 3);

        add_filter('override_load_textdomain', [new Localization(), 'override_load_textdomain'], 10, 3);
        add_filter('load_script_translation_file', [new Localization(), 'load_script_translation_file'], 10, 3);
        add_filter('screen_settings', [$this->getScreenSettings(), 'screen_settings'], 999, 2);
        add_filter('get_terms_orderby', [$this->getTaxOrder(), 'get_terms_orderby'], self::TERMS_PRIORITY, 2);
        add_filter('wp_get_object_terms', [$this->getTaxOrder(), 'wp_get_object_terms'], self::TERMS_PRIORITY, 3);
        add_filter('get_the_terms', [$this->getTaxOrder(), 'wp_get_object_terms'], self::TERMS_PRIORITY, 3);
        add_filter('get_terms', [$this->getTaxOrder(), 'wp_get_object_terms'], self::TERMS_PRIORITY, 3);
        add_filter('tag_cloud_sort', [$this->getTaxOrder(), 'wp_get_object_terms'], self::TERMS_PRIORITY, 3);
        add_filter('acf/format_value_for_api', [$this->getTaxOrder(), 'wp_get_object_terms'], self::TERMS_PRIORITY);
        add_filter('get_the_categories', [$this->getTaxOrder(), 'wp_get_object_terms'], self::TERMS_PRIORITY, 3);
        add_filter('woocommerce_products_general_settings', [
            $this->getWooCommerce(),
            'woocommerce_products_general_settings'
        ]);
        add_filter('RCL/Localize', [$comp, 'localize_wpml']);

        $this->overrideInit();
    }

    /**
     * Get the service.
     *
     * @return rest\Service
     */
    public function getService() {
        return $this->service;
    }

    /**
     * Get the tax order.
     *
     * @return TaxOrder
     */
    public function getTaxOrder() {
        return $this->taxOrder;
    }

    /**
     * Get the screen settings.
     *
     * @return options\ScreenSettings
     */
    public function getScreenSettings() {
        return $this->screenSettings;
    }

    /**
     * Get the WooCommerce handler.
     *
     * @return WooCommerce
     */
    public function getWooCommerce() {
        return $this->wooCommerce;
    }

    /**
     * Get the current taxonomy tree.
     *
     * @return general\TaxTree
     */
    public function getDefaultTaxTree() {
        return $this->taxTree === null ? ($this->taxTree = new TaxTree()) : $this->taxTree;
    }

    /**
     * Get singleton core class.
     *
     * @return Core
     */
    public static function getInstance() {
        return !isset(self::$me) ? (self::$me = new Core()) : self::$me;
    }
}
