<?php
defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

if (!function_exists('rcl_skip_already_admin_notice')) {
    /**
     * Show an admin notice to administrators when the plugin is already active.
     */
    function rcl_skip_already_admin_notice() {
        if (current_user_can('install_plugins')) {
            extract(get_plugin_data(RCL_FILE, true, false));
            echo '<div class=\'notice notice-error\'>
				<p>Currently more plugins of <strong>' .
                $Name .
                '</strong> are active. Please activate at most one of them.</p>' .
                '</div>';
        }
    }
}
add_action('admin_notices', 'rcl_skip_already_admin_notice');
