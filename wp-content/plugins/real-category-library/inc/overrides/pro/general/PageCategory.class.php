<?php
namespace MatthiasWeb\RealCategoryLibrary\lite\general;
use MatthiasWeb\RealCategoryLibrary\base;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Adds a taxonomy to the 'page' post type.
 */
class PageCategory extends base\Base {
    /**
     * Checks if there is already a taxonomy registered.
     */
    public function check() {
        $taxonomy_objects = get_object_taxonomies('page', 'objects');
        if (is_array($taxonomy_objects) && count($taxonomy_objects) > 0) {
            foreach ($taxonomy_objects as $key => $value) {
                if ($value->hierarchical == true) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Register a taxonomy to the pages.
     */
    public function register_page_cat() {
        if (!$this->check()) {
            return;
        }

        $labels = [
            'name' => __('Page Categories', RCL_TD),
            'singular_name' => __('Page Category', RCL_TD),
            'menu_name' => __('Categories', RCL_TD)
        ];

        $args = [
            'label' => __('Page Categories', RCL_TD),
            'labels' => $labels,
            'public' => true,
            'hierarchical' => true,
            'label' => 'Page Categories',
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => ['slug' => 'page_cat', 'with_front' => true],
            'show_admin_column' => false
        ];
        register_taxonomy('page_cat', ['page'], $args);
    }
}
