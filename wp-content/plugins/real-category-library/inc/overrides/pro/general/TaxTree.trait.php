<?php
namespace MatthiasWeb\RealCategoryLibrary\lite\general;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

trait TaxTree {
    public function buildQueryUrl($slug) {
        $query = [
            'post_type' => $this->getTypeNow()
            //'taxonomy' => $this->%,
            //'term' => $slug
        ];

        // Check if woocommerce product attribute
        if ($this->isWcTaxonomy) {
            $query['taxonomy'] = $this->getQueryVar();
            $query['term'] = $slug;
        } else {
            $query[$this->getQueryVar()] = $slug;
        }
        return $query;
    }

    public function isActive($category) {
        $query = $this->getQueryArgs();
        $slug = $category->editableSlug;
        $catId = $category->term_id;

        if (isset($query[$this->getQueryVar()]) && $query[$this->getQueryVar()] == $slug) {
            return true;
        } elseif (
            // Check if woocommerce product attribute
            isset($query['taxonomy']) &&
            isset($query['term']) &&
            $query['taxonomy'] == $this->getQueryVar() &&
            $query['term'] == $slug
        ) {
            return true;
        } elseif (isset($query['cat']) && $query['cat'] == $catId && !isset($query[$this->getQueryVar()])) {
            return true;
        } else {
            return false;
        }
    }

    public function isCurrentAllPosts() {
        $query = $this->getQueryArgs();
        $catId = isset($query['cat']) ? $query['cat'] : null;

        if (
            (isset($query['taxonomy']) &&
                $this->getCore()
                    ->getWooCommerce()
                    ->isWooCommerceTaxonomy($query['taxonomy'])) ||
            is_numeric($catId)
        ) {
            // Check if woocommerce product attribute
            return false;
        } else {
            return empty(isset($query[$this->getQueryVar()]) ? $query[$this->getQueryVar()] : '');
        }
    }
}
