<?php
namespace MatthiasWeb\RealCategoryLibrary\lite;
use MatthiasWeb\RealCategoryLibrary\lite\general;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

trait Core {
    /**
     * @see https://github.com/Capevace/wordpress-plugin-updater
     */
    private $updater;

    public function overrideConstruct() {
        add_action('init', [new general\PageCategory(), 'register_page_cat'], 99999);
        add_filter('http_request_args', [$this, 'http_request_args_lite'], 10, 2);
        add_filter('plugin_row_meta', [$this, 'plugin_row_meta'], 10, 2);

        $this->getUpdater(); // Initially load the updater
    }

    /**
     * We want to know the lite version so it can be shown right to the pro version.
     *
     * @see https://developer.wordpress.org/reference/functions/wp_update_plugins/
     * @see this#plugin_row_meta
     */
    public function http_request_args_lite($args, $url) {
        if (set_url_scheme($url, 'http') === 'http://api.wordpress.org/plugins/update-check/1.1/') {
            $basename = plugin_basename(RCL_FILE);
            $liteSlug = RCL_SLUG . '-lite/index.php';
            $plugins = json_decode($args['body']['plugins'], ARRAY_A);

            // We are sure that our plugin is in it, but recheck...
            if (isset($plugins['plugins'][$basename])) {
                // Create the installed plugin (do not replace)
                $lite = $plugins['plugins'][$basename];
                $lite['Title'] .= ' (Lite)';
                $lite['Name'] .= ' (Lite)';
                $plugins['plugins'][$liteSlug] = $lite;

                // Create the active plugin (do not replace)
                $plugins['active'][] = $liteSlug;

                // Write to the arguments so it is sent to the wp.org server
                $args['body']['plugins'] = wp_json_encode($plugins);
            }
        }
        return $args;
    }

    public function overrideInit() {
        // Show plugin notice
        if (!$this->getUpdater()->isActivated()) {
            add_action('after_plugin_row_' . plugin_basename(RCL_FILE), [$this, 'after_plugin_row'], 10, 2);
        }
    }

    /**
     * Add an "Lite version" title to the plugin row links. For debugging purposes.
     *
     * @see this#http_request_args_lite
     */
    public function plugin_row_meta($links, $file) {
        // Completely ignore errors
        try {
            if (false !== strpos($file, plugin_basename(RCL_FILE))) {
                $update_plugins = get_site_transient('update_plugins');
                if (
                    $update_plugins !== false &&
                    is_object($update_plugins) &&
                    isset($update_plugins->response, $update_plugins->no_update)
                ) {
                    $liteSlug = RCL_SLUG . '-lite/index.php';
                    $no_update = $update_plugins->no_update;
                    $response = $update_plugins->response;
                    $version = 'n/a';

                    // Lite version has update
                    if (isset($response[$liteSlug])) {
                        $version = $response[$liteSlug]->new_version;
                    }

                    // No update, still show the current lite version
                    if (isset($no_update[$liteSlug])) {
                        $version = $no_update[$liteSlug]->new_version;
                    }

                    // Use tooltip in first version string
                    $links[0] = '<span title="Lite version is currently ' . $version . '">' . $links[0] . '</span>';
                }
            }
        } catch (Exception $e) {
            // Silence is golden.
        }
        return $links;
    }

    /**
     * Show a notice in the plugins list that the plugin is not activated, yet.
     */
    public function after_plugin_row($file, $plugin) {
        $wp_list_table = _get_list_table('WP_Plugins_List_Table');
        printf(
            '<tr class="rcl-update-notice active">
	<th colspan="%d" class="check-column">
    	<div class="plugin-update update-message notice inline notice-warning notice-alt">
        	<div class="update-message">%s</div>
    	</div>
    </th>
</tr>',
            $wp_list_table->get_column_count(),
            wpautop(
                __(
                    '<strong>You have not yet entered the license key</strong>. To receive automatic updates, please enter the key in "Enter license".',
                    RCL_TD
                )
            )
        );
    }

    public function isLicenseNoticeDismissed($set = null) {
        $transient = RCL_OPT_PREFIX . '_licenseActivated';
        $value = '1';

        if ($set !== null) {
            if ($set) {
                set_site_transient($transient, $value, 365 * DAY_IN_SECONDS);
            } else {
                delete_site_transient($transient);
            }
        }

        return get_site_transient($transient) === $value;
    }

    public function isLiteNoticeDismissed($set = null) {
        return true;
    }

    /**
     * Send an email newsletter if checked in the updater.
     */
    public function handleNewsletter($email) {
        wp_remote_get(
            'https://matthias-web.com/wp-json/matthiasweb/v1/newsletter/subscribe?email=' .
                urlencode($email) .
                '&referer=' .
                urlencode(home_url())
        );
    }

    public function getUpdater() {
        if ($this->updater === null) {
            $this->updater = \MatthiasWeb\WPU\V4\WPLSController::initClient('https://license.matthias-web.com/', [
                'name' => 'WP Real Categories Management',
                'version' => RCL_VERSION,
                'path' => RCL_FILE,
                'slug' => RCL_SLUG,
                'newsletterPrivacy' => 'https://matthias-web.com/privacypolicy/'
            ]);
            add_action('wpls_email_' . RCL_SLUG, [$this, 'handleNewsletter']);
        }
        return $this->updater;
    }
}
