<?php
namespace MatthiasWeb\RealCategoryLibrary\lite\options;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

trait ScreenSettings {
    public function screen_settings($settings) {
        $taxTree = $this->getCore()->getDefaultTaxTree();

        if (!$taxTree->isAvailable()) {
            return $settings;
        }

        $settings .=
            '<fieldset class="metabox-prefs">
    		<legend>' .
            __('Advanced Settings for this post type', RCL_TD) .
            '</legend>
    		<label><input class="hide-column-tog" name="rcl-active" type="checkbox" id="rcl-active" value="1" ' .
            checked($this->isActive($taxTree), 1, false) .
            '>' .
            __('Tree view', RCL_TD) .
            '</label>
            <label><input class="hide-column-tog" name="rcl-fast-mode" type="checkbox" id="rcl-fast-categories-off" value="1" ' .
            checked($this->isFastMode($taxTree), 1, false) .
            '>' .
            __('Avoid page reload (tree view must be active)', RCL_TD) .
            '</label>
		</fieldset>';

        return $settings;
    }
}
