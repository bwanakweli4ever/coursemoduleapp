<?php
namespace MatthiasWeb\RealCategoryLibrary\overrides\interfce\general;

interface IOverrideTaxTree {
    /**
     * Build the href query url for the category slug.
     *
     * @param string $slug The category slug
     * @return string
     */
    public function buildQueryUrl($slug);

    /**
     * Checks if the passed slug and category id is the current url.
     *
     * @return boolean
     */
    public function isActive($category);

    /**
     * Check if the current selected category type is "All posts". You have to pass a currentUrl for this attribute.
     *
     * @return boolean
     */
    public function isCurrentAllPosts();
}
