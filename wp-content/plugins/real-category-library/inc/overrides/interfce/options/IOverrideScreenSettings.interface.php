<?php
namespace MatthiasWeb\RealCategoryLibrary\overrides\interfce\options;

interface IOverrideScreenSettings {
    /**
     * Show the screen settings for available post types.
     */
    public function screen_settings($settings);
}
