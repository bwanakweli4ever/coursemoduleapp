<?php
namespace MatthiasWeb\RealCategoryLibrary\overrides\interfce;

interface IOverrideCore {
    public function overrideConstruct();

    public function overrideInit();

    /**
     * Set and/or get the value if the license notice is dismissed.
     *
     * @param boolean $set
     * @return boolean
     */
    public function isLicenseNoticeDismissed($set = null);

    /**
     * Set and/or get the value if the lite notice is dismissed.
     * We need this because the tree is always visible in the posts list
     * and if an user still wants to use the lite version it should be
     * able without annoying notices.
     *
     * @param boolean $set
     * @return boolean
     */
    public function isLiteNoticeDismissed($set = null);

    /**
     * Get the updater instance.
     *
     * @see https://github.com/matzeeable/wordpress-plugin-updater
     */
    public function getUpdater();
}
