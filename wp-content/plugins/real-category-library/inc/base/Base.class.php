<?php
namespace MatthiasWeb\RealCategoryLibrary\base;
use MatthiasWeb\RealCategoryLibrary\general;

defined('ABSPATH') or die('No script kiddies please!'); // Avoid direct file request

/**
 * Base class for all available classes for the plugin.
 */
abstract class Base {
    public function getCore() {
        return general\Core::getInstance();
    }

    /**
     * Simple-to-use error_log debug log. This debug is only outprintted when
     * you define RCL_DEBUG=true constant in wp-config.php
     *
     * @param mixed $message The message
     * @param string $methodOrFunction __METHOD__ OR __FUNCTION__
     */
    public function debug($message, $methodOrFunction = null) {
        if (defined('RCL_DEBUG') && RCL_DEBUG) {
            $log =
                (empty($methodOrFunction) ? '' : '(' . $methodOrFunction . ')') .
                ': ' .
                (is_string($message) ? $message : json_encode($message));
            error_log('RCL_DEBUG ' . $log);
        }
    }

    /**
     * Checks if the current plugin is Pro installation.
     *
     * @return boolean
     */
    public function isPro() {
        return RCL_IS_PRO;
    }

    /**
     * Get a plugin relevant table name depending on the RCL_DB_PREFIX constant.
     *
     * @param string $name Append this name to the plugins relevant table with _{$name}.
     * @return string
     */
    public function getTableName($name = '') {
        global $wpdb;
        return $wpdb->prefix . RCL_DB_PREFIX . ($name == '' ? '' : '_' . $name);
    }
}
