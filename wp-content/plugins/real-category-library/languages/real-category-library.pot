# Copyright (C) 2019 Matthias Guenter
# This file is distributed under the same license as the WP Real Categories Management plugin.
msgid ""
msgstr ""
"Project-Id-Version: WP Real Categories Management 3.2.6\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/real-category-library\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2019-09-27T09:11:23+02:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.2.0\n"

#. Plugin Name of the plugin
msgid "WP Real Categories Management"
msgstr ""

#. Plugin URI of the plugin
msgid "https://codecanyon.net/item/wordpress-real-category-management-custom-category-order-tree-view/13580393"
msgstr ""

#. Description of the plugin
msgid "Organize your WordPress categories in a nice way."
msgstr ""

#. Author of the plugin
msgid "Matthias Guenter"
msgstr ""

#. Author URI of the plugin
msgid "https://matthias-web.com"
msgstr ""

#: dist/real-category-library/inc/general/TaxOrder.class.php:144
#: dist/real-category-library-lite/inc/general/TaxOrder.class.php:144
#: inc/general/TaxOrder.class.php:144
msgid "No categorie found for this post type (%s) / taxonomy (%s)."
msgstr ""

#: dist/real-category-library/inc/general/TaxOrder.class.php:191
#: dist/real-category-library-lite/inc/general/TaxOrder.class.php:191
#: inc/general/TaxOrder.class.php:191
msgid "No current item found."
msgstr ""

#: dist/real-category-library/inc/general/WooCommerce.class.php:20
#: dist/real-category-library-lite/inc/general/WooCommerce.class.php:20
#: inc/general/WooCommerce.class.php:20
msgid "Real Category Management"
msgstr ""

#: dist/real-category-library/inc/general/WooCommerce.class.php:27
#: dist/real-category-library-lite/inc/general/WooCommerce.class.php:27
#: inc/general/WooCommerce.class.php:27
msgid "Product attributes"
msgstr ""

#: dist/real-category-library/inc/general/WooCommerce.class.php:29
#: dist/real-category-library-lite/inc/general/WooCommerce.class.php:29
#: inc/general/WooCommerce.class.php:29
msgid "Enable hierarchical product attributes (recommenend for > 3.x)"
msgstr ""

#: dist/real-category-library/inc/general/WooCommerce.class.php:33
#: dist/real-category-library-lite/inc/general/WooCommerce.class.php:33
#: inc/general/WooCommerce.class.php:33
msgid "You are using Real Categories Management Lite, you need Pro to use this feature."
msgstr ""

#: dist/real-category-library/inc/general/WooCommerce.class.php:37
#: dist/real-category-library-lite/inc/general/WooCommerce.class.php:37
#: dist/real-category-library-lite/inc/overrides/lite/options/ScreenSettings.trait.php:21
#: dist/real-category-library-lite/inc/overrides/lite/options/ScreenSettings.trait.php:56
#: inc/general/WooCommerce.class.php:37
#: inc/overrides/lite/options/ScreenSettings.trait.php:21
#: inc/overrides/lite/options/ScreenSettings.trait.php:56
msgid "Learn more about Pro (external link)"
msgstr ""

#: dist/real-category-library/inc/overrides/pro/Core.trait.php:41
#: inc/overrides/pro/Core.trait.php:41
msgid "<strong>You have not yet entered the license key</strong>. To receive automatic updates, please enter the key in \"Enter license\"."
msgstr ""

#: dist/real-category-library/inc/overrides/pro/general/PageCategory.class.php:35
#: dist/real-category-library/inc/overrides/pro/general/PageCategory.class.php:41
#: inc/overrides/pro/general/PageCategory.class.php:35
#: inc/overrides/pro/general/PageCategory.class.php:41
msgid "Page Categories"
msgstr ""

#: dist/real-category-library/inc/overrides/pro/general/PageCategory.class.php:36
#: inc/overrides/pro/general/PageCategory.class.php:36
msgid "Page Category"
msgstr ""

#: dist/real-category-library/inc/overrides/pro/general/PageCategory.class.php:37
#: inc/overrides/pro/general/PageCategory.class.php:37
msgid "Categories"
msgstr ""

#: dist/real-category-library/inc/overrides/pro/options/ScreenSettings.trait.php:17
#: dist/real-category-library-lite/inc/overrides/lite/options/ScreenSettings.trait.php:34
#: inc/overrides/lite/options/ScreenSettings.trait.php:34
#: inc/overrides/pro/options/ScreenSettings.trait.php:17
msgid "Advanced Settings for this post type"
msgstr ""

#: dist/real-category-library/inc/overrides/pro/options/ScreenSettings.trait.php:22
#: dist/real-category-library-lite/inc/overrides/lite/options/ScreenSettings.trait.php:41
#: inc/overrides/lite/options/ScreenSettings.trait.php:41
#: inc/overrides/pro/options/ScreenSettings.trait.php:22
msgid "Tree view"
msgstr ""

#: dist/real-category-library/inc/overrides/pro/options/ScreenSettings.trait.php:27
#: dist/real-category-library-lite/inc/overrides/lite/options/ScreenSettings.trait.php:46
#: inc/overrides/lite/options/ScreenSettings.trait.php:46
#: inc/overrides/pro/options/ScreenSettings.trait.php:27
msgid "Avoid page reload (tree view must be active)"
msgstr ""

#: dist/real-category-library/inc/rest/Post.class.php:45
#: dist/real-category-library-lite/inc/rest/Post.class.php:45
#: inc/rest/Post.class.php:45
msgid "Something went wrong."
msgstr ""

#: dist/real-category-library/inc/rest/Service.class.php:86
#: dist/real-category-library-lite/inc/rest/Service.class.php:86
#: inc/rest/Service.class.php:107
msgid "No type or valid taxonomy provided."
msgstr ""

#: dist/real-category-library/inc/rest/Service.class.php:122
#: dist/real-category-library-lite/inc/rest/Service.class.php:122
#: inc/rest/Service.class.php:143
msgid "No type or taxonomy provided."
msgstr ""

#: dist/real-category-library/inc/rest/Service.class.php:140
#: dist/real-category-library-lite/inc/rest/Service.class.php:140
#: inc/rest/Service.class.php:161
msgid "The passed term id was not found."
msgstr ""

#: dist/real-category-library/inc/rest/Service.class.php:200
#: dist/real-category-library-lite/inc/rest/Service.class.php:200
#: inc/rest/Service.class.php:221
msgid "Forbidden"
msgstr ""

#: dist/real-category-library/inc/rest/Service.class.php:205
#: dist/real-category-library-lite/inc/rest/Service.class.php:205
#: inc/rest/Service.class.php:226
msgid "Real Categories Management is not active for the current user."
msgstr ""

#: dist/real-category-library/inc/rest/Term.class.php:67
#: dist/real-category-library-lite/inc/rest/Term.class.php:67
#: inc/rest/Term.class.php:67
msgid "The term could not be created (unknown error)."
msgstr ""

#: dist/real-category-library/inc/rest/Term.class.php:101
#: dist/real-category-library-lite/inc/rest/Term.class.php:101
#: inc/rest/Term.class.php:101
msgid "The term does not exist."
msgstr ""

#: dist/real-category-library/inc/rest/Term.class.php:105
#: dist/real-category-library-lite/inc/rest/Term.class.php:105
#: inc/rest/Term.class.php:105
msgid "You can not delete the default term."
msgstr ""

#: dist/real-category-library-lite/inc/overrides/lite/options/ScreenSettings.trait.php:14
#: inc/overrides/lite/options/ScreenSettings.trait.php:14
msgid "You are using Real Categories Management Lite, you need Pro to activate categories for pages."
msgstr ""

#: dist/real-category-library-lite/inc/overrides/lite/options/ScreenSettings.trait.php:49
#: inc/overrides/lite/options/ScreenSettings.trait.php:49
msgid "You are using Real Categories Management Lite, you need Pro to use \"Avoid page reload\" and \"Tree view\" for Custom Post Types."
msgstr ""
