��          �   %   �      P  �   Q  $   �  ,   �  
   %  >   0  	   o  $   y  ;   �     �     �  #     1   3     e     u     �  >   �     �  !   �  .        <  	   U  ]   _  }   �  P   ;  $   �  �  �  �   a  0   	  6   7	  
   n	  @   y	     �	  +   �	  M   �	  )   B
  (   l
  9   �
  K   �
          -     =  G   N     �  2   �  @   �      %     F  k   R  �   �  b   c  2   �                                                                        	                          
                           <strong>You have not yet entered the license key</strong>. To receive automatic updates, please enter the key in "Enter license". Advanced Settings for this post type Avoid page reload (tree view must be active) Categories Enable hierarchical product attributes (recommenend for > 3.x) Forbidden Learn more about Pro (external link) No categorie found for this post type (%s) / taxonomy (%s). No current item found. No type or taxonomy provided. No type or valid taxonomy provided. Organize your WordPress categories in a nice way. Page Categories Page Category Product attributes Real Categories Management is not active for the current user. Something went wrong. The passed term id was not found. The term could not be created (unknown error). The term does not exist. Tree view You are using Real Categories Management Lite, you need Pro to activate categories for pages. You are using Real Categories Management Lite, you need Pro to use "Avoid page reload" and "Tree view" for Custom Post Types. You are using Real Categories Management Lite, you need Pro to use this feature. You can not delete the default term. Project-Id-Version: WP Real Categories Management 3.2.6
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/real-category-library
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2019-09-27T08:32:53+02:00
PO-Revision-Date: 2019-09-27 08:53+0200
X-Generator: Poedit 2.2.3
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n != 1);
Language: de_DE
 <strong>Sie haben den Lizenzschlüssel noch nicht eingegeben</strong>. Um automatische Updates zu erhalten, geben Sie bitte den Schlüssel in "Lizenz eingeben" ein. Erweiterte Einstellungen für diesen Beitragstyp Seitenneuladen vermeiden (Baumansicht muss aktiv sein) Kategorien Hierarchische Produktattribute aktivieren (empfohlen für > 3.x) Nicht erlaubt Erfahren Sie mehr über Pro (externer Link) Für diesen Beitragstyp (%s) / Taxonomie (%s) wurde keine Kategorie gefunden. Es wurde kein aktuelles Element gefunden. Kein Typ oder keine Taxonomie angegeben. Es wurde kein Typ oder eine gültige Taxonomie angegeben. Organisieren Sie Ihre WordPress-Kategorien in einer schönen Art und Weise. Seiten Kategorien Seitenkategorie Produktattribute Real Categories Management ist für den aktuellen Benutzer nicht aktiv. Etwas ist schief gelaufen. Die übergebene Kategorie-ID wurde nicht gefunden. Die Kategorie konnte nicht angelegt werden (unbekannter Fehler). Diese Kategorie existiert nicht. Baumansicht Sie verwenden Real Categories Management Lite, Sie benötigen Pro, um Kategorien für Seiten zu aktivieren. Sie verwenden Real Categories Management Lite, Sie benötigen Pro, um "Seitenreload vermeiden" und "Baumansicht" für benutzerdefinierte Beitragsarten zu verwenden. Sie verwenden Real Categories Management Lite, Sie benötigen Pro, um diese Funktion zu verwenden. Sie können die Standard-Kategorie nicht löschen. 