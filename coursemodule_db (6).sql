-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2020 at 11:24 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coursemodule_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_actions`
--

CREATE TABLE `wp_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_actionscheduler_actions`
--

INSERT INTO `wp_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(207, 'action_scheduler/migration_hook', 'complete', '2020-04-27 05:59:05', '2020-04-27 05:59:05', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1587967145;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1587967145;}', 1, 1, '2020-04-27 05:59:11', '2020-04-27 05:59:11', 0, NULL),
(208, 'action_scheduler/migration_hook', 'complete', '2020-05-01 14:54:38', '2020-05-01 14:54:38', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1588344878;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1588344878;}', 1, 1, '2020-05-01 14:55:48', '2020-05-01 14:55:48', 0, NULL),
(209, 'action_scheduler/migration_hook', 'complete', '2020-05-01 16:21:25', '2020-05-01 16:21:25', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1588350085;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1588350085;}', 1, 1, '2020-05-01 16:22:21', '2020-05-01 16:22:21', 0, NULL),
(210, 'wc-admin_import_customers', 'complete', '2020-05-03 22:25:03', '2020-05-03 22:25:03', '[2]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1588544703;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1588544703;}', 2, 1, '2020-05-03 22:25:20', '2020-05-03 22:25:20', 0, NULL),
(211, 'wc-admin_import_customers', 'complete', '2020-05-03 22:25:25', '2020-05-03 22:25:25', '[2]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1588544725;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1588544725;}', 2, 1, '2020-05-03 22:27:54', '2020-05-03 22:27:54', 0, NULL),
(212, 'action_scheduler/migration_hook', 'complete', '2020-05-04 00:33:07', '2020-05-04 00:33:07', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1588552387;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1588552387;}', 1, 1, '2020-05-04 00:33:18', '2020-05-04 00:33:18', 0, NULL),
(213, 'action_scheduler/migration_hook', 'complete', '2020-05-04 00:33:22', '2020-05-04 00:33:22', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1588552402;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1588552402;}', 1, 1, '2020-05-04 00:33:25', '2020-05-04 00:33:25', 0, NULL),
(214, 'action_scheduler/migration_hook', 'complete', '2020-05-04 00:33:22', '2020-05-04 00:33:22', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1588552402;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1588552402;}', 1, 1, '2020-05-04 00:33:26', '2020-05-04 00:33:26', 0, NULL),
(215, 'action_scheduler/migration_hook', 'complete', '2020-05-04 00:40:25', '2020-05-04 00:40:25', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1588552825;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1588552825;}', 1, 1, '2020-05-04 00:42:29', '2020-05-04 00:42:29', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_claims`
--

CREATE TABLE `wp_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_groups`
--

CREATE TABLE `wp_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_actionscheduler_groups`
--

INSERT INTO `wp_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration'),
(2, 'wc-admin-data');

-- --------------------------------------------------------

--
-- Table structure for table `wp_actionscheduler_logs`
--

CREATE TABLE `wp_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_actionscheduler_logs`
--

INSERT INTO `wp_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 207, 'action created', '2020-04-27 05:59:06', '2020-04-27 05:59:06'),
(2, 207, 'action started via Async Request', '2020-04-27 05:59:11', '2020-04-27 05:59:11'),
(3, 207, 'action complete via Async Request', '2020-04-27 05:59:11', '2020-04-27 05:59:11'),
(4, 208, 'action created', '2020-05-01 14:54:38', '2020-05-01 14:54:38'),
(5, 208, 'action started via WP Cron', '2020-05-01 14:55:48', '2020-05-01 14:55:48'),
(6, 208, 'action complete via WP Cron', '2020-05-01 14:55:48', '2020-05-01 14:55:48'),
(7, 209, 'action created', '2020-05-01 16:21:25', '2020-05-01 16:21:25'),
(8, 209, 'action started via WP Cron', '2020-05-01 16:22:21', '2020-05-01 16:22:21'),
(9, 209, 'action complete via WP Cron', '2020-05-01 16:22:21', '2020-05-01 16:22:21'),
(10, 210, 'action created', '2020-05-03 22:24:59', '2020-05-03 22:24:59'),
(11, 210, 'action started via WP Cron', '2020-05-03 22:25:20', '2020-05-03 22:25:20'),
(12, 211, 'action created', '2020-05-03 22:25:20', '2020-05-03 22:25:20'),
(13, 210, 'action complete via WP Cron', '2020-05-03 22:25:20', '2020-05-03 22:25:20'),
(14, 211, 'action started via WP Cron', '2020-05-03 22:27:54', '2020-05-03 22:27:54'),
(15, 211, 'action complete via WP Cron', '2020-05-03 22:27:54', '2020-05-03 22:27:54'),
(16, 212, 'action created', '2020-05-04 00:33:08', '2020-05-04 00:33:08'),
(17, 212, 'action started via WP Cron', '2020-05-04 00:33:18', '2020-05-04 00:33:18'),
(18, 212, 'action complete via WP Cron', '2020-05-04 00:33:18', '2020-05-04 00:33:18'),
(19, 213, 'action created', '2020-05-04 00:33:22', '2020-05-04 00:33:22'),
(20, 214, 'action created', '2020-05-04 00:33:22', '2020-05-04 00:33:22'),
(21, 213, 'action started via Async Request', '2020-05-04 00:33:25', '2020-05-04 00:33:25'),
(22, 213, 'action complete via Async Request', '2020-05-04 00:33:25', '2020-05-04 00:33:25'),
(23, 214, 'action started via Async Request', '2020-05-04 00:33:25', '2020-05-04 00:33:25'),
(24, 214, 'action complete via Async Request', '2020-05-04 00:33:25', '2020-05-04 00:33:25'),
(25, 215, 'action created', '2020-05-04 00:40:25', '2020-05-04 00:40:25'),
(26, 215, 'action started via WP Cron', '2020-05-04 00:42:29', '2020-05-04 00:42:29'),
(27, 215, 'action complete via WP Cron', '2020-05-04 00:42:29', '2020-05-04 00:42:29');

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2020-04-10 19:52:35', '2020-04-10 19:52:35', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/coursemodule/', 'yes'),
(2, 'home', 'http://localhost/coursemodule/', 'yes'),
(3, 'blogname', 'Video Course Module', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'bwanakweli4ever@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/index.php/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:228:{s:18:\"index.php/video/?$\";s:25:\"index.php?post_type=video\";s:48:\"index.php/video/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=video&feed=$matches[1]\";s:43:\"index.php/video/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=video&feed=$matches[1]\";s:35:\"index.php/video/page/([0-9]{1,})/?$\";s:43:\"index.php?post_type=video&paged=$matches[1]\";s:17:\"index.php/task/?$\";s:24:\"index.php?post_type=task\";s:47:\"index.php/task/feed/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=task&feed=$matches[1]\";s:42:\"index.php/task/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=task&feed=$matches[1]\";s:34:\"index.php/task/page/([0-9]{1,})/?$\";s:42:\"index.php?post_type=task&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:57:\"index.php/category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:52:\"index.php/category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:33:\"index.php/category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:45:\"index.php/category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:27:\"index.php/category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:54:\"index.php/tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:49:\"index.php/tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:30:\"index.php/tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:42:\"index.php/tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:24:\"index.php/tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:55:\"index.php/type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:50:\"index.php/type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:31:\"index.php/type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:43:\"index.php/type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:25:\"index.php/type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:43:\"index.php/video/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:53:\"index.php/video/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:73:\"index.php/video/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:68:\"index.php/video/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:68:\"index.php/video/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:49:\"index.php/video/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:32:\"index.php/video/([^/]+)/embed/?$\";s:38:\"index.php?video=$matches[1]&embed=true\";s:36:\"index.php/video/([^/]+)/trackback/?$\";s:32:\"index.php?video=$matches[1]&tb=1\";s:56:\"index.php/video/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?video=$matches[1]&feed=$matches[2]\";s:51:\"index.php/video/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?video=$matches[1]&feed=$matches[2]\";s:44:\"index.php/video/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?video=$matches[1]&paged=$matches[2]\";s:51:\"index.php/video/([^/]+)/comment-page-([0-9]{1,})/?$\";s:45:\"index.php?video=$matches[1]&cpage=$matches[2]\";s:40:\"index.php/video/([^/]+)(?:/([0-9]+))?/?$\";s:44:\"index.php?video=$matches[1]&page=$matches[2]\";s:32:\"index.php/video/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"index.php/video/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"index.php/video/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"index.php/video/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"index.php/video/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"index.php/video/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:42:\"index.php/task/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:52:\"index.php/task/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:72:\"index.php/task/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:67:\"index.php/task/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:67:\"index.php/task/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:48:\"index.php/task/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:31:\"index.php/task/([^/]+)/embed/?$\";s:37:\"index.php?task=$matches[1]&embed=true\";s:35:\"index.php/task/([^/]+)/trackback/?$\";s:31:\"index.php?task=$matches[1]&tb=1\";s:55:\"index.php/task/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?task=$matches[1]&feed=$matches[2]\";s:50:\"index.php/task/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?task=$matches[1]&feed=$matches[2]\";s:43:\"index.php/task/([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?task=$matches[1]&paged=$matches[2]\";s:50:\"index.php/task/([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?task=$matches[1]&cpage=$matches[2]\";s:39:\"index.php/task/([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?task=$matches[1]&page=$matches[2]\";s:31:\"index.php/task/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:41:\"index.php/task/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:61:\"index.php/task/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"index.php/task/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:56:\"index.php/task/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:37:\"index.php/task/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:57:\"index.php/symbol/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:54:\"index.php?tcb_symbols_tax=$matches[1]&feed=$matches[2]\";s:52:\"index.php/symbol/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:54:\"index.php?tcb_symbols_tax=$matches[1]&feed=$matches[2]\";s:33:\"index.php/symbol/([^/]+)/embed/?$\";s:48:\"index.php?tcb_symbols_tax=$matches[1]&embed=true\";s:45:\"index.php/symbol/([^/]+)/page/?([0-9]{1,})/?$\";s:55:\"index.php?tcb_symbols_tax=$matches[1]&paged=$matches[2]\";s:27:\"index.php/symbol/([^/]+)/?$\";s:37:\"index.php?tcb_symbols_tax=$matches[1]\";s:58:\"index.php/tcb_content_template/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"index.php/tcb_content_template/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"index.php/tcb_content_template/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"index.php/tcb_content_template/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"index.php/tcb_content_template/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"index.php/tcb_content_template/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:47:\"index.php/tcb_content_template/([^/]+)/embed/?$\";s:53:\"index.php?tcb_content_template=$matches[1]&embed=true\";s:51:\"index.php/tcb_content_template/([^/]+)/trackback/?$\";s:47:\"index.php?tcb_content_template=$matches[1]&tb=1\";s:59:\"index.php/tcb_content_template/([^/]+)/page/?([0-9]{1,})/?$\";s:60:\"index.php?tcb_content_template=$matches[1]&paged=$matches[2]\";s:66:\"index.php/tcb_content_template/([^/]+)/comment-page-([0-9]{1,})/?$\";s:60:\"index.php?tcb_content_template=$matches[1]&cpage=$matches[2]\";s:55:\"index.php/tcb_content_template/([^/]+)(?:/([0-9]+))?/?$\";s:59:\"index.php?tcb_content_template=$matches[1]&page=$matches[2]\";s:47:\"index.php/tcb_content_template/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"index.php/tcb_content_template/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"index.php/tcb_content_template/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"index.php/tcb_content_template/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"index.php/tcb_content_template/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"index.php/tcb_content_template/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:45:\"index.php/product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:55:\"index.php/product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:75:\"index.php/product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:70:\"index.php/product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:70:\"index.php/product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:51:\"index.php/product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"index.php/product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:38:\"index.php/product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:46:\"index.php/product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:53:\"index.php/product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:42:\"index.php/product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:34:\"index.php/product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"index.php/product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"index.php/product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"index.php/product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"index.php/product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"index.php/product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:42:\"index.php/category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:39:\"index.php/tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:41:\"index.php/video/([^/]+)/wc-api(/(.*))?/?$\";s:46:\"index.php?video=$matches[1]&wc-api=$matches[3]\";s:47:\"index.php/video/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:58:\"index.php/video/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:40:\"index.php/task/([^/]+)/wc-api(/(.*))?/?$\";s:45:\"index.php?task=$matches[1]&wc-api=$matches[3]\";s:46:\"index.php/task/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:57:\"index.php/task/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:56:\"index.php/tcb_content_template/([^/]+)/wc-api(/(.*))?/?$\";s:61:\"index.php?tcb_content_template=$matches[1]&wc-api=$matches[3]\";s:62:\"index.php/tcb_content_template/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:73:\"index.php/tcb_content_template/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:43:\"index.php/product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:49:\"index.php/product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:60:\"index.php/product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:65:\"index.php/product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:60:\"index.php/product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:41:\"index.php/product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:53:\"index.php/product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:35:\"index.php/product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:62:\"index.php/product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:57:\"index.php/product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:38:\"index.php/product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:50:\"index.php/product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:32:\"index.php/product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:42:\"index.php/feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:37:\"index.php/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:18:\"index.php/embed/?$\";s:21:\"index.php?&embed=true\";s:30:\"index.php/page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:37:\"index.php/comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=49&cpage=$matches[1]\";s:27:\"index.php/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:51:\"index.php/comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:46:\"index.php/comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:27:\"index.php/comments/embed/?$\";s:21:\"index.php?&embed=true\";s:36:\"index.php/comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:54:\"index.php/search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:49:\"index.php/search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:30:\"index.php/search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:42:\"index.php/search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:39:\"index.php/search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:24:\"index.php/search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:57:\"index.php/author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:52:\"index.php/author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:33:\"index.php/author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:45:\"index.php/author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:42:\"index.php/author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:27:\"index.php/author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:79:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:74:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:55:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:67:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:64:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:49:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:66:\"index.php/([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:61:\"index.php/([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:42:\"index.php/([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:54:\"index.php/([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:51:\"index.php/([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:36:\"index.php/([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:53:\"index.php/([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:48:\"index.php/([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:29:\"index.php/([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:41:\"index.php/([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:38:\"index.php/([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:23:\"index.php/([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:68:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:78:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:98:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:93:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:93:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:74:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:63:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:67:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:87:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:82:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:75:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:82:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:72:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$\";s:99:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]\";s:72:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:83:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:71:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:57:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:67:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:87:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:82:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:82:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:63:\"index.php/[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:74:\"index.php/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:61:\"index.php/([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:48:\"index.php/([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:37:\"index.php/.?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:47:\"index.php/.?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:67:\"index.php/.?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"index.php/.?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:62:\"index.php/.?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:43:\"index.php/.?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:26:\"index.php/(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:30:\"index.php/(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:50:\"index.php/(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:45:\"index.php/(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:38:\"index.php/(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:45:\"index.php/(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:38:\"index.php/(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:43:\"index.php/(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:35:\"index.php/(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:39:\"index.php/(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:38:\"index.php/(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:41:\"index.php/(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:41:\"index.php/(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:44:\"index.php/(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:42:\"index.php/(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:44:\"index.php/(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:47:\"index.php/(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:50:\"index.php/(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:55:\"index.php/(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:35:\"index.php/(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:41:\"index.php/.?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:52:\"index.php/.?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:34:\"index.php/(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:7:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:31:\"real-category-library/index.php\";i:2;s:45:\"simple-page-ordering/simple-page-ordering.php\";i:3;s:45:\"thrive-visual-editor/thrive-visual-editor.php\";i:4;s:49:\"woocommerce-membership/woocommerce-membership.php\";i:5;s:27:\"woocommerce/woocommerce.php\";i:6;s:29:\"wphave-admin/wphave-admin.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:3:{i:0;s:72:\"C:\\xampp\\htdocs\\coursemodule/wp-content/themes/videoCourse/functions.php\";i:2;s:68:\"C:\\xampp\\htdocs\\coursemodule/wp-content/themes/videoCourse/style.css\";i:3;s:0:\"\";}', 'no'),
(40, 'template', 'videoCourse', 'yes'),
(41, 'stylesheet', 'videoCourse', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '47018', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:29:\"wphave-admin/wphave-admin.php\";s:58:\"wphave_admin_license_uninstall_after_plugin_uninstallation\";}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '49', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'wp_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:121:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:11:\"tve-use-tcb\";b:1;s:10:\"tve-use-td\";b:1;s:12:\"tve-edit-cpt\";b:1;s:13:\"wpsms_sendsms\";b:1;s:12:\"wpsms_outbox\";b:1;s:17:\"wpsms_subscribers\";b:1;s:13:\"wpsms_setting\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:37:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:11:\"tve-use-tcb\";b:1;s:10:\"tve-use-td\";b:1;s:12:\"tve-edit-cpt\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'cron', 'a:24:{i:1589287208;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"0d04ed39571b55704c122d726248bbac\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:1:{i:0;s:7:\"WP Cron\";}s:8:\"interval\";i:60;}}}i:1589287426;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1589287831;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1589288348;a:1:{s:33:\"wc_admin_process_orders_milestone\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1589288379;a:1:{s:29:\"wc_admin_unsnooze_admin_notes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1589295430;a:1:{s:44:\"puc_cron_check_updates-real-category-library\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1589313158;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1589313161;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1589313179;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1589313189;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1589313875;a:1:{s:17:\"thrive_token_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1589320050;a:1:{s:41:\"check_plugin_updates-thrive-visual-editor\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1589327943;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1589328000;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1589349550;a:1:{s:14:\"wc_admin_daily\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1589349553;a:2:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1589360343;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1589659413;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1590451199;a:1:{s:41:\"woocommerce_membership_scheduled_reminder\";a:1:{s:32:\"14e9383bfa96898298193c18a4286c34\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:3:{i:0;i:240;i:1;i:2;i:2;i:1590883199;}}}}i:1590559203;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:11:\"fifteendays\";s:4:\"args\";a:0:{}s:8:\"interval\";i:1296000;}}}i:1590623999;a:1:{s:41:\"woocommerce_membership_scheduled_reminder\";a:1:{s:32:\"14e9383bfa96898298193c18a4286c34\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:3:{i:0;i:240;i:1;i:2;i:2;i:1590883199;}}}}i:1590710399;a:1:{s:41:\"woocommerce_membership_scheduled_reminder\";a:1:{s:32:\"14e9383bfa96898298193c18a4286c34\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:3:{i:0;i:240;i:1;i:2;i:2;i:1590883199;}}}}i:1590883199;a:1:{s:43:\"woocommerce_membership_scheduled_expiration\";a:1:{s:32:\"d1891b52696ae9e0d7a2cf12ac0cd77b\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:2:{i:0;i:240;i:1;i:2;}}}}s:7:\"version\";i:2;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'recovery_keys', 'a:0:{}', 'yes'),
(115, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1586549045;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(142, 'auto_core_update_notified', 'a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:25:\"bwanakweli4ever@gmail.com\";s:7:\"version\";s:5:\"5.2.5\";s:9:\"timestamp\";i:1586548483;}', 'no'),
(144, 'admin_email_lifespan', '1589537476', 'yes'),
(145, 'db_upgraded', '', 'yes'),
(148, 'can_compress_scripts', '1', 'no'),
(149, 'current_theme', 'videoCourse', 'yes'),
(150, 'theme_mods_videoCourse', 'a:5:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:7:\"primary\";i:4;s:9:\"secondary\";i:5;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1586878856;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}s:18:\"custom_css_post_id\";i:-1;s:11:\"custom_logo\";i:174;}', 'yes'),
(151, 'theme_switched', '', 'yes'),
(152, 'recently_activated', 'a:3:{s:39:\"pmpro-woocommerce/pmpro-woocommerce.php\";i:1588552818;s:25:\"wpcomplete/wpcomplete.php\";i:1588350080;s:36:\"real-category-library-lite/index.php\";i:1588344876;}', 'yes'),
(154, 'tcb_symbols_tax_children', 'a:0:{}', 'yes'),
(155, 'tve_version', '2.4.6.2', 'yes'),
(156, 'global_lp_scripts', 'a:0:{}', 'yes'),
(157, 'tve_td_db_version', '1.0.0', 'yes'),
(158, 'tve_tcb_db_version', '1.2', 'yes'),
(159, 'tve_dash_default_cap_set', '1', 'yes'),
(160, 'external_updates-thrive-visual-editor', 'O:8:\"stdClass\":3:{s:9:\"lastCheck\";i:1589278355;s:14:\"checkedVersion\";s:7:\"2.4.6.2\";s:6:\"update\";O:8:\"stdClass\":9:{s:2:\"id\";i:0;s:4:\"slug\";s:20:\"thrive-visual-editor\";s:7:\"version\";s:7:\"2.5.3.4\";s:8:\"homepage\";s:0:\"\";s:12:\"download_url\";s:61:\"http://download.thrivethemes.com/thrive-architect-2.5.3.4.zip\";s:14:\"upgrade_notice\";s:0:\"\";s:6:\"tested\";s:5:\"5.4.0\";s:8:\"requires\";s:0:\"\";s:5:\"icons\";a:1:{s:2:\"1x\";s:113:\"http://localhost/coursemodule/wp-content/plugins/thrive-visual-editor/editor/css/images/thrive-architect-logo.png\";}}}', 'no'),
(162, 'tcb_def_caps_set', '1', 'yes'),
(163, 'tcb_logo_data', 'a:2:{i:0;a:5:{s:2:\"id\";i:0;s:13:\"attachment_id\";s:0:\"\";s:6:\"active\";i:1;s:7:\"default\";i:1;s:4:\"name\";s:4:\"Dark\";}i:1;a:5:{s:2:\"id\";i:1;s:13:\"attachment_id\";s:0:\"\";s:6:\"active\";i:1;s:7:\"default\";i:1;s:4:\"name\";s:5:\"Light\";}}', 'yes'),
(164, 'thrive_tt_shares', 'a:2:{s:8:\"facebook\";a:2:{s:5:\"count\";i:0;s:10:\"last_fetch\";i:1586555260;}s:6:\"google\";a:2:{s:5:\"count\";i:0;s:10:\"last_fetch\";i:1586555262;}}', 'yes'),
(174, 'theme_mods_twentytwenty', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:7:\"primary\";i:4;s:6:\"footer\";i:5;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1586879324;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-1\";a:0:{}s:9:\"sidebar-2\";a:0:{}}}}', 'yes'),
(188, 'recovery_mode_email_last_sent', '1588553718', 'yes'),
(191, 'nav_menu_options', 'a:1:{s:8:\"auto_add\";a:0:{}}', 'yes'),
(216, '_transient_health-check-site-status-result', '{\"good\":10,\"recommended\":5,\"critical\":2}', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(358, 'external_updates-wpcomplete', 'O:8:\"stdClass\":3:{s:9:\"lastCheck\";i:1588350009;s:14:\"checkedVersion\";s:5:\"2.4.1\";s:6:\"update\";O:8:\"stdClass\":9:{s:2:\"id\";i:0;s:4:\"slug\";s:10:\"wpcomplete\";s:7:\"version\";s:8:\"2.8.10.1\";s:8:\"homepage\";N;s:6:\"tested\";N;s:12:\"download_url\";s:55:\"https://wpcomplete.co/downloads/wpcomplete-2.8.10.1.zip\";s:14:\"upgrade_notice\";N;s:8:\"filename\";s:25:\"wpcomplete/wpcomplete.php\";s:12:\"translations\";a:0:{}}}', 'no'),
(359, 'wpcomplete_license_key', '', 'yes'),
(360, 'wpcomplete_role', 'all', 'yes'),
(361, 'wpcomplete_post_type', 'all', 'yes'),
(362, 'wpcomplete_auto_append', 'true', 'yes'),
(363, 'wpcomplete_incomplete_text', 'Mark as complete', 'yes'),
(364, 'wpcomplete_incomplete_active_text', 'Saving...', 'yes'),
(365, 'wpcomplete_incomplete_background', '#ff0000', 'yes'),
(366, 'wpcomplete_incomplete_color', '#ffffff', 'yes'),
(367, 'wpcomplete_completed_text', 'COMPLETED!', 'yes'),
(368, 'wpcomplete_completed_active_text', 'Saving...', 'yes'),
(369, 'wpcomplete_completed_background', '#666666', 'yes'),
(370, 'wpcomplete_completed_color', '#ffffff', 'yes'),
(371, 'wpcomplete_graph_primary', '#97a71d', 'yes'),
(372, 'wpcomplete_graph_secondary', '#ebebeb', 'yes'),
(373, 'wpcomplete_custom_styles', 'li .wpc-lesson {} li .wpc-lesson-complete {} li .wpc-lesson-completed { opacity: .5; } li .wpc-lesson-completed:after { content: \"✔\"; margin-left: 5px; }', 'yes'),
(374, 'wpcomplete_show_widget', 'true', 'yes'),
(378, 'acf_version', '5.8.9', 'yes'),
(417, 'wp_sms_db_version', '5.2.2', 'yes'),
(418, 'wpsms_settings', 'a:11:{s:15:\"rest_api_status\";i:1;s:12:\"gateway_name\";s:8:\"mistasms\";s:16:\"gateway_username\";s:0:\"\";s:16:\"gateway_password\";s:28:\"YWRtaW46YWRtaW4ucGFzc3dvcmQ=\";s:17:\"gateway_sender_id\";s:0:\"\";s:11:\"gateway_key\";s:28:\"YWRtaW46YWRtaW4ucGFzc3dvcmQ=\";s:22:\"account_credit_in_menu\";s:1:\"1\";s:25:\"account_credit_in_sendsms\";s:1:\"1\";s:18:\"wc_notif_new_order\";s:1:\"1\";s:27:\"wc_notif_new_order_template\";s:8:\"helloooo\";s:28:\"edd_notif_new_order_template\";s:0:\"\";}', 'yes'),
(419, 'wps_pp_settings', 'a:0:{}', 'yes'),
(420, 'widget_wpsms_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(421, 'wpsms_show_welcome_page', '', 'yes'),
(422, 'wpsms_first_show_welcome_page', '1', 'yes'),
(423, 'wpsms_gateway_credit', 'Unlimited', 'yes'),
(428, 'wpsms_hide_newsletter', '1', 'yes'),
(653, 'wp_admin_theme_settings_options', 'a:115:{s:8:\"user_box\";s:0:\"\";s:11:\"company_box\";s:0:\"\";s:16:\"company_box_logo\";s:0:\"\";s:21:\"company_box_logo_size\";i:140;s:9:\"thumbnail\";s:0:\"\";s:12:\"post_page_id\";s:0:\"\";s:9:\"hide_help\";s:0:\"\";s:18:\"hide_screen_option\";s:0:\"\";s:15:\"left_menu_width\";i:160;s:16:\"left_menu_expand\";s:0:\"\";s:7:\"spacing\";s:0:\"\";s:17:\"spacing_max_width\";i:2000;s:7:\"credits\";s:0:\"\";s:12:\"credits_text\";s:0:\"\";s:14:\"google_webfont\";s:0:\"\";s:21:\"google_webfont_weight\";s:0:\"\";s:7:\"toolbar\";s:0:\"\";s:22:\"hide_adminbar_comments\";s:0:\"\";s:17:\"hide_adminbar_new\";s:0:\"\";s:23:\"hide_adminbar_customize\";s:0:\"\";s:20:\"hide_adminbar_search\";s:0:\"\";s:15:\"toolbar_wp_icon\";s:0:\"\";s:12:\"toolbar_icon\";s:0:\"\";s:13:\"toolbar_color\";s:7:\"#32373c\";s:14:\"howdy_greeting\";s:0:\"\";s:19:\"howdy_greeting_text\";s:0:\"\";s:11:\"theme_color\";s:7:\"#4777CD\";s:16:\"theme_background\";s:7:\"#545c63\";s:20:\"theme_background_end\";s:7:\"#32373c\";s:13:\"login_disable\";s:0:\"\";s:11:\"login_title\";s:13:\"Welcome Back.\";s:11:\"logo_upload\";s:0:\"\";s:9:\"logo_size\";i:250;s:8:\"login_bg\";s:0:\"\";s:12:\"memory_usage\";s:0:\"\";s:12:\"memory_limit\";s:0:\"\";s:16:\"memory_available\";s:0:\"\";s:11:\"php_version\";s:0:\"\";s:10:\"ip_address\";s:0:\"\";s:10:\"wp_version\";s:0:\"\";s:9:\"css_admin\";s:0:\"\";s:9:\"css_login\";s:0:\"\";s:6:\"wp_svg\";s:0:\"\";s:6:\"wp_ico\";s:0:\"\";s:19:\"disable_page_system\";s:0:\"\";s:15:\"disable_page_wp\";s:0:\"\";s:22:\"disable_page_constants\";s:0:\"\";s:25:\"disable_transient_manager\";s:0:\"\";s:19:\"disable_page_export\";s:0:\"\";s:15:\"disable_page_ms\";s:0:\"\";s:22:\"disable_page_error_log\";s:0:\"\";s:21:\"disable_page_htaccess\";s:0:\"\";s:20:\"disable_page_php_ini\";s:0:\"\";s:23:\"disable_page_robots_txt\";s:0:\"\";s:21:\"disable_theme_options\";s:0:\"\";s:22:\"disable_plugin_subsite\";s:0:\"\";s:14:\"wp_version_tag\";s:0:\"\";s:8:\"wp_emoji\";s:0:\"\";s:13:\"wp_feed_links\";s:0:\"\";s:11:\"wp_rsd_link\";s:0:\"\";s:14:\"wp_wlwmanifest\";s:0:\"\";s:12:\"wp_shortlink\";s:0:\"\";s:11:\"wp_rest_api\";s:0:\"\";s:9:\"wp_oembed\";s:0:\"\";s:10:\"wp_xml_rpc\";s:0:\"\";s:12:\"wp_heartbeat\";s:0:\"\";s:11:\"wp_rel_link\";s:0:\"\";s:16:\"wp_self_pingback\";s:0:\"\";s:16:\"mb_custom_fields\";s:0:\"\";s:16:\"mb_commentstatus\";s:0:\"\";s:11:\"mb_comments\";s:0:\"\";s:9:\"mb_author\";s:0:\"\";s:11:\"mb_category\";s:0:\"\";s:9:\"mb_format\";s:0:\"\";s:13:\"mb_pageparent\";s:0:\"\";s:14:\"mb_postexcerpt\";s:0:\"\";s:12:\"mb_postimage\";s:0:\"\";s:12:\"mb_revisions\";s:0:\"\";s:7:\"mb_slug\";s:0:\"\";s:7:\"mb_tags\";s:0:\"\";s:13:\"mb_trackbacks\";s:0:\"\";s:15:\"dbw_quick_press\";s:0:\"\";s:13:\"dbw_right_now\";s:0:\"\";s:12:\"dbw_activity\";s:0:\"\";s:11:\"dbw_primary\";s:0:\"\";s:11:\"dbw_welcome\";s:0:\"\";s:17:\"dbw_wpat_user_log\";s:0:\"\";s:17:\"dbw_wpat_sys_info\";s:0:\"\";s:19:\"dbw_wpat_count_post\";s:0:\"\";s:19:\"dbw_wpat_count_page\";s:0:\"\";s:22:\"dbw_wpat_count_comment\";s:0:\"\";s:20:\"dbw_wpat_recent_post\";s:0:\"\";s:20:\"dbw_wpat_recent_page\";s:0:\"\";s:23:\"dbw_wpat_recent_comment\";s:0:\"\";s:15:\"dbw_wpat_memory\";s:0:\"\";s:8:\"wt_pages\";s:0:\"\";s:11:\"wt_calendar\";s:0:\"\";s:11:\"wt_archives\";s:0:\"\";s:7:\"wt_meta\";s:0:\"\";s:9:\"wt_search\";s:0:\"\";s:7:\"wt_text\";s:0:\"\";s:13:\"wt_categories\";s:0:\"\";s:15:\"wt_recent_posts\";s:0:\"\";s:18:\"wt_recent_comments\";s:0:\"\";s:6:\"wt_rss\";s:0:\"\";s:12:\"wt_tag_cloud\";s:0:\"\";s:6:\"wt_nav\";s:0:\"\";s:8:\"wt_image\";s:0:\"\";s:8:\"wt_audio\";s:0:\"\";s:8:\"wt_video\";s:0:\"\";s:10:\"wt_gallery\";s:0:\"\";s:7:\"wt_html\";s:0:\"\";s:14:\"wp_header_code\";s:0:\"\";s:14:\"wp_footer_code\";s:0:\"\";s:20:\"meta_referrer_policy\";s:4:\"none\";}', 'yes'),
(654, 'wp_admin_theme_cd_activation_date', '2020-04-25 18:16:39', 'yes'),
(655, 'wp_less_cached_files', 'a:2:{s:17:\"wphave-admin-main\";a:4:{s:4:\"vars\";a:14:{s:8:\"themeurl\";s:62:\"~\"http://localhost/coursemodule/wp-content/themes/videoCourse\"\";s:7:\"lessurl\";s:80:\"~\"http://localhost/coursemodule/wp-content/plugins/wphave-admin/assets/css/less\"\";s:14:\"wpatThemeColor\";s:7:\"#4777CD\";s:22:\"wpatGradientStartColor\";s:13:\"rgb(84,92,99)\";s:20:\"wpatGradientEndColor\";s:13:\"rgb(50,55,60)\";s:16:\"wpatToolbarColor\";s:7:\"#32373c\";s:19:\"wpatSpacingMaxWidth\";s:6:\"2000px\";s:17:\"wpatMenuLeftWidth\";s:5:\"160px\";s:21:\"wpatMenuLeftWidthDiff\";s:5:\"120px\";s:17:\"wpatLoginLogoSize\";s:5:\"250px\";s:15:\"wpatToolbarIcon\";s:4:\"none\";s:11:\"wpatWebFont\";s:4:\"none\";s:11:\"wpatLoginBg\";s:4:\"none\";s:13:\"wpatLoginLogo\";s:4:\"none\";}s:3:\"url\";s:84:\"http://localhost/coursemodule/wp-content/uploads/wp-less-cache/wphave-admin-main.css\";s:7:\"version\";s:32:\"9e1cf376f7827dcdfcf1a6f704fdd37f\";s:4:\"less\";a:4:{s:4:\"root\";s:86:\"C:\\xampp\\htdocs\\coursemodule/wp-content/plugins/wphave-admin/assets/css/less/main.less\";s:8:\"compiled\";s:0:\"\";s:5:\"files\";a:7:{s:86:\"C:\\xampp\\htdocs\\coursemodule\\wp-content\\plugins\\wphave-admin\\assets\\css\\less\\main.less\";i:1572345720;s:89:\"C:\\xampp\\htdocs\\coursemodule\\wp-content\\plugins\\wphave-admin\\assets\\css\\less\\buttons.less\";i:1581947777;s:87:\"C:\\xampp\\htdocs\\coursemodule\\wp-content\\plugins\\wphave-admin\\assets\\css\\less\\forms.less\";i:1581948592;s:87:\"C:\\xampp\\htdocs\\coursemodule\\wp-content\\plugins\\wphave-admin\\assets\\css\\less\\style.less\";i:1581948879;s:87:\"C:\\xampp\\htdocs\\coursemodule\\wp-content\\plugins\\wphave-admin\\assets\\css\\less\\pages.less\";i:1572360033;s:89:\"C:\\xampp\\htdocs\\coursemodule\\wp-content\\plugins\\wphave-admin\\assets\\css\\less\\plugins.less\";i:1582365854;s:94:\"C:\\xampp\\htdocs\\coursemodule\\wp-content\\plugins\\wphave-admin\\assets\\css\\less\\block-editor.less\";i:1581949172;}s:7:\"updated\";i:1587838600;}}s:18:\"wphave-admin-login\";a:4:{s:4:\"vars\";a:14:{s:8:\"themeurl\";s:62:\"~\"http://localhost/coursemodule/wp-content/themes/videoCourse\"\";s:7:\"lessurl\";s:80:\"~\"http://localhost/coursemodule/wp-content/plugins/wphave-admin/assets/css/less\"\";s:14:\"wpatThemeColor\";s:7:\"#4777CD\";s:22:\"wpatGradientStartColor\";s:13:\"rgb(84,92,99)\";s:20:\"wpatGradientEndColor\";s:13:\"rgb(50,55,60)\";s:16:\"wpatToolbarColor\";s:7:\"#32373c\";s:19:\"wpatSpacingMaxWidth\";s:6:\"2000px\";s:17:\"wpatMenuLeftWidth\";s:5:\"160px\";s:21:\"wpatMenuLeftWidthDiff\";s:5:\"120px\";s:17:\"wpatLoginLogoSize\";s:5:\"250px\";s:15:\"wpatToolbarIcon\";s:4:\"none\";s:11:\"wpatWebFont\";s:4:\"none\";s:11:\"wpatLoginBg\";s:4:\"none\";s:13:\"wpatLoginLogo\";s:4:\"none\";}s:3:\"url\";s:85:\"http://localhost/coursemodule/wp-content/uploads/wp-less-cache/wphave-admin-login.css\";s:7:\"version\";s:32:\"f517cf22580ff17923e23bbee6c2a961\";s:4:\"less\";a:4:{s:4:\"root\";s:87:\"C:\\xampp\\htdocs\\coursemodule/wp-content/plugins/wphave-admin/assets/css/less/login.less\";s:8:\"compiled\";s:0:\"\";s:5:\"files\";a:1:{s:87:\"C:\\xampp\\htdocs\\coursemodule\\wp-content\\plugins\\wphave-admin\\assets\\css\\less\\login.less\";i:1573414532;}s:7:\"updated\";i:1587968677;}}}', 'yes'),
(720, 'action_scheduler_hybrid_store_demarkation', '206', 'yes'),
(721, 'schema-ActionScheduler_StoreSchema', '3.0.1587967126', 'yes'),
(722, 'schema-ActionScheduler_LoggerSchema', '2.0.1587967127', 'yes'),
(726, 'woocommerce_store_address', '', 'yes'),
(727, 'woocommerce_store_address_2', '', 'yes'),
(728, 'woocommerce_store_city', '', 'yes'),
(729, 'woocommerce_default_country', 'GB', 'yes'),
(730, 'woocommerce_store_postcode', '', 'yes'),
(731, 'woocommerce_allowed_countries', 'all', 'yes'),
(732, 'woocommerce_all_except_countries', '', 'yes'),
(733, 'woocommerce_specific_allowed_countries', '', 'yes'),
(734, 'woocommerce_ship_to_countries', '', 'yes'),
(735, 'woocommerce_specific_ship_to_countries', '', 'yes'),
(736, 'woocommerce_default_customer_address', 'base', 'yes'),
(737, 'woocommerce_calc_taxes', 'no', 'yes'),
(738, 'woocommerce_enable_coupons', 'yes', 'yes'),
(739, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(740, 'woocommerce_currency', 'GBP', 'yes'),
(741, 'woocommerce_currency_pos', 'left', 'yes'),
(742, 'woocommerce_price_thousand_sep', ',', 'yes'),
(743, 'woocommerce_price_decimal_sep', '.', 'yes'),
(744, 'woocommerce_price_num_decimals', '2', 'yes'),
(745, 'woocommerce_shop_page_id', '236', 'yes'),
(746, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(747, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(748, 'woocommerce_placeholder_image', '206', 'yes'),
(749, 'woocommerce_weight_unit', 'kg', 'yes'),
(750, 'woocommerce_dimension_unit', 'cm', 'yes'),
(751, 'woocommerce_enable_reviews', 'yes', 'yes'),
(752, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(753, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(754, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(755, 'woocommerce_review_rating_required', 'yes', 'no'),
(756, 'woocommerce_manage_stock', 'yes', 'yes'),
(757, 'woocommerce_hold_stock_minutes', '60', 'no'),
(758, 'woocommerce_notify_low_stock', 'yes', 'no'),
(759, 'woocommerce_notify_no_stock', 'yes', 'no'),
(760, 'woocommerce_stock_email_recipient', 'bwanakweli4ever@gmail.com', 'no'),
(761, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(762, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(763, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(764, 'woocommerce_stock_format', '', 'yes'),
(765, 'woocommerce_file_download_method', 'force', 'no'),
(766, 'woocommerce_downloads_require_login', 'no', 'no'),
(767, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(768, 'woocommerce_downloads_add_hash_to_filename', 'yes', 'yes'),
(769, 'woocommerce_prices_include_tax', 'no', 'yes'),
(770, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(771, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(772, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(773, 'woocommerce_tax_classes', '', 'yes'),
(774, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(775, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(776, 'woocommerce_price_display_suffix', '', 'yes'),
(777, 'woocommerce_tax_total_display', 'itemized', 'no'),
(778, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(779, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(780, 'woocommerce_ship_to_destination', 'billing', 'no'),
(781, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(782, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(783, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(784, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(785, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(786, 'woocommerce_registration_generate_username', 'yes', 'no'),
(787, 'woocommerce_registration_generate_password', 'yes', 'no'),
(788, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(789, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(790, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(791, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(792, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(793, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(794, 'woocommerce_trash_pending_orders', '', 'no'),
(795, 'woocommerce_trash_failed_orders', '', 'no'),
(796, 'woocommerce_trash_cancelled_orders', '', 'no'),
(797, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(798, 'woocommerce_email_from_name', 'Video Course Module', 'no'),
(799, 'woocommerce_email_from_address', 'bwanakweli4ever@gmail.com', 'no'),
(800, 'woocommerce_email_header_image', '', 'no'),
(801, 'woocommerce_email_footer_text', '{site_title} &mdash; Built with {WooCommerce}', 'no'),
(802, 'woocommerce_email_base_color', '#96588a', 'no'),
(803, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(804, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(805, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(806, 'woocommerce_cart_page_id', '237', 'no'),
(807, 'woocommerce_checkout_page_id', '238', 'no'),
(808, 'woocommerce_myaccount_page_id', '239', 'no'),
(809, 'woocommerce_terms_page_id', '', 'no'),
(810, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(811, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(812, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(813, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(814, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(815, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(816, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(817, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(818, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(819, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(820, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(821, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(822, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(823, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(824, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(825, 'woocommerce_api_enabled', 'no', 'yes'),
(826, 'woocommerce_allow_tracking', 'no', 'no'),
(827, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(828, 'woocommerce_single_image_width', '600', 'yes'),
(829, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(830, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(831, 'woocommerce_demo_store', 'no', 'no'),
(832, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(833, 'current_theme_supports_woocommerce', 'no', 'yes'),
(834, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(835, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(836, 'product_cat_children', 'a:0:{}', 'yes'),
(837, 'default_product_cat', '28', 'yes'),
(839, 'woocommerce_admin_notices', 'a:2:{i:0;s:7:\"install\";i:1;s:20:\"no_secure_connection\";}', 'yes'),
(842, 'woocommerce_version', '4.0.1', 'yes'),
(843, 'woocommerce_db_version', '4.0.1', 'yes'),
(844, 'action_scheduler_lock_async-request-runner', '1589278427', 'yes'),
(845, 'woocommerce_maxmind_geolocation_settings', 'a:1:{s:15:\"database_prefix\";s:32:\"Ms8DubF7pW82tWi1E4q4Jf9tM3I32nXI\";}', 'yes'),
(846, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(847, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(848, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(849, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(850, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(851, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(852, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(853, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(854, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(855, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(856, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(857, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(858, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(859, 'woocommerce_onboarding_opt_in', 'yes', 'yes'),
(862, 'woocommerce_admin_version', '1.0.3', 'yes'),
(863, 'woocommerce_admin_install_timestamp', '1587967150', 'yes'),
(867, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(868, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(870, 'woocommerce_admin_last_orders_milestone', '0', 'yes'),
(871, 'woocommerce_onboarding_profile', 'a:2:{s:9:\"completed\";b:0;s:7:\"plugins\";s:7:\"skipped\";}', 'yes'),
(872, 'woocommerce_setup_ab_wc_admin_onboarding', 'b', 'yes'),
(878, '_transient_timeout_wc_low_stock_count', '1590559202', 'no'),
(879, '_transient_wc_low_stock_count', '0', 'no'),
(880, '_transient_timeout_wc_outofstock_count', '1590559202', 'no'),
(881, '_transient_wc_outofstock_count', '0', 'no'),
(949, '_transient_product_query-transient-version', '1588544666', 'yes'),
(1350, 'real_utils-transients', '{\"RCL\":{\"raa\":true,\"nr\":1590936633},\"cross\":{\"ncs\":1588949433}}', 'no'),
(1351, 'rcl_db_version', '3.2.7', 'yes'),
(1362, 'wpls_license_real-category-library', 'nulled', 'no'),
(1363, 'wpls_activation_id_real-category-library', 'nulled', 'no'),
(1365, 'external_updates-real-category-library', 'O:8:\"stdClass\":5:{s:9:\"lastCheck\";i:1589278358;s:14:\"checkedVersion\";s:5:\"3.2.7\";s:6:\"update\";O:8:\"stdClass\":10:{s:4:\"slug\";s:21:\"real-category-library\";s:7:\"version\";s:6:\"3.2.17\";s:12:\"download_url\";N;s:12:\"translations\";a:0:{}s:2:\"id\";i:0;s:8:\"homepage\";s:103:\"https://codecanyon.net/item/wordpress-real-category-management-custom-category-order-tree-view/13580393\";s:6:\"tested\";s:3:\"5.4\";s:14:\"upgrade_notice\";N;s:5:\"icons\";a:0:{}s:8:\"filename\";s:31:\"real-category-library/index.php\";}s:11:\"updateClass\";s:22:\"Puc_v4p4_Plugin_Update\";s:15:\"updateBaseClass\";s:13:\"Plugin_Update\";}', 'no'),
(1454, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.4.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.4.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.4.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.4.1\";s:7:\"version\";s:5:\"5.4.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.3\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1589278229;s:15:\"version_checked\";s:5:\"5.4.1\";s:12:\"translations\";a:0:{}}', 'no'),
(1578, 'Task_children', 'a:0:{}', 'yes'),
(2109, 'category_children', 'a:0:{}', 'yes'),
(2128, 'Tasks_children', 'a:0:{}', 'yes'),
(2285, '_transient_woocommerce_reports-transient-version', '1588540252', 'yes'),
(2308, '_transient_shipping-transient-version', '1588542060', 'yes'),
(2309, '_transient_timeout_wc_shipping_method_count_legacy', '1591134060', 'no'),
(2310, '_transient_wc_shipping_method_count_legacy', 'a:2:{s:7:\"version\";s:10:\"1588542060\";s:5:\"value\";i:0;}', 'no'),
(2339, 'rpwcm_options', 'a:7:{s:18:\"rpwcm_redirect_url\";s:0:\"\";s:22:\"rpwcm_restrict_product\";s:1:\"0\";s:23:\"rpwcm_reminders_enabled\";s:1:\"1\";s:20:\"rpwcm_reminders_days\";s:5:\"5,3,2\";s:18:\"rpwcm_admin_access\";s:1:\"1\";s:16:\"rpwcm_block_urls\";a:1:{i:0;a:3:{s:3:\"url\";s:21:\"index.php/course-page\";s:6:\"method\";s:11:\"all_members\";s:5:\"plans\";a:0:{}}}s:19:\"rpwcm_main_site_url\";s:40:\"http://localho%%%RPWCM%%%st/coursemodule\";}', 'yes'),
(2342, 'rightpress_up_nag_2_2_4_woocommerce_membership', '1', 'no'),
(2426, 'action_scheduler_migration_status', 'complete', 'yes'),
(2845, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1589278315;s:7:\"checked\";a:5:{s:14:\"twentynineteen\";s:3:\"1.4\";s:15:\"twentyseventeen\";s:3:\"2.2\";s:13:\"twentysixteen\";s:3:\"2.0\";s:12:\"twentytwenty\";s:3:\"1.2\";s:11:\"videoCourse\";s:3:\"1.0\";}s:8:\"response\";a:3:{s:14:\"twentynineteen\";a:6:{s:5:\"theme\";s:14:\"twentynineteen\";s:11:\"new_version\";s:3:\"1.5\";s:3:\"url\";s:44:\"https://wordpress.org/themes/twentynineteen/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/theme/twentynineteen.1.5.zip\";s:8:\"requires\";s:5:\"4.9.6\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:15:\"twentyseventeen\";a:6:{s:5:\"theme\";s:15:\"twentyseventeen\";s:11:\"new_version\";s:3:\"2.3\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentyseventeen/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentyseventeen.2.3.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}s:13:\"twentysixteen\";a:6:{s:5:\"theme\";s:13:\"twentysixteen\";s:11:\"new_version\";s:3:\"2.1\";s:3:\"url\";s:43:\"https://wordpress.org/themes/twentysixteen/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/theme/twentysixteen.2.1.zip\";s:8:\"requires\";s:3:\"4.4\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}', 'no'),
(2846, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1589278270;s:7:\"checked\";a:9:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.8.9\";s:19:\"akismet/akismet.php\";s:5:\"4.1.2\";s:9:\"hello.php\";s:5:\"1.7.2\";s:45:\"simple-page-ordering/simple-page-ordering.php\";s:5:\"2.3.4\";s:45:\"thrive-visual-editor/thrive-visual-editor.php\";s:7:\"2.4.6.2\";s:27:\"woocommerce/woocommerce.php\";s:5:\"4.0.1\";s:49:\"woocommerce-membership/woocommerce-membership.php\";s:5:\"2.2.4\";s:29:\"wphave-admin/wphave-admin.php\";s:3:\"2.1\";s:31:\"real-category-library/index.php\";s:5:\"3.2.7\";}s:8:\"response\";a:4:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.5\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.4.1\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"4.1.0\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.4.1.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2075035\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2075035\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2075035\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2075035\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.4.1\";s:12:\"requires_php\";s:3:\"7.0\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:36:\"real-category-library-lite/index.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:40:\"w.org/plugins/real-category-library-lite\";s:4:\"slug\";s:26:\"real-category-library-lite\";s:6:\"plugin\";s:36:\"real-category-library-lite/index.php\";s:11:\"new_version\";s:6:\"3.2.16\";s:3:\"url\";s:57:\"https://wordpress.org/plugins/real-category-library-lite/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/real-category-library-lite.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:79:\"https://ps.w.org/real-category-library-lite/assets/icon-128x128.png?rev=2168037\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:82:\"https://ps.w.org/real-category-library-lite/assets/banner-1544x500.png?rev=2168037\";s:2:\"1x\";s:81:\"https://ps.w.org/real-category-library-lite/assets/banner-772x250.png?rev=2168037\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.4.1\";s:12:\"requires_php\";s:5:\"7.0.0\";s:13:\"compatibility\";O:8:\"stdClass\":0:{}}s:45:\"thrive-visual-editor/thrive-visual-editor.php\";O:8:\"stdClass\":8:{s:2:\"id\";i:0;s:4:\"slug\";s:20:\"thrive-visual-editor\";s:11:\"new_version\";s:7:\"2.5.3.3\";s:3:\"url\";s:0:\"\";s:7:\"package\";s:61:\"http://download.thrivethemes.com/thrive-architect-2.5.3.3.zip\";s:6:\"tested\";s:5:\"5.4.0\";s:5:\"icons\";a:1:{s:2:\"1x\";s:113:\"http://localhost/coursemodule/wp-content/plugins/thrive-visual-editor/editor/css/images/thrive-architect-logo.png\";}s:6:\"plugin\";s:45:\"thrive-visual-editor/thrive-visual-editor.php\";}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.8.9\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.8.9.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}s:45:\"simple-page-ordering/simple-page-ordering.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:34:\"w.org/plugins/simple-page-ordering\";s:4:\"slug\";s:20:\"simple-page-ordering\";s:6:\"plugin\";s:45:\"simple-page-ordering/simple-page-ordering.php\";s:11:\"new_version\";s:5:\"2.3.4\";s:3:\"url\";s:51:\"https://wordpress.org/plugins/simple-page-ordering/\";s:7:\"package\";s:69:\"https://downloads.wordpress.org/plugin/simple-page-ordering.2.3.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/simple-page-ordering/assets/icon-256x256.png?rev=971776\";s:2:\"1x\";s:72:\"https://ps.w.org/simple-page-ordering/assets/icon-128x128.png?rev=971776\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:76:\"https://ps.w.org/simple-page-ordering/assets/banner-1544x500.png?rev=1404285\";s:2:\"1x\";s:74:\"https://ps.w.org/simple-page-ordering/assets/banner-772x250.png?rev=971780\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(2857, '_site_transient_timeout_php_check_472f71d2a071d463a95f84346288dc89', '1589739550', 'no'),
(2858, '_site_transient_php_check_472f71d2a071d463a95f84346288dc89', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(2862, '_transient_timeout__woocommerce_helper_subscriptions', '1589279174', 'no'),
(2863, '_transient__woocommerce_helper_subscriptions', 'a:0:{}', 'no'),
(2864, '_site_transient_timeout_theme_roots', '1589280074', 'no'),
(2865, '_site_transient_theme_roots', 'a:5:{s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";s:12:\"twentytwenty\";s:7:\"/themes\";s:11:\"videoCourse\";s:7:\"/themes\";}', 'no'),
(2866, '_transient_timeout__woocommerce_helper_updates', '1589321475', 'no'),
(2867, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1589278275;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_edit_lock', '1586607318:1'),
(4, 6, 'tve_revision_tve_landing_page', ''),
(5, 6, 'tve_revision_tve_disable_theme_dependency', ''),
(6, 6, 'tve_revision_tve_content_before_more', ''),
(7, 6, 'tve_revision_tve_content_more_found', ''),
(8, 6, 'tve_revision_tve_save_post', ''),
(9, 6, 'tve_revision_tve_custom_css', ''),
(10, 6, 'tve_revision_tve_user_custom_css', ''),
(11, 6, 'tve_revision_tve_page_events', ''),
(12, 6, 'tve_revision_tve_globals', ''),
(13, 6, 'tve_revision_tve_global_scripts', ''),
(14, 6, 'tve_revision_thrive_icon_pack', ''),
(15, 6, 'tve_revision_thrive_tcb_post_fonts', ''),
(16, 6, 'tve_revision_tve_has_masonry', ''),
(17, 6, 'tve_revision_tve_has_typefocus', ''),
(18, 6, 'tve_revision_tve_updated_post', ''),
(19, 6, 'tve_revision_tve_has_wistia_popover', ''),
(36, 5, '_wp_page_template', ''),
(37, 2, 'tcb2_ready', '1'),
(38, 5, 'tcb2_ready', '1'),
(50, 11, '_menu_item_type', 'custom'),
(51, 11, '_menu_item_menu_item_parent', '0'),
(52, 11, '_menu_item_object_id', '11'),
(53, 11, '_menu_item_object', 'custom'),
(54, 11, '_menu_item_target', ''),
(55, 11, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(56, 11, '_menu_item_xfn', ''),
(57, 11, '_menu_item_url', '#'),
(66, 13, '_menu_item_type', 'custom'),
(67, 13, '_menu_item_menu_item_parent', '0'),
(68, 13, '_menu_item_object_id', '13'),
(69, 13, '_menu_item_object', 'custom'),
(70, 13, '_menu_item_target', ''),
(71, 13, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(72, 13, '_menu_item_xfn', ''),
(73, 13, '_menu_item_url', '#'),
(84, 18, '_wp_attached_file', '2020/04/Roland-logo.png'),
(85, 18, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:101;s:6:\"height\";i:23;s:4:\"file\";s:23:\"2020/04/Roland-logo.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(89, 21, '_wp_attached_file', '2020/04/client-2.png'),
(90, 21, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:185;s:6:\"height\";i:37;s:4:\"file\";s:20:\"2020/04/client-2.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"client-2-150x37.png\";s:5:\"width\";i:150;s:6:\"height\";i:37;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(97, 5, 'tcb_editor_enabled', '1'),
(98, 24, '_edit_lock', '1587039106:1'),
(99, 25, 'tve_revision_tve_landing_page', ''),
(100, 25, 'tve_revision_tve_disable_theme_dependency', ''),
(101, 25, 'tve_revision_tve_content_before_more', ''),
(102, 25, 'tve_revision_tve_content_more_found', ''),
(103, 25, 'tve_revision_tve_save_post', ''),
(104, 25, 'tve_revision_tve_custom_css', ''),
(105, 25, 'tve_revision_tve_user_custom_css', ''),
(106, 25, 'tve_revision_tve_page_events', ''),
(107, 25, 'tve_revision_tve_globals', ''),
(108, 25, 'tve_revision_tve_global_scripts', ''),
(109, 25, 'tve_revision_thrive_icon_pack', ''),
(110, 25, 'tve_revision_thrive_tcb_post_fonts', ''),
(111, 25, 'tve_revision_tve_has_masonry', ''),
(112, 25, 'tve_revision_tve_has_typefocus', ''),
(113, 25, 'tve_revision_tve_updated_post', ''),
(114, 25, 'tve_revision_tve_has_wistia_popover', ''),
(116, 27, '_menu_item_type', 'custom'),
(117, 27, '_menu_item_menu_item_parent', '0'),
(118, 27, '_menu_item_object_id', '27'),
(119, 27, '_menu_item_object', 'custom'),
(120, 27, '_menu_item_target', ''),
(121, 27, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(122, 27, '_menu_item_xfn', ''),
(123, 27, '_menu_item_url', '#'),
(125, 28, '_menu_item_type', 'custom'),
(126, 28, '_menu_item_menu_item_parent', '0'),
(127, 28, '_menu_item_object_id', '28'),
(128, 28, '_menu_item_object', 'custom'),
(129, 28, '_menu_item_target', ''),
(130, 28, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(131, 28, '_menu_item_xfn', ''),
(132, 28, '_menu_item_url', '#'),
(134, 29, '_menu_item_type', 'custom'),
(135, 29, '_menu_item_menu_item_parent', '0'),
(136, 29, '_menu_item_object_id', '29'),
(137, 29, '_menu_item_object', 'custom'),
(138, 29, '_menu_item_target', ''),
(139, 29, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(140, 29, '_menu_item_xfn', ''),
(141, 29, '_menu_item_url', '#'),
(143, 30, '_edit_lock', '1587002891:1'),
(144, 31, 'tve_revision_tve_landing_page', ''),
(145, 31, 'tve_revision_tve_disable_theme_dependency', ''),
(146, 31, 'tve_revision_tve_content_before_more', ''),
(147, 31, 'tve_revision_tve_content_more_found', ''),
(148, 31, 'tve_revision_tve_save_post', ''),
(149, 31, 'tve_revision_tve_custom_css', ''),
(150, 31, 'tve_revision_tve_user_custom_css', ''),
(151, 31, 'tve_revision_tve_page_events', ''),
(152, 31, 'tve_revision_tve_globals', ''),
(153, 31, 'tve_revision_tve_global_scripts', ''),
(154, 31, 'tve_revision_thrive_icon_pack', ''),
(155, 31, 'tve_revision_thrive_tcb_post_fonts', ''),
(156, 31, 'tve_revision_tve_has_masonry', ''),
(157, 31, 'tve_revision_tve_has_typefocus', ''),
(158, 31, 'tve_revision_tve_updated_post', ''),
(159, 31, 'tve_revision_tve_has_wistia_popover', ''),
(160, 32, 'tve_revision_tve_landing_page', ''),
(161, 32, 'tve_revision_tve_disable_theme_dependency', ''),
(162, 32, 'tve_revision_tve_content_before_more', ''),
(163, 32, 'tve_revision_tve_content_more_found', ''),
(164, 32, 'tve_revision_tve_save_post', ''),
(165, 32, 'tve_revision_tve_custom_css', ''),
(166, 32, 'tve_revision_tve_user_custom_css', ''),
(167, 32, 'tve_revision_tve_page_events', ''),
(168, 32, 'tve_revision_tve_globals', ''),
(169, 32, 'tve_revision_tve_global_scripts', ''),
(170, 32, 'tve_revision_thrive_icon_pack', ''),
(171, 32, 'tve_revision_thrive_tcb_post_fonts', ''),
(172, 32, 'tve_revision_tve_has_masonry', ''),
(173, 32, 'tve_revision_tve_has_typefocus', ''),
(174, 32, 'tve_revision_tve_updated_post', ''),
(175, 32, 'tve_revision_tve_has_wistia_popover', ''),
(176, 33, 'tve_revision_tve_landing_page', ''),
(177, 33, 'tve_revision_tve_disable_theme_dependency', ''),
(178, 33, 'tve_revision_tve_content_before_more', ''),
(179, 33, 'tve_revision_tve_content_more_found', ''),
(180, 33, 'tve_revision_tve_save_post', ''),
(181, 33, 'tve_revision_tve_custom_css', ''),
(182, 33, 'tve_revision_tve_user_custom_css', ''),
(183, 33, 'tve_revision_tve_page_events', ''),
(184, 33, 'tve_revision_tve_globals', ''),
(185, 33, 'tve_revision_tve_global_scripts', ''),
(186, 33, 'tve_revision_thrive_icon_pack', ''),
(187, 33, 'tve_revision_thrive_tcb_post_fonts', ''),
(188, 33, 'tve_revision_tve_has_masonry', ''),
(189, 33, 'tve_revision_tve_has_typefocus', ''),
(190, 33, 'tve_revision_tve_updated_post', ''),
(191, 33, 'tve_revision_tve_has_wistia_popover', ''),
(224, 36, 'tve_revision_tve_landing_page', ''),
(225, 36, 'tve_revision_tve_disable_theme_dependency', ''),
(226, 36, 'tve_revision_tve_content_before_more', ''),
(227, 36, 'tve_revision_tve_content_more_found', ''),
(228, 36, 'tve_revision_tve_save_post', ''),
(229, 36, 'tve_revision_tve_custom_css', ''),
(230, 36, 'tve_revision_tve_user_custom_css', ''),
(231, 36, 'tve_revision_tve_page_events', ''),
(232, 36, 'tve_revision_tve_globals', ''),
(233, 36, 'tve_revision_tve_global_scripts', ''),
(234, 36, 'tve_revision_thrive_icon_pack', ''),
(235, 36, 'tve_revision_thrive_tcb_post_fonts', ''),
(236, 36, 'tve_revision_tve_has_masonry', ''),
(237, 36, 'tve_revision_tve_has_typefocus', ''),
(238, 36, 'tve_revision_tve_updated_post', ''),
(239, 36, 'tve_revision_tve_has_wistia_popover', ''),
(240, 37, '_edit_lock', '1586623780:1'),
(242, 38, 'tve_revision_tve_landing_page', ''),
(243, 38, 'tve_revision_tve_disable_theme_dependency', ''),
(244, 38, 'tve_revision_tve_content_before_more', ''),
(245, 38, 'tve_revision_tve_content_more_found', ''),
(246, 38, 'tve_revision_tve_save_post', ''),
(247, 38, 'tve_revision_tve_custom_css', ''),
(248, 38, 'tve_revision_tve_user_custom_css', ''),
(249, 38, 'tve_revision_tve_page_events', ''),
(250, 38, 'tve_revision_tve_globals', ''),
(251, 38, 'tve_revision_tve_global_scripts', ''),
(252, 38, 'tve_revision_thrive_icon_pack', ''),
(253, 38, 'tve_revision_thrive_tcb_post_fonts', ''),
(254, 38, 'tve_revision_tve_has_masonry', ''),
(255, 38, 'tve_revision_tve_has_typefocus', ''),
(256, 38, 'tve_revision_tve_updated_post', ''),
(257, 38, 'tve_revision_tve_has_wistia_popover', ''),
(258, 37, '_encloseme', '1'),
(259, 39, 'tve_revision_tve_landing_page', ''),
(260, 39, 'tve_revision_tve_disable_theme_dependency', ''),
(261, 39, 'tve_revision_tve_content_before_more', ''),
(262, 39, 'tve_revision_tve_content_more_found', ''),
(263, 39, 'tve_revision_tve_save_post', ''),
(264, 39, 'tve_revision_tve_custom_css', ''),
(265, 39, 'tve_revision_tve_user_custom_css', ''),
(266, 39, 'tve_revision_tve_page_events', ''),
(267, 39, 'tve_revision_tve_globals', ''),
(268, 39, 'tve_revision_tve_global_scripts', ''),
(269, 39, 'tve_revision_thrive_icon_pack', ''),
(270, 39, 'tve_revision_thrive_tcb_post_fonts', ''),
(271, 39, 'tve_revision_tve_has_masonry', ''),
(272, 39, 'tve_revision_tve_has_typefocus', ''),
(273, 39, 'tve_revision_tve_updated_post', ''),
(274, 39, 'tve_revision_tve_has_wistia_popover', ''),
(275, 30, 'tcb2_ready', '1'),
(276, 40, 'tve_revision_tve_landing_page', ''),
(277, 40, 'tve_revision_tve_disable_theme_dependency', ''),
(278, 40, 'tve_revision_tve_content_before_more', ''),
(279, 40, 'tve_revision_tve_content_more_found', ''),
(280, 40, 'tve_revision_tve_save_post', ''),
(281, 40, 'tve_revision_tve_custom_css', ''),
(282, 40, 'tve_revision_tve_user_custom_css', ''),
(283, 40, 'tve_revision_tve_page_events', ''),
(284, 40, 'tve_revision_tve_globals', ''),
(285, 40, 'tve_revision_tve_global_scripts', ''),
(286, 40, 'tve_revision_thrive_icon_pack', ''),
(287, 40, 'tve_revision_thrive_tcb_post_fonts', ''),
(288, 40, 'tve_revision_tve_has_masonry', ''),
(289, 40, 'tve_revision_tve_has_typefocus', ''),
(290, 40, 'tve_revision_tve_updated_post', ''),
(291, 40, 'tve_revision_tve_has_wistia_popover', ''),
(309, 30, '_wp_page_template', ''),
(310, 42, 'tve_revision_tve_landing_page', ''),
(311, 42, 'tve_revision_tve_disable_theme_dependency', ''),
(312, 42, 'tve_revision_tve_content_before_more', ''),
(313, 42, 'tve_revision_tve_content_more_found', ''),
(314, 42, 'tve_revision_tve_save_post', ''),
(315, 42, 'tve_revision_tve_custom_css', ''),
(316, 42, 'tve_revision_tve_user_custom_css', ''),
(317, 42, 'tve_revision_tve_page_events', ''),
(318, 42, 'tve_revision_tve_globals', ''),
(319, 42, 'tve_revision_tve_global_scripts', ''),
(320, 42, 'tve_revision_thrive_icon_pack', ''),
(321, 42, 'tve_revision_thrive_tcb_post_fonts', ''),
(322, 42, 'tve_revision_tve_has_masonry', ''),
(323, 42, 'tve_revision_tve_has_typefocus', ''),
(324, 42, 'tve_revision_tve_updated_post', ''),
(325, 42, 'tve_revision_tve_has_wistia_popover', ''),
(328, 43, '_wp_attached_file', '2020/04/mrsms-banner-99051406db04513c.png'),
(329, 43, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:574;s:6:\"height\";i:274;s:4:\"file\";s:41:\"2020/04/mrsms-banner-99051406db04513c.png\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:41:\"mrsms-banner-99051406db04513c-300x143.png\";s:5:\"width\";i:300;s:6:\"height\";i:143;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:41:\"mrsms-banner-99051406db04513c-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(330, 44, 'tve_revision_tve_landing_page', ''),
(331, 44, 'tve_revision_tve_disable_theme_dependency', ''),
(332, 44, 'tve_revision_tve_content_before_more', ''),
(333, 44, 'tve_revision_tve_content_more_found', ''),
(334, 44, 'tve_revision_tve_save_post', ''),
(335, 44, 'tve_revision_tve_custom_css', ''),
(336, 44, 'tve_revision_tve_user_custom_css', ''),
(337, 44, 'tve_revision_tve_page_events', ''),
(338, 44, 'tve_revision_tve_globals', ''),
(339, 44, 'tve_revision_tve_global_scripts', ''),
(340, 44, 'tve_revision_thrive_icon_pack', ''),
(341, 44, 'tve_revision_thrive_tcb_post_fonts', ''),
(342, 44, 'tve_revision_tve_has_masonry', ''),
(343, 44, 'tve_revision_tve_has_typefocus', ''),
(344, 44, 'tve_revision_tve_updated_post', ''),
(345, 44, 'tve_revision_tve_has_wistia_popover', ''),
(378, 46, 'tve_revision_tve_landing_page', ''),
(379, 46, 'tve_revision_tve_disable_theme_dependency', ''),
(380, 46, 'tve_revision_tve_content_before_more', ''),
(381, 46, 'tve_revision_tve_content_more_found', ''),
(382, 46, 'tve_revision_tve_save_post', ''),
(383, 46, 'tve_revision_tve_custom_css', ''),
(384, 46, 'tve_revision_tve_user_custom_css', ''),
(385, 46, 'tve_revision_tve_page_events', ''),
(386, 46, 'tve_revision_tve_globals', ''),
(387, 46, 'tve_revision_tve_global_scripts', ''),
(388, 46, 'tve_revision_thrive_icon_pack', ''),
(389, 46, 'tve_revision_thrive_tcb_post_fonts', ''),
(390, 46, 'tve_revision_tve_has_masonry', ''),
(391, 46, 'tve_revision_tve_has_typefocus', ''),
(392, 46, 'tve_revision_tve_updated_post', ''),
(393, 46, 'tve_revision_tve_has_wistia_popover', ''),
(395, 47, 'tve_revision_tve_landing_page', ''),
(396, 47, 'tve_revision_tve_disable_theme_dependency', ''),
(397, 47, 'tve_revision_tve_content_before_more', ''),
(398, 47, 'tve_revision_tve_content_more_found', ''),
(399, 47, 'tve_revision_tve_save_post', ''),
(400, 47, 'tve_revision_tve_custom_css', ''),
(401, 47, 'tve_revision_tve_user_custom_css', ''),
(402, 47, 'tve_revision_tve_page_events', ''),
(403, 47, 'tve_revision_tve_globals', ''),
(404, 47, 'tve_revision_tve_global_scripts', ''),
(405, 47, 'tve_revision_thrive_icon_pack', ''),
(406, 47, 'tve_revision_thrive_tcb_post_fonts', ''),
(407, 47, 'tve_revision_tve_has_masonry', ''),
(408, 47, 'tve_revision_tve_has_typefocus', ''),
(409, 47, 'tve_revision_tve_updated_post', ''),
(410, 47, 'tve_revision_tve_has_wistia_popover', ''),
(412, 49, '_edit_lock', '1588543366:1'),
(413, 50, 'tve_revision_tve_landing_page', ''),
(414, 50, 'tve_revision_tve_disable_theme_dependency', ''),
(415, 50, 'tve_revision_tve_content_before_more', ''),
(416, 50, 'tve_revision_tve_content_more_found', ''),
(417, 50, 'tve_revision_tve_save_post', ''),
(418, 50, 'tve_revision_tve_custom_css', ''),
(419, 50, 'tve_revision_tve_user_custom_css', ''),
(420, 50, 'tve_revision_tve_page_events', ''),
(421, 50, 'tve_revision_tve_globals', ''),
(422, 50, 'tve_revision_tve_global_scripts', ''),
(423, 50, 'tve_revision_thrive_icon_pack', ''),
(424, 50, 'tve_revision_thrive_tcb_post_fonts', ''),
(425, 50, 'tve_revision_tve_has_masonry', ''),
(426, 50, 'tve_revision_tve_has_typefocus', ''),
(427, 50, 'tve_revision_tve_updated_post', ''),
(428, 50, 'tve_revision_tve_has_wistia_popover', ''),
(429, 49, '_wp_page_template', 'page-templates/login-template.php'),
(430, 51, '_edit_lock', '1586998460:1'),
(431, 52, 'tve_revision_tve_landing_page', ''),
(432, 52, 'tve_revision_tve_disable_theme_dependency', ''),
(433, 52, 'tve_revision_tve_content_before_more', ''),
(434, 52, 'tve_revision_tve_content_more_found', ''),
(435, 52, 'tve_revision_tve_save_post', ''),
(436, 52, 'tve_revision_tve_custom_css', ''),
(437, 52, 'tve_revision_tve_user_custom_css', ''),
(438, 52, 'tve_revision_tve_page_events', ''),
(439, 52, 'tve_revision_tve_globals', ''),
(440, 52, 'tve_revision_tve_global_scripts', ''),
(441, 52, 'tve_revision_thrive_icon_pack', ''),
(442, 52, 'tve_revision_thrive_tcb_post_fonts', ''),
(443, 52, 'tve_revision_tve_has_masonry', ''),
(444, 52, 'tve_revision_tve_has_typefocus', ''),
(445, 52, 'tve_revision_tve_updated_post', ''),
(446, 52, 'tve_revision_tve_has_wistia_popover', ''),
(447, 53, '_wp_trash_meta_status', 'publish'),
(448, 53, '_wp_trash_meta_time', '1586870299'),
(449, 54, 'tve_revision_tve_landing_page', ''),
(450, 54, 'tve_revision_tve_disable_theme_dependency', ''),
(451, 54, 'tve_revision_tve_content_before_more', ''),
(452, 54, 'tve_revision_tve_content_more_found', ''),
(453, 54, 'tve_revision_tve_save_post', ''),
(454, 54, 'tve_revision_tve_custom_css', ''),
(455, 54, 'tve_revision_tve_user_custom_css', ''),
(456, 54, 'tve_revision_tve_page_events', ''),
(457, 54, 'tve_revision_tve_globals', ''),
(458, 54, 'tve_revision_tve_global_scripts', ''),
(459, 54, 'tve_revision_thrive_icon_pack', ''),
(460, 54, 'tve_revision_thrive_tcb_post_fonts', ''),
(461, 54, 'tve_revision_tve_has_masonry', ''),
(462, 54, 'tve_revision_tve_has_typefocus', ''),
(463, 54, 'tve_revision_tve_updated_post', ''),
(464, 54, 'tve_revision_tve_has_wistia_popover', ''),
(465, 55, 'tve_revision_tve_landing_page', ''),
(466, 55, 'tve_revision_tve_disable_theme_dependency', ''),
(467, 55, 'tve_revision_tve_content_before_more', ''),
(468, 55, 'tve_revision_tve_content_more_found', ''),
(469, 55, 'tve_revision_tve_save_post', ''),
(470, 55, 'tve_revision_tve_custom_css', ''),
(471, 55, 'tve_revision_tve_user_custom_css', ''),
(472, 55, 'tve_revision_tve_page_events', ''),
(473, 55, 'tve_revision_tve_globals', ''),
(474, 55, 'tve_revision_tve_global_scripts', ''),
(475, 55, 'tve_revision_thrive_icon_pack', ''),
(476, 55, 'tve_revision_thrive_tcb_post_fonts', ''),
(477, 55, 'tve_revision_tve_has_masonry', ''),
(478, 55, 'tve_revision_tve_has_typefocus', ''),
(479, 55, 'tve_revision_tve_updated_post', ''),
(480, 55, 'tve_revision_tve_has_wistia_popover', ''),
(482, 49, 'tcb2_ready', '1'),
(483, 56, 'tcb_ct_id', '41678'),
(484, 56, 'tcb_ct_meta', 'a:6:{s:1:\"v\";s:2:\"69\";s:4:\"type\";s:12:\"contentblock\";s:8:\"head_css\";s:7317:\"@media (min-width: 300px){[data-css=\"tve-u-16a20190bbc\"] { padding: 0px !important; margin-bottom: 0px !important; border: none !important; }[data-css=\"tve-u-16a3070f467\"] { padding: 0px !important; margin-bottom: 0px !important; }[data-css=\"tve-u-16a30a99201\"] { border: none !important; }[data-css=\"tve-u-16a30aaa1f6\"] { padding: 0px !important; margin: 0px !important; }[data-css=\"tve-u-16a361f8512\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a361f8513\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a361f8512\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a361f8513\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a361f8512\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a361f8513\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a362044f6\"] { margin: 0px !important; }[data-css=\"tve-u-16a363d57f4\"] { padding: 0px !important; margin-top: 0px !important; margin-bottom: 50px !important; }[data-css=\"tve-u-16a362044f6\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a362044f6\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a362044f6\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a362044f6\"] p { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a48d60c35\"] { padding: 0px !important; }[data-css=\"tve-u-16a48d60c35\"] > .tcb-flex-col > .tcb-col { justify-content: center; }[data-css=\"tve-u-16a48d88c2c\"] { padding-right: 20px !important; }[data-css=\"tve-u-16a48da7a0d\"] { border-left: 1px solid rgb(234, 234, 234) !important; padding-left: 20px !important; padding-right: 20px !important; }[data-css=\"tve-u-16a4eca87b0\"] { max-width: 308px; padding: 0px !important; margin-top: 0px !important; margin-bottom: 0px !important; }[data-css=\"tve-u-16a6d4b031a\"] { max-width: 66.1%; }[data-css=\"tve-u-16a6d4b031d\"] { max-width: 34%; }[data-css=\"tve-u-16a8bbd1432\"] { padding: 0px !important; margin-bottom: 30px !important; }[data-css=\"tve-u-16a9227cf16\"] { border-top: 1px solid rgb(234, 234, 234) !important; border-right: none !important; border-bottom: 1px solid rgb(234, 234, 234) !important; border-left: none !important; border-image: initial !important; }[data-css=\"tve-u-16a922820f9\"] { padding: 15px 0px !important; margin: 0px 0px 30px !important; }:not(#tve) [data-css=\"tve-u-16a9232f0da\"] .tcb-button-link { font-size: 15px !important; }[data-css=\"tve-u-16a9232f0da\"] .tcb-button-link { border-radius: 100px; overflow: hidden; border: 1px solid var(--tcb-local-color-a35c5,rgb(241, 241, 241)); padding: 10px 30px !important; background-color: transparent !important; background-image: none !important; }[data-css=\"tve-u-16a9232f0da\"] { float: none; z-index: 3; position: relative; margin: 0px auto 10px !important; }:not(#tve) [data-css=\"tve-u-16a9232f0da\"] .tcb-button-link span { color: rgb(51, 51, 51); }:not(#tve) [data-css=\"tve-u-16a9232f0da\"]:hover { margin-bottom: 10px !important; }:not(#tve) [data-css=\"tve-u-16a9232f0da\"]:hover .tcb-button-link { border: 1px solid var(--tcb-local-color-5f5e3,rgb(57, 164, 210))  !important; background-image: none !important; background-color: rgba(21, 162, 136, 0) !important; }:not(#tve) [data-css=\"tve-u-16a9232f0da\"]:hover .tcb-button-link span { color: var(--tcb-local-color-5f5e3,rgb(57, 164, 210))  !important; }:not(#tve) [data-css=\"tve-u-16a923a5462\"] .tcb-button-link { font-size: 15px !important; }[data-css=\"tve-u-16a923a5462\"] .tcb-button-link { border-radius: 100px; overflow: hidden; border: none; padding: 10px 30px !important; background-color: transparent !important; background-image: linear-gradient(var(--tcb-local-color-a35c5,rgb(241, 241, 241)), var(--tcb-local-color-a35c5,rgb(241, 241, 241)))  !important; background-size: auto !important; background-position: 50% 50% !important; background-attachment: scroll !important; background-repeat: no-repeat !important; }[data-css=\"tve-u-16a923a5462\"] { float: none; z-index: 3; position: relative; margin: 0px auto 10px !important; }:not(#tve) [data-css=\"tve-u-16a923a5462\"] .tcb-button-link span { color: rgb(51, 51, 51); }:not(#tve) [data-css=\"tve-u-16a923a5462\"]:hover { margin-bottom: 10px !important; }:not(#tve) [data-css=\"tve-u-16a923a5462\"]:hover .tcb-button-link { border: none !important; background-image: none !important; background-color: rgb(57, 164, 210) !important; }:not(#tve) [data-css=\"tve-u-16a923a5462\"]:hover .tcb-button-link span { color: rgb(255, 255, 255) !important; }}@media (max-width: 1023px){[data-css=\"tve-u-16a20190bbc\"] { margin-bottom: 0px !important; }[data-css=\"tve-u-16a3070f467\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-16a363d57f4\"] { margin-top: 0px !important; margin-bottom: 50px !important; }[data-css=\"tve-u-16a48d60c35\"] { flex-wrap: wrap !important; }[data-css=\"tve-u-16a48d60c35\"] .tcb-flex-col { flex-basis: 400px !important; }[data-css=\"tve-u-16a48d88c2c\"] { padding-right: 0px !important; padding-left: 0px !important; padding-top: 0px !important; border-top: none !important; }[data-css=\"tve-u-16a48da7a0d\"] { padding-right: 0px !important; padding-left: 0px !important; padding-top: 25px !important; border-left: none !important; border-top: 1px solid rgb(234, 234, 234) !important; }[data-css=\"tve-u-16a4eca87b0\"] { max-width: 308px; }[data-css=\"tve-u-16a9232f0da\"] { float: left; z-index: 3; position: relative; margin-right: 9px !important; margin-bottom: 10px !important; }[data-css=\"tve-u-16a923a5462\"] { float: right; z-index: 3; position: relative; margin-right: 0px !important; margin-bottom: 0px !important; }}@media (max-width: 767px){[data-css=\"tve-u-16a20190bbc\"] { margin-bottom: 0px !important; }[data-css=\"tve-u-16a3070f467\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-16a361f8512\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a361f8513\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a361f8512\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a361f8513\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-16a363d57f4\"] { padding: 0px !important; margin-top: 0px !important; margin-bottom: 40px !important; }[data-css=\"tve-u-16a48da7a0d\"] { padding: 20px 0px 0px !important; border-left: none !important; border-top: 1px solid rgb(234, 234, 234) !important; }[data-css=\"tve-u-16a48d60c35\"] .tcb-flex-col { flex-basis: 400px !important; }[data-css=\"tve-u-16a4eca87b0\"] { max-width: 308px; }:not(#tve) [data-css=\"tve-u-16a4f32bcfc\"] { font-size: 22px !important; }[data-css=\"tve-u-16a922820f9\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-16a9232f0da\"] .tcb-button-link { padding-left: 25px !important; padding-right: 25px !important; }[data-css=\"tve-u-16a923a5462\"] .tcb-button-link { border: none; padding-left: 25px !important; padding-right: 25px !important; }[data-css=\"tve-u-16a923a5462\"] { float: left; z-index: 3; position: relative; margin-bottom: 0px !important; }[data-css=\"tve-u-16a924b3d0b\"]::after { clear: both; }[data-css=\"tve-u-16a9232f0da\"] { margin-bottom: 10px !important; }}[data-css=\"tve-u-105e613813c\"]{--tcb-local-color-a35c5: rgb(241, 241, 241);--tcb-local-color-5f5e3: rgb(57, 164, 210);}\";s:10:\"custom_css\";s:0:\"\";s:6:\"config\";a:0:{}s:4:\"pack\";s:3:\"137\";}'),
(485, 49, 'tve_content_before_more', '<div class=\"thrv-content-block tcb-local-vars-root thrv_wrapper\" data-css=\"tve-u-1717960edf9\" tcb-template-name=\"Resource List 05\" tcb-template-id=\"41678\" tcb-template-pack=\"137\" data-keep-css_id=\"1\"><div class=\"thrive-group-edit-config\" style=\"display: none !important\">__CONFIG_group_edit__{\"juo8pmn3\":{\"name\":\"All Paragraph(s)\",\"singular\":\"-- Text %s\"},\"jvdr7vrx\":{\"name\":\"All Title(s)\",\"singular\":\"-- Text %s\"},\"jvdrfoj4\":{\"name\":\"All Read Review Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrg8bl\":{\"name\":\"All Visit Website Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrgsih\":{\"name\":\"All Buttons Content Box(s)\",\"singular\":\"-- Content Box %s\"},\"jvdrh1la\":{\"name\":\"All Paragraphs Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrha52\":{\"name\":\"All Buttons Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrhpi3\":{\"name\":\"All Columns(s)\",\"singular\":\"-- Columns %s\"},\"jvdrhrvz\":{\"name\":\"All Content Box(s)\",\"singular\":\"-- Content Box %s\"}}__CONFIG_group_edit__</div><div class=\"thrive-local-colors-config\" style=\"display: none !important\">__CONFIG_local_colors__{\"colors\":{\"a35c5\":\"Button\",\"5f5e3\":\"Button Hover\"},\"gradients\":{}}__CONFIG_local_colors__</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfa\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960edfb\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfc\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960edfd\"></div>\n	<div class=\"tve-cb\"><div class=\"thrv_wrapper thrv_text_element\" data-css=\"tve-u-1717960edfe\"><h2 class=\"\" data-css=\"tve-u-1717960edff\">Recommended Resources</h2></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div></div>\n</div></div>'),
(486, 49, 'tve_content_more_found', ''),
(487, 49, 'tve_custom_css', '@media (min-width: 300px){[data-css=\"tve-u-1717960edfe\"] { padding: 0px !important; margin-bottom: 0px !important; border: none !important; }[data-css=\"tve-u-1717960ee08\"] { padding: 0px !important; margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee01\"] { border: none !important; }[data-css=\"tve-u-1717960edfa\"] { padding: 0px !important; margin: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] { margin: 0px !important; }[data-css=\"tve-u-1717960ee00\"] { padding: 0px !important; margin-top: 0px !important; margin-bottom: 50px !important; }[data-css=\"tve-u-1717960ee03\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] p { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee04\"] { padding: 0px !important; }[data-css=\"tve-u-1717960ee04\"] > .tcb-flex-col > .tcb-col { justify-content: center; }[data-css=\"tve-u-1717960ee06\"] { padding-right: 20px !important; }[data-css=\"tve-u-1717960ee0a\"] { border-left: 1px solid rgb(234, 234, 234) !important; padding-left: 20px !important; padding-right: 20px !important; }[data-css=\"tve-u-1717960ee0b\"] { max-width: 308px; padding: 0px !important; margin-top: 0px !important; margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee05\"] { max-width: 66.1%; }[data-css=\"tve-u-1717960ee09\"] { max-width: 34%; }[data-css=\"tve-u-1717960ee07\"] { padding: 0px !important; margin-bottom: 30px !important; }[data-css=\"tve-u-1717960edfd\"] { border-top: 1px solid rgb(234, 234, 234) !important; border-right: none !important; border-bottom: 1px solid rgb(234, 234, 234) !important; border-left: none !important; border-image: initial !important; }[data-css=\"tve-u-1717960edfc\"] { padding: 15px 0px !important; margin: 0px 0px 30px !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"] .tcb-button-link { font-size: 15px !important; }[data-css=\"tve-u-1717960ee0e\"] .tcb-button-link { border-radius: 100px; overflow: hidden; border: 1px solid var(--tcb-local-color-a35c5,rgb(241, 241, 241)); padding: 10px 30px !important; background-color: transparent !important; background-image: none !important; }[data-css=\"tve-u-1717960ee0e\"] { float: none; z-index: 3; position: relative; margin: 0px auto 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"] .tcb-button-link span { color: rgb(51, 51, 51); }:not(#tve) [data-css=\"tve-u-1717960ee0e\"]:hover { margin-bottom: 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"]:hover .tcb-button-link { border: 1px solid var(--tcb-local-color-5f5e3,rgb(57, 164, 210))  !important; background-image: none !important; background-color: rgba(21, 162, 136, 0) !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"]:hover .tcb-button-link span { color: var(--tcb-local-color-5f5e3,rgb(57, 164, 210))  !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"] .tcb-button-link { font-size: 15px !important; }[data-css=\"tve-u-1717960ee10\"] .tcb-button-link { border-radius: 100px; overflow: hidden; border: none; padding: 10px 30px !important; background-color: transparent !important; background-image: linear-gradient(var(--tcb-local-color-a35c5,rgb(241, 241, 241)), var(--tcb-local-color-a35c5,rgb(241, 241, 241)))  !important; background-size: auto !important; background-position: 50% 50% !important; background-attachment: scroll !important; background-repeat: no-repeat !important; }[data-css=\"tve-u-1717960ee10\"] { float: none; z-index: 3; position: relative; margin: 0px auto 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"] .tcb-button-link span { color: rgb(51, 51, 51); }:not(#tve) [data-css=\"tve-u-1717960ee10\"]:hover { margin-bottom: 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"]:hover .tcb-button-link { border: none !important; background-image: none !important; background-color: rgb(57, 164, 210) !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"]:hover .tcb-button-link span { color: rgb(255, 255, 255) !important; }}@media (max-width: 1023px){[data-css=\"tve-u-1717960edfe\"] { margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee08\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-1717960ee00\"] { margin-top: 0px !important; margin-bottom: 50px !important; }[data-css=\"tve-u-1717960ee04\"] { flex-wrap: wrap !important; }[data-css=\"tve-u-1717960ee04\"] .tcb-flex-col { flex-basis: 400px !important; }[data-css=\"tve-u-1717960ee06\"] { padding-right: 0px !important; padding-left: 0px !important; padding-top: 0px !important; border-top: none !important; }[data-css=\"tve-u-1717960ee0a\"] { padding-right: 0px !important; padding-left: 0px !important; padding-top: 25px !important; border-left: none !important; border-top: 1px solid rgb(234, 234, 234) !important; }[data-css=\"tve-u-1717960ee0b\"] { max-width: 308px; }[data-css=\"tve-u-1717960ee0e\"] { float: left; z-index: 3; position: relative; margin-right: 9px !important; margin-bottom: 10px !important; }[data-css=\"tve-u-1717960ee10\"] { float: right; z-index: 3; position: relative; margin-right: 0px !important; margin-bottom: 0px !important; }}@media (max-width: 767px){[data-css=\"tve-u-1717960edfe\"] { margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee08\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-1717960edfb\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee00\"] { padding: 0px !important; margin-top: 0px !important; margin-bottom: 40px !important; }[data-css=\"tve-u-1717960ee0a\"] { padding: 20px 0px 0px !important; border-left: none !important; border-top: 1px solid rgb(234, 234, 234) !important; }[data-css=\"tve-u-1717960ee04\"] .tcb-flex-col { flex-basis: 400px !important; }[data-css=\"tve-u-1717960ee0b\"] { max-width: 308px; }:not(#tve) [data-css=\"tve-u-1717960edff\"] { font-size: 22px !important; }[data-css=\"tve-u-1717960edfc\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-1717960ee0e\"] .tcb-button-link { padding-left: 25px !important; padding-right: 25px !important; }[data-css=\"tve-u-1717960ee10\"] .tcb-button-link { border: none; padding-left: 25px !important; padding-right: 25px !important; }[data-css=\"tve-u-1717960ee10\"] { float: left; z-index: 3; position: relative; margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee0f\"]::after { clear: both; }[data-css=\"tve-u-1717960ee0e\"] { margin-bottom: 10px !important; }}[data-css=\"tve-u-1717960edf9\"] { --tcb-local-color-a35c5: rgb(241, 241, 241); --tcb-local-color-5f5e3: rgb(57, 164, 210); }'),
(488, 49, 'tve_user_custom_css', ''),
(489, 49, 'tve_page_events', 'a:0:{}');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(490, 49, 'tve_updated_post', '<div class=\"thrv-content-block tcb-local-vars-root thrv_wrapper\" data-css=\"tve-u-1717960edf9\" tcb-template-name=\"Resource List 05\" tcb-template-id=\"41678\" tcb-template-pack=\"137\" data-keep-css_id=\"1\"><div class=\"thrive-group-edit-config\" style=\"display: none !important\">__CONFIG_group_edit__{\"juo8pmn3\":{\"name\":\"All Paragraph(s)\",\"singular\":\"-- Text %s\"},\"jvdr7vrx\":{\"name\":\"All Title(s)\",\"singular\":\"-- Text %s\"},\"jvdrfoj4\":{\"name\":\"All Read Review Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrg8bl\":{\"name\":\"All Visit Website Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrgsih\":{\"name\":\"All Buttons Content Box(s)\",\"singular\":\"-- Content Box %s\"},\"jvdrh1la\":{\"name\":\"All Paragraphs Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrha52\":{\"name\":\"All Buttons Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrhpi3\":{\"name\":\"All Columns(s)\",\"singular\":\"-- Columns %s\"},\"jvdrhrvz\":{\"name\":\"All Content Box(s)\",\"singular\":\"-- Content Box %s\"}}__CONFIG_group_edit__</div><div class=\"thrive-local-colors-config\" style=\"display: none !important\">__CONFIG_local_colors__{\"colors\":{\"a35c5\":\"Button\",\"5f5e3\":\"Button Hover\"},\"gradients\":{}}__CONFIG_local_colors__</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfa\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960edfb\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfc\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960edfd\"></div>\n	<div class=\"tve-cb\"><div class=\"thrv_wrapper thrv_text_element\" data-css=\"tve-u-1717960edfe\"><h2 class=\"\" data-css=\"tve-u-1717960edff\">Recommended Resources</h2></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div></div>\n</div></div>'),
(491, 49, 'tcb_editor_enabled', '1'),
(492, 57, 'tve_revision_tve_landing_page', ''),
(493, 57, 'tve_revision_tve_disable_theme_dependency', ''),
(494, 57, 'tve_revision_tve_content_before_more', '<div class=\"thrv-content-block tcb-local-vars-root thrv_wrapper\" data-css=\"tve-u-1717960edf9\" tcb-template-name=\"Resource List 05\" tcb-template-id=\"41678\" tcb-template-pack=\"137\" data-keep-css_id=\"1\"><div class=\"thrive-group-edit-config\" style=\"display: none !important\">__CONFIG_group_edit__{\"juo8pmn3\":{\"name\":\"All Paragraph(s)\",\"singular\":\"-- Text %s\"},\"jvdr7vrx\":{\"name\":\"All Title(s)\",\"singular\":\"-- Text %s\"},\"jvdrfoj4\":{\"name\":\"All Read Review Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrg8bl\":{\"name\":\"All Visit Website Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrgsih\":{\"name\":\"All Buttons Content Box(s)\",\"singular\":\"-- Content Box %s\"},\"jvdrh1la\":{\"name\":\"All Paragraphs Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrha52\":{\"name\":\"All Buttons Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrhpi3\":{\"name\":\"All Columns(s)\",\"singular\":\"-- Columns %s\"},\"jvdrhrvz\":{\"name\":\"All Content Box(s)\",\"singular\":\"-- Content Box %s\"}}__CONFIG_group_edit__</div><div class=\"thrive-local-colors-config\" style=\"display: none !important\">__CONFIG_local_colors__{\"colors\":{\"a35c5\":\"Button\",\"5f5e3\":\"Button Hover\"},\"gradients\":{}}__CONFIG_local_colors__</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfa\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960edfb\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfc\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960edfd\"></div>\n	<div class=\"tve-cb\"><div class=\"thrv_wrapper thrv_text_element\" data-css=\"tve-u-1717960edfe\"><h2 class=\"\" data-css=\"tve-u-1717960edff\">Recommended Resources</h2></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div></div>\n</div></div>'),
(495, 57, 'tve_revision_tve_content_more_found', ''),
(496, 57, 'tve_revision_tve_save_post', ''),
(497, 57, 'tve_revision_tve_custom_css', '@media (min-width: 300px){[data-css=\"tve-u-1717960edfe\"] { padding: 0px !important; margin-bottom: 0px !important; border: none !important; }[data-css=\"tve-u-1717960ee08\"] { padding: 0px !important; margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee01\"] { border: none !important; }[data-css=\"tve-u-1717960edfa\"] { padding: 0px !important; margin: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] { margin: 0px !important; }[data-css=\"tve-u-1717960ee00\"] { padding: 0px !important; margin-top: 0px !important; margin-bottom: 50px !important; }[data-css=\"tve-u-1717960ee03\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] p { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee04\"] { padding: 0px !important; }[data-css=\"tve-u-1717960ee04\"] > .tcb-flex-col > .tcb-col { justify-content: center; }[data-css=\"tve-u-1717960ee06\"] { padding-right: 20px !important; }[data-css=\"tve-u-1717960ee0a\"] { border-left: 1px solid rgb(234, 234, 234) !important; padding-left: 20px !important; padding-right: 20px !important; }[data-css=\"tve-u-1717960ee0b\"] { max-width: 308px; padding: 0px !important; margin-top: 0px !important; margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee05\"] { max-width: 66.1%; }[data-css=\"tve-u-1717960ee09\"] { max-width: 34%; }[data-css=\"tve-u-1717960ee07\"] { padding: 0px !important; margin-bottom: 30px !important; }[data-css=\"tve-u-1717960edfd\"] { border-top: 1px solid rgb(234, 234, 234) !important; border-right: none !important; border-bottom: 1px solid rgb(234, 234, 234) !important; border-left: none !important; border-image: initial !important; }[data-css=\"tve-u-1717960edfc\"] { padding: 15px 0px !important; margin: 0px 0px 30px !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"] .tcb-button-link { font-size: 15px !important; }[data-css=\"tve-u-1717960ee0e\"] .tcb-button-link { border-radius: 100px; overflow: hidden; border: 1px solid var(--tcb-local-color-a35c5,rgb(241, 241, 241)); padding: 10px 30px !important; background-color: transparent !important; background-image: none !important; }[data-css=\"tve-u-1717960ee0e\"] { float: none; z-index: 3; position: relative; margin: 0px auto 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"] .tcb-button-link span { color: rgb(51, 51, 51); }:not(#tve) [data-css=\"tve-u-1717960ee0e\"]:hover { margin-bottom: 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"]:hover .tcb-button-link { border: 1px solid var(--tcb-local-color-5f5e3,rgb(57, 164, 210))  !important; background-image: none !important; background-color: rgba(21, 162, 136, 0) !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"]:hover .tcb-button-link span { color: var(--tcb-local-color-5f5e3,rgb(57, 164, 210))  !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"] .tcb-button-link { font-size: 15px !important; }[data-css=\"tve-u-1717960ee10\"] .tcb-button-link { border-radius: 100px; overflow: hidden; border: none; padding: 10px 30px !important; background-color: transparent !important; background-image: linear-gradient(var(--tcb-local-color-a35c5,rgb(241, 241, 241)), var(--tcb-local-color-a35c5,rgb(241, 241, 241)))  !important; background-size: auto !important; background-position: 50% 50% !important; background-attachment: scroll !important; background-repeat: no-repeat !important; }[data-css=\"tve-u-1717960ee10\"] { float: none; z-index: 3; position: relative; margin: 0px auto 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"] .tcb-button-link span { color: rgb(51, 51, 51); }:not(#tve) [data-css=\"tve-u-1717960ee10\"]:hover { margin-bottom: 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"]:hover .tcb-button-link { border: none !important; background-image: none !important; background-color: rgb(57, 164, 210) !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"]:hover .tcb-button-link span { color: rgb(255, 255, 255) !important; }}@media (max-width: 1023px){[data-css=\"tve-u-1717960edfe\"] { margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee08\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-1717960ee00\"] { margin-top: 0px !important; margin-bottom: 50px !important; }[data-css=\"tve-u-1717960ee04\"] { flex-wrap: wrap !important; }[data-css=\"tve-u-1717960ee04\"] .tcb-flex-col { flex-basis: 400px !important; }[data-css=\"tve-u-1717960ee06\"] { padding-right: 0px !important; padding-left: 0px !important; padding-top: 0px !important; border-top: none !important; }[data-css=\"tve-u-1717960ee0a\"] { padding-right: 0px !important; padding-left: 0px !important; padding-top: 25px !important; border-left: none !important; border-top: 1px solid rgb(234, 234, 234) !important; }[data-css=\"tve-u-1717960ee0b\"] { max-width: 308px; }[data-css=\"tve-u-1717960ee0e\"] { float: left; z-index: 3; position: relative; margin-right: 9px !important; margin-bottom: 10px !important; }[data-css=\"tve-u-1717960ee10\"] { float: right; z-index: 3; position: relative; margin-right: 0px !important; margin-bottom: 0px !important; }}@media (max-width: 767px){[data-css=\"tve-u-1717960edfe\"] { margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee08\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-1717960edfb\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee00\"] { padding: 0px !important; margin-top: 0px !important; margin-bottom: 40px !important; }[data-css=\"tve-u-1717960ee0a\"] { padding: 20px 0px 0px !important; border-left: none !important; border-top: 1px solid rgb(234, 234, 234) !important; }[data-css=\"tve-u-1717960ee04\"] .tcb-flex-col { flex-basis: 400px !important; }[data-css=\"tve-u-1717960ee0b\"] { max-width: 308px; }:not(#tve) [data-css=\"tve-u-1717960edff\"] { font-size: 22px !important; }[data-css=\"tve-u-1717960edfc\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-1717960ee0e\"] .tcb-button-link { padding-left: 25px !important; padding-right: 25px !important; }[data-css=\"tve-u-1717960ee10\"] .tcb-button-link { border: none; padding-left: 25px !important; padding-right: 25px !important; }[data-css=\"tve-u-1717960ee10\"] { float: left; z-index: 3; position: relative; margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee0f\"]::after { clear: both; }[data-css=\"tve-u-1717960ee0e\"] { margin-bottom: 10px !important; }}[data-css=\"tve-u-1717960edf9\"] { --tcb-local-color-a35c5: rgb(241, 241, 241); --tcb-local-color-5f5e3: rgb(57, 164, 210); }'),
(498, 57, 'tve_revision_tve_user_custom_css', ''),
(499, 57, 'tve_revision_tve_page_events', 'a:0:{}'),
(500, 57, 'tve_revision_tve_globals', ''),
(501, 57, 'tve_revision_tve_global_scripts', ''),
(502, 57, 'tve_revision_thrive_icon_pack', ''),
(503, 57, 'tve_revision_thrive_tcb_post_fonts', ''),
(504, 57, 'tve_revision_tve_has_masonry', ''),
(505, 57, 'tve_revision_tve_has_typefocus', ''),
(506, 57, 'tve_revision_tve_updated_post', '<div class=\"thrv-content-block tcb-local-vars-root thrv_wrapper\" data-css=\"tve-u-1717960edf9\" tcb-template-name=\"Resource List 05\" tcb-template-id=\"41678\" tcb-template-pack=\"137\" data-keep-css_id=\"1\"><div class=\"thrive-group-edit-config\" style=\"display: none !important\">__CONFIG_group_edit__{\"juo8pmn3\":{\"name\":\"All Paragraph(s)\",\"singular\":\"-- Text %s\"},\"jvdr7vrx\":{\"name\":\"All Title(s)\",\"singular\":\"-- Text %s\"},\"jvdrfoj4\":{\"name\":\"All Read Review Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrg8bl\":{\"name\":\"All Visit Website Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrgsih\":{\"name\":\"All Buttons Content Box(s)\",\"singular\":\"-- Content Box %s\"},\"jvdrh1la\":{\"name\":\"All Paragraphs Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrha52\":{\"name\":\"All Buttons Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrhpi3\":{\"name\":\"All Columns(s)\",\"singular\":\"-- Columns %s\"},\"jvdrhrvz\":{\"name\":\"All Content Box(s)\",\"singular\":\"-- Content Box %s\"}}__CONFIG_group_edit__</div><div class=\"thrive-local-colors-config\" style=\"display: none !important\">__CONFIG_local_colors__{\"colors\":{\"a35c5\":\"Button\",\"5f5e3\":\"Button Hover\"},\"gradients\":{}}__CONFIG_local_colors__</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfa\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960edfb\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfc\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960edfd\"></div>\n	<div class=\"tve-cb\"><div class=\"thrv_wrapper thrv_text_element\" data-css=\"tve-u-1717960edfe\"><h2 class=\"\" data-css=\"tve-u-1717960edff\">Recommended Resources</h2></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div></div>\n</div></div>'),
(507, 57, 'tve_revision_tve_has_wistia_popover', ''),
(508, 49, 'tve_globals', 'a:2:{s:1:\"e\";s:1:\"1\";s:8:\"font_cls\";a:0:{}}'),
(509, 49, 'thrive_tcb_post_fonts', 'a:0:{}'),
(510, 49, 'thrive_icon_pack', '0'),
(511, 49, 'tve_has_masonry', '0'),
(512, 49, 'tve_has_typefocus', '0'),
(513, 49, 'tve_has_wistia_popover', '0'),
(514, 58, 'tve_revision_tve_landing_page', ''),
(515, 58, 'tve_revision_tve_disable_theme_dependency', ''),
(516, 58, 'tve_revision_tve_content_before_more', '<div class=\"thrv-content-block tcb-local-vars-root thrv_wrapper\" data-css=\"tve-u-1717960edf9\" tcb-template-name=\"Resource List 05\" tcb-template-id=\"41678\" tcb-template-pack=\"137\" data-keep-css_id=\"1\"><div class=\"thrive-group-edit-config\" style=\"display: none !important\">__CONFIG_group_edit__{\"juo8pmn3\":{\"name\":\"All Paragraph(s)\",\"singular\":\"-- Text %s\"},\"jvdr7vrx\":{\"name\":\"All Title(s)\",\"singular\":\"-- Text %s\"},\"jvdrfoj4\":{\"name\":\"All Read Review Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrg8bl\":{\"name\":\"All Visit Website Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrgsih\":{\"name\":\"All Buttons Content Box(s)\",\"singular\":\"-- Content Box %s\"},\"jvdrh1la\":{\"name\":\"All Paragraphs Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrha52\":{\"name\":\"All Buttons Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrhpi3\":{\"name\":\"All Columns(s)\",\"singular\":\"-- Columns %s\"},\"jvdrhrvz\":{\"name\":\"All Content Box(s)\",\"singular\":\"-- Content Box %s\"}}__CONFIG_group_edit__</div><div class=\"thrive-local-colors-config\" style=\"display: none !important\">__CONFIG_local_colors__{\"colors\":{\"a35c5\":\"Button\",\"5f5e3\":\"Button Hover\"},\"gradients\":{}}__CONFIG_local_colors__</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfa\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960edfb\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfc\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960edfd\"></div>\n	<div class=\"tve-cb\"><div class=\"thrv_wrapper thrv_text_element\" data-css=\"tve-u-1717960edfe\"><h2 class=\"\" data-css=\"tve-u-1717960edff\">Recommended Resources</h2></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div></div>\n</div></div>'),
(517, 58, 'tve_revision_tve_content_more_found', ''),
(518, 58, 'tve_revision_tve_save_post', '');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(519, 58, 'tve_revision_tve_custom_css', '@media (min-width: 300px){[data-css=\"tve-u-1717960edfe\"] { padding: 0px !important; margin-bottom: 0px !important; border: none !important; }[data-css=\"tve-u-1717960ee08\"] { padding: 0px !important; margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee01\"] { border: none !important; }[data-css=\"tve-u-1717960edfa\"] { padding: 0px !important; margin: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] { margin: 0px !important; }[data-css=\"tve-u-1717960ee00\"] { padding: 0px !important; margin-top: 0px !important; margin-bottom: 50px !important; }[data-css=\"tve-u-1717960ee03\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] p { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee04\"] { padding: 0px !important; }[data-css=\"tve-u-1717960ee04\"] > .tcb-flex-col > .tcb-col { justify-content: center; }[data-css=\"tve-u-1717960ee06\"] { padding-right: 20px !important; }[data-css=\"tve-u-1717960ee0a\"] { border-left: 1px solid rgb(234, 234, 234) !important; padding-left: 20px !important; padding-right: 20px !important; }[data-css=\"tve-u-1717960ee0b\"] { max-width: 308px; padding: 0px !important; margin-top: 0px !important; margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee05\"] { max-width: 66.1%; }[data-css=\"tve-u-1717960ee09\"] { max-width: 34%; }[data-css=\"tve-u-1717960ee07\"] { padding: 0px !important; margin-bottom: 30px !important; }[data-css=\"tve-u-1717960edfd\"] { border-top: 1px solid rgb(234, 234, 234) !important; border-right: none !important; border-bottom: 1px solid rgb(234, 234, 234) !important; border-left: none !important; border-image: initial !important; }[data-css=\"tve-u-1717960edfc\"] { padding: 15px 0px !important; margin: 0px 0px 30px !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"] .tcb-button-link { font-size: 15px !important; }[data-css=\"tve-u-1717960ee0e\"] .tcb-button-link { border-radius: 100px; overflow: hidden; border: 1px solid var(--tcb-local-color-a35c5,rgb(241, 241, 241)); padding: 10px 30px !important; background-color: transparent !important; background-image: none !important; }[data-css=\"tve-u-1717960ee0e\"] { float: none; z-index: 3; position: relative; margin: 0px auto 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"] .tcb-button-link span { color: rgb(51, 51, 51); }:not(#tve) [data-css=\"tve-u-1717960ee0e\"]:hover { margin-bottom: 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"]:hover .tcb-button-link { border: 1px solid var(--tcb-local-color-5f5e3,rgb(57, 164, 210))  !important; background-image: none !important; background-color: rgba(21, 162, 136, 0) !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"]:hover .tcb-button-link span { color: var(--tcb-local-color-5f5e3,rgb(57, 164, 210))  !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"] .tcb-button-link { font-size: 15px !important; }[data-css=\"tve-u-1717960ee10\"] .tcb-button-link { border-radius: 100px; overflow: hidden; border: none; padding: 10px 30px !important; background-color: transparent !important; background-image: linear-gradient(var(--tcb-local-color-a35c5,rgb(241, 241, 241)), var(--tcb-local-color-a35c5,rgb(241, 241, 241)))  !important; background-size: auto !important; background-position: 50% 50% !important; background-attachment: scroll !important; background-repeat: no-repeat !important; }[data-css=\"tve-u-1717960ee10\"] { float: none; z-index: 3; position: relative; margin: 0px auto 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"] .tcb-button-link span { color: rgb(51, 51, 51); }:not(#tve) [data-css=\"tve-u-1717960ee10\"]:hover { margin-bottom: 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"]:hover .tcb-button-link { border: none !important; background-image: none !important; background-color: rgb(57, 164, 210) !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"]:hover .tcb-button-link span { color: rgb(255, 255, 255) !important; }}@media (max-width: 1023px){[data-css=\"tve-u-1717960edfe\"] { margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee08\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-1717960ee00\"] { margin-top: 0px !important; margin-bottom: 50px !important; }[data-css=\"tve-u-1717960ee04\"] { flex-wrap: wrap !important; }[data-css=\"tve-u-1717960ee04\"] .tcb-flex-col { flex-basis: 400px !important; }[data-css=\"tve-u-1717960ee06\"] { padding-right: 0px !important; padding-left: 0px !important; padding-top: 0px !important; border-top: none !important; }[data-css=\"tve-u-1717960ee0a\"] { padding-right: 0px !important; padding-left: 0px !important; padding-top: 25px !important; border-left: none !important; border-top: 1px solid rgb(234, 234, 234) !important; }[data-css=\"tve-u-1717960ee0b\"] { max-width: 308px; }[data-css=\"tve-u-1717960ee0e\"] { float: left; z-index: 3; position: relative; margin-right: 9px !important; margin-bottom: 10px !important; }[data-css=\"tve-u-1717960ee10\"] { float: right; z-index: 3; position: relative; margin-right: 0px !important; margin-bottom: 0px !important; }}@media (max-width: 767px){[data-css=\"tve-u-1717960edfe\"] { margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee08\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-1717960edfb\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee00\"] { padding: 0px !important; margin-top: 0px !important; margin-bottom: 40px !important; }[data-css=\"tve-u-1717960ee0a\"] { padding: 20px 0px 0px !important; border-left: none !important; border-top: 1px solid rgb(234, 234, 234) !important; }[data-css=\"tve-u-1717960ee04\"] .tcb-flex-col { flex-basis: 400px !important; }[data-css=\"tve-u-1717960ee0b\"] { max-width: 308px; }:not(#tve) [data-css=\"tve-u-1717960edff\"] { font-size: 22px !important; }[data-css=\"tve-u-1717960edfc\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-1717960ee0e\"] .tcb-button-link { padding-left: 25px !important; padding-right: 25px !important; }[data-css=\"tve-u-1717960ee10\"] .tcb-button-link { border: none; padding-left: 25px !important; padding-right: 25px !important; }[data-css=\"tve-u-1717960ee10\"] { float: left; z-index: 3; position: relative; margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee0f\"]::after { clear: both; }[data-css=\"tve-u-1717960ee0e\"] { margin-bottom: 10px !important; }}[data-css=\"tve-u-1717960edf9\"] { --tcb-local-color-a35c5: rgb(241, 241, 241); --tcb-local-color-5f5e3: rgb(57, 164, 210); }'),
(520, 58, 'tve_revision_tve_user_custom_css', ''),
(521, 58, 'tve_revision_tve_page_events', 'a:0:{}'),
(522, 58, 'tve_revision_tve_globals', 'a:2:{s:1:\"e\";s:1:\"1\";s:8:\"font_cls\";a:0:{}}'),
(523, 58, 'tve_revision_tve_global_scripts', ''),
(524, 58, 'tve_revision_thrive_icon_pack', '0'),
(525, 58, 'tve_revision_thrive_tcb_post_fonts', 'a:0:{}'),
(526, 58, 'tve_revision_tve_has_masonry', '0'),
(527, 58, 'tve_revision_tve_has_typefocus', '0'),
(528, 58, 'tve_revision_tve_updated_post', '<div class=\"thrv-content-block tcb-local-vars-root thrv_wrapper\" data-css=\"tve-u-1717960edf9\" tcb-template-name=\"Resource List 05\" tcb-template-id=\"41678\" tcb-template-pack=\"137\" data-keep-css_id=\"1\"><div class=\"thrive-group-edit-config\" style=\"display: none !important\">__CONFIG_group_edit__{\"juo8pmn3\":{\"name\":\"All Paragraph(s)\",\"singular\":\"-- Text %s\"},\"jvdr7vrx\":{\"name\":\"All Title(s)\",\"singular\":\"-- Text %s\"},\"jvdrfoj4\":{\"name\":\"All Read Review Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrg8bl\":{\"name\":\"All Visit Website Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrgsih\":{\"name\":\"All Buttons Content Box(s)\",\"singular\":\"-- Content Box %s\"},\"jvdrh1la\":{\"name\":\"All Paragraphs Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrha52\":{\"name\":\"All Buttons Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrhpi3\":{\"name\":\"All Columns(s)\",\"singular\":\"-- Columns %s\"},\"jvdrhrvz\":{\"name\":\"All Content Box(s)\",\"singular\":\"-- Content Box %s\"}}__CONFIG_group_edit__</div><div class=\"thrive-local-colors-config\" style=\"display: none !important\">__CONFIG_local_colors__{\"colors\":{\"a35c5\":\"Button\",\"5f5e3\":\"Button Hover\"},\"gradients\":{}}__CONFIG_local_colors__</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfa\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960edfb\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfc\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960edfd\"></div>\n	<div class=\"tve-cb\"><div class=\"thrv_wrapper thrv_text_element\" data-css=\"tve-u-1717960edfe\"><h2 class=\"\" data-css=\"tve-u-1717960edff\">Recommended Resources</h2></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div></div>\n</div></div>'),
(529, 58, 'tve_revision_tve_has_wistia_popover', '0'),
(530, 59, 'tve_revision_tve_landing_page', ''),
(531, 59, 'tve_revision_tve_disable_theme_dependency', ''),
(532, 59, 'tve_revision_tve_content_before_more', '<div class=\"thrv-content-block tcb-local-vars-root thrv_wrapper\" data-css=\"tve-u-1717960edf9\" tcb-template-name=\"Resource List 05\" tcb-template-id=\"41678\" tcb-template-pack=\"137\" data-keep-css_id=\"1\"><div class=\"thrive-group-edit-config\" style=\"display: none !important\">__CONFIG_group_edit__{\"juo8pmn3\":{\"name\":\"All Paragraph(s)\",\"singular\":\"-- Text %s\"},\"jvdr7vrx\":{\"name\":\"All Title(s)\",\"singular\":\"-- Text %s\"},\"jvdrfoj4\":{\"name\":\"All Read Review Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrg8bl\":{\"name\":\"All Visit Website Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrgsih\":{\"name\":\"All Buttons Content Box(s)\",\"singular\":\"-- Content Box %s\"},\"jvdrh1la\":{\"name\":\"All Paragraphs Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrha52\":{\"name\":\"All Buttons Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrhpi3\":{\"name\":\"All Columns(s)\",\"singular\":\"-- Columns %s\"},\"jvdrhrvz\":{\"name\":\"All Content Box(s)\",\"singular\":\"-- Content Box %s\"}}__CONFIG_group_edit__</div><div class=\"thrive-local-colors-config\" style=\"display: none !important\">__CONFIG_local_colors__{\"colors\":{\"a35c5\":\"Button\",\"5f5e3\":\"Button Hover\"},\"gradients\":{}}__CONFIG_local_colors__</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfa\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960edfb\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfc\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960edfd\"></div>\n	<div class=\"tve-cb\"><div class=\"thrv_wrapper thrv_text_element\" data-css=\"tve-u-1717960edfe\"><h2 class=\"\" data-css=\"tve-u-1717960edff\">Recommended Resources</h2></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div></div>\n</div></div>'),
(533, 59, 'tve_revision_tve_content_more_found', ''),
(534, 59, 'tve_revision_tve_save_post', ''),
(535, 59, 'tve_revision_tve_custom_css', '@media (min-width: 300px){[data-css=\"tve-u-1717960edfe\"] { padding: 0px !important; margin-bottom: 0px !important; border: none !important; }[data-css=\"tve-u-1717960ee08\"] { padding: 0px !important; margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee01\"] { border: none !important; }[data-css=\"tve-u-1717960edfa\"] { padding: 0px !important; margin: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] { margin: 0px !important; }[data-css=\"tve-u-1717960ee00\"] { padding: 0px !important; margin-top: 0px !important; margin-bottom: 50px !important; }[data-css=\"tve-u-1717960ee03\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] h2 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee03\"] p { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee04\"] { padding: 0px !important; }[data-css=\"tve-u-1717960ee04\"] > .tcb-flex-col > .tcb-col { justify-content: center; }[data-css=\"tve-u-1717960ee06\"] { padding-right: 20px !important; }[data-css=\"tve-u-1717960ee0a\"] { border-left: 1px solid rgb(234, 234, 234) !important; padding-left: 20px !important; padding-right: 20px !important; }[data-css=\"tve-u-1717960ee0b\"] { max-width: 308px; padding: 0px !important; margin-top: 0px !important; margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee05\"] { max-width: 66.1%; }[data-css=\"tve-u-1717960ee09\"] { max-width: 34%; }[data-css=\"tve-u-1717960ee07\"] { padding: 0px !important; margin-bottom: 30px !important; }[data-css=\"tve-u-1717960edfd\"] { border-top: 1px solid rgb(234, 234, 234) !important; border-right: none !important; border-bottom: 1px solid rgb(234, 234, 234) !important; border-left: none !important; border-image: initial !important; }[data-css=\"tve-u-1717960edfc\"] { padding: 15px 0px !important; margin: 0px 0px 30px !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"] .tcb-button-link { font-size: 15px !important; }[data-css=\"tve-u-1717960ee0e\"] .tcb-button-link { border-radius: 100px; overflow: hidden; border: 1px solid var(--tcb-local-color-a35c5,rgb(241, 241, 241)); padding: 10px 30px !important; background-color: transparent !important; background-image: none !important; }[data-css=\"tve-u-1717960ee0e\"] { float: none; z-index: 3; position: relative; margin: 0px auto 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"] .tcb-button-link span { color: rgb(51, 51, 51); }:not(#tve) [data-css=\"tve-u-1717960ee0e\"]:hover { margin-bottom: 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"]:hover .tcb-button-link { border: 1px solid var(--tcb-local-color-5f5e3,rgb(57, 164, 210))  !important; background-image: none !important; background-color: rgba(21, 162, 136, 0) !important; }:not(#tve) [data-css=\"tve-u-1717960ee0e\"]:hover .tcb-button-link span { color: var(--tcb-local-color-5f5e3,rgb(57, 164, 210))  !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"] .tcb-button-link { font-size: 15px !important; }[data-css=\"tve-u-1717960ee10\"] .tcb-button-link { border-radius: 100px; overflow: hidden; border: none; padding: 10px 30px !important; background-color: transparent !important; background-image: linear-gradient(var(--tcb-local-color-a35c5,rgb(241, 241, 241)), var(--tcb-local-color-a35c5,rgb(241, 241, 241)))  !important; background-size: auto !important; background-position: 50% 50% !important; background-attachment: scroll !important; background-repeat: no-repeat !important; }[data-css=\"tve-u-1717960ee10\"] { float: none; z-index: 3; position: relative; margin: 0px auto 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"] .tcb-button-link span { color: rgb(51, 51, 51); }:not(#tve) [data-css=\"tve-u-1717960ee10\"]:hover { margin-bottom: 10px !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"]:hover .tcb-button-link { border: none !important; background-image: none !important; background-color: rgb(57, 164, 210) !important; }:not(#tve) [data-css=\"tve-u-1717960ee10\"]:hover .tcb-button-link span { color: rgb(255, 255, 255) !important; }}@media (max-width: 1023px){[data-css=\"tve-u-1717960edfe\"] { margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee08\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-1717960ee00\"] { margin-top: 0px !important; margin-bottom: 50px !important; }[data-css=\"tve-u-1717960ee04\"] { flex-wrap: wrap !important; }[data-css=\"tve-u-1717960ee04\"] .tcb-flex-col { flex-basis: 400px !important; }[data-css=\"tve-u-1717960ee06\"] { padding-right: 0px !important; padding-left: 0px !important; padding-top: 0px !important; border-top: none !important; }[data-css=\"tve-u-1717960ee0a\"] { padding-right: 0px !important; padding-left: 0px !important; padding-top: 25px !important; border-left: none !important; border-top: 1px solid rgb(234, 234, 234) !important; }[data-css=\"tve-u-1717960ee0b\"] { max-width: 308px; }[data-css=\"tve-u-1717960ee0e\"] { float: left; z-index: 3; position: relative; margin-right: 9px !important; margin-bottom: 10px !important; }[data-css=\"tve-u-1717960ee10\"] { float: right; z-index: 3; position: relative; margin-right: 0px !important; margin-bottom: 0px !important; }}@media (max-width: 767px){[data-css=\"tve-u-1717960edfe\"] { margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee08\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-1717960edfb\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h1 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960edfb\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee02\"] h3 { margin: 0px !important; padding: 0px !important; }[data-css=\"tve-u-1717960ee00\"] { padding: 0px !important; margin-top: 0px !important; margin-bottom: 40px !important; }[data-css=\"tve-u-1717960ee0a\"] { padding: 20px 0px 0px !important; border-left: none !important; border-top: 1px solid rgb(234, 234, 234) !important; }[data-css=\"tve-u-1717960ee04\"] .tcb-flex-col { flex-basis: 400px !important; }[data-css=\"tve-u-1717960ee0b\"] { max-width: 308px; }:not(#tve) [data-css=\"tve-u-1717960edff\"] { font-size: 22px !important; }[data-css=\"tve-u-1717960edfc\"] { margin-bottom: 20px !important; }[data-css=\"tve-u-1717960ee0e\"] .tcb-button-link { padding-left: 25px !important; padding-right: 25px !important; }[data-css=\"tve-u-1717960ee10\"] .tcb-button-link { border: none; padding-left: 25px !important; padding-right: 25px !important; }[data-css=\"tve-u-1717960ee10\"] { float: left; z-index: 3; position: relative; margin-bottom: 0px !important; }[data-css=\"tve-u-1717960ee0f\"]::after { clear: both; }[data-css=\"tve-u-1717960ee0e\"] { margin-bottom: 10px !important; }}[data-css=\"tve-u-1717960edf9\"] { --tcb-local-color-a35c5: rgb(241, 241, 241); --tcb-local-color-5f5e3: rgb(57, 164, 210); }'),
(536, 59, 'tve_revision_tve_user_custom_css', ''),
(537, 59, 'tve_revision_tve_page_events', 'a:0:{}'),
(538, 59, 'tve_revision_tve_globals', 'a:2:{s:1:\"e\";s:1:\"1\";s:8:\"font_cls\";a:0:{}}'),
(539, 59, 'tve_revision_tve_global_scripts', ''),
(540, 59, 'tve_revision_thrive_icon_pack', '0'),
(541, 59, 'tve_revision_thrive_tcb_post_fonts', 'a:0:{}'),
(542, 59, 'tve_revision_tve_has_masonry', '0'),
(543, 59, 'tve_revision_tve_has_typefocus', '0'),
(544, 59, 'tve_revision_tve_updated_post', '<div class=\"thrv-content-block tcb-local-vars-root thrv_wrapper\" data-css=\"tve-u-1717960edf9\" tcb-template-name=\"Resource List 05\" tcb-template-id=\"41678\" tcb-template-pack=\"137\" data-keep-css_id=\"1\"><div class=\"thrive-group-edit-config\" style=\"display: none !important\">__CONFIG_group_edit__{\"juo8pmn3\":{\"name\":\"All Paragraph(s)\",\"singular\":\"-- Text %s\"},\"jvdr7vrx\":{\"name\":\"All Title(s)\",\"singular\":\"-- Text %s\"},\"jvdrfoj4\":{\"name\":\"All Read Review Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrg8bl\":{\"name\":\"All Visit Website Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrgsih\":{\"name\":\"All Buttons Content Box(s)\",\"singular\":\"-- Content Box %s\"},\"jvdrh1la\":{\"name\":\"All Paragraphs Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrha52\":{\"name\":\"All Buttons Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrhpi3\":{\"name\":\"All Columns(s)\",\"singular\":\"-- Columns %s\"},\"jvdrhrvz\":{\"name\":\"All Content Box(s)\",\"singular\":\"-- Content Box %s\"}}__CONFIG_group_edit__</div><div class=\"thrive-local-colors-config\" style=\"display: none !important\">__CONFIG_local_colors__{\"colors\":{\"a35c5\":\"Button\",\"5f5e3\":\"Button Hover\"},\"gradients\":{}}__CONFIG_local_colors__</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfa\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960edfb\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-1717960edfc\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960edfd\"></div>\n	<div class=\"tve-cb\"><div class=\"thrv_wrapper thrv_text_element\" data-css=\"tve-u-1717960edfe\"><h2 class=\"\" data-css=\"tve-u-1717960edff\">Recommended Resources</h2></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-1717960ee00\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-1717960ee01\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee02\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-1717960ee03\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-1717960ee04\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-1717960ee05\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-1717960ee06\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-1717960ee07\"><h3 class=\"\">Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-1717960ee08\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-1717960ee09\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-1717960ee0a\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-1717960ee0b\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-1717960ee0c\"><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0d\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-1717960ee0e\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-1717960ee0f\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-1717960ee10\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div></div>\n</div></div>'),
(545, 59, 'tve_revision_tve_has_wistia_popover', '0'),
(562, 61, 'tve_revision_tve_landing_page', ''),
(563, 61, 'tve_revision_tve_disable_theme_dependency', ''),
(564, 61, 'tve_revision_tve_content_before_more', ''),
(565, 61, 'tve_revision_tve_content_more_found', ''),
(566, 61, 'tve_revision_tve_save_post', ''),
(567, 61, 'tve_revision_tve_custom_css', ''),
(568, 61, 'tve_revision_tve_user_custom_css', ''),
(569, 61, 'tve_revision_tve_page_events', ''),
(570, 61, 'tve_revision_tve_globals', ''),
(571, 61, 'tve_revision_tve_global_scripts', ''),
(572, 61, 'tve_revision_thrive_icon_pack', ''),
(573, 61, 'tve_revision_thrive_tcb_post_fonts', ''),
(574, 61, 'tve_revision_tve_has_masonry', ''),
(575, 61, 'tve_revision_tve_has_typefocus', ''),
(576, 61, 'tve_revision_tve_updated_post', ''),
(577, 61, 'tve_revision_tve_has_wistia_popover', ''),
(578, 62, 'tve_revision_tve_landing_page', ''),
(579, 62, 'tve_revision_tve_disable_theme_dependency', ''),
(580, 62, 'tve_revision_tve_content_before_more', ''),
(581, 62, 'tve_revision_tve_content_more_found', ''),
(582, 62, 'tve_revision_tve_save_post', ''),
(583, 62, 'tve_revision_tve_custom_css', ''),
(584, 62, 'tve_revision_tve_user_custom_css', ''),
(585, 62, 'tve_revision_tve_page_events', ''),
(586, 62, 'tve_revision_tve_globals', ''),
(587, 62, 'tve_revision_tve_global_scripts', ''),
(588, 62, 'tve_revision_thrive_icon_pack', ''),
(589, 62, 'tve_revision_thrive_tcb_post_fonts', ''),
(590, 62, 'tve_revision_tve_has_masonry', ''),
(591, 62, 'tve_revision_tve_has_typefocus', ''),
(592, 62, 'tve_revision_tve_updated_post', ''),
(593, 62, 'tve_revision_tve_has_wistia_popover', ''),
(594, 63, 'tve_revision_tve_landing_page', ''),
(595, 63, 'tve_revision_tve_disable_theme_dependency', ''),
(596, 63, 'tve_revision_tve_content_before_more', ''),
(597, 63, 'tve_revision_tve_content_more_found', ''),
(598, 63, 'tve_revision_tve_save_post', ''),
(599, 63, 'tve_revision_tve_custom_css', ''),
(600, 63, 'tve_revision_tve_user_custom_css', ''),
(601, 63, 'tve_revision_tve_page_events', ''),
(602, 63, 'tve_revision_tve_globals', ''),
(603, 63, 'tve_revision_tve_global_scripts', ''),
(604, 63, 'tve_revision_thrive_icon_pack', ''),
(605, 63, 'tve_revision_thrive_tcb_post_fonts', ''),
(606, 63, 'tve_revision_tve_has_masonry', ''),
(607, 63, 'tve_revision_tve_has_typefocus', ''),
(608, 63, 'tve_revision_tve_updated_post', ''),
(609, 63, 'tve_revision_tve_has_wistia_popover', ''),
(610, 64, '_wp_trash_meta_status', 'publish'),
(611, 64, '_wp_trash_meta_time', '1586887756'),
(612, 65, '_wp_attached_file', '2020/04/roland.jpg'),
(613, 65, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:222;s:6:\"height\";i:68;s:4:\"file\";s:18:\"2020/04/roland.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"roland-150x68.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:68;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Alex Bwanakweli\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1586948449\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(614, 66, '_wp_trash_meta_status', 'publish'),
(615, 66, '_wp_trash_meta_time', '1586937751'),
(616, 67, '_edit_lock', '1588581399:1'),
(617, 68, 'tve_revision_tve_landing_page', ''),
(618, 68, 'tve_revision_tve_disable_theme_dependency', ''),
(619, 68, 'tve_revision_tve_content_before_more', ''),
(620, 68, 'tve_revision_tve_content_more_found', ''),
(621, 68, 'tve_revision_tve_save_post', ''),
(622, 68, 'tve_revision_tve_custom_css', ''),
(623, 68, 'tve_revision_tve_user_custom_css', ''),
(624, 68, 'tve_revision_tve_page_events', ''),
(625, 68, 'tve_revision_tve_globals', ''),
(626, 68, 'tve_revision_tve_global_scripts', ''),
(627, 68, 'tve_revision_thrive_icon_pack', ''),
(628, 68, 'tve_revision_thrive_tcb_post_fonts', ''),
(629, 68, 'tve_revision_tve_has_masonry', ''),
(630, 68, 'tve_revision_tve_has_typefocus', ''),
(631, 68, 'tve_revision_tve_updated_post', ''),
(632, 68, 'tve_revision_tve_has_wistia_popover', ''),
(633, 67, '_wp_page_template', 'page-templates/register-template.php'),
(639, 74, '_edit_lock', '1586999355:1'),
(640, 75, 'tve_revision_tve_landing_page', ''),
(641, 75, 'tve_revision_tve_disable_theme_dependency', ''),
(642, 75, 'tve_revision_tve_content_before_more', ''),
(643, 75, 'tve_revision_tve_content_more_found', ''),
(644, 75, 'tve_revision_tve_save_post', ''),
(645, 75, 'tve_revision_tve_custom_css', ''),
(646, 75, 'tve_revision_tve_user_custom_css', ''),
(647, 75, 'tve_revision_tve_page_events', ''),
(648, 75, 'tve_revision_tve_globals', ''),
(649, 75, 'tve_revision_tve_global_scripts', ''),
(650, 75, 'tve_revision_thrive_icon_pack', ''),
(651, 75, 'tve_revision_thrive_tcb_post_fonts', ''),
(652, 75, 'tve_revision_tve_has_masonry', ''),
(653, 75, 'tve_revision_tve_has_typefocus', ''),
(654, 75, 'tve_revision_tve_updated_post', ''),
(655, 75, 'tve_revision_tve_has_wistia_popover', ''),
(656, 74, '_thumbnail_id', '65'),
(663, 74, '_edit_last', '1'),
(664, 74, 'wpcomplete', '{\"redirect\":{\"title\":\"Dashboard (Page #51)\",\"url\":\"http://localhost/coursemodule/index.php/dashboard/\"}}'),
(667, 82, '_edit_lock', '1588420210:1'),
(668, 82, '_edit_last', '1'),
(669, 86, '_edit_lock', '1588537743:1'),
(670, 87, 'tve_revision_tve_landing_page', ''),
(671, 87, 'tve_revision_tve_disable_theme_dependency', ''),
(672, 87, 'tve_revision_tve_content_before_more', ''),
(673, 87, 'tve_revision_tve_content_more_found', ''),
(674, 87, 'tve_revision_tve_save_post', ''),
(675, 87, 'tve_revision_tve_custom_css', ''),
(676, 87, 'tve_revision_tve_user_custom_css', ''),
(677, 87, 'tve_revision_tve_page_events', ''),
(678, 87, 'tve_revision_tve_globals', ''),
(679, 87, 'tve_revision_tve_global_scripts', ''),
(680, 87, 'tve_revision_thrive_icon_pack', ''),
(681, 87, 'tve_revision_thrive_tcb_post_fonts', ''),
(682, 87, 'tve_revision_tve_has_masonry', ''),
(683, 87, 'tve_revision_tve_has_typefocus', ''),
(684, 87, 'tve_revision_tve_updated_post', ''),
(685, 87, 'tve_revision_tve_has_wistia_popover', ''),
(686, 86, '_edit_last', '1'),
(687, 88, 'tve_revision_tve_landing_page', ''),
(688, 88, 'tve_revision_tve_disable_theme_dependency', ''),
(689, 88, 'tve_revision_tve_content_before_more', ''),
(690, 88, 'tve_revision_tve_content_more_found', ''),
(691, 88, 'tve_revision_tve_save_post', ''),
(692, 88, 'tve_revision_tve_custom_css', ''),
(693, 88, 'tve_revision_tve_user_custom_css', ''),
(694, 88, 'tve_revision_tve_page_events', ''),
(695, 88, 'tve_revision_tve_globals', ''),
(696, 88, 'tve_revision_tve_global_scripts', '');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(697, 88, 'tve_revision_thrive_icon_pack', ''),
(698, 88, 'tve_revision_thrive_tcb_post_fonts', ''),
(699, 88, 'tve_revision_tve_has_masonry', ''),
(700, 88, 'tve_revision_tve_has_typefocus', ''),
(701, 88, 'tve_revision_tve_updated_post', ''),
(702, 88, 'tve_revision_tve_has_wistia_popover', ''),
(703, 86, 'video_title', 'Test course 33'),
(704, 86, '_video_title', 'field_5e97ae5a532d7'),
(705, 86, 'video_link', 'https://player.vimeo.com/video/17588964'),
(706, 86, '_video_link', 'field_5e97ae87532d8'),
(707, 86, 'short_description', 'hhhhhhhhh Our Smooth Jazz Music can be used for relaxing, calm, romantic dinner, cooking, at work and relaxation. This relaxing  Jazz music best suited for study, work, cooking, like dinner music, background music, romantic music, relaxation music or music for stress relief. Let the beautiful, soothing and inspirational music help you relax!'),
(708, 86, '_short_description', 'field_5e97aed1532d9'),
(709, 88, 'video_title', 'Test course'),
(710, 88, '_video_title', 'field_5e97ae5a532d7'),
(711, 88, 'video_link', 'https://youtu.be/X2gNr65invQ'),
(712, 88, '_video_link', 'field_5e97ae87532d8'),
(713, 88, 'short_description', 'Our Smooth Jazz Music can be used for relaxing, calm, romantic dinner, cooking, at work and relaxation. This relaxing  Jazz music best suited for study, work, cooking, like dinner music, background music, romantic music, relaxation music or music for stress relief. Let the beautiful, soothing and inspirational music help you relax!'),
(714, 88, '_short_description', 'field_5e97aed1532d9'),
(715, 86, 'wpcomplete', '[]'),
(716, 89, '_edit_lock', '1588429219:1'),
(717, 90, 'tve_revision_tve_landing_page', ''),
(718, 90, 'tve_revision_tve_disable_theme_dependency', ''),
(719, 90, 'tve_revision_tve_content_before_more', ''),
(720, 90, 'tve_revision_tve_content_more_found', ''),
(721, 90, 'tve_revision_tve_save_post', ''),
(722, 90, 'tve_revision_tve_custom_css', ''),
(723, 90, 'tve_revision_tve_user_custom_css', ''),
(724, 90, 'tve_revision_tve_page_events', ''),
(725, 90, 'tve_revision_tve_globals', ''),
(726, 90, 'tve_revision_tve_global_scripts', ''),
(727, 90, 'tve_revision_thrive_icon_pack', ''),
(728, 90, 'tve_revision_thrive_tcb_post_fonts', ''),
(729, 90, 'tve_revision_tve_has_masonry', ''),
(730, 90, 'tve_revision_tve_has_typefocus', ''),
(731, 90, 'tve_revision_tve_updated_post', ''),
(732, 90, 'tve_revision_tve_has_wistia_popover', ''),
(733, 89, '_edit_last', '1'),
(734, 91, 'tve_revision_tve_landing_page', ''),
(735, 91, 'tve_revision_tve_disable_theme_dependency', ''),
(736, 91, 'tve_revision_tve_content_before_more', ''),
(737, 91, 'tve_revision_tve_content_more_found', ''),
(738, 91, 'tve_revision_tve_save_post', ''),
(739, 91, 'tve_revision_tve_custom_css', ''),
(740, 91, 'tve_revision_tve_user_custom_css', ''),
(741, 91, 'tve_revision_tve_page_events', ''),
(742, 91, 'tve_revision_tve_globals', ''),
(743, 91, 'tve_revision_tve_global_scripts', ''),
(744, 91, 'tve_revision_thrive_icon_pack', ''),
(745, 91, 'tve_revision_thrive_tcb_post_fonts', ''),
(746, 91, 'tve_revision_tve_has_masonry', ''),
(747, 91, 'tve_revision_tve_has_typefocus', ''),
(748, 91, 'tve_revision_tve_updated_post', ''),
(749, 91, 'tve_revision_tve_has_wistia_popover', ''),
(750, 89, 'wpcomplete', '[]'),
(751, 89, 'video_title', 'Test video 2'),
(752, 89, '_video_title', 'field_5e97ae5a532d7'),
(753, 89, 'video_link', 'https://player.vimeo.com/video/1844255'),
(754, 89, '_video_link', 'field_5e97ae87532d8'),
(755, 89, 'short_description', 'Our Smooth Jazz Music can be used for relaxing, calm, romantic dinner, cooking, at work and relaxation. This relaxing  Jazz music best suited for study, work, cooking, like dinner music, background music, romantic music, relaxation music or music for stress relief. Let the beautiful, soothing and inspirational music help you relax!'),
(756, 89, '_short_description', 'field_5e97aed1532d9'),
(757, 91, 'video_title', 'Test video 2'),
(758, 91, '_video_title', 'field_5e97ae5a532d7'),
(759, 91, 'video_link', 'https://youtu.be/m2HpqawZB6g'),
(760, 91, '_video_link', 'field_5e97ae87532d8'),
(761, 91, 'short_description', 'Our Smooth Jazz Music can be used for relaxing, calm, romantic dinner, cooking, at work and relaxation. This relaxing  Jazz music best suited for study, work, cooking, like dinner music, background music, romantic music, relaxation music or music for stress relief. Let the beautiful, soothing and inspirational music help you relax!'),
(762, 91, '_short_description', 'field_5e97aed1532d9'),
(763, 30, 'tcb_editor_disabled', '1'),
(764, 92, '_edit_lock', '1587032674:1'),
(765, 93, 'tve_revision_tve_landing_page', ''),
(766, 93, 'tve_revision_tve_disable_theme_dependency', ''),
(767, 93, 'tve_revision_tve_content_before_more', ''),
(768, 93, 'tve_revision_tve_content_more_found', ''),
(769, 93, 'tve_revision_tve_save_post', ''),
(770, 93, 'tve_revision_tve_custom_css', ''),
(771, 93, 'tve_revision_tve_user_custom_css', ''),
(772, 93, 'tve_revision_tve_page_events', ''),
(773, 93, 'tve_revision_tve_globals', ''),
(774, 93, 'tve_revision_tve_global_scripts', ''),
(775, 93, 'tve_revision_thrive_icon_pack', ''),
(776, 93, 'tve_revision_thrive_tcb_post_fonts', ''),
(777, 93, 'tve_revision_tve_has_masonry', ''),
(778, 93, 'tve_revision_tve_has_typefocus', ''),
(779, 93, 'tve_revision_tve_updated_post', ''),
(780, 93, 'tve_revision_tve_has_wistia_popover', ''),
(783, 96, '_edit_lock', '1587769306:1'),
(784, 97, 'tve_revision_tve_landing_page', ''),
(785, 97, 'tve_revision_tve_disable_theme_dependency', ''),
(786, 97, 'tve_revision_tve_content_before_more', ''),
(787, 97, 'tve_revision_tve_content_more_found', ''),
(788, 97, 'tve_revision_tve_save_post', ''),
(789, 97, 'tve_revision_tve_custom_css', ''),
(790, 97, 'tve_revision_tve_user_custom_css', ''),
(791, 97, 'tve_revision_tve_page_events', ''),
(792, 97, 'tve_revision_tve_globals', ''),
(793, 97, 'tve_revision_tve_global_scripts', ''),
(794, 97, 'tve_revision_thrive_icon_pack', ''),
(795, 97, 'tve_revision_thrive_tcb_post_fonts', ''),
(796, 97, 'tve_revision_tve_has_masonry', ''),
(797, 97, 'tve_revision_tve_has_typefocus', ''),
(798, 97, 'tve_revision_tve_updated_post', ''),
(799, 97, 'tve_revision_tve_has_wistia_popover', ''),
(800, 96, '_wp_page_template', 'page-templates/showcourses-template.php'),
(810, 108, 'tve_revision_tve_landing_page', ''),
(811, 108, 'tve_revision_tve_disable_theme_dependency', ''),
(812, 108, 'tve_revision_tve_content_before_more', ''),
(813, 108, 'tve_revision_tve_content_more_found', ''),
(814, 108, 'tve_revision_tve_save_post', ''),
(815, 108, 'tve_revision_tve_custom_css', ''),
(816, 108, 'tve_revision_tve_user_custom_css', ''),
(817, 108, 'tve_revision_tve_page_events', ''),
(818, 108, 'tve_revision_tve_globals', ''),
(819, 108, 'tve_revision_tve_global_scripts', ''),
(820, 108, 'tve_revision_thrive_icon_pack', ''),
(821, 108, 'tve_revision_thrive_tcb_post_fonts', ''),
(822, 108, 'tve_revision_tve_has_masonry', ''),
(823, 108, 'tve_revision_tve_has_typefocus', ''),
(824, 108, 'tve_revision_tve_updated_post', ''),
(825, 108, 'tve_revision_tve_has_wistia_popover', ''),
(826, 109, '_edit_lock', '1587299911:1'),
(827, 110, 'tve_revision_tve_landing_page', ''),
(828, 110, 'tve_revision_tve_disable_theme_dependency', ''),
(829, 110, 'tve_revision_tve_content_before_more', ''),
(830, 110, 'tve_revision_tve_content_more_found', ''),
(831, 110, 'tve_revision_tve_save_post', ''),
(832, 110, 'tve_revision_tve_custom_css', ''),
(833, 110, 'tve_revision_tve_user_custom_css', ''),
(834, 110, 'tve_revision_tve_page_events', ''),
(835, 110, 'tve_revision_tve_globals', ''),
(836, 110, 'tve_revision_tve_global_scripts', ''),
(837, 110, 'tve_revision_thrive_icon_pack', ''),
(838, 110, 'tve_revision_thrive_tcb_post_fonts', ''),
(839, 110, 'tve_revision_tve_has_masonry', ''),
(840, 110, 'tve_revision_tve_has_typefocus', ''),
(841, 110, 'tve_revision_tve_updated_post', ''),
(842, 110, 'tve_revision_tve_has_wistia_popover', ''),
(843, 111, 'tve_revision_tve_landing_page', ''),
(844, 111, 'tve_revision_tve_disable_theme_dependency', ''),
(845, 111, 'tve_revision_tve_content_before_more', ''),
(846, 111, 'tve_revision_tve_content_more_found', ''),
(847, 111, 'tve_revision_tve_save_post', ''),
(848, 111, 'tve_revision_tve_custom_css', ''),
(849, 111, 'tve_revision_tve_user_custom_css', ''),
(850, 111, 'tve_revision_tve_page_events', ''),
(851, 111, 'tve_revision_tve_globals', ''),
(852, 111, 'tve_revision_tve_global_scripts', ''),
(853, 111, 'tve_revision_thrive_icon_pack', ''),
(854, 111, 'tve_revision_thrive_tcb_post_fonts', ''),
(855, 111, 'tve_revision_tve_has_masonry', ''),
(856, 111, 'tve_revision_tve_has_typefocus', ''),
(857, 111, 'tve_revision_tve_updated_post', ''),
(858, 111, 'tve_revision_tve_has_wistia_popover', ''),
(859, 109, '_wp_page_template', 'page-templates/listvideos-template.php'),
(861, 113, 'tve_revision_tve_landing_page', ''),
(862, 113, 'tve_revision_tve_disable_theme_dependency', ''),
(863, 113, 'tve_revision_tve_content_before_more', ''),
(864, 113, 'tve_revision_tve_content_more_found', ''),
(865, 113, 'tve_revision_tve_save_post', ''),
(866, 113, 'tve_revision_tve_custom_css', ''),
(867, 113, 'tve_revision_tve_user_custom_css', ''),
(868, 113, 'tve_revision_tve_page_events', ''),
(869, 113, 'tve_revision_tve_globals', ''),
(870, 113, 'tve_revision_tve_global_scripts', ''),
(871, 113, 'tve_revision_thrive_icon_pack', ''),
(872, 113, 'tve_revision_thrive_tcb_post_fonts', ''),
(873, 113, 'tve_revision_tve_has_masonry', ''),
(874, 113, 'tve_revision_tve_has_typefocus', ''),
(875, 113, 'tve_revision_tve_updated_post', ''),
(876, 113, 'tve_revision_tve_has_wistia_popover', ''),
(877, 86, '_wp_old_slug', 'test'),
(878, 114, 'tve_revision_tve_landing_page', ''),
(879, 114, 'tve_revision_tve_disable_theme_dependency', ''),
(880, 114, 'tve_revision_tve_content_before_more', ''),
(881, 114, 'tve_revision_tve_content_more_found', ''),
(882, 114, 'tve_revision_tve_save_post', ''),
(883, 114, 'tve_revision_tve_custom_css', ''),
(884, 114, 'tve_revision_tve_user_custom_css', ''),
(885, 114, 'tve_revision_tve_page_events', ''),
(886, 114, 'tve_revision_tve_globals', ''),
(887, 114, 'tve_revision_tve_global_scripts', ''),
(888, 114, 'tve_revision_thrive_icon_pack', ''),
(889, 114, 'tve_revision_thrive_tcb_post_fonts', ''),
(890, 114, 'tve_revision_tve_has_masonry', ''),
(891, 114, 'tve_revision_tve_has_typefocus', ''),
(892, 114, 'tve_revision_tve_updated_post', ''),
(893, 114, 'tve_revision_tve_has_wistia_popover', ''),
(894, 89, '_wp_old_slug', 'test-2'),
(895, 117, 'tve_revision_tve_landing_page', ''),
(896, 117, 'tve_revision_tve_disable_theme_dependency', ''),
(897, 117, 'tve_revision_tve_content_before_more', ''),
(898, 117, 'tve_revision_tve_content_more_found', ''),
(899, 117, 'tve_revision_tve_save_post', ''),
(900, 117, 'tve_revision_tve_custom_css', ''),
(901, 117, 'tve_revision_tve_user_custom_css', ''),
(902, 117, 'tve_revision_tve_page_events', ''),
(903, 117, 'tve_revision_tve_globals', ''),
(904, 117, 'tve_revision_tve_global_scripts', ''),
(905, 117, 'tve_revision_thrive_icon_pack', ''),
(906, 117, 'tve_revision_thrive_tcb_post_fonts', ''),
(907, 117, 'tve_revision_tve_has_masonry', ''),
(908, 117, 'tve_revision_tve_has_typefocus', ''),
(909, 117, 'tve_revision_tve_updated_post', ''),
(910, 117, 'tve_revision_tve_has_wistia_popover', ''),
(911, 86, 'video_length_', '4:76'),
(912, 86, '_video_length_', 'field_5e9c7221c0dd3'),
(913, 117, 'video_title', 'Test course'),
(914, 117, '_video_title', 'field_5e97ae5a532d7'),
(915, 117, 'video_link', 'https://youtu.be/X2gNr65invQ'),
(916, 117, '_video_link', 'field_5e97ae87532d8'),
(917, 117, 'short_description', 'Our Smooth Jazz Music can be used for relaxing, calm, romantic dinner, cooking, at work and relaxation. This relaxing  Jazz music best suited for study, work, cooking, like dinner music, background music, romantic music, relaxation music or music for stress relief. Let the beautiful, soothing and inspirational music help you relax!'),
(918, 117, '_short_description', 'field_5e97aed1532d9'),
(919, 117, 'video_length_', '4:76'),
(920, 117, '_video_length_', 'field_5e9c7221c0dd3'),
(921, 118, 'tve_revision_tve_landing_page', ''),
(922, 118, 'tve_revision_tve_disable_theme_dependency', ''),
(923, 118, 'tve_revision_tve_content_before_more', ''),
(924, 118, 'tve_revision_tve_content_more_found', ''),
(925, 118, 'tve_revision_tve_save_post', ''),
(926, 118, 'tve_revision_tve_custom_css', ''),
(927, 118, 'tve_revision_tve_user_custom_css', ''),
(928, 118, 'tve_revision_tve_page_events', ''),
(929, 118, 'tve_revision_tve_globals', ''),
(930, 118, 'tve_revision_tve_global_scripts', ''),
(931, 118, 'tve_revision_thrive_icon_pack', ''),
(932, 118, 'tve_revision_thrive_tcb_post_fonts', ''),
(933, 118, 'tve_revision_tve_has_masonry', ''),
(934, 118, 'tve_revision_tve_has_typefocus', ''),
(935, 118, 'tve_revision_tve_updated_post', ''),
(936, 118, 'tve_revision_tve_has_wistia_popover', ''),
(937, 89, 'video_length_', '10:26'),
(938, 89, '_video_length_', 'field_5e9c7221c0dd3'),
(939, 118, 'video_title', 'Test video 2'),
(940, 118, '_video_title', 'field_5e97ae5a532d7'),
(941, 118, 'video_link', 'https://youtu.be/m2HpqawZB6g'),
(942, 118, '_video_link', 'field_5e97ae87532d8'),
(943, 118, 'short_description', 'Our Smooth Jazz Music can be used for relaxing, calm, romantic dinner, cooking, at work and relaxation. This relaxing  Jazz music best suited for study, work, cooking, like dinner music, background music, romantic music, relaxation music or music for stress relief. Let the beautiful, soothing and inspirational music help you relax!'),
(944, 118, '_short_description', 'field_5e97aed1532d9'),
(945, 118, 'video_length_', '10:26'),
(946, 118, '_video_length_', 'field_5e9c7221c0dd3'),
(947, 119, '_edit_lock', '1587951969:1'),
(948, 120, 'tve_revision_tve_landing_page', ''),
(949, 120, 'tve_revision_tve_disable_theme_dependency', ''),
(950, 120, 'tve_revision_tve_content_before_more', ''),
(951, 120, 'tve_revision_tve_content_more_found', ''),
(952, 120, 'tve_revision_tve_save_post', ''),
(953, 120, 'tve_revision_tve_custom_css', ''),
(954, 120, 'tve_revision_tve_user_custom_css', ''),
(955, 120, 'tve_revision_tve_page_events', ''),
(956, 120, 'tve_revision_tve_globals', ''),
(957, 120, 'tve_revision_tve_global_scripts', ''),
(958, 120, 'tve_revision_thrive_icon_pack', ''),
(959, 120, 'tve_revision_thrive_tcb_post_fonts', ''),
(960, 120, 'tve_revision_tve_has_masonry', ''),
(961, 120, 'tve_revision_tve_has_typefocus', ''),
(962, 120, 'tve_revision_tve_updated_post', ''),
(963, 120, 'tve_revision_tve_has_wistia_popover', ''),
(964, 119, '_edit_last', '1'),
(965, 121, 'tve_revision_tve_landing_page', ''),
(966, 121, 'tve_revision_tve_disable_theme_dependency', ''),
(967, 121, 'tve_revision_tve_content_before_more', ''),
(968, 121, 'tve_revision_tve_content_more_found', ''),
(969, 121, 'tve_revision_tve_save_post', ''),
(970, 121, 'tve_revision_tve_custom_css', ''),
(971, 121, 'tve_revision_tve_user_custom_css', ''),
(972, 121, 'tve_revision_tve_page_events', ''),
(973, 121, 'tve_revision_tve_globals', ''),
(974, 121, 'tve_revision_tve_global_scripts', ''),
(975, 121, 'tve_revision_thrive_icon_pack', ''),
(976, 121, 'tve_revision_thrive_tcb_post_fonts', ''),
(977, 121, 'tve_revision_tve_has_masonry', ''),
(978, 121, 'tve_revision_tve_has_typefocus', ''),
(979, 121, 'tve_revision_tve_updated_post', ''),
(980, 121, 'tve_revision_tve_has_wistia_popover', ''),
(981, 119, 'wpcomplete', '[]'),
(982, 119, 'video_title', 'Targeting Kampagne'),
(983, 119, '_video_title', 'field_5e97ae5a532d7'),
(984, 119, 'video_link', 'https://player.vimeo.com/video/1844255'),
(985, 119, '_video_link', 'field_5e97ae87532d8'),
(986, 119, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod\r\n tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(987, 119, '_short_description', 'field_5e97aed1532d9'),
(988, 119, 'video_length_', '20:00'),
(989, 119, '_video_length_', 'field_5e9c7221c0dd3'),
(990, 121, 'video_title', 'Targeting Kampagne'),
(991, 121, '_video_title', 'field_5e97ae5a532d7'),
(992, 121, 'video_link', 'https://mistasms.com'),
(993, 121, '_video_link', 'field_5e97ae87532d8'),
(994, 121, 'short_description', 'hellooooo'),
(995, 121, '_short_description', 'field_5e97aed1532d9'),
(996, 121, 'video_length_', '20:00'),
(997, 121, '_video_length_', 'field_5e9c7221c0dd3'),
(998, 2, '_edit_lock', '1587318863:1'),
(1000, 123, 'tve_revision_tve_landing_page', ''),
(1001, 123, 'tve_revision_tve_disable_theme_dependency', ''),
(1002, 123, 'tve_revision_tve_content_before_more', ''),
(1003, 123, 'tve_revision_tve_content_more_found', ''),
(1004, 123, 'tve_revision_tve_save_post', ''),
(1005, 123, 'tve_revision_tve_custom_css', ''),
(1006, 123, 'tve_revision_tve_user_custom_css', ''),
(1007, 123, 'tve_revision_tve_page_events', ''),
(1008, 123, 'tve_revision_tve_globals', ''),
(1009, 123, 'tve_revision_tve_global_scripts', ''),
(1010, 123, 'tve_revision_thrive_icon_pack', ''),
(1011, 123, 'tve_revision_thrive_tcb_post_fonts', ''),
(1012, 123, 'tve_revision_tve_has_masonry', ''),
(1013, 123, 'tve_revision_tve_has_typefocus', ''),
(1014, 123, 'tve_revision_tve_updated_post', ''),
(1015, 123, 'tve_revision_tve_has_wistia_popover', ''),
(1016, 123, 'video_title', 'Targeting Kampagne'),
(1017, 123, '_video_title', 'field_5e97ae5a532d7'),
(1018, 123, 'video_link', 'https://mistasms.com'),
(1019, 123, '_video_link', 'field_5e97ae87532d8'),
(1020, 123, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod\r\n tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(1021, 123, '_short_description', 'field_5e97aed1532d9'),
(1022, 123, 'video_length_', '20:00'),
(1023, 123, '_video_length_', 'field_5e9c7221c0dd3'),
(1024, 124, 'tve_revision_tve_landing_page', ''),
(1025, 124, 'tve_revision_tve_disable_theme_dependency', ''),
(1026, 124, 'tve_revision_tve_content_before_more', ''),
(1027, 124, 'tve_revision_tve_content_more_found', ''),
(1028, 124, 'tve_revision_tve_save_post', ''),
(1029, 124, 'tve_revision_tve_custom_css', ''),
(1030, 124, 'tve_revision_tve_user_custom_css', ''),
(1031, 124, 'tve_revision_tve_page_events', ''),
(1032, 124, 'tve_revision_tve_globals', ''),
(1033, 124, 'tve_revision_tve_global_scripts', ''),
(1034, 124, 'tve_revision_thrive_icon_pack', ''),
(1035, 124, 'tve_revision_thrive_tcb_post_fonts', ''),
(1036, 124, 'tve_revision_tve_has_masonry', ''),
(1037, 124, 'tve_revision_tve_has_typefocus', ''),
(1038, 124, 'tve_revision_tve_updated_post', ''),
(1039, 124, 'tve_revision_tve_has_wistia_popover', ''),
(1040, 124, 'video_title', 'Targeting Kampagne'),
(1041, 124, '_video_title', 'field_5e97ae5a532d7'),
(1042, 124, 'video_link', 'https://player.vimeo.com/video/17588964'),
(1043, 124, '_video_link', 'field_5e97ae87532d8'),
(1044, 124, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod\r\n tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(1045, 124, '_short_description', 'field_5e97aed1532d9'),
(1046, 124, 'video_length_', '20:00'),
(1047, 124, '_video_length_', 'field_5e9c7221c0dd3'),
(1048, 125, '_edit_lock', '1588432715:1'),
(1049, 126, 'tve_revision_tve_landing_page', ''),
(1050, 126, 'tve_revision_tve_disable_theme_dependency', ''),
(1051, 126, 'tve_revision_tve_content_before_more', ''),
(1052, 126, 'tve_revision_tve_content_more_found', ''),
(1053, 126, 'tve_revision_tve_save_post', ''),
(1054, 126, 'tve_revision_tve_custom_css', ''),
(1055, 126, 'tve_revision_tve_user_custom_css', ''),
(1056, 126, 'tve_revision_tve_page_events', ''),
(1057, 126, 'tve_revision_tve_globals', ''),
(1058, 126, 'tve_revision_tve_global_scripts', ''),
(1059, 126, 'tve_revision_thrive_icon_pack', ''),
(1060, 126, 'tve_revision_thrive_tcb_post_fonts', ''),
(1061, 126, 'tve_revision_tve_has_masonry', ''),
(1062, 126, 'tve_revision_tve_has_typefocus', ''),
(1063, 126, 'tve_revision_tve_updated_post', ''),
(1064, 126, 'tve_revision_tve_has_wistia_popover', ''),
(1065, 125, '_edit_last', '1'),
(1066, 127, 'tve_revision_tve_landing_page', ''),
(1067, 127, 'tve_revision_tve_disable_theme_dependency', ''),
(1068, 127, 'tve_revision_tve_content_before_more', ''),
(1069, 127, 'tve_revision_tve_content_more_found', ''),
(1070, 127, 'tve_revision_tve_save_post', ''),
(1071, 127, 'tve_revision_tve_custom_css', ''),
(1072, 127, 'tve_revision_tve_user_custom_css', ''),
(1073, 127, 'tve_revision_tve_page_events', ''),
(1074, 127, 'tve_revision_tve_globals', ''),
(1075, 127, 'tve_revision_tve_global_scripts', ''),
(1076, 127, 'tve_revision_thrive_icon_pack', ''),
(1077, 127, 'tve_revision_thrive_tcb_post_fonts', ''),
(1078, 127, 'tve_revision_tve_has_masonry', ''),
(1079, 127, 'tve_revision_tve_has_typefocus', ''),
(1080, 127, 'tve_revision_tve_updated_post', ''),
(1081, 127, 'tve_revision_tve_has_wistia_popover', ''),
(1082, 125, 'video_title', 'Automatische kampaign'),
(1083, 125, '_video_title', 'field_5e97ae5a532d7'),
(1084, 125, 'video_link', 'https://player.vimeo.com/video/1844255'),
(1085, 125, '_video_link', 'field_5e97ae87532d8'),
(1086, 125, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(1087, 125, '_short_description', 'field_5e97aed1532d9'),
(1088, 125, 'video_length_', '2:59'),
(1089, 125, '_video_length_', 'field_5e9c7221c0dd3'),
(1090, 127, 'video_title', 'Automatische kampaign'),
(1091, 127, '_video_title', 'field_5e97ae5a532d7'),
(1092, 127, 'video_link', 'https://player.vimeo.com/video/17588965'),
(1093, 127, '_video_link', 'field_5e97ae87532d8'),
(1094, 127, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(1095, 127, '_short_description', 'field_5e97aed1532d9'),
(1096, 127, 'video_length_', '2:59'),
(1097, 127, '_video_length_', 'field_5e9c7221c0dd3'),
(1098, 128, 'tve_revision_tve_landing_page', ''),
(1099, 128, 'tve_revision_tve_disable_theme_dependency', ''),
(1100, 128, 'tve_revision_tve_content_before_more', ''),
(1101, 128, 'tve_revision_tve_content_more_found', ''),
(1102, 128, 'tve_revision_tve_save_post', ''),
(1103, 128, 'tve_revision_tve_custom_css', ''),
(1104, 128, 'tve_revision_tve_user_custom_css', ''),
(1105, 128, 'tve_revision_tve_page_events', ''),
(1106, 128, 'tve_revision_tve_globals', ''),
(1107, 128, 'tve_revision_tve_global_scripts', ''),
(1108, 128, 'tve_revision_thrive_icon_pack', ''),
(1109, 128, 'tve_revision_thrive_tcb_post_fonts', ''),
(1110, 128, 'tve_revision_tve_has_masonry', ''),
(1111, 128, 'tve_revision_tve_has_typefocus', ''),
(1112, 128, 'tve_revision_tve_updated_post', ''),
(1113, 128, 'tve_revision_tve_has_wistia_popover', ''),
(1114, 128, 'video_title', 'Automatische kampaign'),
(1115, 128, '_video_title', 'field_5e97ae5a532d7'),
(1116, 128, 'video_link', 'https://player.vimeo.com/video/17588965'),
(1117, 128, '_video_link', 'field_5e97ae87532d8'),
(1118, 128, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(1119, 128, '_short_description', 'field_5e97aed1532d9'),
(1120, 128, 'video_length_', '2:59'),
(1121, 128, '_video_length_', 'field_5e9c7221c0dd3'),
(1122, 129, 'tve_revision_tve_landing_page', ''),
(1123, 129, 'tve_revision_tve_disable_theme_dependency', ''),
(1124, 129, 'tve_revision_tve_content_before_more', ''),
(1125, 129, 'tve_revision_tve_content_more_found', ''),
(1126, 129, 'tve_revision_tve_save_post', ''),
(1127, 129, 'tve_revision_tve_custom_css', ''),
(1128, 129, 'tve_revision_tve_user_custom_css', ''),
(1129, 129, 'tve_revision_tve_page_events', ''),
(1130, 129, 'tve_revision_tve_globals', ''),
(1131, 129, 'tve_revision_tve_global_scripts', ''),
(1132, 129, 'tve_revision_thrive_icon_pack', ''),
(1133, 129, 'tve_revision_thrive_tcb_post_fonts', ''),
(1134, 129, 'tve_revision_tve_has_masonry', ''),
(1135, 129, 'tve_revision_tve_has_typefocus', ''),
(1136, 129, 'tve_revision_tve_updated_post', ''),
(1137, 129, 'tve_revision_tve_has_wistia_popover', ''),
(1138, 129, 'video_title', 'Automatische kampaign'),
(1139, 129, '_video_title', 'field_5e97ae5a532d7'),
(1140, 129, 'video_link', 'https://player.vimeo.com/video/17588965'),
(1141, 129, '_video_link', 'field_5e97ae87532d8'),
(1142, 129, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(1143, 129, '_short_description', 'field_5e97aed1532d9'),
(1144, 129, 'video_length_', '2:59'),
(1145, 129, '_video_length_', 'field_5e9c7221c0dd3'),
(1146, 130, 'tve_revision_tve_landing_page', ''),
(1147, 130, 'tve_revision_tve_disable_theme_dependency', ''),
(1148, 130, 'tve_revision_tve_content_before_more', ''),
(1149, 130, 'tve_revision_tve_content_more_found', ''),
(1150, 130, 'tve_revision_tve_save_post', ''),
(1151, 130, 'tve_revision_tve_custom_css', ''),
(1152, 130, 'tve_revision_tve_user_custom_css', ''),
(1153, 130, 'tve_revision_tve_page_events', ''),
(1154, 130, 'tve_revision_tve_globals', ''),
(1155, 130, 'tve_revision_tve_global_scripts', ''),
(1156, 130, 'tve_revision_thrive_icon_pack', ''),
(1157, 130, 'tve_revision_thrive_tcb_post_fonts', ''),
(1158, 130, 'tve_revision_tve_has_masonry', ''),
(1159, 130, 'tve_revision_tve_has_typefocus', ''),
(1160, 130, 'tve_revision_tve_updated_post', ''),
(1161, 130, 'tve_revision_tve_has_wistia_popover', ''),
(1162, 130, 'video_title', 'Test video 2'),
(1163, 130, '_video_title', 'field_5e97ae5a532d7'),
(1164, 130, 'video_link', 'https://player.vimeo.com/video/17588964'),
(1165, 130, '_video_link', 'field_5e97ae87532d8'),
(1166, 130, 'short_description', 'Our Smooth Jazz Music can be used for relaxing, calm, romantic dinner, cooking, at work and relaxation. This relaxing  Jazz music best suited for study, work, cooking, like dinner music, background music, romantic music, relaxation music or music for stress relief. Let the beautiful, soothing and inspirational music help you relax!'),
(1167, 130, '_short_description', 'field_5e97aed1532d9'),
(1168, 130, 'video_length_', '10:26'),
(1169, 130, '_video_length_', 'field_5e9c7221c0dd3'),
(1170, 131, 'tve_revision_tve_landing_page', ''),
(1171, 131, 'tve_revision_tve_disable_theme_dependency', ''),
(1172, 131, 'tve_revision_tve_content_before_more', ''),
(1173, 131, 'tve_revision_tve_content_more_found', ''),
(1174, 131, 'tve_revision_tve_save_post', ''),
(1175, 131, 'tve_revision_tve_custom_css', ''),
(1176, 131, 'tve_revision_tve_user_custom_css', ''),
(1177, 131, 'tve_revision_tve_page_events', ''),
(1178, 131, 'tve_revision_tve_globals', ''),
(1179, 131, 'tve_revision_tve_global_scripts', ''),
(1180, 131, 'tve_revision_thrive_icon_pack', ''),
(1181, 131, 'tve_revision_thrive_tcb_post_fonts', ''),
(1182, 131, 'tve_revision_tve_has_masonry', ''),
(1183, 131, 'tve_revision_tve_has_typefocus', ''),
(1184, 131, 'tve_revision_tve_updated_post', ''),
(1185, 131, 'tve_revision_tve_has_wistia_popover', ''),
(1186, 131, 'video_title', 'Test course'),
(1187, 131, '_video_title', 'field_5e97ae5a532d7'),
(1188, 131, 'video_link', 'https://player.vimeo.com/video/17588964'),
(1189, 131, '_video_link', 'field_5e97ae87532d8'),
(1190, 131, 'short_description', 'Our Smooth Jazz Music can be used for relaxing, calm, romantic dinner, cooking, at work and relaxation. This relaxing  Jazz music best suited for study, work, cooking, like dinner music, background music, romantic music, relaxation music or music for stress relief. Let the beautiful, soothing and inspirational music help you relax!'),
(1191, 131, '_short_description', 'field_5e97aed1532d9'),
(1192, 131, 'video_length_', '4:76'),
(1193, 131, '_video_length_', 'field_5e9c7221c0dd3'),
(1194, 132, 'tve_revision_tve_landing_page', ''),
(1195, 132, 'tve_revision_tve_disable_theme_dependency', ''),
(1196, 132, 'tve_revision_tve_content_before_more', ''),
(1197, 132, 'tve_revision_tve_content_more_found', ''),
(1198, 132, 'tve_revision_tve_save_post', ''),
(1199, 132, 'tve_revision_tve_custom_css', ''),
(1200, 132, 'tve_revision_tve_user_custom_css', ''),
(1201, 132, 'tve_revision_tve_page_events', ''),
(1202, 132, 'tve_revision_tve_globals', ''),
(1203, 132, 'tve_revision_tve_global_scripts', ''),
(1204, 132, 'tve_revision_thrive_icon_pack', ''),
(1205, 132, 'tve_revision_thrive_tcb_post_fonts', ''),
(1206, 132, 'tve_revision_tve_has_masonry', ''),
(1207, 132, 'tve_revision_tve_has_typefocus', ''),
(1208, 132, 'tve_revision_tve_updated_post', ''),
(1209, 132, 'tve_revision_tve_has_wistia_popover', ''),
(1210, 133, 'tve_revision_tve_landing_page', ''),
(1211, 133, 'tve_revision_tve_disable_theme_dependency', ''),
(1212, 133, 'tve_revision_tve_content_before_more', ''),
(1213, 133, 'tve_revision_tve_content_more_found', ''),
(1214, 133, 'tve_revision_tve_save_post', ''),
(1215, 133, 'tve_revision_tve_custom_css', ''),
(1216, 133, 'tve_revision_tve_user_custom_css', ''),
(1217, 133, 'tve_revision_tve_page_events', ''),
(1218, 133, 'tve_revision_tve_globals', ''),
(1219, 133, 'tve_revision_tve_global_scripts', ''),
(1220, 133, 'tve_revision_thrive_icon_pack', ''),
(1221, 133, 'tve_revision_thrive_tcb_post_fonts', ''),
(1222, 133, 'tve_revision_tve_has_masonry', ''),
(1223, 133, 'tve_revision_tve_has_typefocus', ''),
(1224, 133, 'tve_revision_tve_updated_post', ''),
(1225, 133, 'tve_revision_tve_has_wistia_popover', ''),
(1226, 133, 'video_title', 'Test course 33'),
(1227, 133, '_video_title', 'field_5e97ae5a532d7'),
(1228, 133, 'video_link', 'https://player.vimeo.com/video/17588964'),
(1229, 133, '_video_link', 'field_5e97ae87532d8'),
(1230, 133, 'short_description', 'hhhhhhhhh Our Smooth Jazz Music can be used for relaxing, calm, romantic dinner, cooking, at work and relaxation. This relaxing  Jazz music best suited for study, work, cooking, like dinner music, background music, romantic music, relaxation music or music for stress relief. Let the beautiful, soothing and inspirational music help you relax!'),
(1231, 133, '_short_description', 'field_5e97aed1532d9'),
(1232, 133, 'video_length_', '4:76'),
(1233, 133, '_video_length_', 'field_5e9c7221c0dd3'),
(1237, 137, '_edit_lock', '1587581293:1'),
(1238, 137, '_edit_last', '1'),
(1239, 139, '_edit_lock', '1587484195:1'),
(1240, 140, 'tve_revision_tve_landing_page', ''),
(1241, 140, 'tve_revision_tve_disable_theme_dependency', ''),
(1242, 140, 'tve_revision_tve_content_before_more', ''),
(1243, 140, 'tve_revision_tve_content_more_found', ''),
(1244, 140, 'tve_revision_tve_save_post', ''),
(1245, 140, 'tve_revision_tve_custom_css', ''),
(1246, 140, 'tve_revision_tve_user_custom_css', ''),
(1247, 140, 'tve_revision_tve_page_events', ''),
(1248, 140, 'tve_revision_tve_globals', ''),
(1249, 140, 'tve_revision_tve_global_scripts', ''),
(1250, 140, 'tve_revision_thrive_icon_pack', ''),
(1251, 140, 'tve_revision_thrive_tcb_post_fonts', ''),
(1252, 140, 'tve_revision_tve_has_masonry', ''),
(1253, 140, 'tve_revision_tve_has_typefocus', ''),
(1254, 140, 'tve_revision_tve_updated_post', ''),
(1255, 140, 'tve_revision_tve_has_wistia_popover', ''),
(1256, 139, '_edit_last', '1'),
(1257, 141, 'tve_revision_tve_landing_page', ''),
(1258, 141, 'tve_revision_tve_disable_theme_dependency', ''),
(1259, 141, 'tve_revision_tve_content_before_more', ''),
(1260, 141, 'tve_revision_tve_content_more_found', ''),
(1261, 141, 'tve_revision_tve_save_post', ''),
(1262, 141, 'tve_revision_tve_custom_css', ''),
(1263, 141, 'tve_revision_tve_user_custom_css', ''),
(1264, 141, 'tve_revision_tve_page_events', ''),
(1265, 141, 'tve_revision_tve_globals', ''),
(1266, 141, 'tve_revision_tve_global_scripts', ''),
(1267, 141, 'tve_revision_thrive_icon_pack', ''),
(1268, 141, 'tve_revision_thrive_tcb_post_fonts', ''),
(1269, 141, 'tve_revision_tve_has_masonry', ''),
(1270, 141, 'tve_revision_tve_has_typefocus', ''),
(1271, 141, 'tve_revision_tve_updated_post', ''),
(1272, 141, 'tve_revision_tve_has_wistia_popover', ''),
(1273, 139, 'task', '119'),
(1274, 139, '_task', 'field_5e9e0f0f9bab3'),
(1275, 141, 'task', '119'),
(1276, 141, '_task', 'field_5e9e0f0f9bab3'),
(1277, 142, 'tve_revision_tve_landing_page', ''),
(1278, 142, 'tve_revision_tve_disable_theme_dependency', ''),
(1279, 142, 'tve_revision_tve_content_before_more', ''),
(1280, 142, 'tve_revision_tve_content_more_found', ''),
(1281, 142, 'tve_revision_tve_save_post', ''),
(1282, 142, 'tve_revision_tve_custom_css', ''),
(1283, 142, 'tve_revision_tve_user_custom_css', ''),
(1284, 142, 'tve_revision_tve_page_events', ''),
(1285, 142, 'tve_revision_tve_globals', ''),
(1286, 142, 'tve_revision_tve_global_scripts', ''),
(1287, 142, 'tve_revision_thrive_icon_pack', ''),
(1288, 142, 'tve_revision_thrive_tcb_post_fonts', ''),
(1289, 142, 'tve_revision_tve_has_masonry', ''),
(1290, 142, 'tve_revision_tve_has_typefocus', ''),
(1291, 142, 'tve_revision_tve_updated_post', ''),
(1292, 142, 'tve_revision_tve_has_wistia_popover', ''),
(1293, 139, 'video_task', '119'),
(1294, 139, '_video_task', 'field_5e9e0f0f9bab3'),
(1295, 142, 'task', '119'),
(1296, 142, '_task', 'field_5e9e0f0f9bab3'),
(1297, 142, 'video_task', '119'),
(1298, 142, '_video_task', 'field_5e9e0f0f9bab3'),
(1299, 143, '_edit_lock', '1587504847:1'),
(1300, 144, 'tve_revision_tve_landing_page', ''),
(1301, 144, 'tve_revision_tve_disable_theme_dependency', ''),
(1302, 144, 'tve_revision_tve_content_before_more', ''),
(1303, 144, 'tve_revision_tve_content_more_found', ''),
(1304, 144, 'tve_revision_tve_save_post', ''),
(1305, 144, 'tve_revision_tve_custom_css', ''),
(1306, 144, 'tve_revision_tve_user_custom_css', ''),
(1307, 144, 'tve_revision_tve_page_events', ''),
(1308, 144, 'tve_revision_tve_globals', ''),
(1309, 144, 'tve_revision_tve_global_scripts', ''),
(1310, 144, 'tve_revision_thrive_icon_pack', ''),
(1311, 144, 'tve_revision_thrive_tcb_post_fonts', ''),
(1312, 144, 'tve_revision_tve_has_masonry', ''),
(1313, 144, 'tve_revision_tve_has_typefocus', ''),
(1314, 144, 'tve_revision_tve_updated_post', ''),
(1315, 144, 'tve_revision_tve_has_wistia_popover', ''),
(1316, 143, '_edit_last', '1'),
(1317, 145, 'tve_revision_tve_landing_page', ''),
(1318, 145, 'tve_revision_tve_disable_theme_dependency', ''),
(1319, 145, 'tve_revision_tve_content_before_more', ''),
(1320, 145, 'tve_revision_tve_content_more_found', ''),
(1321, 145, 'tve_revision_tve_save_post', ''),
(1322, 145, 'tve_revision_tve_custom_css', ''),
(1323, 145, 'tve_revision_tve_user_custom_css', ''),
(1324, 145, 'tve_revision_tve_page_events', ''),
(1325, 145, 'tve_revision_tve_globals', ''),
(1326, 145, 'tve_revision_tve_global_scripts', ''),
(1327, 145, 'tve_revision_thrive_icon_pack', ''),
(1328, 145, 'tve_revision_thrive_tcb_post_fonts', ''),
(1329, 145, 'tve_revision_tve_has_masonry', ''),
(1330, 145, 'tve_revision_tve_has_typefocus', ''),
(1331, 145, 'tve_revision_tve_updated_post', ''),
(1332, 145, 'tve_revision_tve_has_wistia_popover', ''),
(1333, 143, 'video_task', '119'),
(1334, 143, '_video_task', 'field_5e9e0f0f9bab3'),
(1335, 145, 'video_task', '119'),
(1336, 145, '_video_task', 'field_5e9e0f0f9bab3'),
(1337, 146, '_edit_lock', '1587581133:1'),
(1338, 147, 'tve_revision_tve_landing_page', ''),
(1339, 147, 'tve_revision_tve_disable_theme_dependency', ''),
(1340, 147, 'tve_revision_tve_content_before_more', ''),
(1341, 147, 'tve_revision_tve_content_more_found', ''),
(1342, 147, 'tve_revision_tve_save_post', ''),
(1343, 147, 'tve_revision_tve_custom_css', ''),
(1344, 147, 'tve_revision_tve_user_custom_css', ''),
(1345, 147, 'tve_revision_tve_page_events', ''),
(1346, 147, 'tve_revision_tve_globals', ''),
(1347, 147, 'tve_revision_tve_global_scripts', ''),
(1348, 147, 'tve_revision_thrive_icon_pack', ''),
(1349, 147, 'tve_revision_thrive_tcb_post_fonts', ''),
(1350, 147, 'tve_revision_tve_has_masonry', ''),
(1351, 147, 'tve_revision_tve_has_typefocus', ''),
(1352, 147, 'tve_revision_tve_updated_post', ''),
(1353, 147, 'tve_revision_tve_has_wistia_popover', ''),
(1354, 146, '_edit_last', '1'),
(1355, 148, 'tve_revision_tve_landing_page', ''),
(1356, 148, 'tve_revision_tve_disable_theme_dependency', ''),
(1357, 148, 'tve_revision_tve_content_before_more', ''),
(1358, 148, 'tve_revision_tve_content_more_found', ''),
(1359, 148, 'tve_revision_tve_save_post', ''),
(1360, 148, 'tve_revision_tve_custom_css', ''),
(1361, 148, 'tve_revision_tve_user_custom_css', ''),
(1362, 148, 'tve_revision_tve_page_events', ''),
(1363, 148, 'tve_revision_tve_globals', ''),
(1364, 148, 'tve_revision_tve_global_scripts', ''),
(1365, 148, 'tve_revision_thrive_icon_pack', ''),
(1366, 148, 'tve_revision_thrive_tcb_post_fonts', ''),
(1367, 148, 'tve_revision_tve_has_masonry', ''),
(1368, 148, 'tve_revision_tve_has_typefocus', ''),
(1369, 148, 'tve_revision_tve_updated_post', ''),
(1370, 148, 'tve_revision_tve_has_wistia_popover', ''),
(1371, 146, 'video_task', '89'),
(1372, 146, '_video_task', 'field_5e9e0f0f9bab3'),
(1373, 148, 'video_task', '89'),
(1374, 148, '_video_task', 'field_5e9e0f0f9bab3'),
(1378, 153, 'tve_revision_tve_landing_page', ''),
(1379, 153, 'tve_revision_tve_disable_theme_dependency', ''),
(1380, 153, 'tve_revision_tve_content_before_more', ''),
(1381, 153, 'tve_revision_tve_content_more_found', ''),
(1382, 153, 'tve_revision_tve_save_post', ''),
(1383, 153, 'tve_revision_tve_custom_css', ''),
(1384, 153, 'tve_revision_tve_user_custom_css', ''),
(1385, 153, 'tve_revision_tve_page_events', ''),
(1386, 153, 'tve_revision_tve_globals', ''),
(1387, 153, 'tve_revision_tve_global_scripts', ''),
(1388, 153, 'tve_revision_thrive_icon_pack', ''),
(1389, 153, 'tve_revision_thrive_tcb_post_fonts', ''),
(1390, 153, 'tve_revision_tve_has_masonry', ''),
(1391, 153, 'tve_revision_tve_has_typefocus', ''),
(1392, 153, 'tve_revision_tve_updated_post', ''),
(1393, 153, 'tve_revision_tve_has_wistia_popover', ''),
(1394, 139, 'completed', '1'),
(1395, 139, '_completed', 'field_5e9e2af07c1ea'),
(1396, 153, 'task', '119'),
(1397, 153, '_task', 'field_5e9e0f0f9bab3'),
(1398, 153, 'video_task', '119'),
(1399, 153, '_video_task', 'field_5e9e0f0f9bab3'),
(1400, 153, 'completed', '1'),
(1401, 153, '_completed', 'field_5e9e2af07c1ea'),
(1402, 154, 'tve_revision_tve_landing_page', ''),
(1403, 154, 'tve_revision_tve_disable_theme_dependency', ''),
(1404, 154, 'tve_revision_tve_content_before_more', ''),
(1405, 154, 'tve_revision_tve_content_more_found', ''),
(1406, 154, 'tve_revision_tve_save_post', ''),
(1407, 154, 'tve_revision_tve_custom_css', ''),
(1408, 154, 'tve_revision_tve_user_custom_css', ''),
(1409, 154, 'tve_revision_tve_page_events', ''),
(1410, 154, 'tve_revision_tve_globals', ''),
(1411, 154, 'tve_revision_tve_global_scripts', ''),
(1412, 154, 'tve_revision_thrive_icon_pack', ''),
(1413, 154, 'tve_revision_thrive_tcb_post_fonts', ''),
(1414, 154, 'tve_revision_tve_has_masonry', ''),
(1415, 154, 'tve_revision_tve_has_typefocus', ''),
(1416, 154, 'tve_revision_tve_updated_post', ''),
(1417, 154, 'tve_revision_tve_has_wistia_popover', ''),
(1418, 143, 'completed', '1'),
(1419, 143, '_completed', 'field_5e9e2af07c1ea'),
(1420, 154, 'video_task', '119'),
(1421, 154, '_video_task', 'field_5e9e0f0f9bab3'),
(1422, 154, 'completed', '1'),
(1423, 154, '_completed', 'field_5e9e2af07c1ea'),
(1424, 143, 'mask_completed', '2'),
(1425, 143, '_mask_completed', 'field_5e9e2af07c1ea'),
(1426, 154, 'mask_completed', '0'),
(1427, 154, '_mask_completed', 'field_5e9e2af07c1ea'),
(1428, 155, 'tve_revision_tve_landing_page', ''),
(1429, 155, 'tve_revision_tve_disable_theme_dependency', ''),
(1430, 155, 'tve_revision_tve_content_before_more', ''),
(1431, 155, 'tve_revision_tve_content_more_found', ''),
(1432, 155, 'tve_revision_tve_save_post', ''),
(1433, 155, 'tve_revision_tve_custom_css', ''),
(1434, 155, 'tve_revision_tve_user_custom_css', ''),
(1435, 155, 'tve_revision_tve_page_events', ''),
(1436, 155, 'tve_revision_tve_globals', ''),
(1437, 155, 'tve_revision_tve_global_scripts', ''),
(1438, 155, 'tve_revision_thrive_icon_pack', ''),
(1439, 155, 'tve_revision_thrive_tcb_post_fonts', ''),
(1440, 155, 'tve_revision_tve_has_masonry', ''),
(1441, 155, 'tve_revision_tve_has_typefocus', ''),
(1442, 155, 'tve_revision_tve_updated_post', ''),
(1443, 155, 'tve_revision_tve_has_wistia_popover', ''),
(1444, 155, 'video_task', '119'),
(1445, 155, '_video_task', 'field_5e9e0f0f9bab3'),
(1446, 155, 'completed', '1'),
(1447, 155, '_completed', 'field_5e9e2af07c1ea'),
(1448, 155, 'mask_completed', '1'),
(1449, 155, '_mask_completed', 'field_5e9e2af07c1ea'),
(1450, 139, 'mask_completed', '1'),
(1451, 139, '_mask_completed', 'field_5e9e2af07c1ea'),
(1452, 153, 'mask_completed', '0'),
(1453, 153, '_mask_completed', 'field_5e9e2af07c1ea'),
(1454, 156, 'tve_revision_tve_landing_page', ''),
(1455, 156, 'tve_revision_tve_disable_theme_dependency', ''),
(1456, 156, 'tve_revision_tve_content_before_more', ''),
(1457, 156, 'tve_revision_tve_content_more_found', ''),
(1458, 156, 'tve_revision_tve_save_post', ''),
(1459, 156, 'tve_revision_tve_custom_css', ''),
(1460, 156, 'tve_revision_tve_user_custom_css', ''),
(1461, 156, 'tve_revision_tve_page_events', ''),
(1462, 156, 'tve_revision_tve_globals', ''),
(1463, 156, 'tve_revision_tve_global_scripts', ''),
(1464, 156, 'tve_revision_thrive_icon_pack', ''),
(1465, 156, 'tve_revision_thrive_tcb_post_fonts', ''),
(1466, 156, 'tve_revision_tve_has_masonry', ''),
(1467, 156, 'tve_revision_tve_has_typefocus', ''),
(1468, 156, 'tve_revision_tve_updated_post', ''),
(1469, 156, 'tve_revision_tve_has_wistia_popover', ''),
(1470, 156, 'task', '119'),
(1471, 156, '_task', 'field_5e9e0f0f9bab3'),
(1472, 156, 'video_task', '119'),
(1473, 156, '_video_task', 'field_5e9e0f0f9bab3'),
(1474, 156, 'completed', '1'),
(1475, 156, '_completed', 'field_5e9e2af07c1ea'),
(1476, 156, 'mask_completed', '8'),
(1477, 156, '_mask_completed', 'field_5e9e2af07c1ea'),
(1478, 157, 'tve_revision_tve_landing_page', ''),
(1479, 157, 'tve_revision_tve_disable_theme_dependency', ''),
(1480, 157, 'tve_revision_tve_content_before_more', ''),
(1481, 157, 'tve_revision_tve_content_more_found', ''),
(1482, 157, 'tve_revision_tve_save_post', ''),
(1483, 157, 'tve_revision_tve_custom_css', ''),
(1484, 157, 'tve_revision_tve_user_custom_css', ''),
(1485, 157, 'tve_revision_tve_page_events', ''),
(1486, 157, 'tve_revision_tve_globals', ''),
(1487, 157, 'tve_revision_tve_global_scripts', ''),
(1488, 157, 'tve_revision_thrive_icon_pack', ''),
(1489, 157, 'tve_revision_thrive_tcb_post_fonts', ''),
(1490, 157, 'tve_revision_tve_has_masonry', ''),
(1491, 157, 'tve_revision_tve_has_typefocus', ''),
(1492, 157, 'tve_revision_tve_updated_post', ''),
(1493, 157, 'tve_revision_tve_has_wistia_popover', ''),
(1494, 157, 'task', '119'),
(1495, 157, '_task', 'field_5e9e0f0f9bab3'),
(1496, 157, 'video_task', '119'),
(1497, 157, '_video_task', 'field_5e9e0f0f9bab3'),
(1498, 157, 'completed', '1'),
(1499, 157, '_completed', 'field_5e9e2af07c1ea'),
(1500, 157, 'mask_completed', '1'),
(1501, 157, '_mask_completed', 'field_5e9e2af07c1ea'),
(1502, 158, 'tve_revision_tve_landing_page', ''),
(1503, 158, 'tve_revision_tve_disable_theme_dependency', ''),
(1504, 158, 'tve_revision_tve_content_before_more', ''),
(1505, 158, 'tve_revision_tve_content_more_found', ''),
(1506, 158, 'tve_revision_tve_save_post', ''),
(1507, 158, 'tve_revision_tve_custom_css', ''),
(1508, 158, 'tve_revision_tve_user_custom_css', ''),
(1509, 158, 'tve_revision_tve_page_events', ''),
(1510, 158, 'tve_revision_tve_globals', ''),
(1511, 158, 'tve_revision_tve_global_scripts', ''),
(1512, 158, 'tve_revision_thrive_icon_pack', ''),
(1513, 158, 'tve_revision_thrive_tcb_post_fonts', ''),
(1514, 158, 'tve_revision_tve_has_masonry', ''),
(1515, 158, 'tve_revision_tve_has_typefocus', ''),
(1516, 158, 'tve_revision_tve_updated_post', ''),
(1517, 158, 'tve_revision_tve_has_wistia_popover', ''),
(1518, 158, 'video_task', '119'),
(1519, 158, '_video_task', 'field_5e9e0f0f9bab3'),
(1520, 158, 'completed', '1'),
(1521, 158, '_completed', 'field_5e9e2af07c1ea'),
(1522, 158, 'mask_completed', '2'),
(1523, 158, '_mask_completed', 'field_5e9e2af07c1ea'),
(1524, 159, 'tve_revision_tve_landing_page', ''),
(1525, 159, 'tve_revision_tve_disable_theme_dependency', ''),
(1526, 159, 'tve_revision_tve_content_before_more', ''),
(1527, 159, 'tve_revision_tve_content_more_found', ''),
(1528, 159, 'tve_revision_tve_save_post', ''),
(1529, 159, 'tve_revision_tve_custom_css', ''),
(1530, 159, 'tve_revision_tve_user_custom_css', ''),
(1531, 159, 'tve_revision_tve_page_events', ''),
(1532, 159, 'tve_revision_tve_globals', ''),
(1533, 159, 'tve_revision_tve_global_scripts', ''),
(1534, 159, 'tve_revision_thrive_icon_pack', ''),
(1535, 159, 'tve_revision_thrive_tcb_post_fonts', ''),
(1536, 159, 'tve_revision_tve_has_masonry', ''),
(1537, 159, 'tve_revision_tve_has_typefocus', ''),
(1538, 159, 'tve_revision_tve_updated_post', ''),
(1539, 159, 'tve_revision_tve_has_wistia_popover', ''),
(1540, 139, 'mark_as_completed', 'pending'),
(1541, 139, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(1542, 159, 'task', '119'),
(1543, 159, '_task', 'field_5e9e0f0f9bab3'),
(1544, 159, 'video_task', '119'),
(1545, 159, '_video_task', 'field_5e9e0f0f9bab3'),
(1546, 159, 'completed', '1'),
(1547, 159, '_completed', 'field_5e9e2af07c1ea'),
(1548, 159, 'mask_completed', '1'),
(1549, 159, '_mask_completed', 'field_5e9e2af07c1ea'),
(1550, 159, 'mark_as_completed', '1'),
(1551, 159, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(1552, 160, 'tve_revision_tve_landing_page', ''),
(1553, 160, 'tve_revision_tve_disable_theme_dependency', ''),
(1554, 160, 'tve_revision_tve_content_before_more', ''),
(1555, 160, 'tve_revision_tve_content_more_found', ''),
(1556, 160, 'tve_revision_tve_save_post', ''),
(1557, 160, 'tve_revision_tve_custom_css', ''),
(1558, 160, 'tve_revision_tve_user_custom_css', ''),
(1559, 160, 'tve_revision_tve_page_events', ''),
(1560, 160, 'tve_revision_tve_globals', ''),
(1561, 160, 'tve_revision_tve_global_scripts', ''),
(1562, 160, 'tve_revision_thrive_icon_pack', ''),
(1563, 160, 'tve_revision_thrive_tcb_post_fonts', ''),
(1564, 160, 'tve_revision_tve_has_masonry', ''),
(1565, 160, 'tve_revision_tve_has_typefocus', ''),
(1566, 160, 'tve_revision_tve_updated_post', ''),
(1567, 160, 'tve_revision_tve_has_wistia_popover', ''),
(1568, 160, 'task', '119'),
(1569, 160, '_task', 'field_5e9e0f0f9bab3'),
(1570, 160, 'video_task', '119'),
(1571, 160, '_video_task', 'field_5e9e0f0f9bab3'),
(1572, 160, 'completed', '1'),
(1573, 160, '_completed', 'field_5e9e2af07c1ea'),
(1574, 160, 'mask_completed', '1'),
(1575, 160, '_mask_completed', 'field_5e9e2af07c1ea'),
(1576, 160, 'mark_as_completed', '2'),
(1577, 160, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(1578, 161, 'tve_revision_tve_landing_page', ''),
(1579, 161, 'tve_revision_tve_disable_theme_dependency', ''),
(1580, 161, 'tve_revision_tve_content_before_more', ''),
(1581, 161, 'tve_revision_tve_content_more_found', ''),
(1582, 161, 'tve_revision_tve_save_post', ''),
(1583, 161, 'tve_revision_tve_custom_css', ''),
(1584, 161, 'tve_revision_tve_user_custom_css', ''),
(1585, 161, 'tve_revision_tve_page_events', ''),
(1586, 161, 'tve_revision_tve_globals', ''),
(1587, 161, 'tve_revision_tve_global_scripts', ''),
(1588, 161, 'tve_revision_thrive_icon_pack', ''),
(1589, 161, 'tve_revision_thrive_tcb_post_fonts', ''),
(1590, 161, 'tve_revision_tve_has_masonry', ''),
(1591, 161, 'tve_revision_tve_has_typefocus', ''),
(1592, 161, 'tve_revision_tve_updated_post', ''),
(1593, 161, 'tve_revision_tve_has_wistia_popover', ''),
(1594, 143, 'mark_as_completed', 'completed'),
(1595, 143, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(1596, 161, 'video_task', '119'),
(1597, 161, '_video_task', 'field_5e9e0f0f9bab3'),
(1598, 161, 'completed', '1'),
(1599, 161, '_completed', 'field_5e9e2af07c1ea'),
(1600, 161, 'mask_completed', '2'),
(1601, 161, '_mask_completed', 'field_5e9e2af07c1ea'),
(1602, 161, 'mark_as_completed', '7'),
(1603, 161, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(1604, 143, 'mark_as_complete', '1'),
(1605, 163, 'tve_revision_tve_landing_page', ''),
(1606, 163, 'tve_revision_tve_disable_theme_dependency', ''),
(1607, 163, 'tve_revision_tve_content_before_more', ''),
(1608, 163, 'tve_revision_tve_content_more_found', ''),
(1609, 163, 'tve_revision_tve_save_post', ''),
(1610, 163, 'tve_revision_tve_custom_css', ''),
(1611, 163, 'tve_revision_tve_user_custom_css', ''),
(1612, 163, 'tve_revision_tve_page_events', ''),
(1613, 163, 'tve_revision_tve_globals', ''),
(1614, 163, 'tve_revision_tve_global_scripts', ''),
(1615, 163, 'tve_revision_thrive_icon_pack', ''),
(1616, 163, 'tve_revision_thrive_tcb_post_fonts', ''),
(1617, 163, 'tve_revision_tve_has_masonry', ''),
(1618, 163, 'tve_revision_tve_has_typefocus', ''),
(1619, 163, 'tve_revision_tve_updated_post', ''),
(1620, 163, 'tve_revision_tve_has_wistia_popover', ''),
(1621, 146, 'mark_as_completed', 'completed'),
(1622, 146, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(1623, 163, 'video_task', '89'),
(1624, 163, '_video_task', 'field_5e9e0f0f9bab3'),
(1625, 163, 'mark_as_completed', 'completed'),
(1626, 163, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(1627, 164, 'tve_revision_tve_landing_page', ''),
(1628, 164, 'tve_revision_tve_disable_theme_dependency', ''),
(1629, 164, 'tve_revision_tve_content_before_more', ''),
(1630, 164, 'tve_revision_tve_content_more_found', ''),
(1631, 164, 'tve_revision_tve_save_post', ''),
(1632, 164, 'tve_revision_tve_custom_css', ''),
(1633, 164, 'tve_revision_tve_user_custom_css', ''),
(1634, 164, 'tve_revision_tve_page_events', ''),
(1635, 164, 'tve_revision_tve_globals', ''),
(1636, 164, 'tve_revision_tve_global_scripts', ''),
(1637, 164, 'tve_revision_thrive_icon_pack', ''),
(1638, 164, 'tve_revision_thrive_tcb_post_fonts', ''),
(1639, 164, 'tve_revision_tve_has_masonry', ''),
(1640, 164, 'tve_revision_tve_has_typefocus', ''),
(1641, 164, 'tve_revision_tve_updated_post', ''),
(1642, 164, 'tve_revision_tve_has_wistia_popover', ''),
(1643, 164, 'video_task', '89'),
(1644, 164, '_video_task', 'field_5e9e0f0f9bab3'),
(1645, 164, 'mark_as_completed', 'pending'),
(1646, 164, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(1647, 165, 'tve_revision_tve_landing_page', ''),
(1648, 165, 'tve_revision_tve_disable_theme_dependency', ''),
(1649, 165, 'tve_revision_tve_content_before_more', ''),
(1650, 165, 'tve_revision_tve_content_more_found', ''),
(1651, 165, 'tve_revision_tve_save_post', ''),
(1652, 165, 'tve_revision_tve_custom_css', ''),
(1653, 165, 'tve_revision_tve_user_custom_css', ''),
(1654, 165, 'tve_revision_tve_page_events', ''),
(1655, 165, 'tve_revision_tve_globals', ''),
(1656, 165, 'tve_revision_tve_global_scripts', ''),
(1657, 165, 'tve_revision_thrive_icon_pack', ''),
(1658, 165, 'tve_revision_thrive_tcb_post_fonts', ''),
(1659, 165, 'tve_revision_tve_has_masonry', ''),
(1660, 165, 'tve_revision_tve_has_typefocus', ''),
(1661, 165, 'tve_revision_tve_updated_post', ''),
(1662, 165, 'tve_revision_tve_has_wistia_popover', ''),
(1663, 119, 'mark_as_watched', 'completed'),
(1664, 119, '_mark_as_watched', 'field_5e9f67bdac35a'),
(1665, 165, 'video_title', 'Targeting Kampagne'),
(1666, 165, '_video_title', 'field_5e97ae5a532d7'),
(1667, 165, 'video_link', 'https://player.vimeo.com/video/17588964'),
(1668, 165, '_video_link', 'field_5e97ae87532d8');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1669, 165, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod\r\n tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(1670, 165, '_short_description', 'field_5e97aed1532d9'),
(1671, 165, 'video_length_', '20:00'),
(1672, 165, '_video_length_', 'field_5e9c7221c0dd3'),
(1673, 165, 'mark_as_watched', 'pending123'),
(1674, 165, '_mark_as_watched', 'field_5e9f67bdac35a'),
(1675, 166, 'tve_revision_tve_landing_page', ''),
(1676, 166, 'tve_revision_tve_disable_theme_dependency', ''),
(1677, 166, 'tve_revision_tve_content_before_more', ''),
(1678, 166, 'tve_revision_tve_content_more_found', ''),
(1679, 166, 'tve_revision_tve_save_post', ''),
(1680, 166, 'tve_revision_tve_custom_css', ''),
(1681, 166, 'tve_revision_tve_user_custom_css', ''),
(1682, 166, 'tve_revision_tve_page_events', ''),
(1683, 166, 'tve_revision_tve_globals', ''),
(1684, 166, 'tve_revision_tve_global_scripts', ''),
(1685, 166, 'tve_revision_thrive_icon_pack', ''),
(1686, 166, 'tve_revision_thrive_tcb_post_fonts', ''),
(1687, 166, 'tve_revision_tve_has_masonry', ''),
(1688, 166, 'tve_revision_tve_has_typefocus', ''),
(1689, 166, 'tve_revision_tve_updated_post', ''),
(1690, 166, 'tve_revision_tve_has_wistia_popover', ''),
(1691, 166, 'video_title', 'Targeting Kampagne'),
(1692, 166, '_video_title', 'field_5e97ae5a532d7'),
(1693, 166, 'video_link', 'https://player.vimeo.com/video/17588964'),
(1694, 166, '_video_link', 'field_5e97ae87532d8'),
(1695, 166, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod\r\n tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(1696, 166, '_short_description', 'field_5e97aed1532d9'),
(1697, 166, 'video_length_', '20:00'),
(1698, 166, '_video_length_', 'field_5e9c7221c0dd3'),
(1699, 166, 'mark_as_watched', 'completed'),
(1700, 166, '_mark_as_watched', 'field_5e9f67bdac35a'),
(1701, 143, 'mark_as_watched', 'pending'),
(1702, 139, 'mark_as_watched', 'pending'),
(1703, 167, '_edit_lock', '1588086884:1'),
(1704, 168, 'tve_revision_tve_landing_page', ''),
(1705, 168, 'tve_revision_tve_disable_theme_dependency', ''),
(1706, 168, 'tve_revision_tve_content_before_more', ''),
(1707, 168, 'tve_revision_tve_content_more_found', ''),
(1708, 168, 'tve_revision_tve_save_post', ''),
(1709, 168, 'tve_revision_tve_custom_css', ''),
(1710, 168, 'tve_revision_tve_user_custom_css', ''),
(1711, 168, 'tve_revision_tve_page_events', ''),
(1712, 168, 'tve_revision_tve_globals', ''),
(1713, 168, 'tve_revision_tve_global_scripts', ''),
(1714, 168, 'tve_revision_thrive_icon_pack', ''),
(1715, 168, 'tve_revision_thrive_tcb_post_fonts', ''),
(1716, 168, 'tve_revision_tve_has_masonry', ''),
(1717, 168, 'tve_revision_tve_has_typefocus', ''),
(1718, 168, 'tve_revision_tve_updated_post', ''),
(1719, 168, 'tve_revision_tve_has_wistia_popover', ''),
(1720, 167, '_edit_last', '1'),
(1721, 169, 'tve_revision_tve_landing_page', ''),
(1722, 169, 'tve_revision_tve_disable_theme_dependency', ''),
(1723, 169, 'tve_revision_tve_content_before_more', ''),
(1724, 169, 'tve_revision_tve_content_more_found', ''),
(1725, 169, 'tve_revision_tve_save_post', ''),
(1726, 169, 'tve_revision_tve_custom_css', ''),
(1727, 169, 'tve_revision_tve_user_custom_css', ''),
(1728, 169, 'tve_revision_tve_page_events', ''),
(1729, 169, 'tve_revision_tve_globals', ''),
(1730, 169, 'tve_revision_tve_global_scripts', ''),
(1731, 169, 'tve_revision_thrive_icon_pack', ''),
(1732, 169, 'tve_revision_thrive_tcb_post_fonts', ''),
(1733, 169, 'tve_revision_tve_has_masonry', ''),
(1734, 169, 'tve_revision_tve_has_typefocus', ''),
(1735, 169, 'tve_revision_tve_updated_post', ''),
(1736, 169, 'tve_revision_tve_has_wistia_popover', ''),
(1737, 167, 'video_task', '119'),
(1738, 167, '_video_task', 'field_5e9e0f0f9bab3'),
(1739, 167, 'mark_as_completed', 'pending'),
(1740, 167, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(1741, 169, 'video_task', '119'),
(1742, 169, '_video_task', 'field_5e9e0f0f9bab3'),
(1743, 169, 'mark_as_completed', 'pending'),
(1744, 169, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(1745, 167, 'mark_as_watched', 'pending'),
(1747, 171, '_edit_lock', '1587857068:1'),
(1748, 172, 'tve_revision_tve_landing_page', ''),
(1749, 172, 'tve_revision_tve_disable_theme_dependency', ''),
(1750, 172, 'tve_revision_tve_content_before_more', ''),
(1751, 172, 'tve_revision_tve_content_more_found', ''),
(1752, 172, 'tve_revision_tve_save_post', ''),
(1753, 172, 'tve_revision_tve_custom_css', ''),
(1754, 172, 'tve_revision_tve_user_custom_css', ''),
(1755, 172, 'tve_revision_tve_page_events', ''),
(1756, 172, 'tve_revision_tve_globals', ''),
(1757, 172, 'tve_revision_tve_global_scripts', ''),
(1758, 172, 'tve_revision_thrive_icon_pack', ''),
(1759, 172, 'tve_revision_thrive_tcb_post_fonts', ''),
(1760, 172, 'tve_revision_tve_has_masonry', ''),
(1761, 172, 'tve_revision_tve_has_typefocus', ''),
(1762, 172, 'tve_revision_tve_updated_post', ''),
(1763, 172, 'tve_revision_tve_has_wistia_popover', ''),
(1764, 171, '_edit_last', '1'),
(1765, 173, 'tve_revision_tve_landing_page', ''),
(1766, 173, 'tve_revision_tve_disable_theme_dependency', ''),
(1767, 173, 'tve_revision_tve_content_before_more', ''),
(1768, 173, 'tve_revision_tve_content_more_found', ''),
(1769, 173, 'tve_revision_tve_save_post', ''),
(1770, 173, 'tve_revision_tve_custom_css', ''),
(1771, 173, 'tve_revision_tve_user_custom_css', ''),
(1772, 173, 'tve_revision_tve_page_events', ''),
(1773, 173, 'tve_revision_tve_globals', ''),
(1774, 173, 'tve_revision_tve_global_scripts', ''),
(1775, 173, 'tve_revision_thrive_icon_pack', ''),
(1776, 173, 'tve_revision_thrive_tcb_post_fonts', ''),
(1777, 173, 'tve_revision_tve_has_masonry', ''),
(1778, 173, 'tve_revision_tve_has_typefocus', ''),
(1779, 173, 'tve_revision_tve_updated_post', ''),
(1780, 173, 'tve_revision_tve_has_wistia_popover', ''),
(1781, 171, 'video_task', '125'),
(1782, 171, '_video_task', 'field_5e9e0f0f9bab3'),
(1783, 171, 'mark_as_completed', 'completed'),
(1784, 171, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(1785, 173, 'video_task', '119'),
(1786, 173, '_video_task', 'field_5e9e0f0f9bab3'),
(1787, 173, 'mark_as_completed', 'pending'),
(1788, 173, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(1789, 174, '_wp_attached_file', '2020/04/Roland-Logo.jpg'),
(1790, 174, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:150;s:6:\"height\";i:48;s:4:\"file\";s:23:\"2020/04/Roland-Logo.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Alex Bwanakweli\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1587776369\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1791, 175, '_wp_trash_meta_status', 'publish'),
(1792, 175, '_wp_trash_meta_time', '1587765717'),
(1793, 176, 'tve_revision_tve_landing_page', ''),
(1794, 176, 'tve_revision_tve_disable_theme_dependency', ''),
(1795, 176, 'tve_revision_tve_content_before_more', ''),
(1796, 176, 'tve_revision_tve_content_more_found', ''),
(1797, 176, 'tve_revision_tve_save_post', ''),
(1798, 176, 'tve_revision_tve_custom_css', ''),
(1799, 176, 'tve_revision_tve_user_custom_css', ''),
(1800, 176, 'tve_revision_tve_page_events', ''),
(1801, 176, 'tve_revision_tve_globals', ''),
(1802, 176, 'tve_revision_tve_global_scripts', ''),
(1803, 176, 'tve_revision_thrive_icon_pack', ''),
(1804, 176, 'tve_revision_thrive_tcb_post_fonts', ''),
(1805, 176, 'tve_revision_tve_has_masonry', ''),
(1806, 176, 'tve_revision_tve_has_typefocus', ''),
(1807, 176, 'tve_revision_tve_updated_post', ''),
(1808, 176, 'tve_revision_tve_has_wistia_popover', ''),
(1809, 177, 'tve_revision_tve_landing_page', ''),
(1810, 177, 'tve_revision_tve_disable_theme_dependency', ''),
(1811, 177, 'tve_revision_tve_content_before_more', ''),
(1812, 177, 'tve_revision_tve_content_more_found', ''),
(1813, 177, 'tve_revision_tve_save_post', ''),
(1814, 177, 'tve_revision_tve_custom_css', ''),
(1815, 177, 'tve_revision_tve_user_custom_css', ''),
(1816, 177, 'tve_revision_tve_page_events', ''),
(1817, 177, 'tve_revision_tve_globals', ''),
(1818, 177, 'tve_revision_tve_global_scripts', ''),
(1819, 177, 'tve_revision_thrive_icon_pack', ''),
(1820, 177, 'tve_revision_thrive_tcb_post_fonts', ''),
(1821, 177, 'tve_revision_tve_has_masonry', ''),
(1822, 177, 'tve_revision_tve_has_typefocus', ''),
(1823, 177, 'tve_revision_tve_updated_post', ''),
(1824, 177, 'tve_revision_tve_has_wistia_popover', ''),
(1825, 177, 'video_task', '125'),
(1826, 177, '_video_task', 'field_5e9e0f0f9bab3'),
(1827, 177, 'mark_as_completed', 'completed'),
(1828, 177, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(1829, 178, '_menu_item_type', 'post_type'),
(1830, 178, '_menu_item_menu_item_parent', '0'),
(1831, 178, '_menu_item_object_id', '96'),
(1832, 178, '_menu_item_object', 'page'),
(1833, 178, '_menu_item_target', ''),
(1834, 178, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1835, 178, '_menu_item_xfn', ''),
(1836, 178, '_menu_item_url', ''),
(1838, 179, '_wp_trash_meta_status', 'publish'),
(1839, 179, '_wp_trash_meta_time', '1587836659'),
(1840, 180, '_wp_trash_meta_status', 'publish'),
(1841, 180, '_wp_trash_meta_time', '1587836687'),
(1842, 181, 'tve_revision_tve_landing_page', ''),
(1843, 181, 'tve_revision_tve_disable_theme_dependency', ''),
(1844, 181, 'tve_revision_tve_content_before_more', ''),
(1845, 181, 'tve_revision_tve_content_more_found', ''),
(1846, 181, 'tve_revision_tve_save_post', ''),
(1847, 181, 'tve_revision_tve_custom_css', ''),
(1848, 181, 'tve_revision_tve_user_custom_css', ''),
(1849, 181, 'tve_revision_tve_page_events', ''),
(1850, 181, 'tve_revision_tve_globals', ''),
(1851, 181, 'tve_revision_tve_global_scripts', ''),
(1852, 181, 'tve_revision_thrive_icon_pack', ''),
(1853, 181, 'tve_revision_thrive_tcb_post_fonts', ''),
(1854, 181, 'tve_revision_tve_has_masonry', ''),
(1855, 181, 'tve_revision_tve_has_typefocus', ''),
(1856, 181, 'tve_revision_tve_updated_post', ''),
(1857, 181, 'tve_revision_tve_has_wistia_popover', ''),
(1858, 125, 'mark_as_watched', 'completed'),
(1859, 125, '_mark_as_watched', 'field_5e9f67bdac35a'),
(1860, 181, 'video_title', 'Automatische kampaign'),
(1861, 181, '_video_title', 'field_5e97ae5a532d7'),
(1862, 181, 'video_link', 'https://player.vimeo.com/video/1844255'),
(1863, 181, '_video_link', 'field_5e97ae87532d8'),
(1864, 181, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(1865, 181, '_short_description', 'field_5e97aed1532d9'),
(1866, 181, 'video_length_', '2:59'),
(1867, 181, '_video_length_', 'field_5e9c7221c0dd3'),
(1868, 181, 'mark_as_watched', 'pending'),
(1869, 181, '_mark_as_watched', 'field_5e9f67bdac35a'),
(1870, 182, 'tve_revision_tve_landing_page', ''),
(1871, 182, 'tve_revision_tve_disable_theme_dependency', ''),
(1872, 182, 'tve_revision_tve_content_before_more', ''),
(1873, 182, 'tve_revision_tve_content_more_found', ''),
(1874, 182, 'tve_revision_tve_save_post', ''),
(1875, 182, 'tve_revision_tve_custom_css', ''),
(1876, 182, 'tve_revision_tve_user_custom_css', ''),
(1877, 182, 'tve_revision_tve_page_events', ''),
(1878, 182, 'tve_revision_tve_globals', ''),
(1879, 182, 'tve_revision_tve_global_scripts', ''),
(1880, 182, 'tve_revision_thrive_icon_pack', ''),
(1881, 182, 'tve_revision_thrive_tcb_post_fonts', ''),
(1882, 182, 'tve_revision_tve_has_masonry', ''),
(1883, 182, 'tve_revision_tve_has_typefocus', ''),
(1884, 182, 'tve_revision_tve_updated_post', ''),
(1885, 182, 'tve_revision_tve_has_wistia_popover', ''),
(1886, 182, 'video_title', 'Automatische kampaign'),
(1887, 182, '_video_title', 'field_5e97ae5a532d7'),
(1888, 182, 'video_link', 'https://player.vimeo.com/video/1844255'),
(1889, 182, '_video_link', 'field_5e97ae87532d8'),
(1890, 182, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(1891, 182, '_short_description', 'field_5e97aed1532d9'),
(1892, 182, 'video_length_', '2:59'),
(1893, 182, '_video_length_', 'field_5e9c7221c0dd3'),
(1894, 182, 'mark_as_watched', 'pending'),
(1895, 182, '_mark_as_watched', 'field_5e9f67bdac35a'),
(1896, 171, 'wpcomplete', '[]'),
(1897, 183, 'tve_revision_tve_landing_page', ''),
(1898, 183, 'tve_revision_tve_disable_theme_dependency', ''),
(1899, 183, 'tve_revision_tve_content_before_more', ''),
(1900, 183, 'tve_revision_tve_content_more_found', ''),
(1901, 183, 'tve_revision_tve_save_post', ''),
(1902, 183, 'tve_revision_tve_custom_css', ''),
(1903, 183, 'tve_revision_tve_user_custom_css', ''),
(1904, 183, 'tve_revision_tve_page_events', ''),
(1905, 183, 'tve_revision_tve_globals', ''),
(1906, 183, 'tve_revision_tve_global_scripts', ''),
(1907, 183, 'tve_revision_thrive_icon_pack', ''),
(1908, 183, 'tve_revision_thrive_tcb_post_fonts', ''),
(1909, 183, 'tve_revision_tve_has_masonry', ''),
(1910, 183, 'tve_revision_tve_has_typefocus', ''),
(1911, 183, 'tve_revision_tve_updated_post', ''),
(1912, 183, 'tve_revision_tve_has_wistia_popover', ''),
(1913, 183, 'video_title', 'Automatische kampaign'),
(1914, 183, '_video_title', 'field_5e97ae5a532d7'),
(1915, 183, 'video_link', 'https://player.vimeo.com/video/1844255'),
(1916, 183, '_video_link', 'field_5e97ae87532d8'),
(1917, 183, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(1918, 183, '_short_description', 'field_5e97aed1532d9'),
(1919, 183, 'video_length_', '2:59'),
(1920, 183, '_video_length_', 'field_5e9c7221c0dd3'),
(1921, 183, 'mark_as_watched', 'pending12'),
(1922, 183, '_mark_as_watched', 'field_5e9f67bdac35a'),
(1923, 184, 'tve_revision_tve_landing_page', ''),
(1924, 184, 'tve_revision_tve_disable_theme_dependency', ''),
(1925, 184, 'tve_revision_tve_content_before_more', ''),
(1926, 184, 'tve_revision_tve_content_more_found', ''),
(1927, 184, 'tve_revision_tve_save_post', ''),
(1928, 184, 'tve_revision_tve_custom_css', ''),
(1929, 184, 'tve_revision_tve_user_custom_css', ''),
(1930, 184, 'tve_revision_tve_page_events', ''),
(1931, 184, 'tve_revision_tve_globals', ''),
(1932, 184, 'tve_revision_tve_global_scripts', ''),
(1933, 184, 'tve_revision_thrive_icon_pack', ''),
(1934, 184, 'tve_revision_thrive_tcb_post_fonts', ''),
(1935, 184, 'tve_revision_tve_has_masonry', ''),
(1936, 184, 'tve_revision_tve_has_typefocus', ''),
(1937, 184, 'tve_revision_tve_updated_post', ''),
(1938, 184, 'tve_revision_tve_has_wistia_popover', ''),
(1939, 184, 'video_title', 'Automatische kampaign'),
(1940, 184, '_video_title', 'field_5e97ae5a532d7'),
(1941, 184, 'video_link', 'https://player.vimeo.com/video/1844255'),
(1942, 184, '_video_link', 'field_5e97ae87532d8'),
(1943, 184, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(1944, 184, '_short_description', 'field_5e97aed1532d9'),
(1945, 184, 'video_length_', '2:59'),
(1946, 184, '_video_length_', 'field_5e9c7221c0dd3'),
(1947, 184, 'mark_as_watched', 'pending'),
(1948, 184, '_mark_as_watched', 'field_5e9f67bdac35a'),
(1949, 185, 'tve_revision_tve_landing_page', ''),
(1950, 185, 'tve_revision_tve_disable_theme_dependency', ''),
(1951, 185, 'tve_revision_tve_content_before_more', ''),
(1952, 185, 'tve_revision_tve_content_more_found', ''),
(1953, 185, 'tve_revision_tve_save_post', ''),
(1954, 185, 'tve_revision_tve_custom_css', ''),
(1955, 185, 'tve_revision_tve_user_custom_css', ''),
(1956, 185, 'tve_revision_tve_page_events', ''),
(1957, 185, 'tve_revision_tve_globals', ''),
(1958, 185, 'tve_revision_tve_global_scripts', ''),
(1959, 185, 'tve_revision_thrive_icon_pack', ''),
(1960, 185, 'tve_revision_thrive_tcb_post_fonts', ''),
(1961, 185, 'tve_revision_tve_has_masonry', ''),
(1962, 185, 'tve_revision_tve_has_typefocus', ''),
(1963, 185, 'tve_revision_tve_updated_post', ''),
(1964, 185, 'tve_revision_tve_has_wistia_popover', ''),
(1965, 185, 'video_title', 'Targeting Kampagne'),
(1966, 185, '_video_title', 'field_5e97ae5a532d7'),
(1967, 185, 'video_link', 'https://player.vimeo.com/video/1844255'),
(1968, 185, '_video_link', 'field_5e97ae87532d8'),
(1969, 185, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod\r\n tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(1970, 185, '_short_description', 'field_5e97aed1532d9'),
(1971, 185, 'video_length_', '20:00'),
(1972, 185, '_video_length_', 'field_5e9c7221c0dd3'),
(1973, 185, 'mark_as_watched', 'pending'),
(1974, 185, '_mark_as_watched', 'field_5e9f67bdac35a'),
(1975, 186, 'tve_revision_tve_landing_page', ''),
(1976, 186, 'tve_revision_tve_disable_theme_dependency', ''),
(1977, 186, 'tve_revision_tve_content_before_more', ''),
(1978, 186, 'tve_revision_tve_content_more_found', ''),
(1979, 186, 'tve_revision_tve_save_post', ''),
(1980, 186, 'tve_revision_tve_custom_css', ''),
(1981, 186, 'tve_revision_tve_user_custom_css', ''),
(1982, 186, 'tve_revision_tve_page_events', ''),
(1983, 186, 'tve_revision_tve_globals', ''),
(1984, 186, 'tve_revision_tve_global_scripts', ''),
(1985, 186, 'tve_revision_thrive_icon_pack', ''),
(1986, 186, 'tve_revision_thrive_tcb_post_fonts', ''),
(1987, 186, 'tve_revision_tve_has_masonry', ''),
(1988, 186, 'tve_revision_tve_has_typefocus', ''),
(1989, 186, 'tve_revision_tve_updated_post', ''),
(1990, 186, 'tve_revision_tve_has_wistia_popover', ''),
(1991, 89, 'mark_as_watched', 'completed'),
(1992, 89, '_mark_as_watched', 'field_5e9f67bdac35a'),
(1993, 186, 'video_title', 'Test video 2'),
(1994, 186, '_video_title', 'field_5e97ae5a532d7'),
(1995, 186, 'video_link', 'https://player.vimeo.com/video/1844255'),
(1996, 186, '_video_link', 'field_5e97ae87532d8'),
(1997, 186, 'short_description', 'Our Smooth Jazz Music can be used for relaxing, calm, romantic dinner, cooking, at work and relaxation. This relaxing  Jazz music best suited for study, work, cooking, like dinner music, background music, romantic music, relaxation music or music for stress relief. Let the beautiful, soothing and inspirational music help you relax!'),
(1998, 186, '_short_description', 'field_5e97aed1532d9'),
(1999, 186, 'video_length_', '10:26'),
(2000, 186, '_video_length_', 'field_5e9c7221c0dd3'),
(2001, 186, 'mark_as_watched', 'pending'),
(2002, 186, '_mark_as_watched', 'field_5e9f67bdac35a'),
(2003, 188, '_edit_lock', '1587957044:1'),
(2004, 189, 'tve_revision_tve_landing_page', ''),
(2005, 189, 'tve_revision_tve_disable_theme_dependency', ''),
(2006, 189, 'tve_revision_tve_content_before_more', ''),
(2007, 189, 'tve_revision_tve_content_more_found', ''),
(2008, 189, 'tve_revision_tve_save_post', ''),
(2009, 189, 'tve_revision_tve_custom_css', ''),
(2010, 189, 'tve_revision_tve_user_custom_css', ''),
(2011, 189, 'tve_revision_tve_page_events', ''),
(2012, 189, 'tve_revision_tve_globals', ''),
(2013, 189, 'tve_revision_tve_global_scripts', ''),
(2014, 189, 'tve_revision_thrive_icon_pack', ''),
(2015, 189, 'tve_revision_thrive_tcb_post_fonts', ''),
(2016, 189, 'tve_revision_tve_has_masonry', ''),
(2017, 189, 'tve_revision_tve_has_typefocus', ''),
(2018, 189, 'tve_revision_tve_updated_post', ''),
(2019, 189, 'tve_revision_tve_has_wistia_popover', ''),
(2020, 188, '_edit_last', '1'),
(2021, 190, 'tve_revision_tve_landing_page', ''),
(2022, 190, 'tve_revision_tve_disable_theme_dependency', ''),
(2023, 190, 'tve_revision_tve_content_before_more', ''),
(2024, 190, 'tve_revision_tve_content_more_found', ''),
(2025, 190, 'tve_revision_tve_save_post', ''),
(2026, 190, 'tve_revision_tve_custom_css', ''),
(2027, 190, 'tve_revision_tve_user_custom_css', ''),
(2028, 190, 'tve_revision_tve_page_events', ''),
(2029, 190, 'tve_revision_tve_globals', ''),
(2030, 190, 'tve_revision_tve_global_scripts', ''),
(2031, 190, 'tve_revision_thrive_icon_pack', ''),
(2032, 190, 'tve_revision_thrive_tcb_post_fonts', ''),
(2033, 190, 'tve_revision_tve_has_masonry', ''),
(2034, 190, 'tve_revision_tve_has_typefocus', ''),
(2035, 190, 'tve_revision_tve_updated_post', ''),
(2036, 190, 'tve_revision_tve_has_wistia_popover', ''),
(2037, 188, 'video_task', '86'),
(2038, 188, '_video_task', 'field_5e9e0f0f9bab3'),
(2039, 188, 'mark_as_completed', 'pending'),
(2040, 188, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(2041, 190, 'video_task', '86'),
(2042, 190, '_video_task', 'field_5e9e0f0f9bab3'),
(2043, 190, 'mark_as_completed', 'pending'),
(2044, 190, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(2045, 191, '_edit_lock', '1588366514:1'),
(2046, 192, 'tve_revision_tve_landing_page', ''),
(2047, 192, 'tve_revision_tve_disable_theme_dependency', ''),
(2048, 192, 'tve_revision_tve_content_before_more', ''),
(2049, 192, 'tve_revision_tve_content_more_found', ''),
(2050, 192, 'tve_revision_tve_save_post', ''),
(2051, 192, 'tve_revision_tve_custom_css', ''),
(2052, 192, 'tve_revision_tve_user_custom_css', ''),
(2053, 192, 'tve_revision_tve_page_events', ''),
(2054, 192, 'tve_revision_tve_globals', ''),
(2055, 192, 'tve_revision_tve_global_scripts', ''),
(2056, 192, 'tve_revision_thrive_icon_pack', ''),
(2057, 192, 'tve_revision_thrive_tcb_post_fonts', ''),
(2058, 192, 'tve_revision_tve_has_masonry', ''),
(2059, 192, 'tve_revision_tve_has_typefocus', ''),
(2060, 192, 'tve_revision_tve_updated_post', ''),
(2061, 192, 'tve_revision_tve_has_wistia_popover', ''),
(2062, 191, '_edit_last', '1'),
(2063, 193, 'tve_revision_tve_landing_page', ''),
(2064, 193, 'tve_revision_tve_disable_theme_dependency', ''),
(2065, 193, 'tve_revision_tve_content_before_more', ''),
(2066, 193, 'tve_revision_tve_content_more_found', ''),
(2067, 193, 'tve_revision_tve_save_post', ''),
(2068, 193, 'tve_revision_tve_custom_css', ''),
(2069, 193, 'tve_revision_tve_user_custom_css', ''),
(2070, 193, 'tve_revision_tve_page_events', ''),
(2071, 193, 'tve_revision_tve_globals', ''),
(2072, 193, 'tve_revision_tve_global_scripts', ''),
(2073, 193, 'tve_revision_thrive_icon_pack', ''),
(2074, 193, 'tve_revision_thrive_tcb_post_fonts', ''),
(2075, 193, 'tve_revision_tve_has_masonry', ''),
(2076, 193, 'tve_revision_tve_has_typefocus', ''),
(2077, 193, 'tve_revision_tve_updated_post', ''),
(2078, 193, 'tve_revision_tve_has_wistia_popover', ''),
(2079, 191, 'video_title', 'Test Magic'),
(2080, 191, '_video_title', 'field_5e97ae5a532d7'),
(2081, 191, 'video_link', 'https://player.vimeo.com/video/17588964'),
(2082, 191, '_video_link', 'field_5e97ae87532d8'),
(2083, 191, 'short_description', 'tewjjdjdjdkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk\r\ndjhklmkjjjjjjddddddddddddddddddddddddddddddddddddddddddddd'),
(2084, 191, '_short_description', 'field_5e97aed1532d9'),
(2085, 191, 'video_length_', '2:00'),
(2086, 191, '_video_length_', 'field_5e9c7221c0dd3'),
(2087, 191, 'mark_as_watched', 'completed'),
(2088, 191, '_mark_as_watched', 'field_5e9f67bdac35a'),
(2089, 193, 'video_title', 'Test Magic'),
(2090, 193, '_video_title', 'field_5e97ae5a532d7'),
(2091, 193, 'video_link', 'https://player.vimeo.com/video/17588964'),
(2092, 193, '_video_link', 'field_5e97ae87532d8'),
(2093, 193, 'short_description', 'tewjjdjdjdkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk\r\ndjhklmkjjjjjjddddddddddddddddddddddddddddddddddddddddddddd'),
(2094, 193, '_short_description', 'field_5e97aed1532d9'),
(2095, 193, 'video_length_', '2:00'),
(2096, 193, '_video_length_', 'field_5e9c7221c0dd3'),
(2097, 193, 'mark_as_watched', 'pending'),
(2098, 193, '_mark_as_watched', 'field_5e9f67bdac35a'),
(2099, 194, '_edit_lock', '1587957367:1'),
(2100, 195, 'tve_revision_tve_landing_page', ''),
(2101, 195, 'tve_revision_tve_disable_theme_dependency', ''),
(2102, 195, 'tve_revision_tve_content_before_more', ''),
(2103, 195, 'tve_revision_tve_content_more_found', ''),
(2104, 195, 'tve_revision_tve_save_post', ''),
(2105, 195, 'tve_revision_tve_custom_css', ''),
(2106, 195, 'tve_revision_tve_user_custom_css', ''),
(2107, 195, 'tve_revision_tve_page_events', ''),
(2108, 195, 'tve_revision_tve_globals', ''),
(2109, 195, 'tve_revision_tve_global_scripts', ''),
(2110, 195, 'tve_revision_thrive_icon_pack', ''),
(2111, 195, 'tve_revision_thrive_tcb_post_fonts', ''),
(2112, 195, 'tve_revision_tve_has_masonry', ''),
(2113, 195, 'tve_revision_tve_has_typefocus', ''),
(2114, 195, 'tve_revision_tve_updated_post', ''),
(2115, 195, 'tve_revision_tve_has_wistia_popover', ''),
(2116, 194, '_edit_last', '1'),
(2117, 196, 'tve_revision_tve_landing_page', ''),
(2118, 196, 'tve_revision_tve_disable_theme_dependency', ''),
(2119, 196, 'tve_revision_tve_content_before_more', ''),
(2120, 196, 'tve_revision_tve_content_more_found', ''),
(2121, 196, 'tve_revision_tve_save_post', ''),
(2122, 196, 'tve_revision_tve_custom_css', ''),
(2123, 196, 'tve_revision_tve_user_custom_css', ''),
(2124, 196, 'tve_revision_tve_page_events', ''),
(2125, 196, 'tve_revision_tve_globals', ''),
(2126, 196, 'tve_revision_tve_global_scripts', ''),
(2127, 196, 'tve_revision_thrive_icon_pack', ''),
(2128, 196, 'tve_revision_thrive_tcb_post_fonts', ''),
(2129, 196, 'tve_revision_tve_has_masonry', ''),
(2130, 196, 'tve_revision_tve_has_typefocus', ''),
(2131, 196, 'tve_revision_tve_updated_post', ''),
(2132, 196, 'tve_revision_tve_has_wistia_popover', ''),
(2133, 194, 'video_task', '191'),
(2134, 194, '_video_task', 'field_5e9e0f0f9bab3'),
(2135, 194, 'mark_as_completed', 'pending'),
(2136, 194, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(2137, 196, 'video_task', '191'),
(2138, 196, '_video_task', 'field_5e9e0f0f9bab3'),
(2139, 196, 'mark_as_completed', 'pending'),
(2140, 196, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(2141, 197, '_edit_lock', '1587957904:1'),
(2142, 198, 'tve_revision_tve_landing_page', ''),
(2143, 198, 'tve_revision_tve_disable_theme_dependency', ''),
(2144, 198, 'tve_revision_tve_content_before_more', ''),
(2145, 198, 'tve_revision_tve_content_more_found', ''),
(2146, 198, 'tve_revision_tve_save_post', ''),
(2147, 198, 'tve_revision_tve_custom_css', ''),
(2148, 198, 'tve_revision_tve_user_custom_css', ''),
(2149, 198, 'tve_revision_tve_page_events', ''),
(2150, 198, 'tve_revision_tve_globals', ''),
(2151, 198, 'tve_revision_tve_global_scripts', ''),
(2152, 198, 'tve_revision_thrive_icon_pack', ''),
(2153, 198, 'tve_revision_thrive_tcb_post_fonts', ''),
(2154, 198, 'tve_revision_tve_has_masonry', ''),
(2155, 198, 'tve_revision_tve_has_typefocus', ''),
(2156, 198, 'tve_revision_tve_updated_post', ''),
(2157, 198, 'tve_revision_tve_has_wistia_popover', ''),
(2158, 197, '_edit_last', '1'),
(2159, 199, 'tve_revision_tve_landing_page', ''),
(2160, 199, 'tve_revision_tve_disable_theme_dependency', ''),
(2161, 199, 'tve_revision_tve_content_before_more', ''),
(2162, 199, 'tve_revision_tve_content_more_found', ''),
(2163, 199, 'tve_revision_tve_save_post', ''),
(2164, 199, 'tve_revision_tve_custom_css', ''),
(2165, 199, 'tve_revision_tve_user_custom_css', ''),
(2166, 199, 'tve_revision_tve_page_events', ''),
(2167, 199, 'tve_revision_tve_globals', ''),
(2168, 199, 'tve_revision_tve_global_scripts', ''),
(2169, 199, 'tve_revision_thrive_icon_pack', ''),
(2170, 199, 'tve_revision_thrive_tcb_post_fonts', ''),
(2171, 199, 'tve_revision_tve_has_masonry', ''),
(2172, 199, 'tve_revision_tve_has_typefocus', ''),
(2173, 199, 'tve_revision_tve_updated_post', ''),
(2174, 199, 'tve_revision_tve_has_wistia_popover', ''),
(2175, 197, 'video_task', '119'),
(2176, 197, '_video_task', 'field_5e9e0f0f9bab3'),
(2177, 197, 'mark_as_completed', 'pending'),
(2178, 197, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(2179, 199, 'video_task', '119'),
(2180, 199, '_video_task', 'field_5e9e0f0f9bab3'),
(2181, 199, 'mark_as_completed', 'pending'),
(2182, 199, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(2183, 200, '_edit_lock', '1587965678:1'),
(2184, 201, 'tve_revision_tve_landing_page', ''),
(2185, 201, 'tve_revision_tve_disable_theme_dependency', ''),
(2186, 201, 'tve_revision_tve_content_before_more', ''),
(2187, 201, 'tve_revision_tve_content_more_found', ''),
(2188, 201, 'tve_revision_tve_save_post', ''),
(2189, 201, 'tve_revision_tve_custom_css', ''),
(2190, 201, 'tve_revision_tve_user_custom_css', ''),
(2191, 201, 'tve_revision_tve_page_events', ''),
(2192, 201, 'tve_revision_tve_globals', ''),
(2193, 201, 'tve_revision_tve_global_scripts', ''),
(2194, 201, 'tve_revision_thrive_icon_pack', ''),
(2195, 201, 'tve_revision_thrive_tcb_post_fonts', ''),
(2196, 201, 'tve_revision_tve_has_masonry', ''),
(2197, 201, 'tve_revision_tve_has_typefocus', ''),
(2198, 201, 'tve_revision_tve_updated_post', ''),
(2199, 201, 'tve_revision_tve_has_wistia_popover', ''),
(2200, 200, '_edit_last', '1'),
(2201, 202, 'tve_revision_tve_landing_page', ''),
(2202, 202, 'tve_revision_tve_disable_theme_dependency', ''),
(2203, 202, 'tve_revision_tve_content_before_more', ''),
(2204, 202, 'tve_revision_tve_content_more_found', ''),
(2205, 202, 'tve_revision_tve_save_post', ''),
(2206, 202, 'tve_revision_tve_custom_css', ''),
(2207, 202, 'tve_revision_tve_user_custom_css', ''),
(2208, 202, 'tve_revision_tve_page_events', ''),
(2209, 202, 'tve_revision_tve_globals', ''),
(2210, 202, 'tve_revision_tve_global_scripts', ''),
(2211, 202, 'tve_revision_thrive_icon_pack', ''),
(2212, 202, 'tve_revision_thrive_tcb_post_fonts', ''),
(2213, 202, 'tve_revision_tve_has_masonry', ''),
(2214, 202, 'tve_revision_tve_has_typefocus', ''),
(2215, 202, 'tve_revision_tve_updated_post', ''),
(2216, 202, 'tve_revision_tve_has_wistia_popover', ''),
(2217, 200, 'video_title', 'test 123'),
(2218, 200, '_video_title', 'field_5e97ae5a532d7'),
(2219, 200, 'video_link', 'https://player.vimeo.com/video/17588964'),
(2220, 200, '_video_link', 'field_5e97ae87532d8'),
(2221, 200, 'short_description', 'tewjjdjdjdkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk djhklmkjjjjjjddddddddddddddddddddddddddddddddddddddddddddd'),
(2222, 200, '_short_description', 'field_5e97aed1532d9'),
(2223, 200, 'video_length_', '2:00'),
(2224, 200, '_video_length_', 'field_5e9c7221c0dd3'),
(2225, 200, 'mark_as_watched', 'completed'),
(2226, 200, '_mark_as_watched', 'field_5e9f67bdac35a'),
(2227, 202, 'video_title', 'test 123'),
(2228, 202, '_video_title', 'field_5e97ae5a532d7'),
(2229, 202, 'video_link', 'https://player.vimeo.com/video/17588964'),
(2230, 202, '_video_link', 'field_5e97ae87532d8'),
(2231, 202, 'short_description', 'tewjjdjdjdkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk djhklmkjjjjjjddddddddddddddddddddddddddddddddddddddddddddd'),
(2232, 202, '_short_description', 'field_5e97aed1532d9'),
(2233, 202, 'video_length_', '2:00'),
(2234, 202, '_video_length_', 'field_5e9c7221c0dd3'),
(2235, 202, 'mark_as_watched', 'pending'),
(2236, 202, '_mark_as_watched', 'field_5e9f67bdac35a'),
(2237, 203, '_edit_lock', '1587975973:1'),
(2238, 204, 'tve_revision_tve_landing_page', ''),
(2239, 204, 'tve_revision_tve_disable_theme_dependency', ''),
(2240, 204, 'tve_revision_tve_content_before_more', ''),
(2241, 204, 'tve_revision_tve_content_more_found', ''),
(2242, 204, 'tve_revision_tve_save_post', ''),
(2243, 204, 'tve_revision_tve_custom_css', ''),
(2244, 204, 'tve_revision_tve_user_custom_css', ''),
(2245, 204, 'tve_revision_tve_page_events', ''),
(2246, 204, 'tve_revision_tve_globals', ''),
(2247, 204, 'tve_revision_tve_global_scripts', ''),
(2248, 204, 'tve_revision_thrive_icon_pack', ''),
(2249, 204, 'tve_revision_thrive_tcb_post_fonts', ''),
(2250, 204, 'tve_revision_tve_has_masonry', ''),
(2251, 204, 'tve_revision_tve_has_typefocus', ''),
(2252, 204, 'tve_revision_tve_updated_post', ''),
(2253, 204, 'tve_revision_tve_has_wistia_popover', ''),
(2254, 203, '_edit_last', '1'),
(2255, 205, 'tve_revision_tve_landing_page', ''),
(2256, 205, 'tve_revision_tve_disable_theme_dependency', ''),
(2257, 205, 'tve_revision_tve_content_before_more', ''),
(2258, 205, 'tve_revision_tve_content_more_found', ''),
(2259, 205, 'tve_revision_tve_save_post', ''),
(2260, 205, 'tve_revision_tve_custom_css', ''),
(2261, 205, 'tve_revision_tve_user_custom_css', ''),
(2262, 205, 'tve_revision_tve_page_events', ''),
(2263, 205, 'tve_revision_tve_globals', ''),
(2264, 205, 'tve_revision_tve_global_scripts', ''),
(2265, 205, 'tve_revision_thrive_icon_pack', ''),
(2266, 205, 'tve_revision_thrive_tcb_post_fonts', ''),
(2267, 205, 'tve_revision_tve_has_masonry', ''),
(2268, 205, 'tve_revision_tve_has_typefocus', ''),
(2269, 205, 'tve_revision_tve_updated_post', ''),
(2270, 205, 'tve_revision_tve_has_wistia_popover', ''),
(2271, 203, 'video_task', '200'),
(2272, 203, '_video_task', 'field_5e9e0f0f9bab3'),
(2273, 203, 'mark_as_completed', 'completed'),
(2274, 203, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(2275, 205, 'video_task', '200'),
(2276, 205, '_video_task', 'field_5e9e0f0f9bab3'),
(2277, 205, 'mark_as_completed', 'pending'),
(2278, 205, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(2279, 206, '_wp_attached_file', 'woocommerce-placeholder.png'),
(2280, 206, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(2282, 208, '_edit_lock', '1588010427:1'),
(2283, 209, 'tve_revision_tve_landing_page', ''),
(2284, 209, 'tve_revision_tve_disable_theme_dependency', ''),
(2285, 209, 'tve_revision_tve_content_before_more', ''),
(2286, 209, 'tve_revision_tve_content_more_found', ''),
(2287, 209, 'tve_revision_tve_save_post', ''),
(2288, 209, 'tve_revision_tve_custom_css', ''),
(2289, 209, 'tve_revision_tve_user_custom_css', ''),
(2290, 209, 'tve_revision_tve_page_events', ''),
(2291, 209, 'tve_revision_tve_globals', ''),
(2292, 209, 'tve_revision_tve_global_scripts', ''),
(2293, 209, 'tve_revision_thrive_icon_pack', ''),
(2294, 209, 'tve_revision_thrive_tcb_post_fonts', ''),
(2295, 209, 'tve_revision_tve_has_masonry', ''),
(2296, 209, 'tve_revision_tve_has_typefocus', ''),
(2297, 209, 'tve_revision_tve_updated_post', ''),
(2298, 209, 'tve_revision_tve_has_wistia_popover', ''),
(2299, 208, '_edit_last', '1'),
(2300, 210, 'tve_revision_tve_landing_page', ''),
(2301, 210, 'tve_revision_tve_disable_theme_dependency', ''),
(2302, 210, 'tve_revision_tve_content_before_more', ''),
(2303, 210, 'tve_revision_tve_content_more_found', ''),
(2304, 210, 'tve_revision_tve_save_post', ''),
(2305, 210, 'tve_revision_tve_custom_css', ''),
(2306, 210, 'tve_revision_tve_user_custom_css', ''),
(2307, 210, 'tve_revision_tve_page_events', ''),
(2308, 210, 'tve_revision_tve_globals', ''),
(2309, 210, 'tve_revision_tve_global_scripts', ''),
(2310, 210, 'tve_revision_thrive_icon_pack', ''),
(2311, 210, 'tve_revision_thrive_tcb_post_fonts', ''),
(2312, 210, 'tve_revision_tve_has_masonry', ''),
(2313, 210, 'tve_revision_tve_has_typefocus', ''),
(2314, 210, 'tve_revision_tve_updated_post', ''),
(2315, 210, 'tve_revision_tve_has_wistia_popover', ''),
(2316, 208, 'video_title', 'GOLANG programming'),
(2317, 208, '_video_title', 'field_5e97ae5a532d7'),
(2318, 208, 'video_link', 'https://player.vimeo.com/video/1844255'),
(2319, 208, '_video_link', 'field_5e97ae87532d8'),
(2320, 208, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(2321, 208, '_short_description', 'field_5e97aed1532d9'),
(2322, 208, 'video_length_', '2:54'),
(2323, 208, '_video_length_', 'field_5e9c7221c0dd3'),
(2324, 208, 'mark_as_watched', 'pending'),
(2325, 208, '_mark_as_watched', 'field_5e9f67bdac35a'),
(2326, 210, 'video_title', ''),
(2327, 210, '_video_title', 'field_5e97ae5a532d7'),
(2328, 210, 'video_link', 'https://player.vimeo.com/video/1844255'),
(2329, 210, '_video_link', 'field_5e97ae87532d8'),
(2330, 210, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(2331, 210, '_short_description', 'field_5e97aed1532d9'),
(2332, 210, 'video_length_', '2:54'),
(2333, 210, '_video_length_', 'field_5e9c7221c0dd3'),
(2334, 210, 'mark_as_watched', 'pending'),
(2335, 210, '_mark_as_watched', 'field_5e9f67bdac35a'),
(2336, 211, 'tve_revision_tve_landing_page', ''),
(2337, 211, 'tve_revision_tve_disable_theme_dependency', ''),
(2338, 211, 'tve_revision_tve_content_before_more', ''),
(2339, 211, 'tve_revision_tve_content_more_found', ''),
(2340, 211, 'tve_revision_tve_save_post', ''),
(2341, 211, 'tve_revision_tve_custom_css', ''),
(2342, 211, 'tve_revision_tve_user_custom_css', ''),
(2343, 211, 'tve_revision_tve_page_events', ''),
(2344, 211, 'tve_revision_tve_globals', ''),
(2345, 211, 'tve_revision_tve_global_scripts', ''),
(2346, 211, 'tve_revision_thrive_icon_pack', ''),
(2347, 211, 'tve_revision_thrive_tcb_post_fonts', ''),
(2348, 211, 'tve_revision_tve_has_masonry', ''),
(2349, 211, 'tve_revision_tve_has_typefocus', ''),
(2350, 211, 'tve_revision_tve_updated_post', ''),
(2351, 211, 'tve_revision_tve_has_wistia_popover', ''),
(2352, 211, 'video_title', 'GOLANG programming'),
(2353, 211, '_video_title', 'field_5e97ae5a532d7'),
(2354, 211, 'video_link', 'https://player.vimeo.com/video/1844255'),
(2355, 211, '_video_link', 'field_5e97ae87532d8'),
(2356, 211, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(2357, 211, '_short_description', 'field_5e97aed1532d9'),
(2358, 211, 'video_length_', '2:54'),
(2359, 211, '_video_length_', 'field_5e9c7221c0dd3'),
(2360, 211, 'mark_as_watched', 'pending'),
(2361, 211, '_mark_as_watched', 'field_5e9f67bdac35a'),
(2362, 212, '_edit_lock', '1588342794:1'),
(2363, 213, 'tve_revision_tve_landing_page', ''),
(2364, 213, 'tve_revision_tve_disable_theme_dependency', ''),
(2365, 213, 'tve_revision_tve_content_before_more', ''),
(2366, 213, 'tve_revision_tve_content_more_found', ''),
(2367, 213, 'tve_revision_tve_save_post', ''),
(2368, 213, 'tve_revision_tve_custom_css', ''),
(2369, 213, 'tve_revision_tve_user_custom_css', ''),
(2370, 213, 'tve_revision_tve_page_events', ''),
(2371, 213, 'tve_revision_tve_globals', ''),
(2372, 213, 'tve_revision_tve_global_scripts', ''),
(2373, 213, 'tve_revision_thrive_icon_pack', ''),
(2374, 213, 'tve_revision_thrive_tcb_post_fonts', ''),
(2375, 213, 'tve_revision_tve_has_masonry', ''),
(2376, 213, 'tve_revision_tve_has_typefocus', ''),
(2377, 213, 'tve_revision_tve_updated_post', ''),
(2378, 213, 'tve_revision_tve_has_wistia_popover', ''),
(2379, 212, '_edit_last', '1'),
(2380, 214, 'tve_revision_tve_landing_page', ''),
(2381, 214, 'tve_revision_tve_disable_theme_dependency', ''),
(2382, 214, 'tve_revision_tve_content_before_more', ''),
(2383, 214, 'tve_revision_tve_content_more_found', ''),
(2384, 214, 'tve_revision_tve_save_post', ''),
(2385, 214, 'tve_revision_tve_custom_css', ''),
(2386, 214, 'tve_revision_tve_user_custom_css', ''),
(2387, 214, 'tve_revision_tve_page_events', ''),
(2388, 214, 'tve_revision_tve_globals', ''),
(2389, 214, 'tve_revision_tve_global_scripts', ''),
(2390, 214, 'tve_revision_thrive_icon_pack', ''),
(2391, 214, 'tve_revision_thrive_tcb_post_fonts', ''),
(2392, 214, 'tve_revision_tve_has_masonry', ''),
(2393, 214, 'tve_revision_tve_has_typefocus', ''),
(2394, 214, 'tve_revision_tve_updated_post', ''),
(2395, 214, 'tve_revision_tve_has_wistia_popover', ''),
(2396, 212, 'video_task', '208'),
(2397, 212, '_video_task', 'field_5e9e0f0f9bab3'),
(2398, 212, 'mark_as_completed', 'pending'),
(2399, 212, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(2400, 214, 'video_task', '208'),
(2401, 214, '_video_task', 'field_5e9e0f0f9bab3'),
(2402, 214, 'mark_as_completed', 'pending'),
(2403, 214, '_mark_as_completed', 'field_5e9e2af07c1ea'),
(2404, 215, '_menu_item_type', 'taxonomy'),
(2405, 215, '_menu_item_menu_item_parent', '0'),
(2406, 215, '_menu_item_object_id', '10'),
(2407, 215, '_menu_item_object', 'category'),
(2408, 215, '_menu_item_target', ''),
(2409, 215, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(2410, 215, '_menu_item_xfn', ''),
(2411, 215, '_menu_item_url', ''),
(2412, 215, '_menu_item_orphaned', '1588013986'),
(2413, 216, '_menu_item_type', 'taxonomy'),
(2414, 216, '_menu_item_menu_item_parent', '0'),
(2415, 216, '_menu_item_object_id', '9'),
(2416, 216, '_menu_item_object', 'category'),
(2417, 216, '_menu_item_target', ''),
(2418, 216, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(2419, 216, '_menu_item_xfn', ''),
(2420, 216, '_menu_item_url', ''),
(2421, 216, '_menu_item_orphaned', '1588013986'),
(2422, 217, '_menu_item_type', 'taxonomy'),
(2423, 217, '_menu_item_menu_item_parent', '0'),
(2424, 217, '_menu_item_object_id', '12'),
(2425, 217, '_menu_item_object', 'category'),
(2426, 217, '_menu_item_target', ''),
(2427, 217, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(2428, 217, '_menu_item_xfn', ''),
(2429, 217, '_menu_item_url', ''),
(2430, 217, '_menu_item_orphaned', '1588013986'),
(2431, 218, '_menu_item_type', 'taxonomy'),
(2432, 218, '_menu_item_menu_item_parent', '0'),
(2433, 218, '_menu_item_object_id', '11'),
(2434, 218, '_menu_item_object', 'category'),
(2435, 218, '_menu_item_target', ''),
(2436, 218, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(2437, 218, '_menu_item_xfn', ''),
(2438, 218, '_menu_item_url', ''),
(2439, 218, '_menu_item_orphaned', '1588013986'),
(2440, 219, '_menu_item_type', 'taxonomy'),
(2441, 219, '_menu_item_menu_item_parent', '0'),
(2442, 219, '_menu_item_object_id', '13'),
(2443, 219, '_menu_item_object', 'category'),
(2444, 219, '_menu_item_target', ''),
(2445, 219, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(2446, 219, '_menu_item_xfn', ''),
(2447, 219, '_menu_item_url', ''),
(2448, 219, '_menu_item_orphaned', '1588013986'),
(2449, 220, '_menu_item_type', 'post_type'),
(2450, 220, '_menu_item_menu_item_parent', '0'),
(2451, 220, '_menu_item_object_id', '191'),
(2452, 220, '_menu_item_object', 'video'),
(2453, 220, '_menu_item_target', ''),
(2454, 220, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(2455, 220, '_menu_item_xfn', ''),
(2456, 220, '_menu_item_url', ''),
(2457, 220, '_menu_item_orphaned', '1588014027'),
(2458, 221, '_menu_item_type', 'post_type'),
(2459, 221, '_menu_item_menu_item_parent', '0'),
(2460, 221, '_menu_item_object_id', '167'),
(2461, 221, '_menu_item_object', 'video'),
(2462, 221, '_menu_item_target', ''),
(2463, 221, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(2464, 221, '_menu_item_xfn', ''),
(2465, 221, '_menu_item_url', ''),
(2466, 221, '_menu_item_orphaned', '1588014027'),
(2467, 222, '_menu_item_type', 'post_type'),
(2468, 222, '_menu_item_menu_item_parent', '0'),
(2469, 222, '_menu_item_object_id', '125'),
(2470, 222, '_menu_item_object', 'video'),
(2471, 222, '_menu_item_target', ''),
(2472, 222, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(2473, 222, '_menu_item_xfn', ''),
(2474, 222, '_menu_item_url', ''),
(2475, 222, '_menu_item_orphaned', '1588014027'),
(2476, 223, '_menu_item_type', 'post_type'),
(2477, 223, '_menu_item_menu_item_parent', '0'),
(2478, 223, '_menu_item_object_id', '212'),
(2479, 223, '_menu_item_object', 'task'),
(2480, 223, '_menu_item_target', ''),
(2481, 223, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(2482, 223, '_menu_item_xfn', ''),
(2483, 223, '_menu_item_url', ''),
(2484, 223, '_menu_item_orphaned', '1588014045'),
(2485, 224, '_menu_item_type', 'post_type'),
(2486, 224, '_menu_item_menu_item_parent', '0'),
(2487, 224, '_menu_item_object_id', '197'),
(2488, 224, '_menu_item_object', 'task'),
(2489, 224, '_menu_item_target', ''),
(2490, 224, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(2491, 224, '_menu_item_xfn', ''),
(2492, 224, '_menu_item_url', ''),
(2493, 224, '_menu_item_orphaned', '1588014045'),
(2496, 226, 'tve_revision_tve_landing_page', ''),
(2497, 226, 'tve_revision_tve_disable_theme_dependency', ''),
(2498, 226, 'tve_revision_tve_content_before_more', ''),
(2499, 226, 'tve_revision_tve_content_more_found', ''),
(2500, 226, 'tve_revision_tve_save_post', ''),
(2501, 226, 'tve_revision_tve_custom_css', ''),
(2502, 226, 'tve_revision_tve_user_custom_css', ''),
(2503, 226, 'tve_revision_tve_page_events', ''),
(2504, 226, 'tve_revision_tve_globals', ''),
(2505, 226, 'tve_revision_tve_global_scripts', ''),
(2506, 226, 'tve_revision_thrive_icon_pack', ''),
(2507, 226, 'tve_revision_thrive_tcb_post_fonts', ''),
(2508, 226, 'tve_revision_tve_has_masonry', ''),
(2509, 226, 'tve_revision_tve_has_typefocus', ''),
(2510, 226, 'tve_revision_tve_updated_post', ''),
(2511, 226, 'tve_revision_tve_has_wistia_popover', ''),
(2513, 229, 'tve_revision_tve_landing_page', ''),
(2514, 229, 'tve_revision_tve_disable_theme_dependency', ''),
(2515, 229, 'tve_revision_tve_content_before_more', ''),
(2516, 229, 'tve_revision_tve_content_more_found', ''),
(2517, 229, 'tve_revision_tve_save_post', ''),
(2518, 229, 'tve_revision_tve_custom_css', ''),
(2519, 229, 'tve_revision_tve_user_custom_css', ''),
(2520, 229, 'tve_revision_tve_page_events', ''),
(2521, 229, 'tve_revision_tve_globals', ''),
(2522, 229, 'tve_revision_tve_global_scripts', ''),
(2523, 229, 'tve_revision_thrive_icon_pack', ''),
(2524, 229, 'tve_revision_thrive_tcb_post_fonts', ''),
(2525, 229, 'tve_revision_tve_has_masonry', ''),
(2526, 229, 'tve_revision_tve_has_typefocus', ''),
(2527, 229, 'tve_revision_tve_updated_post', ''),
(2528, 229, 'tve_revision_tve_has_wistia_popover', ''),
(2529, 89, 'to_dos', 'a:3:{i:0;s:2:\"32\";i:1;s:2:\"33\";i:2;s:2:\"34\";}'),
(2530, 89, '_to_dos', 'field_5eac8d7f4065c'),
(2531, 229, 'video_title', 'Test video 2'),
(2532, 229, '_video_title', 'field_5e97ae5a532d7'),
(2533, 229, 'video_link', 'https://player.vimeo.com/video/1844255'),
(2534, 229, '_video_link', 'field_5e97ae87532d8'),
(2535, 229, 'short_description', 'Our Smooth Jazz Music can be used for relaxing, calm, romantic dinner, cooking, at work and relaxation. This relaxing  Jazz music best suited for study, work, cooking, like dinner music, background music, romantic music, relaxation music or music for stress relief. Let the beautiful, soothing and inspirational music help you relax!'),
(2536, 229, '_short_description', 'field_5e97aed1532d9'),
(2537, 229, 'video_length_', '10:26'),
(2538, 229, '_video_length_', 'field_5e9c7221c0dd3'),
(2539, 229, 'mark_as_watched', 'completed'),
(2540, 229, '_mark_as_watched', 'field_5e9f67bdac35a'),
(2541, 229, 'to_dos', 'a:3:{i:0;s:2:\"32\";i:1;s:2:\"33\";i:2;s:2:\"34\";}'),
(2542, 229, '_to_dos', 'field_5eac8d7f4065c'),
(2543, 137, '_wp_trash_meta_status', 'publish'),
(2544, 137, '_wp_trash_meta_time', '1588369138'),
(2545, 137, '_wp_desired_post_slug', 'group_5e9e0e736c9df'),
(2546, 138, '_wp_trash_meta_status', 'publish'),
(2547, 138, '_wp_trash_meta_time', '1588369138'),
(2548, 138, '_wp_desired_post_slug', 'field_5e9e0f0f9bab3'),
(2549, 149, '_wp_trash_meta_status', 'publish'),
(2550, 149, '_wp_trash_meta_time', '1588369138'),
(2551, 149, '_wp_desired_post_slug', 'field_5e9e2af07c1ea'),
(2552, 230, 'tve_revision_tve_landing_page', ''),
(2553, 230, 'tve_revision_tve_disable_theme_dependency', ''),
(2554, 230, 'tve_revision_tve_content_before_more', ''),
(2555, 230, 'tve_revision_tve_content_more_found', ''),
(2556, 230, 'tve_revision_tve_save_post', ''),
(2557, 230, 'tve_revision_tve_custom_css', ''),
(2558, 230, 'tve_revision_tve_user_custom_css', ''),
(2559, 230, 'tve_revision_tve_page_events', ''),
(2560, 230, 'tve_revision_tve_globals', ''),
(2561, 230, 'tve_revision_tve_global_scripts', ''),
(2562, 230, 'tve_revision_thrive_icon_pack', ''),
(2563, 230, 'tve_revision_thrive_tcb_post_fonts', ''),
(2564, 230, 'tve_revision_tve_has_masonry', ''),
(2565, 230, 'tve_revision_tve_has_typefocus', ''),
(2566, 230, 'tve_revision_tve_updated_post', ''),
(2567, 230, 'tve_revision_tve_has_wistia_popover', ''),
(2568, 125, 'to_dos', 'a:3:{i:0;s:2:\"37\";i:1;s:2:\"35\";i:2;s:2:\"36\";}'),
(2569, 125, '_to_dos', 'field_5eac8d7f4065c'),
(2570, 230, 'video_title', 'Automatische kampaign'),
(2571, 230, '_video_title', 'field_5e97ae5a532d7'),
(2572, 230, 'video_link', 'https://player.vimeo.com/video/1844255'),
(2573, 230, '_video_link', 'field_5e97ae87532d8'),
(2574, 230, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(2575, 230, '_short_description', 'field_5e97aed1532d9'),
(2576, 230, 'video_length_', '2:59'),
(2577, 230, '_video_length_', 'field_5e9c7221c0dd3'),
(2578, 230, 'mark_as_watched', 'completed'),
(2579, 230, '_mark_as_watched', 'field_5e9f67bdac35a'),
(2580, 230, 'to_dos', 'a:3:{i:0;s:2:\"35\";i:1;s:2:\"36\";i:2;s:2:\"37\";}'),
(2581, 230, '_to_dos', 'field_5eac8d7f4065c'),
(2582, 231, 'tve_revision_tve_landing_page', ''),
(2583, 231, 'tve_revision_tve_disable_theme_dependency', ''),
(2584, 231, 'tve_revision_tve_content_before_more', ''),
(2585, 231, 'tve_revision_tve_content_more_found', ''),
(2586, 231, 'tve_revision_tve_save_post', ''),
(2587, 231, 'tve_revision_tve_custom_css', ''),
(2588, 231, 'tve_revision_tve_user_custom_css', ''),
(2589, 231, 'tve_revision_tve_page_events', ''),
(2590, 231, 'tve_revision_tve_globals', ''),
(2591, 231, 'tve_revision_tve_global_scripts', ''),
(2592, 231, 'tve_revision_thrive_icon_pack', ''),
(2593, 231, 'tve_revision_thrive_tcb_post_fonts', ''),
(2594, 231, 'tve_revision_tve_has_masonry', ''),
(2595, 231, 'tve_revision_tve_has_typefocus', ''),
(2596, 231, 'tve_revision_tve_updated_post', ''),
(2597, 231, 'tve_revision_tve_has_wistia_popover', ''),
(2598, 231, 'video_title', 'Automatische kampaign'),
(2599, 231, '_video_title', 'field_5e97ae5a532d7'),
(2600, 231, 'video_link', 'https://player.vimeo.com/video/1844255'),
(2601, 231, '_video_link', 'field_5e97ae87532d8'),
(2602, 231, 'short_description', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eimod tempor invidut ut labore et dolore magna aliquyam erat, sed diam valuptua.'),
(2603, 231, '_short_description', 'field_5e97aed1532d9'),
(2604, 231, 'video_length_', '2:59'),
(2605, 231, '_video_length_', 'field_5e9c7221c0dd3'),
(2606, 231, 'mark_as_watched', 'completed'),
(2607, 231, '_mark_as_watched', 'field_5e9f67bdac35a'),
(2608, 231, 'to_dos', 'a:3:{i:0;s:2:\"37\";i:1;s:2:\"35\";i:2;s:2:\"36\";}'),
(2609, 231, '_to_dos', 'field_5eac8d7f4065c');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2611, 233, '_edit_lock', '1588433489:1'),
(2612, 234, 'tve_revision_tve_landing_page', ''),
(2613, 234, 'tve_revision_tve_disable_theme_dependency', ''),
(2614, 234, 'tve_revision_tve_content_before_more', ''),
(2615, 234, 'tve_revision_tve_content_more_found', ''),
(2616, 234, 'tve_revision_tve_save_post', ''),
(2617, 234, 'tve_revision_tve_custom_css', ''),
(2618, 234, 'tve_revision_tve_user_custom_css', ''),
(2619, 234, 'tve_revision_tve_page_events', ''),
(2620, 234, 'tve_revision_tve_globals', ''),
(2621, 234, 'tve_revision_tve_global_scripts', ''),
(2622, 234, 'tve_revision_thrive_icon_pack', ''),
(2623, 234, 'tve_revision_thrive_tcb_post_fonts', ''),
(2624, 234, 'tve_revision_tve_has_masonry', ''),
(2625, 234, 'tve_revision_tve_has_typefocus', ''),
(2626, 234, 'tve_revision_tve_updated_post', ''),
(2627, 234, 'tve_revision_tve_has_wistia_popover', ''),
(2628, 233, '_wp_page_template', 'page-templates/bibliothek-template.php'),
(2629, 235, '_menu_item_type', 'post_type'),
(2630, 235, '_menu_item_menu_item_parent', '0'),
(2631, 235, '_menu_item_object_id', '233'),
(2632, 235, '_menu_item_object', 'page'),
(2633, 235, '_menu_item_target', ''),
(2634, 235, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(2635, 235, '_menu_item_xfn', ''),
(2636, 235, '_menu_item_url', ''),
(2638, 240, '_edit_lock', '1588546234:1'),
(2639, 240, '_edit_last', '1'),
(2640, 240, 'key', 'test_plan'),
(2641, 240, 'add_new_users_automatically', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2020-04-10 19:52:35', '2020-04-10 19:52:35', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2020-04-10 19:52:35', '2020-04-10 19:52:35', '', 0, 'http://localhost/coursemodule/?p=1', 0, 'post', '', 1),
(2, 1, '2020-04-10 19:52:35', '2020-04-10 19:52:35', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost/coursemodule/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2020-04-10 19:52:35', '2020-04-10 19:52:35', '', 0, 'http://localhost/coursemodule/?page_id=2', 0, 'page', '', 0),
(3, 1, '2020-04-10 19:52:35', '2020-04-10 19:52:35', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost/coursemodule.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2020-04-10 19:52:35', '2020-04-10 19:52:35', '', 0, 'http://localhost/coursemodule/?page_id=3', 0, 'page', '', 0),
(5, 1, '2020-04-10 20:18:51', '2020-04-10 20:18:51', '', 'Register', '', 'publish', 'closed', 'closed', '', 'register', '', '', '2020-04-11 12:16:30', '2020-04-11 12:16:30', '', 0, 'http://localhost/coursemodule/?page_id=5', 0, 'page', '', 0),
(6, 1, '2020-04-10 20:18:51', '2020-04-10 20:18:51', '', 'Register', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2020-04-10 20:18:51', '2020-04-10 20:18:51', '', 5, 'http://localhost/coursemodule/index.php/2020/04/10/5-revision-v1/', 0, 'revision', '', 0),
(11, 1, '2020-04-11 10:04:40', '2020-04-11 10:04:40', '', '<i class=\"fas fa-headset\"> </i>Live Calls', '', 'publish', 'closed', 'closed', '', 'live-calls', '', '', '2020-05-02 15:35:01', '2020-05-02 15:35:01', '', 0, 'http://localhost/coursemodule/index.php/2020/04/11/live-calls/', 2, 'nav_menu_item', '', 0),
(13, 1, '2020-04-11 10:04:41', '2020-04-11 10:04:41', '', '<i class=\"fas fa-link\"> </i>Resources', '', 'publish', 'closed', 'closed', '', 'resources', '', '', '2020-05-02 15:35:02', '2020-05-02 15:35:02', '', 0, 'http://localhost/coursemodule/index.php/2020/04/11/resources/', 4, 'nav_menu_item', '', 0),
(18, 1, '2020-04-11 11:36:05', '2020-04-11 11:36:05', '', 'Roland-logo', '', 'inherit', 'open', 'closed', '', 'roland-logo', '', '', '2020-04-11 11:36:05', '2020-04-11 11:36:05', '', 0, 'http://localhost/coursemodule/wp-content/uploads/2020/04/Roland-logo.png', 0, 'attachment', 'image/png', 0),
(21, 1, '2020-04-11 11:41:27', '2020-04-11 11:41:27', '', 'client-2', '', 'inherit', 'open', 'closed', '', 'client-2', '', '', '2020-04-11 11:41:27', '2020-04-11 11:41:27', '', 0, 'http://localhost/coursemodule/wp-content/uploads/2020/04/client-2.png', 0, 'attachment', 'image/png', 0),
(24, 1, '2020-04-16 10:29:46', '0000-00-00 00:00:00', '<!-- wp:image -->\n<figure class=\"wp-block-image\"><img alt=\"\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:shortcode /-->', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-04-16 10:29:46', '2020-04-16 10:29:46', '', 0, 'http://localhost/coursemodule/?page_id=24', 0, 'page', '', 0),
(25, 1, '2020-04-11 12:18:01', '2020-04-11 12:18:01', '', 'Auto Draft', '', 'inherit', 'closed', 'closed', '', '24-revision-v1', '', '', '2020-04-11 12:18:01', '2020-04-11 12:18:01', '', 24, 'http://localhost/coursemodule/index.php/2020/04/11/24-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2020-04-11 14:33:51', '2020-04-11 14:33:51', '', 'Impressum', '', 'publish', 'closed', 'closed', '', 'impressum', '', '', '2020-04-11 14:33:51', '2020-04-11 14:33:51', '', 0, 'http://localhost/coursemodule/?p=27', 1, 'nav_menu_item', '', 0),
(28, 1, '2020-04-11 14:33:51', '2020-04-11 14:33:51', '', 'Datenschutz', '', 'publish', 'closed', 'closed', '', 'datenschutz', '', '', '2020-04-11 14:33:51', '2020-04-11 14:33:51', '', 0, 'http://localhost/coursemodule/?p=28', 2, 'nav_menu_item', '', 0),
(29, 1, '2020-04-11 14:33:51', '2020-04-11 14:33:51', '', 'AGB', '', 'publish', 'closed', 'closed', '', 'agb', '', '', '2020-04-11 14:33:51', '2020-04-11 14:33:51', '', 0, 'http://localhost/coursemodule/?p=29', 3, 'nav_menu_item', '', 0),
(30, 1, '2020-04-11 14:41:36', '2020-04-11 14:41:36', '<!-- wp:spacer -->\n<div style=\"height:100px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:image {\"id\":43,\"width\":700,\"height\":334,\"sizeSlug\":\"large\",\"className\":\"is-style-default\"} -->\n<figure class=\"wp-block-image size-large is-resized is-style-default\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/mrsms-banner-99051406db04513c.png\" alt=\"\" class=\"wp-image-43\" width=\"700\" height=\"334\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:image {\"id\":21,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/client-2.png\" alt=\"\" class=\"wp-image-21\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'test page', '', 'publish', 'closed', 'closed', '', 'test-page', '', '', '2020-04-14 18:04:40', '2020-04-14 18:04:40', '', 0, 'http://localhost/coursemodule/?page_id=30', 0, 'page', '', 0),
(31, 1, '2020-04-11 14:41:36', '2020-04-11 14:41:36', '', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-11 14:41:36', '2020-04-11 14:41:36', '', 30, 'http://localhost/coursemodule/index.php/2020/04/11/30-revision-v1/', 0, 'revision', '', 0),
(32, 1, '2020-04-11 15:02:02', '2020-04-11 15:02:02', '<!-- wp:table -->\n<figure class=\"wp-block-table\"><table><tbody><tr><td></td><td></td></tr><tr><td></td><td></td></tr></tbody></table></figure>\n<!-- /wp:table -->', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-11 15:02:02', '2020-04-11 15:02:02', '', 30, 'http://localhost/coursemodule/index.php/2020/04/11/30-revision-v1/', 0, 'revision', '', 0),
(33, 1, '2020-04-11 15:08:14', '2020-04-11 15:08:14', '<!-- wp:table -->\n<figure class=\"wp-block-table\"><table><tbody><tr><td></td><td></td></tr><tr><td></td><td></td></tr></tbody></table></figure>\n<!-- /wp:table -->\n\n<!-- wp:image {\"id\":21,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/client-2.png\" alt=\"\" class=\"wp-image-21\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-11 15:08:14', '2020-04-11 15:08:14', '', 30, 'http://localhost/coursemodule/index.php/2020/04/11/30-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2020-04-11 16:17:45', '2020-04-11 16:17:45', '<!-- wp:table {\"hasFixedLayout\":true} -->\n<figure class=\"wp-block-table\"><table class=\"has-fixed-layout\"><tbody><tr><td></td><td></td></tr><tr><td></td><td></td></tr></tbody></table></figure>\n<!-- /wp:table -->\n\n<!-- wp:image {\"id\":21,\"width\":248,\"height\":50,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large is-resized\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/client-2.png\" alt=\"\" class=\"wp-image-21\" width=\"248\" height=\"50\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-11 16:17:45', '2020-04-11 16:17:45', '', 30, 'http://localhost/coursemodule/index.php/2020/04/11/30-revision-v1/', 0, 'revision', '', 0),
(37, 1, '2020-04-11 16:19:24', '2020-04-11 16:19:24', '<!-- wp:paragraph -->\n<p>hello</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'tst-pst', '', 'publish', 'open', 'open', '', 'tst-pst', '', '', '2020-04-11 16:19:33', '2020-04-11 16:19:33', '', 0, 'http://localhost/coursemodule/?p=37', 0, 'post', '', 0),
(38, 1, '2020-04-11 16:19:24', '2020-04-11 16:19:24', '<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'tst-pst', '', 'inherit', 'closed', 'closed', '', '37-revision-v1', '', '', '2020-04-11 16:19:24', '2020-04-11 16:19:24', '', 37, 'http://localhost/coursemodule/index.php/2020/04/11/37-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2020-04-11 16:19:33', '2020-04-11 16:19:33', '<!-- wp:paragraph -->\n<p>hello</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'tst-pst', '', 'inherit', 'closed', 'closed', '', '37-revision-v1', '', '', '2020-04-11 16:19:33', '2020-04-11 16:19:33', '', 37, 'http://localhost/coursemodule/index.php/2020/04/11/37-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2020-04-11 16:53:12', '2020-04-11 16:53:12', '<!-- wp:table {\"hasFixedLayout\":true} -->\n<figure class=\"wp-block-table\"><table class=\"has-fixed-layout\"><tbody><tr><td></td><td></td></tr><tr><td></td><td></td></tr></tbody></table></figure>\n<!-- /wp:table -->\n\n<!-- wp:image {\"id\":21,\"width\":248,\"height\":50,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large is-resized\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/client-2.png\" alt=\"\" class=\"wp-image-21\" width=\"248\" height=\"50\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:list -->\n<ul><li>hjdhjhdhhjsdfhjfdshfhjdhjf</li></ul>\n<!-- /wp:list -->', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-11 16:53:12', '2020-04-11 16:53:12', '', 30, 'http://localhost/coursemodule/index.php/2020/04/11/30-revision-v1/', 0, 'revision', '', 0),
(42, 1, '2020-04-11 22:52:57', '2020-04-11 22:52:57', '', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-11 22:52:57', '2020-04-11 22:52:57', '', 30, 'http://localhost/coursemodule/index.php/2020/04/11/30-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2020-04-11 23:07:41', '2020-04-11 23:07:41', '', 'mrsms-banner-99051406db04513c', '', 'inherit', 'open', 'closed', '', 'mrsms-banner-99051406db04513c', '', '', '2020-04-11 23:07:41', '2020-04-11 23:07:41', '', 30, 'http://localhost/coursemodule/wp-content/uploads/2020/04/mrsms-banner-99051406db04513c.png', 0, 'attachment', 'image/png', 0),
(44, 1, '2020-04-11 23:07:46', '2020-04-11 23:07:46', '<!-- wp:image {\"id\":43,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/mrsms-banner-99051406db04513c.png\" alt=\"\" class=\"wp-image-43\"/></figure>\n<!-- /wp:image -->', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-11 23:07:46', '2020-04-11 23:07:46', '', 30, 'http://localhost/coursemodule/index.php/2020/04/11/30-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2020-04-14 10:45:12', '2020-04-14 10:45:12', '<!-- wp:spacer -->\n<div style=\"height:100px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:image {\"id\":43,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/mrsms-banner-99051406db04513c.png\" alt=\"\" class=\"wp-image-43\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:media-text {\"mediaId\":21,\"mediaLink\":\"http://localhost/coursemodule/client-2/\",\"mediaType\":\"image\"} -->\n<div class=\"wp-block-media-text alignwide is-stacked-on-mobile\"><figure class=\"wp-block-media-text__media\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/client-2.png\" alt=\"\" class=\"wp-image-21\"/></figure><div class=\"wp-block-media-text__content\"><!-- wp:paragraph {\"placeholder\":\"Content…\",\"fontSize\":\"large\"} -->\n<p class=\"has-large-font-size\">smSAMDm,m,dDd</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>jsdjjdsjkDS</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph --></div></div>\n<!-- /wp:media-text -->', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-14 10:45:12', '2020-04-14 10:45:12', '', 30, 'http://localhost/coursemodule/index.php/2020/04/14/30-revision-v1/', 0, 'revision', '', 0),
(47, 1, '2020-04-14 11:13:10', '2020-04-14 11:13:10', '<!-- wp:spacer -->\n<div style=\"height:100px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:image {\"id\":43,\"sizeSlug\":\"large\",\"className\":\"is-style-default\"} -->\n<figure class=\"wp-block-image size-large is-style-default\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/mrsms-banner-99051406db04513c.png\" alt=\"\" class=\"wp-image-43\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:media-text {\"mediaId\":21,\"mediaLink\":\"http://localhost/coursemodule/client-2/\",\"mediaType\":\"image\"} -->\n<div class=\"wp-block-media-text alignwide is-stacked-on-mobile\"><figure class=\"wp-block-media-text__media\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/client-2.png\" alt=\"\" class=\"wp-image-21\"/></figure><div class=\"wp-block-media-text__content\"><!-- wp:paragraph {\"placeholder\":\"Content…\",\"fontSize\":\"large\"} -->\n<p class=\"has-large-font-size\">smSAMDm,m,dDd</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>jsdjjdsjkDS</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph {\"textColor\":\"vivid-green-cyan\"} -->\n<p class=\"has-text-color has-vivid-green-cyan-color\"></p>\n<!-- /wp:paragraph --></div></div>\n<!-- /wp:media-text -->', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-14 11:13:10', '2020-04-14 11:13:10', '', 30, 'http://localhost/coursemodule/index.php/2020/04/14/30-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2020-04-14 12:04:12', '2020-04-14 12:04:12', '', 'Login', '', 'publish', 'closed', 'closed', '', 'login', '', '', '2020-04-14 15:55:24', '2020-04-14 15:55:24', '', 0, 'http://localhost/coursemodule/?page_id=49', 0, 'page', '', 0),
(50, 1, '2020-04-14 12:04:12', '2020-04-14 12:04:12', '', 'Login', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2020-04-14 12:04:12', '2020-04-14 12:04:12', '', 49, 'http://localhost/coursemodule/index.php/2020/04/14/49-revision-v1/', 0, 'revision', '', 0),
(51, 1, '2020-04-14 12:35:46', '2020-04-14 12:35:46', '', 'Dashboard', '', 'publish', 'closed', 'closed', '', 'dashboard', '', '', '2020-04-14 12:35:46', '2020-04-14 12:35:46', '', 0, 'http://localhost/coursemodule/?page_id=51', 0, 'page', '', 0),
(52, 1, '2020-04-14 12:35:46', '2020-04-14 12:35:46', '', 'Dashboard', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2020-04-14 12:35:46', '2020-04-14 12:35:46', '', 51, 'http://localhost/coursemodule/index.php/2020/04/14/51-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2020-04-14 13:18:18', '2020-04-14 13:18:18', '{\n    \"show_on_front\": {\n        \"value\": \"page\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-04-14 13:18:18\"\n    },\n    \"page_on_front\": {\n        \"value\": \"49\",\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-04-14 13:18:18\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '7a25184d-631f-49c0-b95c-508a58f154ab', '', '', '2020-04-14 13:18:18', '2020-04-14 13:18:18', '', 0, 'http://localhost/coursemodule/index.php/2020/04/14/7a25184d-631f-49c0-b95c-508a58f154ab/', 0, 'customize_changeset', '', 0),
(54, 1, '2020-04-14 15:11:34', '2020-04-14 15:11:34', '<!-- wp:image {\"id\":43,\"sizeSlug\":\"large\",\"className\":\"is-style-default\"} -->\n<figure class=\"wp-block-image size-large is-style-default\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/mrsms-banner-99051406db04513c.png\" alt=\"\" class=\"wp-image-43\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:media-text {\"mediaId\":21,\"mediaLink\":\"http://localhost/coursemodule/client-2/\",\"mediaType\":\"image\"} -->\n<div class=\"wp-block-media-text alignwide is-stacked-on-mobile\"><figure class=\"wp-block-media-text__media\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/client-2.png\" alt=\"\" class=\"wp-image-21\"/></figure><div class=\"wp-block-media-text__content\"><!-- wp:paragraph {\"placeholder\":\"Content…\",\"fontSize\":\"large\"} -->\n<p class=\"has-large-font-size\">smSAMDm,m,dDd</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>jsdjjdsjkDS</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph {\"textColor\":\"vivid-green-cyan\"} -->\n<p class=\"has-text-color has-vivid-green-cyan-color\"></p>\n<!-- /wp:paragraph --></div></div>\n<!-- /wp:media-text -->', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-14 15:11:34', '2020-04-14 15:11:34', '', 30, 'http://localhost/coursemodule/index.php/2020/04/14/30-revision-v1/', 0, 'revision', '', 0),
(55, 1, '2020-04-14 15:30:52', '2020-04-14 15:30:52', '<!-- wp:image {\"id\":43,\"width\":700,\"height\":334,\"sizeSlug\":\"large\",\"className\":\"is-style-default\"} -->\n<figure class=\"wp-block-image size-large is-resized is-style-default\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/mrsms-banner-99051406db04513c.png\" alt=\"\" class=\"wp-image-43\" width=\"700\" height=\"334\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:media-text {\"mediaId\":21,\"mediaLink\":\"http://localhost/coursemodule/client-2/\",\"mediaType\":\"image\"} -->\n<div class=\"wp-block-media-text alignwide is-stacked-on-mobile\"><figure class=\"wp-block-media-text__media\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/client-2.png\" alt=\"\" class=\"wp-image-21\"/></figure><div class=\"wp-block-media-text__content\"><!-- wp:paragraph {\"placeholder\":\"Content…\",\"fontSize\":\"large\"} -->\n<p class=\"has-large-font-size\">smSAMDm,m,dDd</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>jsdjjdsjkDS</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph {\"textColor\":\"vivid-green-cyan\"} -->\n<p class=\"has-text-color has-vivid-green-cyan-color\"></p>\n<!-- /wp:paragraph --></div></div>\n<!-- /wp:media-text -->', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-14 15:30:52', '2020-04-14 15:30:52', '', 30, 'http://localhost/coursemodule/index.php/2020/04/14/30-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2020-04-14 15:47:57', '2020-04-14 15:47:57', '<div class=\"thrv-content-block tcb-local-vars-root thrv_wrapper\" data-css=\"tve-u-105e613813c\"><div class=\"thrive-group-edit-config\" style=\"display: none !important\">__CONFIG_group_edit__{\"juo8pmn3\":{\"name\":\"All Paragraph(s)\",\"singular\":\"-- Text %s\"},\"jvdr7vrx\":{\"name\":\"All Title(s)\",\"singular\":\"-- Text %s\"},\"jvdrfoj4\":{\"name\":\"All Read Review Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrg8bl\":{\"name\":\"All Visit Website Button(s)\",\"singular\":\"-- Button %s\"},\"jvdrgsih\":{\"name\":\"All Buttons Content Box(s)\",\"singular\":\"-- Content Box %s\"},\"jvdrh1la\":{\"name\":\"All Paragraphs Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrha52\":{\"name\":\"All Buttons Column(s)\",\"singular\":\"-- Column %s\"},\"jvdrhpi3\":{\"name\":\"All Columns(s)\",\"singular\":\"-- Columns %s\"},\"jvdrhrvz\":{\"name\":\"All Content Box(s)\",\"singular\":\"-- Content Box %s\"}}__CONFIG_group_edit__</div><div class=\"thrive-local-colors-config\" style=\"display: none !important\">__CONFIG_local_colors__{\"colors\":{\"a35c5\":\"Button\",\"5f5e3\":\"Button Hover\"},\"gradients\":{}}__CONFIG_local_colors__</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-16a30aaa1f6\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-16a361f8512\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box\" data-css=\"tve-u-16a922820f9\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-16a9227cf16\"></div>\n	<div class=\"tve-cb\"><div class=\"thrv_wrapper thrv_text_element\" data-css=\"tve-u-16a20190bbc\"><h2 data-css=\"tve-u-16a4f32bcfc\">Recommended Resources</h2></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-16a363d57f4\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-16a30a99201\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-16a361f8513\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-16a362044f6\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-16a48d60c35\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-16a6d4b031a\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-16a48d88c2c\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-16a8bbd1432\"><h3>Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-16a3070f467\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-16a6d4b031d\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-16a48da7a0d\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-16a4eca87b0\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-16a4ecad6e4\"><div class=\"tcb-clear\" data-css=\"tve-u-16a92489b6a\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-16a9232f0da\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-16a924b3d0b\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-16a923a5462\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-16a363d57f4\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-16a30a99201\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-16a361f8513\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-16a362044f6\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-16a48d60c35\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-16a6d4b031a\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-16a48d88c2c\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-16a8bbd1432\"><h3>Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-16a3070f467\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-16a6d4b031d\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-16a48da7a0d\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-16a4eca87b0\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-16a4ecad6e4\"><div class=\"tcb-clear\" data-css=\"tve-u-16a92489b6a\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-16a9232f0da\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-16a924b3d0b\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-16a923a5462\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrhrvz\" data-css=\"tve-u-16a363d57f4\">\n	<div class=\"tve-content-box-background\" data-css=\"tve-u-16a30a99201\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-16a361f8513\"><div class=\"thrv_wrapper thrv-columns dynamic-group-jvdrhpi3\" data-css=\"tve-u-16a362044f6\"><div class=\"tcb-flex-row tcb-resized tcb-medium-wrap v-2 tcb--cols--2\" data-css=\"tve-u-16a48d60c35\"><div class=\"tcb-flex-col c-66\" data-css=\"tve-u-16a6d4b031a\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrh1la\" data-css=\"tve-u-16a48d88c2c\"><div class=\"thrv_wrapper thrv_text_element dynamic-group-jvdr7vrx\" data-css=\"tve-u-16a8bbd1432\"><h3>Title goes here</h3></div><div class=\"thrv_wrapper thrv_text_element dynamic-group-juo8pmn3\" data-css=\"tve-u-16a3070f467\"><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p></div></div></div><div class=\"tcb-flex-col c-33\" data-css=\"tve-u-16a6d4b031d\" style=\"\"><div class=\"tcb-col dynamic-group-jvdrha52\" data-css=\"tve-u-16a48da7a0d\"><div class=\"thrv_wrapper thrv_contentbox_shortcode thrv-content-box dynamic-group-jvdrgsih\" data-css=\"tve-u-16a4eca87b0\">\n	<div class=\"tve-content-box-background\"></div>\n	<div class=\"tve-cb\" data-css=\"tve-u-16a4ecad6e4\"><div class=\"tcb-clear\" data-css=\"tve-u-16a92489b6a\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrfoj4\" data-css=\"tve-u-16a9232f0da\" data-tcb_hover_state_parent=\"\" data-float=\"1\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Read Review</span></span>\n	</a>\n</div></div><div class=\"tcb-clear\" data-css=\"tve-u-16a924b3d0b\"><div class=\"thrv_wrapper thrv-button dynamic-group-jvdrg8bl\" data-css=\"tve-u-16a923a5462\" data-tcb_hover_state_parent=\"\">\n	<a href=\"#\" class=\"tcb-button-link\">\n		<span class=\"tcb-button-texts\"><span class=\"tcb-button-text thrv-inline-text\">Visit Website</span></span>\n	</a>\n</div></div></div>\n</div></div></div></div></div></div>\n</div></div>\n</div></div>', 'Resource List 05', '', 'publish', 'closed', 'closed', '', 'resource-list-05', '', '', '2020-04-14 15:47:57', '2020-04-14 15:47:57', '', 0, 'http://localhost/coursemodule/index.php/tcb_content_template/resource-list-05/', 0, 'tcb_content_template', '', 0),
(57, 1, '2020-04-14 15:48:08', '2020-04-14 15:48:08', '<h2>Recommended Resources</h2><h3>Title goes here</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p><h3>Title goes here</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p><h3>Title goes here</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>', 'Login', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2020-04-14 15:48:08', '2020-04-14 15:48:08', '', 49, 'http://localhost/coursemodule/index.php/2020/04/14/49-revision-v1/', 0, 'revision', '', 0),
(58, 1, '2020-04-14 15:48:09', '2020-04-14 15:48:09', '<h2>Recommended Resources</h2><h3>Title goes here</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p><h3>Title goes here</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p><h3>Title goes here</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>', 'Login', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2020-04-14 15:48:09', '2020-04-14 15:48:09', '', 49, 'http://localhost/coursemodule/index.php/2020/04/14/49-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2020-04-14 15:55:24', '2020-04-14 15:55:24', '', 'Login', '', 'inherit', 'closed', 'closed', '', '49-revision-v1', '', '', '2020-04-14 15:55:24', '2020-04-14 15:55:24', '', 49, 'http://localhost/coursemodule/index.php/2020/04/14/49-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2020-04-14 17:10:02', '2020-04-14 17:10:02', '<!-- wp:spacer -->\n<div style=\"height:100px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:image {\"id\":43,\"width\":700,\"height\":334,\"sizeSlug\":\"large\",\"className\":\"is-style-default\"} -->\n<figure class=\"wp-block-image size-large is-resized is-style-default\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/mrsms-banner-99051406db04513c.png\" alt=\"\" class=\"wp-image-43\" width=\"700\" height=\"334\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:media-text {\"mediaId\":21,\"mediaLink\":\"http://localhost/coursemodule/client-2/\",\"mediaType\":\"image\"} -->\n<div class=\"wp-block-media-text alignwide is-stacked-on-mobile\"><figure class=\"wp-block-media-text__media\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/client-2.png\" alt=\"\" class=\"wp-image-21\"/></figure><div class=\"wp-block-media-text__content\"><!-- wp:paragraph {\"placeholder\":\"Content…\",\"fontSize\":\"large\"} -->\n<p class=\"has-large-font-size\">smSAMDm,m,dDd</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>jsdjjdsjkDS</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph {\"textColor\":\"vivid-green-cyan\"} -->\n<p class=\"has-text-color has-vivid-green-cyan-color\"></p>\n<!-- /wp:paragraph --></div></div>\n<!-- /wp:media-text -->', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-14 17:10:02', '2020-04-14 17:10:02', '', 30, 'http://localhost/coursemodule/index.php/2020/04/14/30-revision-v1/', 0, 'revision', '', 0),
(62, 1, '2020-04-14 17:54:56', '2020-04-14 17:54:56', '<!-- wp:spacer -->\n<div style=\"height:100px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:image {\"id\":43,\"width\":700,\"height\":334,\"sizeSlug\":\"large\",\"className\":\"is-style-default\"} -->\n<figure class=\"wp-block-image size-large is-resized is-style-default\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/mrsms-banner-99051406db04513c.png\" alt=\"\" class=\"wp-image-43\" width=\"700\" height=\"334\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:image {\"id\":21,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/client-2.png\" alt=\"\" class=\"wp-image-21\"/></figure>\n<!-- /wp:image -->', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-14 17:54:56', '2020-04-14 17:54:56', '', 30, 'http://localhost/coursemodule/index.php/2020/04/14/30-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2020-04-14 18:04:40', '2020-04-14 18:04:40', '<!-- wp:spacer -->\n<div style=\"height:100px\" aria-hidden=\"true\" class=\"wp-block-spacer\"></div>\n<!-- /wp:spacer -->\n\n<!-- wp:image {\"id\":43,\"width\":700,\"height\":334,\"sizeSlug\":\"large\",\"className\":\"is-style-default\"} -->\n<figure class=\"wp-block-image size-large is-resized is-style-default\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/mrsms-banner-99051406db04513c.png\" alt=\"\" class=\"wp-image-43\" width=\"700\" height=\"334\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:image {\"id\":21,\"sizeSlug\":\"large\"} -->\n<figure class=\"wp-block-image size-large\"><img src=\"http://localhost/coursemodule/wp-content/uploads/2020/04/client-2.png\" alt=\"\" class=\"wp-image-21\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'test page', '', 'inherit', 'closed', 'closed', '', '30-revision-v1', '', '', '2020-04-14 18:04:40', '2020-04-14 18:04:40', '', 30, 'http://localhost/coursemodule/index.php/2020/04/14/30-revision-v1/', 0, 'revision', '', 0),
(64, 1, '2020-04-14 18:09:15', '2020-04-14 18:09:15', '{\n    \"videoCourse::custom_logo\": {\n        \"value\": 18,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-04-14 18:09:15\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '744c532c-b0f0-448e-af69-8b65fe4bdd23', '', '', '2020-04-14 18:09:15', '2020-04-14 18:09:15', '', 0, 'http://localhost/coursemodule/index.php/2020/04/14/744c532c-b0f0-448e-af69-8b65fe4bdd23/', 0, 'customize_changeset', '', 0),
(65, 1, '2020-04-15 08:02:21', '2020-04-15 08:02:21', '', 'roland', '', 'inherit', 'open', 'closed', '', 'roland', '', '', '2020-04-15 08:02:21', '2020-04-15 08:02:21', '', 0, 'http://localhost/coursemodule/wp-content/uploads/2020/04/roland.jpg', 0, 'attachment', 'image/jpeg', 0),
(66, 1, '2020-04-15 08:02:31', '2020-04-15 08:02:31', '{\n    \"videoCourse::custom_logo\": {\n        \"value\": 65,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-04-15 08:02:31\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'c89ecc74-9343-447a-b943-0f40ccda0a53', '', '', '2020-04-15 08:02:31', '2020-04-15 08:02:31', '', 0, 'http://localhost/coursemodule/index.php/2020/04/15/c89ecc74-9343-447a-b943-0f40ccda0a53/', 0, 'customize_changeset', '', 0),
(67, 1, '2020-04-15 08:04:32', '2020-04-15 08:04:32', '', 'Profile', '', 'publish', 'closed', 'closed', '', 'profile', '', '', '2020-04-15 08:05:02', '2020-04-15 08:05:02', '', 0, 'http://localhost/coursemodule/?page_id=67', 0, 'page', '', 0),
(68, 1, '2020-04-15 08:04:32', '2020-04-15 08:04:32', '', 'Profile', '', 'inherit', 'closed', 'closed', '', '67-revision-v1', '', '', '2020-04-15 08:04:32', '2020-04-15 08:04:32', '', 67, 'http://localhost/coursemodule/index.php/2020/04/15/67-revision-v1/', 0, 'revision', '', 0),
(74, 1, '2020-04-15 11:25:44', '2020-04-15 11:25:44', '<!-- wp:paragraph -->\n<p>jjghjjggjgjgj</p>\n<!-- /wp:paragraph -->', 'test course', '', 'publish', 'open', 'closed', '', 'test-course', '', '', '2020-04-16 00:57:46', '2020-04-16 00:57:46', '', 0, 'http://localhost/coursemodule/?post_type=coursemodules&#038;p=74', 0, 'coursemodules', '', 0),
(75, 1, '2020-04-15 11:25:44', '2020-04-15 11:25:44', '<!-- wp:paragraph -->\n<p>jjghjjggjgjgj</p>\n<!-- /wp:paragraph -->', 'test course', '', 'inherit', 'closed', 'closed', '', '74-revision-v1', '', '', '2020-04-15 11:25:44', '2020-04-15 11:25:44', '', 74, 'http://localhost/coursemodule/index.php/2020/04/15/74-revision-v1/', 0, 'revision', '', 0),
(82, 1, '2020-04-16 01:01:03', '2020-04-16 01:01:03', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:5:\"video\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:8:\"seamless\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Video', 'video', 'publish', 'closed', 'closed', '', 'group_5e97ae3f3e27f', '', '', '2020-05-01 22:40:35', '2020-05-01 22:40:35', '', 0, 'http://localhost/coursemodule/?post_type=acf-field-group&#038;p=82', 0, 'acf-field-group', '', 0),
(83, 1, '2020-04-16 01:04:49', '2020-04-16 01:04:49', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:11:\"Video Title\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Video Title', 'video_title', 'publish', 'closed', 'closed', '', 'field_5e97ae5a532d7', '', '', '2020-04-16 01:04:49', '2020-04-16 01:04:49', '', 82, 'http://localhost/coursemodule/?post_type=acf-field&p=83', 0, 'acf-field', '', 0),
(84, 1, '2020-04-16 01:04:49', '2020-04-16 01:04:49', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Video Link', 'video_link', 'publish', 'closed', 'closed', '', 'field_5e97ae87532d8', '', '', '2020-04-16 01:04:49', '2020-04-16 01:04:49', '', 82, 'http://localhost/coursemodule/?post_type=acf-field&p=84', 1, 'acf-field', '', 0),
(85, 1, '2020-04-16 01:04:49', '2020-04-16 01:04:49', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:46:\"Brief description about  this particular video\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Short Description', 'short_description', 'publish', 'closed', 'closed', '', 'field_5e97aed1532d9', '', '', '2020-04-16 01:04:49', '2020-04-16 01:04:49', '', 82, 'http://localhost/coursemodule/?post_type=acf-field&p=85', 2, 'acf-field', '', 0),
(86, 1, '2020-04-16 01:25:49', '2020-04-16 01:25:49', '', 'Exakte Kompagne 12', '', 'publish', 'open', 'closed', '', 'exakte-kompagne', '', '', '2020-05-03 16:35:14', '2020-05-03 16:35:14', '', 0, 'http://localhost/coursemodule/?post_type=video&#038;p=86', 2, 'video', '', 0),
(87, 1, '2020-04-16 01:25:49', '2020-04-16 01:25:49', '', 'Test', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2020-04-16 01:25:49', '2020-04-16 01:25:49', '', 86, 'http://localhost/coursemodule/index.php/2020/04/16/86-revision-v1/', 0, 'revision', '', 0),
(88, 1, '2020-04-16 01:25:51', '2020-04-16 01:25:51', '', 'Test', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2020-04-16 01:25:51', '2020-04-16 01:25:51', '', 86, 'http://localhost/coursemodule/index.php/2020/04/16/86-revision-v1/', 0, 'revision', '', 0),
(89, 1, '2020-04-16 01:30:56', '2020-04-16 01:30:56', '', 'Sponsored Brands', '', 'publish', 'open', 'closed', '', 'sponsered-brands', '', '', '2020-05-03 10:04:38', '2020-05-03 10:04:38', '', 0, 'http://localhost/coursemodule/?post_type=video&#038;p=89', 3, 'video', '', 0),
(90, 1, '2020-04-16 01:30:56', '2020-04-16 01:30:56', '', 'Test 2', '', 'inherit', 'closed', 'closed', '', '89-revision-v1', '', '', '2020-04-16 01:30:56', '2020-04-16 01:30:56', '', 89, 'http://localhost/coursemodule/index.php/2020/04/16/89-revision-v1/', 0, 'revision', '', 0),
(91, 1, '2020-04-16 01:30:58', '2020-04-16 01:30:58', '', 'Test 2', '', 'inherit', 'closed', 'closed', '', '89-revision-v1', '', '', '2020-04-16 01:30:58', '2020-04-16 01:30:58', '', 89, 'http://localhost/coursemodule/index.php/2020/04/16/89-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2020-05-01 15:26:57', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'draft', 'open', 'closed', '', '', '', '', '2020-05-01 15:26:57', '2020-05-01 15:26:57', '', 0, 'http://localhost/coursemodule/?post_type=video&#038;p=92', 1, 'video', '', 0),
(93, 1, '2020-04-16 02:19:42', '2020-04-16 02:19:42', '', 'Auto Draft', '', 'inherit', 'closed', 'closed', '', '92-revision-v1', '', '', '2020-04-16 02:19:42', '2020-04-16 02:19:42', '', 92, 'http://localhost/coursemodule/index.php/2020/04/16/92-revision-v1/', 0, 'revision', '', 0),
(96, 1, '2020-04-16 12:16:07', '2020-04-16 12:16:07', '', 'Training', '', 'publish', 'closed', 'closed', '', 'course-page', '', '', '2020-04-24 22:20:03', '2020-04-24 22:20:03', '', 0, 'http://localhost/coursemodule/?page_id=96', 0, 'page', '', 0),
(97, 1, '2020-04-16 12:16:07', '2020-04-16 12:16:07', '', 'Course Page', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2020-04-16 12:16:07', '2020-04-16 12:16:07', '', 96, 'http://localhost/coursemodule/index.php/2020/04/16/96-revision-v1/', 0, 'revision', '', 0),
(108, 1, '2020-04-19 12:16:57', '2020-04-19 12:16:57', '', 'show courses', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2020-04-19 12:16:57', '2020-04-19 12:16:57', '', 96, 'http://localhost/coursemodule/index.php/2020/04/19/96-revision-v1/', 0, 'revision', '', 0),
(109, 1, '2020-04-19 12:35:07', '2020-04-19 12:35:07', '', 'view complete course', '', 'publish', 'closed', 'closed', '', 'view-complete-course', '', '', '2020-04-19 12:40:52', '2020-04-19 12:40:52', '', 0, 'http://localhost/coursemodule/?page_id=109', 0, 'page', '', 0),
(110, 1, '2020-04-19 12:35:07', '2020-04-19 12:35:07', '', 'view complete course', '', 'inherit', 'closed', 'closed', '', '109-revision-v1', '', '', '2020-04-19 12:35:07', '2020-04-19 12:35:07', '', 109, 'http://localhost/coursemodule/index.php/2020/04/19/109-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(111, 1, '2020-04-19 12:39:59', '2020-04-19 12:39:59', '', 'view complete course', '', 'inherit', 'closed', 'closed', '', '109-autosave-v1', '', '', '2020-04-19 12:39:59', '2020-04-19 12:39:59', '', 109, 'http://localhost/coursemodule/index.php/2020/04/19/109-autosave-v1/', 0, 'revision', '', 0),
(113, 1, '2020-04-19 15:43:12', '2020-04-19 15:43:12', '', 'Exakte Kompagne', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2020-04-19 15:43:12', '2020-04-19 15:43:12', '', 86, 'http://localhost/coursemodule/index.php/2020/04/19/86-revision-v1/', 0, 'revision', '', 0),
(114, 1, '2020-04-19 15:43:40', '2020-04-19 15:43:40', '', 'Sponsored Brands', '', 'inherit', 'closed', 'closed', '', '89-revision-v1', '', '', '2020-04-19 15:43:40', '2020-04-19 15:43:40', '', 89, 'http://localhost/coursemodule/index.php/2020/04/19/89-revision-v1/', 0, 'revision', '', 0),
(115, 1, '2020-04-19 15:46:21', '2020-04-19 15:46:21', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Video Length', 'video_length_', 'publish', 'closed', 'closed', '', 'field_5e9c7221c0dd3', '', '', '2020-04-19 15:46:21', '2020-04-19 15:46:21', '', 82, 'http://localhost/coursemodule/?post_type=acf-field&p=115', 3, 'acf-field', '', 0),
(117, 1, '2020-04-19 15:48:01', '2020-04-19 15:48:01', '', 'Exakte Kompagne', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2020-04-19 15:48:01', '2020-04-19 15:48:01', '', 86, 'http://localhost/coursemodule/index.php/2020/04/19/86-revision-v1/', 0, 'revision', '', 0),
(118, 1, '2020-04-19 15:48:24', '2020-04-19 15:48:24', '', 'Sponsored Brands', '', 'inherit', 'closed', 'closed', '', '89-revision-v1', '', '', '2020-04-19 15:48:24', '2020-04-19 15:48:24', '', 89, 'http://localhost/coursemodule/index.php/2020/04/19/89-revision-v1/', 0, 'revision', '', 0),
(119, 1, '2020-04-19 17:09:46', '2020-04-19 17:09:46', '', 'Targeting Kampagne', '', 'publish', 'open', 'closed', '', 'targeting-kampagne', '', '', '2020-05-01 16:38:07', '2020-05-01 16:38:07', '', 0, 'http://localhost/coursemodule/?post_type=video&#038;p=119', 10, 'video', '', 0),
(120, 1, '2020-04-19 17:09:46', '2020-04-19 17:09:46', '', 'Targeting Kampagne', '', 'inherit', 'closed', 'closed', '', '119-revision-v1', '', '', '2020-04-19 17:09:46', '2020-04-19 17:09:46', '', 119, 'http://localhost/coursemodule/index.php/2020/04/19/119-revision-v1/', 0, 'revision', '', 0),
(121, 1, '2020-04-19 17:09:49', '2020-04-19 17:09:49', '', 'Targeting Kampagne', '', 'inherit', 'closed', 'closed', '', '119-revision-v1', '', '', '2020-04-19 17:09:49', '2020-04-19 17:09:49', '', 119, 'http://localhost/coursemodule/index.php/2020/04/19/119-revision-v1/', 0, 'revision', '', 0),
(123, 1, '2020-04-19 19:28:52', '2020-04-19 19:28:52', '', 'Targeting Kampagne', '', 'inherit', 'closed', 'closed', '', '119-revision-v1', '', '', '2020-04-19 19:28:52', '2020-04-19 19:28:52', '', 119, 'http://localhost/coursemodule/index.php/2020/04/19/119-revision-v1/', 0, 'revision', '', 0),
(124, 1, '2020-04-19 19:41:43', '2020-04-19 19:41:43', '', 'Targeting Kampagne', '', 'inherit', 'closed', 'closed', '', '119-revision-v1', '', '', '2020-04-19 19:41:43', '2020-04-19 19:41:43', '', 119, 'http://localhost/coursemodule/index.php/2020/04/19/119-revision-v1/', 0, 'revision', '', 0),
(125, 1, '2020-04-19 20:46:32', '2020-04-19 20:46:32', '', 'Automatische kampaign', '', 'publish', 'open', 'closed', '', 'automatische-kampaign', '', '', '2020-05-02 15:44:15', '2020-05-02 15:44:15', '', 0, 'http://localhost/coursemodule/?post_type=video&#038;p=125', 4, 'video', '', 0),
(126, 1, '2020-04-19 20:46:32', '2020-04-19 20:46:32', '', 'Automatische kampaign', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2020-04-19 20:46:32', '2020-04-19 20:46:32', '', 125, 'http://localhost/coursemodule/index.php/2020/04/19/125-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2020-04-19 20:46:34', '2020-04-19 20:46:34', '', 'Automatische kampaign', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2020-04-19 20:46:34', '2020-04-19 20:46:34', '', 125, 'http://localhost/coursemodule/index.php/2020/04/19/125-revision-v1/', 0, 'revision', '', 0),
(128, 1, '2020-04-19 20:47:28', '2020-04-19 20:47:28', '', 'Automatische kampaign', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2020-04-19 20:47:28', '2020-04-19 20:47:28', '', 125, 'http://localhost/coursemodule/index.php/2020/04/19/125-revision-v1/', 0, 'revision', '', 0),
(129, 1, '2020-04-20 00:13:01', '2020-04-20 00:13:01', '', 'Automatische kampaign', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2020-04-20 00:13:01', '2020-04-20 00:13:01', '', 125, 'http://localhost/coursemodule/index.php/2020/04/20/125-revision-v1/', 0, 'revision', '', 0),
(130, 1, '2020-04-20 14:46:24', '2020-04-20 14:46:24', '', 'Sponsored Brands', '', 'inherit', 'closed', 'closed', '', '89-revision-v1', '', '', '2020-04-20 14:46:24', '2020-04-20 14:46:24', '', 89, 'http://localhost/coursemodule/index.php/2020/04/20/89-revision-v1/', 0, 'revision', '', 0),
(131, 1, '2020-04-20 14:47:10', '2020-04-20 14:47:10', '', 'Exakte Kompagne', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2020-04-20 14:47:10', '2020-04-20 14:47:10', '', 86, 'http://localhost/coursemodule/index.php/2020/04/20/86-revision-v1/', 0, 'revision', '', 0),
(132, 1, '2020-04-20 20:06:52', '2020-04-20 20:06:52', '', 'Exakte Kompagne 12', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2020-04-20 20:06:52', '2020-04-20 20:06:52', '', 86, 'http://localhost/coursemodule/index.php/2020/04/20/86-revision-v1/', 0, 'revision', '', 0),
(133, 1, '2020-04-20 20:06:57', '2020-04-20 20:06:57', '', 'Exakte Kompagne 12', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2020-04-20 20:06:57', '2020-04-20 20:06:57', '', 86, 'http://localhost/coursemodule/index.php/2020/04/20/86-revision-v1/', 0, 'revision', '', 0),
(137, 1, '2020-04-20 21:07:23', '2020-04-20 21:07:23', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"task\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";a:14:{i:0;s:9:\"permalink\";i:1;s:11:\"the_content\";i:2;s:7:\"excerpt\";i:3;s:10:\"discussion\";i:4;s:8:\"comments\";i:5;s:9:\"revisions\";i:6;s:4:\"slug\";i:7;s:6:\"author\";i:8;s:6:\"format\";i:9;s:15:\"page_attributes\";i:10;s:14:\"featured_image\";i:11;s:10:\"categories\";i:12;s:4:\"tags\";i:13;s:15:\"send-trackbacks\";}s:11:\"description\";s:0:\"\";}', 'Tasks', 'tasks', 'trash', 'closed', 'closed', '', 'group_5e9e0e736c9df__trashed', '', '', '2020-05-01 21:38:58', '2020-05-01 21:38:58', '', 0, 'http://localhost/coursemodule/?post_type=acf-field-group&#038;p=137', 0, 'acf-field-group', '', 0),
(138, 1, '2020-04-20 21:10:05', '2020-04-20 21:10:05', 'a:11:{s:4:\"type\";s:11:\"post_object\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:5:\"video\";}s:8:\"taxonomy\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:13:\"return_format\";s:2:\"id\";s:2:\"ui\";i:1;}', 'Select video this task belongs to', 'video_task', 'trash', 'closed', 'closed', '', 'field_5e9e0f0f9bab3__trashed', '', '', '2020-05-01 21:38:58', '2020-05-01 21:38:58', '', 137, 'http://localhost/coursemodule/?post_type=acf-field&#038;p=138', 0, 'acf-field', '', 0),
(139, 1, '2020-04-20 21:12:54', '2020-04-20 21:12:54', '', 'Visit 5 companies in 5 days', '', 'publish', 'open', 'closed', '', 'visit-5-companies-in-5-days', '', '', '2020-05-01 15:26:58', '2020-05-01 15:26:58', '', 0, 'http://localhost/coursemodule/?post_type=task&#038;p=139', 11, 'video', '', 0),
(140, 1, '2020-04-20 21:12:54', '2020-04-20 21:12:54', '', 'Visit 5 companies in 5 days', '', 'inherit', 'closed', 'closed', '', '139-revision-v1', '', '', '2020-04-20 21:12:54', '2020-04-20 21:12:54', '', 139, 'http://localhost/coursemodule/index.php/2020/04/20/139-revision-v1/', 0, 'revision', '', 0),
(141, 1, '2020-04-20 21:12:56', '2020-04-20 21:12:56', '', 'Visit 5 companies in 5 days', '', 'inherit', 'closed', 'closed', '', '139-revision-v1', '', '', '2020-04-20 21:12:56', '2020-04-20 21:12:56', '', 139, 'http://localhost/coursemodule/index.php/2020/04/20/139-revision-v1/', 0, 'revision', '', 0),
(142, 1, '2020-04-20 21:16:43', '2020-04-20 21:16:43', '', 'Visit 5 companies in 5 days', '', 'inherit', 'closed', 'closed', '', '139-revision-v1', '', '', '2020-04-20 21:16:43', '2020-04-20 21:16:43', '', 139, 'http://localhost/coursemodule/index.php/2020/04/20/139-revision-v1/', 0, 'revision', '', 0),
(143, 1, '2020-04-20 21:58:29', '2020-04-20 21:58:29', '', 'Conduct data collection using  Survey', '', 'publish', 'open', 'closed', '', 'conduct-data-collection-using-survey', '', '', '2020-05-01 16:38:07', '2020-05-01 16:38:07', '', 0, 'http://localhost/coursemodule/?post_type=task&#038;p=143', 8, 'video', '', 0),
(144, 1, '2020-04-20 21:58:29', '2020-04-20 21:58:29', '', 'Conduct data collection using  Survey', '', 'inherit', 'closed', 'closed', '', '143-revision-v1', '', '', '2020-04-20 21:58:29', '2020-04-20 21:58:29', '', 143, 'http://localhost/coursemodule/index.php/2020/04/20/143-revision-v1/', 0, 'revision', '', 0),
(145, 1, '2020-04-20 21:58:31', '2020-04-20 21:58:31', '', 'Conduct data collection using  Survey', '', 'inherit', 'closed', 'closed', '', '143-revision-v1', '', '', '2020-04-20 21:58:31', '2020-04-20 21:58:31', '', 143, 'http://localhost/coursemodule/index.php/2020/04/20/143-revision-v1/', 0, 'revision', '', 0),
(146, 1, '2020-04-20 22:51:35', '2020-04-20 22:51:35', '', 'sponsored-brands sample task', '', 'publish', 'open', 'closed', '', 'sponsored-brands-sample-task', '', '', '2020-05-01 17:22:46', '2020-05-01 17:22:46', '', 0, 'http://localhost/coursemodule/?post_type=task&#038;p=146', 3, 'task', '', 0),
(147, 1, '2020-04-20 22:51:35', '2020-04-20 22:51:35', '', 'sponsored-brands sample task', '', 'inherit', 'closed', 'closed', '', '146-revision-v1', '', '', '2020-04-20 22:51:35', '2020-04-20 22:51:35', '', 146, 'http://localhost/coursemodule/index.php/2020/04/20/146-revision-v1/', 0, 'revision', '', 0),
(148, 1, '2020-04-20 22:51:39', '2020-04-20 22:51:39', '', 'sponsored-brands sample task', '', 'inherit', 'closed', 'closed', '', '146-revision-v1', '', '', '2020-04-20 22:51:39', '2020-04-20 22:51:39', '', 146, 'http://localhost/coursemodule/index.php/2020/04/20/146-revision-v1/', 0, 'revision', '', 0),
(149, 1, '2020-04-20 23:07:44', '2020-04-20 23:07:44', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";i:0;s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', '', 'mark_as_watched', 'trash', 'closed', 'closed', '', 'field_5e9e2af07c1ea__trashed', '', '', '2020-05-01 21:38:58', '2020-05-01 21:38:58', '', 137, 'http://localhost/coursemodule/?post_type=acf-field&#038;p=149', 1, 'acf-field', '', 0),
(153, 1, '2020-04-20 23:47:33', '2020-04-20 23:47:33', '', 'Visit 5 companies in 5 days', '', 'inherit', 'closed', 'closed', '', '139-revision-v1', '', '', '2020-04-20 23:47:33', '2020-04-20 23:47:33', '', 139, 'http://localhost/coursemodule/index.php/2020/04/20/139-revision-v1/', 0, 'revision', '', 0),
(154, 1, '2020-04-20 23:54:43', '2020-04-20 23:54:43', '', 'Conduct data collection using  Survey', '', 'inherit', 'closed', 'closed', '', '143-revision-v1', '', '', '2020-04-20 23:54:43', '2020-04-20 23:54:43', '', 143, 'http://localhost/coursemodule/index.php/2020/04/20/143-revision-v1/', 0, 'revision', '', 0),
(155, 1, '2020-04-20 23:59:36', '2020-04-20 23:59:36', '', 'Conduct data collection using  Survey', '', 'inherit', 'closed', 'closed', '', '143-revision-v1', '', '', '2020-04-20 23:59:36', '2020-04-20 23:59:36', '', 143, 'http://localhost/coursemodule/index.php/2020/04/20/143-revision-v1/', 0, 'revision', '', 0),
(156, 1, '2020-04-21 00:54:15', '2020-04-21 00:54:15', '', 'Visit 5 companies in 5 days', '', 'inherit', 'closed', 'closed', '', '139-revision-v1', '', '', '2020-04-21 00:54:15', '2020-04-21 00:54:15', '', 139, 'http://localhost/coursemodule/index.php/2020/04/21/139-revision-v1/', 0, 'revision', '', 0),
(157, 1, '2020-04-21 15:16:42', '2020-04-21 15:16:42', '', 'Visit 5 companies in 5 days', '', 'inherit', 'closed', 'closed', '', '139-revision-v1', '', '', '2020-04-21 15:16:42', '2020-04-21 15:16:42', '', 139, 'http://localhost/coursemodule/index.php/2020/04/21/139-revision-v1/', 0, 'revision', '', 0),
(158, 1, '2020-04-21 15:17:19', '2020-04-21 15:17:19', '', 'Conduct data collection using  Survey', '', 'inherit', 'closed', 'closed', '', '143-revision-v1', '', '', '2020-04-21 15:17:19', '2020-04-21 15:17:19', '', 143, 'http://localhost/coursemodule/index.php/2020/04/21/143-revision-v1/', 0, 'revision', '', 0),
(159, 1, '2020-04-21 15:31:38', '2020-04-21 15:31:38', '', 'Visit 5 companies in 5 days', '', 'inherit', 'closed', 'closed', '', '139-revision-v1', '', '', '2020-04-21 15:31:38', '2020-04-21 15:31:38', '', 139, 'http://localhost/coursemodule/index.php/2020/04/21/139-revision-v1/', 0, 'revision', '', 0),
(160, 1, '2020-04-21 15:52:03', '2020-04-21 15:52:03', '', 'Visit 5 companies in 5 days', '', 'inherit', 'closed', 'closed', '', '139-revision-v1', '', '', '2020-04-21 15:52:03', '2020-04-21 15:52:03', '', 139, 'http://localhost/coursemodule/index.php/2020/04/21/139-revision-v1/', 0, 'revision', '', 0),
(161, 1, '2020-04-21 15:52:42', '2020-04-21 15:52:42', '', 'Conduct data collection using  Survey', '', 'inherit', 'closed', 'closed', '', '143-revision-v1', '', '', '2020-04-21 15:52:42', '2020-04-21 15:52:42', '', 143, 'http://localhost/coursemodule/index.php/2020/04/21/143-revision-v1/', 0, 'revision', '', 0),
(162, 1, '2020-04-21 21:38:44', '2020-04-21 21:38:44', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:7:\"pending\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', '', 'mark_as_watched', 'publish', 'closed', 'closed', '', 'field_5e9f67bdac35a', '', '', '2020-04-21 21:38:44', '2020-04-21 21:38:44', '', 82, 'http://localhost/coursemodule/?post_type=acf-field&p=162', 4, 'acf-field', '', 0),
(163, 1, '2020-04-21 22:34:04', '2020-04-21 22:34:04', '', 'sponsored-brands sample task', '', 'inherit', 'closed', 'closed', '', '146-revision-v1', '', '', '2020-04-21 22:34:04', '2020-04-21 22:34:04', '', 146, 'http://localhost/coursemodule/index.php/2020/04/21/146-revision-v1/', 0, 'revision', '', 0),
(164, 1, '2020-04-21 22:36:27', '2020-04-21 22:36:27', '', 'sponsored-brands sample task', '', 'inherit', 'closed', 'closed', '', '146-revision-v1', '', '', '2020-04-21 22:36:27', '2020-04-21 22:36:27', '', 146, 'http://localhost/coursemodule/index.php/2020/04/21/146-revision-v1/', 0, 'revision', '', 0),
(165, 1, '2020-04-22 19:13:00', '2020-04-22 19:13:00', '', 'Targeting Kampagne', '', 'inherit', 'closed', 'closed', '', '119-revision-v1', '', '', '2020-04-22 19:13:00', '2020-04-22 19:13:00', '', 119, 'http://localhost/coursemodule/index.php/2020/04/22/119-revision-v1/', 0, 'revision', '', 0),
(166, 1, '2020-04-22 19:25:47', '2020-04-22 19:25:47', '', 'Targeting Kampagne', '', 'inherit', 'closed', 'closed', '', '119-revision-v1', '', '', '2020-04-22 19:25:47', '2020-04-22 19:25:47', '', 119, 'http://localhost/coursemodule/index.php/2020/04/22/119-revision-v1/', 0, 'revision', '', 0),
(167, 1, '2020-04-22 20:46:19', '2020-04-22 20:46:19', '', 'Take a tour in 5 companies', '', 'publish', 'open', 'closed', '', 'take-a-tour-in-5-companies', '', '', '2020-05-01 16:38:07', '2020-05-01 16:38:07', '', 0, 'http://localhost/coursemodule/?post_type=task&#038;p=167', 9, 'video', '', 0),
(168, 1, '2020-04-22 20:46:19', '2020-04-22 20:46:19', '', 'Take a tour in 5 companies', '', 'inherit', 'closed', 'closed', '', '167-revision-v1', '', '', '2020-04-22 20:46:19', '2020-04-22 20:46:19', '', 167, 'http://localhost/coursemodule/index.php/2020/04/22/167-revision-v1/', 0, 'revision', '', 0),
(169, 1, '2020-04-22 20:46:21', '2020-04-22 20:46:21', '', 'Take a tour in 5 companies', '', 'inherit', 'closed', 'closed', '', '167-revision-v1', '', '', '2020-04-22 20:46:21', '2020-04-22 20:46:21', '', 167, 'http://localhost/coursemodule/index.php/2020/04/22/167-revision-v1/', 0, 'revision', '', 0),
(171, 1, '2020-04-22 20:54:00', '2020-04-22 20:54:00', '', 'Take a tour in 5 companies', '', 'publish', 'open', 'closed', '', 'take-a-tour-in-5-companies', '', '', '2020-05-01 17:22:46', '2020-05-01 17:22:46', '', 0, 'http://localhost/coursemodule/?post_type=task&#038;p=171', 4, 'task', '', 0),
(172, 1, '2020-04-22 20:54:00', '2020-04-22 20:54:00', '', 'Take a tour in 5 companies', '', 'inherit', 'closed', 'closed', '', '171-revision-v1', '', '', '2020-04-22 20:54:00', '2020-04-22 20:54:00', '', 171, 'http://localhost/coursemodule/index.php/2020/04/22/171-revision-v1/', 0, 'revision', '', 0),
(173, 1, '2020-04-22 20:54:03', '2020-04-22 20:54:03', '', 'Take a tour in 5 companies', '', 'inherit', 'closed', 'closed', '', '171-revision-v1', '', '', '2020-04-22 20:54:03', '2020-04-22 20:54:03', '', 171, 'http://localhost/coursemodule/index.php/2020/04/22/171-revision-v1/', 0, 'revision', '', 0),
(174, 1, '2020-04-24 22:01:48', '2020-04-24 22:01:48', '', 'Roland Logo', '', 'inherit', 'open', 'closed', '', 'roland-logo-2', '', '', '2020-04-24 22:01:48', '2020-04-24 22:01:48', '', 0, 'http://localhost/coursemodule/wp-content/uploads/2020/04/Roland-Logo.jpg', 0, 'attachment', 'image/jpeg', 0),
(175, 1, '2020-04-24 22:01:57', '2020-04-24 22:01:57', '{\n    \"videoCourse::custom_logo\": {\n        \"value\": 174,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-04-24 22:01:57\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', 'e89f309b-5052-4055-a880-6c59c1e00f11', '', '', '2020-04-24 22:01:57', '2020-04-24 22:01:57', '', 0, 'http://localhost/coursemodule/index.php/2020/04/24/e89f309b-5052-4055-a880-6c59c1e00f11/', 0, 'customize_changeset', '', 0),
(176, 1, '2020-04-24 22:20:03', '2020-04-24 22:20:03', '', 'Training', '', 'inherit', 'closed', 'closed', '', '96-revision-v1', '', '', '2020-04-24 22:20:03', '2020-04-24 22:20:03', '', 96, 'http://localhost/coursemodule/index.php/2020/04/24/96-revision-v1/', 0, 'revision', '', 0),
(177, 1, '2020-04-25 01:39:51', '2020-04-25 01:39:51', '', 'Take a tour in 5 companies', '', 'inherit', 'closed', 'closed', '', '171-revision-v1', '', '', '2020-04-25 01:39:51', '2020-04-25 01:39:51', '', 171, 'http://localhost/coursemodule/index.php/2020/04/25/171-revision-v1/', 0, 'revision', '', 0),
(178, 1, '2020-04-25 17:42:23', '2020-04-25 17:42:23', '', '<i class=\"fas fa-play\"> </i> Trainings', '', 'publish', 'closed', 'closed', '', 'trainings-2', '', '', '2020-05-02 15:35:01', '2020-05-02 15:35:01', '', 0, 'http://localhost/coursemodule/?p=178', 1, 'nav_menu_item', '', 0),
(179, 1, '2020-04-25 17:44:19', '2020-04-25 17:44:19', '{\n    \"videoCourse::custom_logo\": {\n        \"value\": 18,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-04-25 17:44:19\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '43453bac-5ccf-4ebd-9526-5c6d5d41d601', '', '', '2020-04-25 17:44:19', '2020-04-25 17:44:19', '', 0, 'http://localhost/coursemodule/index.php/2020/04/25/43453bac-5ccf-4ebd-9526-5c6d5d41d601/', 0, 'customize_changeset', '', 0),
(180, 1, '2020-04-25 17:44:47', '2020-04-25 17:44:47', '{\n    \"videoCourse::custom_logo\": {\n        \"value\": 174,\n        \"type\": \"theme_mod\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2020-04-25 17:44:47\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '52fe3193-11af-489c-b28b-df001dc29b54', '', '', '2020-04-25 17:44:47', '2020-04-25 17:44:47', '', 0, 'http://localhost/coursemodule/index.php/2020/04/25/52fe3193-11af-489c-b28b-df001dc29b54/', 0, 'customize_changeset', '', 0),
(181, 1, '2020-04-25 18:40:54', '2020-04-25 18:40:54', '', 'Automatische kampaign', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2020-04-25 18:40:54', '2020-04-25 18:40:54', '', 125, 'http://localhost/coursemodule/index.php/2020/04/25/125-revision-v1/', 0, 'revision', '', 0),
(182, 1, '2020-04-25 18:42:35', '2020-04-25 18:42:35', '', 'Automatische kampaign', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2020-04-25 18:42:35', '2020-04-25 18:42:35', '', 125, 'http://localhost/coursemodule/index.php/2020/04/25/125-revision-v1/', 0, 'revision', '', 0),
(183, 1, '2020-04-25 23:52:39', '2020-04-25 23:52:39', '', 'Automatische kampaign', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2020-04-25 23:52:39', '2020-04-25 23:52:39', '', 125, 'http://localhost/coursemodule/index.php/2020/04/25/125-revision-v1/', 0, 'revision', '', 0),
(184, 1, '2020-04-25 23:55:33', '2020-04-25 23:55:33', '', 'Automatische kampaign', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2020-04-25 23:55:33', '2020-04-25 23:55:33', '', 125, 'http://localhost/coursemodule/index.php/2020/04/25/125-revision-v1/', 0, 'revision', '', 0),
(185, 1, '2020-04-27 01:46:05', '2020-04-27 01:46:05', '', 'Targeting Kampagne', '', 'inherit', 'closed', 'closed', '', '119-revision-v1', '', '', '2020-04-27 01:46:05', '2020-04-27 01:46:05', '', 119, 'http://localhost/coursemodule/index.php/2020/04/27/119-revision-v1/', 0, 'revision', '', 0),
(186, 1, '2020-04-27 01:46:38', '2020-04-27 01:46:38', '', 'Sponsored Brands', '', 'inherit', 'closed', 'closed', '', '89-revision-v1', '', '', '2020-04-27 01:46:38', '2020-04-27 01:46:38', '', 89, 'http://localhost/coursemodule/index.php/2020/04/27/89-revision-v1/', 0, 'revision', '', 0),
(188, 1, '2020-04-27 03:08:27', '2020-04-27 03:08:27', '', 'Go to JKK and see funiture', '', 'publish', 'open', 'closed', '', 'go-to-jkk-and-see-funiture', '', '', '2020-05-01 17:22:46', '2020-05-01 17:22:46', '', 0, 'http://localhost/coursemodule/?post_type=task&#038;p=188', 1, 'task', '', 0),
(189, 1, '2020-04-27 03:08:27', '2020-04-27 03:08:27', '', 'Go to JKK and see funiture', '', 'inherit', 'closed', 'closed', '', '188-revision-v1', '', '', '2020-04-27 03:08:27', '2020-04-27 03:08:27', '', 188, 'http://localhost/coursemodule/index.php/2020/04/27/188-revision-v1/', 0, 'revision', '', 0),
(190, 1, '2020-04-27 03:08:30', '2020-04-27 03:08:30', '', 'Go to JKK and see funiture', '', 'inherit', 'closed', 'closed', '', '188-revision-v1', '', '', '2020-04-27 03:08:30', '2020-04-27 03:08:30', '', 188, 'http://localhost/coursemodule/index.php/2020/04/27/188-revision-v1/', 0, 'revision', '', 0),
(191, 1, '2020-04-27 03:15:16', '2020-04-27 03:15:16', '', 'Test Magic', '', 'publish', 'open', 'closed', '', 'test-magic', '', '', '2020-05-02 15:44:10', '2020-05-02 15:44:10', '', 0, 'http://localhost/coursemodule/?post_type=video&#038;p=191', 5, 'video', '', 0),
(192, 1, '2020-04-27 03:15:16', '2020-04-27 03:15:16', '', 'Test Magic', '', 'inherit', 'closed', 'closed', '', '191-revision-v1', '', '', '2020-04-27 03:15:16', '2020-04-27 03:15:16', '', 191, 'http://localhost/coursemodule/index.php/2020/04/27/191-revision-v1/', 0, 'revision', '', 0),
(193, 1, '2020-04-27 03:15:19', '2020-04-27 03:15:19', '', 'Test Magic', '', 'inherit', 'closed', 'closed', '', '191-revision-v1', '', '', '2020-04-27 03:15:19', '2020-04-27 03:15:19', '', 191, 'http://localhost/coursemodule/index.php/2020/04/27/191-revision-v1/', 0, 'revision', '', 0),
(194, 1, '2020-04-27 03:16:43', '2020-04-27 03:16:43', '', 'test task item', '', 'publish', 'open', 'closed', '', 'test-task-item', '', '', '2020-05-01 17:22:46', '2020-05-01 17:22:46', '', 0, 'http://localhost/coursemodule/?post_type=task&#038;p=194', 6, 'task', '', 0),
(195, 1, '2020-04-27 03:16:43', '2020-04-27 03:16:43', '', 'test task item', '', 'inherit', 'closed', 'closed', '', '194-revision-v1', '', '', '2020-04-27 03:16:43', '2020-04-27 03:16:43', '', 194, 'http://localhost/coursemodule/index.php/2020/04/27/194-revision-v1/', 0, 'revision', '', 0),
(196, 1, '2020-04-27 03:16:46', '2020-04-27 03:16:46', '', 'test task item', '', 'inherit', 'closed', 'closed', '', '194-revision-v1', '', '', '2020-04-27 03:16:46', '2020-04-27 03:16:46', '', 194, 'http://localhost/coursemodule/index.php/2020/04/27/194-revision-v1/', 0, 'revision', '', 0),
(197, 1, '2020-04-27 03:19:22', '2020-04-27 03:19:22', '', 'test 2 crazy thing', '', 'publish', 'open', 'closed', '', 'test-2-crazy-thing', '', '', '2020-05-01 17:22:46', '2020-05-01 17:22:46', '', 0, 'http://localhost/coursemodule/?post_type=task&#038;p=197', 5, 'task', '', 0),
(198, 1, '2020-04-27 03:19:22', '2020-04-27 03:19:22', '', 'test 2 crazy thing', '', 'inherit', 'closed', 'closed', '', '197-revision-v1', '', '', '2020-04-27 03:19:22', '2020-04-27 03:19:22', '', 197, 'http://localhost/coursemodule/index.php/2020/04/27/197-revision-v1/', 0, 'revision', '', 0),
(199, 1, '2020-04-27 03:19:27', '2020-04-27 03:19:27', '', 'test 2 crazy thing', '', 'inherit', 'closed', 'closed', '', '197-revision-v1', '', '', '2020-04-27 03:19:27', '2020-04-27 03:19:27', '', 197, 'http://localhost/coursemodule/index.php/2020/04/27/197-revision-v1/', 0, 'revision', '', 0),
(200, 1, '2020-04-27 03:28:54', '2020-04-27 03:28:54', '', 'Testing 123', '', 'publish', 'open', 'closed', '', 'testing-123', '', '', '2020-05-01 17:45:15', '2020-05-01 17:45:15', '', 0, 'http://localhost/coursemodule/?post_type=video&#038;p=200', 6, 'video', '', 0),
(201, 1, '2020-04-27 03:28:54', '2020-04-27 03:28:54', '', 'Testing 123', '', 'inherit', 'closed', 'closed', '', '200-revision-v1', '', '', '2020-04-27 03:28:54', '2020-04-27 03:28:54', '', 200, 'http://localhost/coursemodule/index.php/2020/04/27/200-revision-v1/', 0, 'revision', '', 0),
(202, 1, '2020-04-27 03:28:59', '2020-04-27 03:28:59', '', 'Testing 123', '', 'inherit', 'closed', 'closed', '', '200-revision-v1', '', '', '2020-04-27 03:28:59', '2020-04-27 03:28:59', '', 200, 'http://localhost/coursemodule/index.php/2020/04/27/200-revision-v1/', 0, 'revision', '', 0),
(203, 1, '2020-04-27 03:30:39', '2020-04-27 03:30:39', '', 'final test task', '', 'publish', 'open', 'closed', '', 'final-test-task', '', '', '2020-05-01 17:48:31', '2020-05-01 17:48:31', '', 0, 'http://localhost/coursemodule/?post_type=task&#038;p=203', 2, 'task', '', 0),
(204, 1, '2020-04-27 03:30:39', '2020-04-27 03:30:39', '', 'final test task', '', 'inherit', 'closed', 'closed', '', '203-revision-v1', '', '', '2020-04-27 03:30:39', '2020-04-27 03:30:39', '', 203, 'http://localhost/coursemodule/index.php/2020/04/27/203-revision-v1/', 0, 'revision', '', 0),
(205, 1, '2020-04-27 03:30:43', '2020-04-27 03:30:43', '', 'final test task', '', 'inherit', 'closed', 'closed', '', '203-revision-v1', '', '', '2020-04-27 03:30:43', '2020-04-27 03:30:43', '', 203, 'http://localhost/coursemodule/index.php/2020/04/27/203-revision-v1/', 0, 'revision', '', 0),
(206, 1, '2020-04-27 05:59:03', '2020-04-27 05:59:03', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2020-04-27 05:59:03', '2020-04-27 05:59:03', '', 0, 'http://localhost/coursemodule/wp-content/uploads/2020/04/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(208, 1, '2020-04-27 17:27:51', '2020-04-27 17:27:51', '', 'GOLANG programming', '', 'publish', 'open', 'closed', '', 'golang-programming', '', '', '2020-05-01 17:45:15', '2020-05-01 17:45:15', '', 0, 'http://localhost/coursemodule/?post_type=video&#038;p=208', 7, 'video', '', 0),
(209, 1, '2020-04-27 17:27:51', '2020-04-27 17:27:51', '', 'GOLANG programming', '', 'inherit', 'closed', 'closed', '', '208-revision-v1', '', '', '2020-04-27 17:27:51', '2020-04-27 17:27:51', '', 208, 'http://localhost/coursemodule/index.php/2020/04/27/208-revision-v1/', 0, 'revision', '', 0),
(210, 1, '2020-04-27 17:27:55', '2020-04-27 17:27:55', '', 'GOLANG programming', '', 'inherit', 'closed', 'closed', '', '208-revision-v1', '', '', '2020-04-27 17:27:55', '2020-04-27 17:27:55', '', 208, 'http://localhost/coursemodule/index.php/2020/04/27/208-revision-v1/', 0, 'revision', '', 0),
(211, 1, '2020-04-27 17:28:23', '2020-04-27 17:28:23', '', 'GOLANG programming', '', 'inherit', 'closed', 'closed', '', '208-revision-v1', '', '', '2020-04-27 17:28:23', '2020-04-27 17:28:23', '', 208, 'http://localhost/coursemodule/index.php/2020/04/27/208-revision-v1/', 0, 'revision', '', 0),
(212, 1, '2020-04-27 18:07:49', '2020-04-27 18:07:49', '', 'Write 1M  lines of code', '', 'publish', 'open', 'closed', '', 'write-1m-lines-of-code', '', '', '2020-05-01 17:22:46', '2020-05-01 17:22:46', '', 0, 'http://localhost/coursemodule/?post_type=task&#038;p=212', 7, 'task', '', 0),
(213, 1, '2020-04-27 18:07:49', '2020-04-27 18:07:49', '', 'Write 1M  lines of code', '', 'inherit', 'closed', 'closed', '', '212-revision-v1', '', '', '2020-04-27 18:07:49', '2020-04-27 18:07:49', '', 212, 'http://localhost/coursemodule/index.php/2020/04/27/212-revision-v1/', 0, 'revision', '', 0),
(214, 1, '2020-04-27 18:07:51', '2020-04-27 18:07:51', '', 'Write 1M  lines of code', '', 'inherit', 'closed', 'closed', '', '212-revision-v1', '', '', '2020-04-27 18:07:51', '2020-04-27 18:07:51', '', 212, 'http://localhost/coursemodule/index.php/2020/04/27/212-revision-v1/', 0, 'revision', '', 0),
(215, 1, '2020-04-27 18:59:46', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-04-27 18:59:46', '0000-00-00 00:00:00', '', 0, 'http://localhost/coursemodule/?p=215', 1, 'nav_menu_item', '', 0),
(216, 1, '2020-04-27 18:59:46', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-04-27 18:59:46', '0000-00-00 00:00:00', '', 0, 'http://localhost/coursemodule/?p=216', 1, 'nav_menu_item', '', 0),
(217, 1, '2020-04-27 18:59:46', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-04-27 18:59:46', '0000-00-00 00:00:00', '', 0, 'http://localhost/coursemodule/?p=217', 1, 'nav_menu_item', '', 0),
(218, 1, '2020-04-27 18:59:46', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-04-27 18:59:46', '0000-00-00 00:00:00', '', 0, 'http://localhost/coursemodule/?p=218', 1, 'nav_menu_item', '', 0),
(219, 1, '2020-04-27 18:59:46', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-04-27 18:59:46', '0000-00-00 00:00:00', '', 0, 'http://localhost/coursemodule/?p=219', 1, 'nav_menu_item', '', 0),
(220, 1, '2020-04-27 19:00:27', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-04-27 19:00:27', '0000-00-00 00:00:00', '', 0, 'http://localhost/coursemodule/?p=220', 1, 'nav_menu_item', '', 0),
(221, 1, '2020-04-27 19:00:27', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-04-27 19:00:27', '0000-00-00 00:00:00', '', 0, 'http://localhost/coursemodule/?p=221', 1, 'nav_menu_item', '', 0),
(222, 1, '2020-04-27 19:00:27', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-04-27 19:00:27', '0000-00-00 00:00:00', '', 0, 'http://localhost/coursemodule/?p=222', 1, 'nav_menu_item', '', 0),
(223, 1, '2020-04-27 19:00:45', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-04-27 19:00:45', '0000-00-00 00:00:00', '', 0, 'http://localhost/coursemodule/?p=223', 1, 'nav_menu_item', '', 0),
(224, 1, '2020-04-27 19:00:45', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2020-04-27 19:00:45', '0000-00-00 00:00:00', '', 0, 'http://localhost/coursemodule/?p=224', 1, 'nav_menu_item', '', 0),
(226, 1, '2020-05-01 20:36:06', '2020-05-01 20:36:06', '', 'Test Magic', '', 'inherit', 'closed', 'closed', '', '191-autosave-v1', '', '', '2020-05-01 20:36:06', '2020-05-01 20:36:06', '', 191, 'http://localhost/coursemodule/index.php/2020/05/01/191-autosave-v1/', 0, 'revision', '', 0),
(227, 1, '2020-05-01 21:01:02', '2020-05-01 21:01:02', 'a:13:{s:4:\"type\";s:8:\"taxonomy\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:8:\"taxonomy\";s:5:\"Tasks\";s:10:\"field_type\";s:12:\"multi_select\";s:10:\"allow_null\";i:0;s:8:\"add_term\";i:1;s:10:\"save_terms\";i:0;s:10:\"load_terms\";i:0;s:13:\"return_format\";s:2:\"id\";s:8:\"multiple\";i:0;}', 'To Dos', 'to_dos', 'publish', 'closed', 'closed', '', 'field_5eac8d7f4065c', '', '', '2020-05-01 22:40:35', '2020-05-01 22:40:35', '', 82, 'http://localhost/coursemodule/?post_type=acf-field&#038;p=227', 5, 'acf-field', '', 0),
(229, 1, '2020-05-01 21:07:45', '2020-05-01 21:07:45', '', 'Sponsored Brands', '', 'inherit', 'closed', 'closed', '', '89-revision-v1', '', '', '2020-05-01 21:07:45', '2020-05-01 21:07:45', '', 89, 'http://localhost/coursemodule/index.php/2020/05/01/89-revision-v1/', 0, 'revision', '', 0),
(230, 1, '2020-05-02 14:25:20', '2020-05-02 14:25:20', '', 'Automatische kampaign', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2020-05-02 14:25:20', '2020-05-02 14:25:20', '', 125, 'http://localhost/coursemodule/index.php/2020/05/02/125-revision-v1/', 0, 'revision', '', 0),
(231, 1, '2020-05-02 15:00:30', '2020-05-02 15:00:30', '', 'Automatische kampaign', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2020-05-02 15:00:30', '2020-05-02 15:00:30', '', 125, 'http://localhost/coursemodule/index.php/2020/05/02/125-revision-v1/', 0, 'revision', '', 0),
(233, 1, '2020-05-02 15:25:01', '2020-05-02 15:25:01', '', 'Bibliotek', '', 'publish', 'closed', 'closed', '', 'bibliotek', '', '', '2020-05-02 15:25:01', '2020-05-02 15:25:01', '', 0, 'http://localhost/coursemodule/?page_id=233', 0, 'page', '', 0),
(234, 1, '2020-05-02 15:25:01', '2020-05-02 15:25:01', '', 'Bibliotek', '', 'inherit', 'closed', 'closed', '', '233-revision-v1', '', '', '2020-05-02 15:25:01', '2020-05-02 15:25:01', '', 233, 'http://localhost/coursemodule/index.php/2020/05/02/233-revision-v1/', 0, 'revision', '', 0),
(235, 1, '2020-05-02 15:35:01', '2020-05-02 15:35:01', '', '<i class=\"fas fa-search\"> </i>Bibliothek', '', 'publish', 'closed', 'closed', '', 'bibliothek-2', '', '', '2020-05-02 15:35:01', '2020-05-02 15:35:01', '', 0, 'http://localhost/coursemodule/?p=235', 3, 'nav_menu_item', '', 0),
(236, 1, '2020-05-03 21:11:35', '2020-05-03 21:11:35', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2020-05-03 21:11:35', '2020-05-03 21:11:35', '', 0, 'http://localhost/coursemodule/index.php/shop/', 0, 'page', '', 0),
(237, 1, '2020-05-03 21:11:35', '2020-05-03 21:11:35', '<!-- wp:shortcode -->[woocommerce_cart]<!-- /wp:shortcode -->', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2020-05-03 21:11:35', '2020-05-03 21:11:35', '', 0, 'http://localhost/coursemodule/index.php/cart/', 0, 'page', '', 0),
(238, 1, '2020-05-03 21:11:35', '2020-05-03 21:11:35', '<!-- wp:shortcode -->[woocommerce_checkout]<!-- /wp:shortcode -->', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2020-05-03 21:11:35', '2020-05-03 21:11:35', '', 0, 'http://localhost/coursemodule/index.php/checkout/', 0, 'page', '', 0),
(239, 1, '2020-05-03 21:11:35', '2020-05-03 21:11:35', '<!-- wp:shortcode -->[woocommerce_my_account]<!-- /wp:shortcode -->', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2020-05-03 21:11:35', '2020-05-03 21:11:35', '', 0, 'http://localhost/coursemodule/index.php/my-account/', 0, 'page', '', 0),
(240, 1, '2020-05-03 22:24:25', '2020-05-03 22:24:25', '', 'Test Plan', '', 'publish', 'closed', 'closed', '', 'test-plan', '', '', '2020-05-03 22:24:58', '2020-05-03 22:24:58', '', 0, 'http://localhost/coursemodule/?post_type=membership_plan&#038;p=240', 0, 'membership_plan', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_sms_send`
--

CREATE TABLE `wp_sms_send` (
  `ID` int(10) NOT NULL,
  `date` datetime DEFAULT NULL,
  `sender` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `recipient` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `response` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_sms_send`
--

INSERT INTO `wp_sms_send` (`ID`, `date`, `sender`, `message`, `recipient`, `response`, `status`) VALUES
(1, '2020-04-17 08:10:27', 'ALEX', 'TEST', '250787490069', 'NULL', 'success');

-- --------------------------------------------------------

--
-- Table structure for table `wp_sms_subscribes`
--

CREATE TABLE `wp_sms_subscribes` (
  `ID` int(10) NOT NULL,
  `date` datetime DEFAULT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `activate_key` int(11) DEFAULT NULL,
  `group_ID` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_sms_subscribes_group`
--

CREATE TABLE `wp_sms_subscribes_group` (
  `ID` int(10) NOT NULL,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_tcb_api_error_log`
--

CREATE TABLE `wp_tcb_api_error_log` (
  `id` int(11) NOT NULL,
  `date` datetime DEFAULT NULL,
  `error_message` varchar(400) DEFAULT NULL,
  `api_data` text,
  `connection` varchar(64) DEFAULT NULL,
  `list_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wp_td_fields`
--

CREATE TABLE `wp_td_fields` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `type` int(11) NOT NULL,
  `data` text,
  `is_default` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_td_fields`
--

INSERT INTO `wp_td_fields` (`id`, `group_id`, `name`, `type`, `data`, `is_default`, `created_at`, `updated_at`) VALUES
(1, 1, 'Name', 0, NULL, 1, NULL, NULL),
(2, 1, 'Address', 1, NULL, 1, NULL, NULL),
(3, 1, 'Phone number', 2, NULL, 1, NULL, NULL),
(4, 1, 'Alternative phone number', 2, NULL, 1, NULL, NULL),
(5, 1, 'Email address', 3, NULL, 1, NULL, NULL),
(6, 2, 'Privacy policy', 4, 'a:2:{s:4:\"text\";s:14:\"Privacy policy\";s:3:\"url\";s:0:\"\";}', 1, NULL, NULL),
(7, 2, 'Disclaimer', 4, 'a:2:{s:4:\"text\";s:10:\"Disclaimer\";s:3:\"url\";s:0:\"\";}', 1, NULL, NULL),
(8, 2, 'Terms and Conditions', 4, 'a:2:{s:4:\"text\";s:20:\"Terms and Conditions\";s:3:\"url\";s:0:\"\";}', 1, NULL, NULL),
(9, 2, 'Contact', 4, 'a:2:{s:4:\"text\";s:7:\"Contact\";s:3:\"url\";s:0:\"\";}', 1, NULL, NULL),
(10, 3, 'Facebook Page', 4, NULL, 1, NULL, NULL),
(11, 3, 'YouTube', 4, NULL, 1, NULL, NULL),
(12, 3, 'LinkedIn', 4, NULL, 1, NULL, NULL),
(13, 3, 'Pinterest', 4, NULL, 1, NULL, NULL),
(14, 3, 'Instagram', 4, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_td_groups`
--

CREATE TABLE `wp_td_groups` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `is_default` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wp_td_groups`
--

INSERT INTO `wp_td_groups` (`id`, `name`, `is_default`, `created_at`, `updated_at`) VALUES
(1, 'Company', 1, NULL, NULL),
(2, 'Legal', 1, NULL, NULL),
(3, 'Social', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`, `term_order`) VALUES
(1, 'Uncategorized', 'uncategorized', 0, 7),
(2, 'Headers', 'headers', 0, 0),
(3, 'Footers', 'footers', 0, 0),
(4, 'Main', 'main', 0, 0),
(5, 'secondary', 'secondary', 0, 0),
(7, 'ddd', 'ddd', 0, 0),
(9, 'Einführung', 'einfuhrung', 0, 5),
(10, 'Die Kompagnen', 'die-kompagnen', 0, 2),
(11, 'Listing', 'listing', 0, 0),
(12, 'Kampagnen', 'kampagnen', 0, 3),
(13, 'Optimierung', 'optimierung', 0, 1),
(14, 'Umsatz steigern', 'umsatz-steigern', 0, 4),
(15, 'simple', 'simple', 0, 0),
(16, 'grouped', 'grouped', 0, 0),
(17, 'variable', 'variable', 0, 0),
(18, 'external', 'external', 0, 0),
(19, 'exclude-from-search', 'exclude-from-search', 0, 0),
(20, 'exclude-from-catalog', 'exclude-from-catalog', 0, 0),
(21, 'featured', 'featured', 0, 0),
(22, 'outofstock', 'outofstock', 0, 0),
(23, 'rated-1', 'rated-1', 0, 0),
(24, 'rated-2', 'rated-2', 0, 0),
(25, 'rated-3', 'rated-3', 0, 0),
(26, 'rated-4', 'rated-4', 0, 0),
(27, 'rated-5', 'rated-5', 0, 0),
(28, 'Uncategorized', 'uncategorized', 0, 0),
(29, 'Test Category', 'test-cat', 0, 6),
(30, 'categories', 'categories', 0, 0),
(32, 'task1', 'task1', 0, 1),
(33, 'task2', 'task2', 0, 2),
(34, 'task3', 'task3', 0, 3),
(35, 'automatic- task1', 'automatic-task1', 0, 4),
(36, 'automatic- task2', 'automatic-task2', 0, 5),
(37, 'automatic- task3', 'automatic-task3', 0, 6),
(38, 'sample task I', 'sample-task-i', 0, 7),
(39, 'sample task II', 'sample-task-ii', 0, 8),
(40, 'sample task III', 'sample-task-iii', 0, 9),
(41, 'enabled', 'enabled', 0, 0),
(42, 'disabled', 'disabled', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 7),
(11, 4, 0),
(13, 4, 0),
(27, 5, 0),
(28, 5, 0),
(29, 5, 0),
(37, 1, 7),
(74, 1, 7),
(86, 9, 5),
(86, 10, 2),
(89, 9, 5),
(119, 11, 0),
(125, 9, 5),
(178, 4, 0),
(191, 9, 5),
(200, 9, 5),
(208, 10, 2),
(235, 4, 0),
(240, 41, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 2),
(2, 2, 'tcb_symbols_tax', '', 0, 0),
(3, 3, 'tcb_symbols_tax', '', 0, 0),
(4, 4, 'nav_menu', '', 0, 4),
(5, 5, 'nav_menu', '', 0, 3),
(7, 7, 'post_tag', '', 0, 0),
(9, 9, 'category', '', 0, 5),
(10, 10, 'category', '', 0, 2),
(11, 11, 'category', '', 0, 1),
(12, 12, 'category', '', 0, 0),
(13, 13, 'category', '', 0, 0),
(14, 14, 'category', '', 0, 0),
(15, 15, 'product_type', '', 0, 0),
(16, 16, 'product_type', '', 0, 0),
(17, 17, 'product_type', '', 0, 0),
(18, 18, 'product_type', '', 0, 0),
(19, 19, 'product_visibility', '', 0, 0),
(20, 20, 'product_visibility', '', 0, 0),
(21, 21, 'product_visibility', '', 0, 0),
(22, 22, 'product_visibility', '', 0, 0),
(23, 23, 'product_visibility', '', 0, 0),
(24, 24, 'product_visibility', '', 0, 0),
(25, 25, 'product_visibility', '', 0, 0),
(26, 26, 'product_visibility', '', 0, 0),
(27, 27, 'product_visibility', '', 0, 0),
(28, 28, 'product_cat', '', 0, 0),
(29, 29, 'category', '', 0, 0),
(30, 30, 'nav_menu', '', 0, 0),
(32, 32, 'Tasks', '', 0, 0),
(33, 33, 'Tasks', '', 0, 0),
(34, 34, 'Tasks', '', 0, 0),
(35, 35, 'Tasks', '', 0, 0),
(36, 36, 'Tasks', '', 0, 0),
(37, 37, 'Tasks', '', 0, 0),
(38, 38, 'Tasks', '', 0, 0),
(39, 39, 'Tasks', '', 0, 0),
(40, 40, 'Tasks', '', 0, 0),
(41, 41, 'plan_status', '', 0, 1),
(42, 42, 'plan_status', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', 'Alex'),
(3, 1, 'last_name', 'Bwanakweli'),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'theme_editor_notice'),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:3:{s:64:\"c9ceaeb7879ff4085525f660f39476bb9da07d4abbe7290780c73c697e65a6c7\";a:4:{s:10:\"expiration\";i:1589957676;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36\";s:5:\"login\";i:1588748076;}s:64:\"b8b8372e67d02d85c8eb2fafbeafd767c75830783892d85aa1e4316cb49a4855\";a:4:{s:10:\"expiration\";i:1590344741;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36\";s:5:\"login\";i:1589135141;}s:64:\"37a40fd1a3e63ac2ae7c49247f3ba8b5e0daba4ee2809ff231ed6de8c0a7dc10\";a:4:{s:10:\"expiration\";i:1590487839;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36\";s:5:\"login\";i:1589278239;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '187'),
(18, 1, 'tcb_edit_key', 'fed2e2b634'),
(19, 1, 'nav_menu_recently_edited', '4'),
(20, 1, 'managenav-menuscolumnshidden', 'a:0:{}'),
(21, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}'),
(22, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(23, 1, 'wp_user-settings', 'libraryContent=browse'),
(24, 1, 'wp_user-settings-time', '1586607349'),
(25, 2, 'nickname', 'demo'),
(26, 2, 'first_name', 'Alex'),
(27, 2, 'last_name', 'Bwanakweli'),
(28, 2, 'description', ''),
(29, 2, 'rich_editing', 'true'),
(30, 2, 'syntax_highlighting', 'true'),
(31, 2, 'comment_shortcuts', 'false'),
(32, 2, 'admin_color', 'fresh'),
(33, 2, 'use_ssl', '0'),
(34, 2, 'show_admin_bar_front', 'true'),
(35, 2, 'locale', ''),
(36, 2, 'wp_capabilities', 'a:2:{s:10:\"subscriber\";b:1;s:9:\"test_plan\";b:1;}'),
(37, 2, 'wp_user_level', '0'),
(38, 2, 'dismissed_wp_pointers', ''),
(40, 2, 'tcb_edit_key', '2f999fc6cf'),
(41, 2, 'wpcomplete', '[]'),
(42, 1, 'wpcomplete', '{\"74\":{\"first_seen\":\"2020-04-16 00:57:47\"},\"86\":{\"first_seen\":\"2020-04-16 01:29:14\"},\"89\":{\"first_seen\":\"2020-04-16 01:31:00\"},\"119\":{\"first_seen\":\"2020-04-19 17:09:51\"},\"171\":{\"first_seen\":\"2020-04-25 23:26:17\"}}'),
(43, 1, 'closedpostboxes_video', 'a:0:{}'),
(44, 1, 'metaboxhidden_video', 'a:0:{}'),
(45, 1, 'closedpostboxes_task', 'a:0:{}'),
(46, 1, 'metaboxhidden_task', 'a:0:{}'),
(47, 1, '_last_login', 'May 12, 2020 10:10 am'),
(48, 1, 'closedpostboxes_dashboard', 'a:1:{i:0;s:27:\"wp_count_comments_db_widget\";}'),
(49, 1, 'metaboxhidden_dashboard', 'a:11:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:25:\"wp_recent_posts_db_widget\";i:3;s:25:\"wp_recent_pages_db_widget\";i:4;s:28:\"wp_recent_comments_db_widget\";i:5;s:24:\"wp_count_posts_db_widget\";i:6;s:24:\"wp_count_pages_db_widget\";i:7;s:33:\"wp_search_engine_notice_db_widget\";i:8;s:21:\"dashboard_quick_press\";i:9;s:17:\"dashboard_primary\";i:10;s:27:\"wp_count_comments_db_widget\";}'),
(50, 1, 'meta-box-order_dashboard', 'a:4:{s:6:\"normal\";s:252:\"dashboard_right_now,dashboard_activity,wpcomplete-course-statistics,logged_in_db_widget,wp_recent_posts_db_widget,wp_recent_pages_db_widget,wp_recent_comments_db_widget,wp_count_posts_db_widget,wp_count_pages_db_widget,wp_search_engine_notice_db_widget\";s:4:\"side\";s:61:\"dashboard_quick_press,dashboard_primary,system_info_db_widget\";s:7:\"column3\";s:19:\"wp_memory_db_widget\";s:7:\"column4\";s:49:\"wp_count_comments_db_widget,dashboard_site_health\";}'),
(51, 1, '_woocommerce_tracks_anon_id', 'woo:mPmY+TuLnBs9bf8iw8T5qFXp'),
(52, 1, 'wc_last_active', '1589241600'),
(54, 1, '_logged_in', '1'),
(55, 2, '_order_count', '0'),
(56, 2, 'billing_first_name', 'Alex'),
(57, 2, 'billing_last_name', 'Bwanakweli'),
(58, 2, 'billing_company', ''),
(59, 2, 'billing_address_1', ''),
(60, 2, 'billing_address_2', ''),
(61, 2, 'billing_city', ''),
(62, 2, 'billing_postcode', ''),
(63, 2, 'billing_country', ''),
(64, 2, 'billing_state', ''),
(65, 2, 'billing_phone', ''),
(66, 2, 'billing_email', 'mistasms.mail@gmail.com'),
(67, 2, 'shipping_first_name', ''),
(68, 2, 'shipping_last_name', ''),
(69, 2, 'shipping_company', ''),
(70, 2, 'shipping_address_1', ''),
(71, 2, 'shipping_address_2', ''),
(72, 2, 'shipping_city', ''),
(73, 2, 'shipping_postcode', ''),
(74, 2, 'shipping_country', ''),
(75, 2, 'shipping_state', ''),
(76, 2, 'last_update', '1588544720'),
(78, 2, 'wc_last_active', '1588636800'),
(80, 2, '_last_login', 'May 5, 2020 2:48 pm'),
(81, 2, '_logged_in', '1'),
(83, 2, 'session_tokens', 'a:3:{s:64:\"40343ad5b30d9a24e62b355baf129d576261e3c688f84e8375ea7b484d8a10f1\";a:4:{s:10:\"expiration\";i:1589179136;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36\";s:5:\"login\";i:1587969536;}s:64:\"a4fac6977b41449a3309276996482635da5c699781b37c1545c88a3e377ba5d1\";a:4:{s:10:\"expiration\";i:1589754135;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36\";s:5:\"login\";i:1588544535;}s:64:\"b7d52dc9f34889602ebf3eb4b06692dfe065ec11edcb8be4a39ad6e1cf6c1374\";a:4:{s:10:\"expiration\";i:1589899721;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36\";s:5:\"login\";i:1588690121;}}'),
(86, 1, '_order_count', '0'),
(87, 1, 'wp_rcl_tax_video', 'category'),
(88, 1, 'wp_rcl_tax_task', 'category'),
(89, 1, 'wp_rcl_tax_post', 'category'),
(90, 1, 'wp_rcl_tax_page', 'page_cat'),
(93, 2, '_rpwcm_test_plan_since', '1588544698'),
(94, 2, '_rpwcm_test_plan_expires', '1590883199'),
(95, 1, 'last_update', '1588576537'),
(96, 1, 'woocommerce_admin_activity_panel_inbox_last_read', '1588576527505'),
(97, 1, '32', 'pending'),
(99, 1, '33', 'pending'),
(100, 1, '34', 'completed'),
(101, 1, '34', 'completed'),
(103, 2, '32', 'completed'),
(104, 2, '33', 'completed'),
(106, 1, '86', 'watched'),
(108, 1, '191', 'watched');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$BmM8FPWCk/A.GPuphzHrtxTKxOcYJI/', 'admin', 'bwanakweli4ever@gmail.com', '', '2020-04-10 19:52:33', '', 0, 'admin'),
(2, 'demo', '$P$BC4L6rW4D3fOjDHaaUn3sa8ArteorZ0', 'demo', 'mistasms.mail@gmail.com', '', '2020-04-14 13:57:59', '', 0, 'Alex Bwanakweli');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_admin_notes`
--

CREATE TABLE `wp_wc_admin_notes` (
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_data` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_wc_admin_notes`
--

INSERT INTO `wp_wc_admin_notes` (`note_id`, `name`, `type`, `locale`, `title`, `content`, `icon`, `content_data`, `status`, `source`, `date_created`, `date_reminder`, `is_snoozable`) VALUES
(1, 'wc-admin-welcome-note', 'info', 'en_US', 'New feature(s)', 'Welcome to the new WooCommerce experience! In this new release you\'ll be able to have a glimpse of how your store is doing in the Dashboard, manage important aspects of your business (such as managing orders, stock, reviews) from anywhere in the interface, dive into your store data with a completely new Analytics section and more!', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-04-27 05:59:10', NULL, 0),
(2, 'wc-admin-store-notice-setting-moved', 'info', 'en_US', 'Looking for the Store Notice setting?', 'It can now be found in the Customizer.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-04-27 05:59:10', NULL, 0),
(3, 'wc-admin-wc-helper-connection', 'info', 'en_US', 'Connect to WooCommerce.com', 'Connect to get important product notifications and updates.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-04-27 05:59:11', NULL, 0),
(4, 'wc-admin-add-first-product', 'info', 'en_US', 'Add your first product', 'Grow your revenue by adding products to your store. Add products manually, import from a sheet, or migrate from another platform.', 'product', '{}', 'unactioned', 'woocommerce-admin', '2020-04-27 05:59:12', NULL, 0),
(5, 'wc-admin-onboarding-profiler-reminder', 'update', 'en_US', 'Welcome to WooCommerce! Set up your store and start selling', 'We\'re here to help you going through the most important steps to get your store up and running.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-04-27 06:00:00', NULL, 0),
(6, 'wc-admin-mobile-app', 'info', 'en_US', 'Install Woo mobile app', 'Install the WooCommerce mobile app to manage orders, receive sales notifications, and view key metrics — wherever you are.', 'phone', '{}', 'unactioned', 'woocommerce-admin', '2020-04-29 12:04:39', NULL, 0),
(7, 'wc-admin-store-notice-giving-feedback', 'info', 'en_US', 'Review your experience', 'If you like WooCommerce Admin please leave us a 5 star rating. A huge thanks in advance!', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-05-01 11:32:19', NULL, 0),
(8, 'wc-admin-usage-tracking-opt-in', 'info', 'en_US', 'Help WooCommerce improve with usage tracking', 'Gathering usage data allows us to improve WooCommerce. Your store will be considered as we evaluate new features, judge the quality of an update, or determine if an improvement makes sense. You can always visit the <a href=\"http://localhost/coursemodule/wp-admin/admin.php?page=wc-settings&#038;tab=advanced&#038;section=woocommerce_com\" target=\"_blank\">Settings</a> and choose to stop sharing data. <a href=\"https://woocommerce.com/usage-tracking\" target=\"_blank\">Read more</a> about what data we collect.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2020-05-04 07:12:09', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_admin_note_actions`
--

CREATE TABLE `wp_wc_admin_note_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `query` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_wc_admin_note_actions`
--

INSERT INTO `wp_wc_admin_note_actions` (`action_id`, `note_id`, `name`, `label`, `query`, `status`, `is_primary`) VALUES
(1, 1, 'learn-more', 'Learn more', 'https://woocommerce.wordpress.com/', 'actioned', 0),
(2, 2, 'open-customizer', 'Open Customizer', 'customize.php', 'actioned', 0),
(3, 3, 'connect', 'Connect', '?page=wc-addons&section=helper', 'actioned', 0),
(4, 4, 'add-a-product', 'Add a product', 'http://localhost/coursemodule/wp-admin/post-new.php?post_type=product', 'actioned', 1),
(5, 5, 'continue-profiler', 'Continue Store Setup', 'http://localhost/coursemodule/wp-admin/admin.php?page=wc-admin&enable_onboarding=1', 'unactioned', 1),
(6, 5, 'skip-profiler', 'Skip Setup', 'http://localhost/coursemodule/wp-admin/admin.php?page=wc-admin&reset_profiler=0', 'actioned', 0),
(7, 6, 'learn-more', 'Learn more', 'https://woocommerce.com/mobile/', 'actioned', 0),
(8, 7, 'share-feedback', 'Review', 'https://wordpress.org/support/plugin/woocommerce-admin/reviews/?rate=5#new-post', 'actioned', 0),
(9, 8, 'tracking-dismiss', 'Dismiss', '', 'actioned', 0),
(10, 8, 'tracking-opt-in', 'Activate usage tracking', '', 'actioned', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_category_lookup`
--

CREATE TABLE `wp_wc_category_lookup` (
  `category_tree_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_wc_category_lookup`
--

INSERT INTO `wp_wc_category_lookup` (`category_tree_id`, `category_id`) VALUES
(28, 28);

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_customer_lookup`
--

CREATE TABLE `wp_wc_customer_lookup` (
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_last_active` timestamp NULL DEFAULT NULL,
  `date_registered` timestamp NULL DEFAULT NULL,
  `country` char(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `postcode` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_download_log`
--

CREATE TABLE `wp_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_coupon_lookup`
--

CREATE TABLE `wp_wc_order_coupon_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `discount_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_product_lookup`
--

CREATE TABLE `wp_wc_order_product_lookup` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_qty` int(11) NOT NULL,
  `product_net_revenue` double NOT NULL DEFAULT '0',
  `product_gross_revenue` double NOT NULL DEFAULT '0',
  `coupon_amount` double NOT NULL DEFAULT '0',
  `tax_amount` double NOT NULL DEFAULT '0',
  `shipping_amount` double NOT NULL DEFAULT '0',
  `shipping_tax_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_stats`
--

CREATE TABLE `wp_wc_order_stats` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num_items_sold` int(11) NOT NULL DEFAULT '0',
  `total_sales` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `shipping_total` double NOT NULL DEFAULT '0',
  `net_total` double NOT NULL DEFAULT '0',
  `returning_customer` tinyint(1) DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_order_tax_lookup`
--

CREATE TABLE `wp_wc_order_tax_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_tax` double NOT NULL DEFAULT '0',
  `order_tax` double NOT NULL DEFAULT '0',
  `total_tax` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_product_meta_lookup`
--

CREATE TABLE `wp_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(19,4) DEFAULT NULL,
  `max_price` decimal(19,4) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0',
  `tax_status` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'taxable',
  `tax_class` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_tax_rate_classes`
--

CREATE TABLE `wp_wc_tax_rate_classes` (
  `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_wc_tax_rate_classes`
--

INSERT INTO `wp_wc_tax_rate_classes` (`tax_rate_class_id`, `name`, `slug`) VALUES
(1, 'Reduced rate', 'reduced-rate'),
(2, 'Zero rate', 'zero-rate');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_webhooks`
--

CREATE TABLE `wp_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_api_keys`
--

CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_log`
--

CREATE TABLE `wp_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_itemmeta`
--

CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokens`
--

CREATE TABLE `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_sessions`
--

CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_sessions`
--

INSERT INTO `wp_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(13, '1', 'a:7:{s:4:\"cart\";s:6:\"a:0:{}\";s:11:\"cart_totals\";s:367:\"a:15:{s:8:\"subtotal\";i:0;s:12:\"subtotal_tax\";i:0;s:14:\"shipping_total\";i:0;s:12:\"shipping_tax\";i:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";i:0;s:12:\"discount_tax\";i:0;s:19:\"cart_contents_total\";i:0;s:17:\"cart_contents_tax\";i:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";i:0;s:7:\"fee_tax\";i:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";i:0;s:9:\"total_tax\";i:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";s:8:\"customer\";s:739:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:25:\"2020-05-04T07:15:37+00:00\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:0:\"\";s:7:\"country\";s:2:\"GB\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:0:\"\";s:16:\"shipping_country\";s:2:\"GB\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:25:\"bwanakweli4ever@gmail.com\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1589307930);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zones`
--

CREATE TABLE `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rates`
--

CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`);

--
-- Indexes for table `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Indexes for table `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Indexes for table `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_sms_send`
--
ALTER TABLE `wp_sms_send`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `wp_sms_subscribes`
--
ALTER TABLE `wp_sms_subscribes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `wp_sms_subscribes_group`
--
ALTER TABLE `wp_sms_subscribes_group`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `wp_tcb_api_error_log`
--
ALTER TABLE `wp_tcb_api_error_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_td_fields`
--
ALTER TABLE `wp_td_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_td_groups`
--
ALTER TABLE `wp_td_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_wc_admin_notes`
--
ALTER TABLE `wp_wc_admin_notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `wp_wc_admin_note_actions`
--
ALTER TABLE `wp_wc_admin_note_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `note_id` (`note_id`);

--
-- Indexes for table `wp_wc_category_lookup`
--
ALTER TABLE `wp_wc_category_lookup`
  ADD PRIMARY KEY (`category_tree_id`,`category_id`);

--
-- Indexes for table `wp_wc_customer_lookup`
--
ALTER TABLE `wp_wc_customer_lookup`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `wp_wc_order_coupon_lookup`
--
ALTER TABLE `wp_wc_order_coupon_lookup`
  ADD PRIMARY KEY (`order_id`,`coupon_id`),
  ADD KEY `coupon_id` (`coupon_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `wp_wc_order_product_lookup`
--
ALTER TABLE `wp_wc_order_product_lookup`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `wp_wc_order_stats`
--
ALTER TABLE `wp_wc_order_stats`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `status` (`status`(191));

--
-- Indexes for table `wp_wc_order_tax_lookup`
--
ALTER TABLE `wp_wc_order_tax_lookup`
  ADD PRIMARY KEY (`order_id`,`tax_rate_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `wp_wc_product_meta_lookup`
--
ALTER TABLE `wp_wc_product_meta_lookup`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `virtual` (`virtual`),
  ADD KEY `downloadable` (`downloadable`),
  ADD KEY `stock_status` (`stock_status`),
  ADD KEY `stock_quantity` (`stock_quantity`),
  ADD KEY `onsale` (`onsale`),
  ADD KEY `min_max_price` (`min_price`,`max_price`);

--
-- Indexes for table `wp_wc_tax_rate_classes`
--
ALTER TABLE `wp_wc_tax_rate_classes`
  ADD PRIMARY KEY (`tax_rate_class_id`),
  ADD UNIQUE KEY `slug` (`slug`(191));

--
-- Indexes for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`);

--
-- Indexes for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_key` (`session_key`);

--
-- Indexes for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2868;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2642;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;

--
-- AUTO_INCREMENT for table `wp_sms_send`
--
ALTER TABLE `wp_sms_send`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_sms_subscribes`
--
ALTER TABLE `wp_sms_subscribes`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_sms_subscribes_group`
--
ALTER TABLE `wp_sms_subscribes_group`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_tcb_api_error_log`
--
ALTER TABLE `wp_tcb_api_error_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_td_fields`
--
ALTER TABLE `wp_td_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `wp_td_groups`
--
ALTER TABLE `wp_td_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_wc_admin_notes`
--
ALTER TABLE `wp_wc_admin_notes`
  MODIFY `note_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wp_wc_admin_note_actions`
--
ALTER TABLE `wp_wc_admin_note_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `wp_wc_customer_lookup`
--
ALTER TABLE `wp_wc_customer_lookup`
  MODIFY `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wc_tax_rate_classes`
--
ALTER TABLE `wp_wc_tax_rate_classes`
  MODIFY `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD CONSTRAINT `fk_wp_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `wp_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
